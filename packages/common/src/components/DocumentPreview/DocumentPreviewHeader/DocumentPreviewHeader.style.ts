// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core/styles';

export const useDocumentPreviewHeaderStyles = makeStyles(() => ({
  wrapper: {
    display: 'flex',
    padding: '0 10px',
    height: 60,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  title: {
    flexGrow: 1,
    verticalAlign: 'middle',
  },
}));
