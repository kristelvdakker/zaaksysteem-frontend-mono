// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
import { H3 } from '@mintlab/ui/App/Material/Typography';
import { DocumentPreviewPropsType } from '../DocumentPreview.types';
import { useDocumentPreviewHeaderStyles } from './DocumentPreviewHeader.style';

interface DocumentPreviewHeaderPropsType extends DocumentPreviewPropsType {}

export const DocumentPreviewHeader: React.ComponentType<DocumentPreviewHeaderPropsType> = ({
  title,
  downloadUrl,
  onClose,
}) => {
  const classes = useDocumentPreviewHeaderStyles();
  const [t] = useTranslation('common');
  const LinkComponent = React.forwardRef<HTMLAnchorElement>(
    ({ children, ...props }, ref) => (
      <a {...props} ref={ref} href={downloadUrl} download>
        {children}
      </a>
    )
  );
  LinkComponent.displayName = 'DownloadLink';

  return title || downloadUrl ? (
    <header className={classes.wrapper}>
      <H3 classes={{ root: classes.title }}>{title}</H3>

      {downloadUrl && (
        <Button component={LinkComponent} presets={['text', 'primary']}>
          {t('filePreview.downloadDocument')}
        </Button>
      )}

      {onClose && (
        <Button presets={['primary', 'icon', 'medium']} action={onClose}>
          close
        </Button>
      )}
    </header>
  ) : null;
};
