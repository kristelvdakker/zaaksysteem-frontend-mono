// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core/styles';

export const useDocumentPreviewStyles = makeStyles(
  ({ mintlab: { greyscale } }: any) => ({
    wrapper: {
      height: '100%',
      display: 'flex',
      width: '100%',
      flexDirection: 'column',
      backgroundColor: greyscale.darkest,
    },
    viewer: {
      flex: 1,
    },
  })
);
