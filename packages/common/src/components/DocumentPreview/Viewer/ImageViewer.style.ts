// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core/styles';

export const useImageViewerStyles = makeStyles(
  ({ mintlab: { greyscale } }: any) => ({
    wrapper: {
      position: 'relative',
      backgroundColor: greyscale.darkest,
      width: '100%',
      flex: 1,
      display: 'flex',
      overflow: 'hidden',
    },
    controls: {
      position: 'absolute',
      bottom: 20,
      right: 20,
      zIndex: 1,
    },
    imageScrollWrapper: {
      overflow: 'auto',
      width: '100%',
      height: '100%',
    },
    imageWrapper: {
      flex: 1,
      textAlign: 'center',
    },
    image: {
      maxWidth: '100%',
      transformOrigin: 'top left',
    },
  })
);
