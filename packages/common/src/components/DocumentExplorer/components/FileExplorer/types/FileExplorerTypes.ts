// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  ColumnType,
  RowType,
} from '@mintlab/ui/App/Zaaksysteem/SortableTable/types/SortableTableTypes';

export type FileExplorerPropsType = {
  items: CommonItemType[];
  columns: ColumnType[];
  onFolderOpen: (folder: DirectoryItemType) => void;
  onFileOpen: (file: DocumentItemType) => void;
  loading?: boolean;
  noRowsMessage: string;
  [key: string]: any;
};

export type RowClickEvent = {
  event: React.MouseEvent;
  rowData: DocumentItemType | DirectoryItemType;
};

export interface CommonItemType extends RowType {
  modified?: Date;
  type: string;
}

type AssignmentTypes = 'employee' | 'role' | 'department';
export interface DocumentItemType extends CommonItemType {
  type: 'document';
  mimeType: string;
  extension?: string;
  pending?: boolean;
  description?: string;
  accepted: boolean;
  beingVirusScanned: boolean;
  selected: boolean;
  preview?: {
    url: string;
    contentType?: string;
  };
  download?: {
    url: string;
  };
  thumbnail?: {
    url: string;
  };
  document_number?: number;
  rejectionReason?: string;
  rejectionName?: string;
  assignment?: {
    [A in AssignmentTypes]?: {
      name?: string | null;
    };
  };
}

export interface DirectoryItemType extends CommonItemType {
  type: 'directory';
}

export type ItemType = DocumentItemType | DirectoryItemType;
