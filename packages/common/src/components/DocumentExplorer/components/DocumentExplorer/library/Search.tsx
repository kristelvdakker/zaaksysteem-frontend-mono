// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDebouncedCallback } from 'use-debounce';
// @ts-ignore
import TextField from '@mintlab/ui/App/Material/TextField';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';

type SearchPropsType = {
  onChange: (value: string) => void;
  onClose: () => void;
};

const DELAY = 300;
const MINLENGTH = 3;

const Search = ({ onChange, onClose }: SearchPropsType) => {
  const [searchTerm, setSearchTerm] = useState('');
  const [t] = useTranslation('DocumentExplorer');
  const [debouncedCallback] = useDebouncedCallback(value => {
    if (!value.length || value.length >= MINLENGTH) onChange(value);
  }, DELAY);

  const handleChange = (value: React.ChangeEvent<HTMLInputElement>) => {
    const searchValue = value.target.value;
    setSearchTerm(searchValue);
    debouncedCallback(searchValue.trim());
  };

  const handleClose = () => {
    setSearchTerm('');
    onClose();
  };

  const handleSearchKeyPress = (input: React.KeyboardEvent) => {
    const key = input.key.toLowerCase();

    if (key === 'enter') onChange(searchTerm);
    if (key === 'escape') handleClose();
  };

  return (
    <TextField
      value={searchTerm}
      id="search"
      name="search"
      onChange={handleChange}
      onKeyDown={handleSearchKeyPress}
      closeAction={handleClose}
      placeholder={t('search.placeholder')}
      startAdornment={<Icon size="small">{iconNames.search}</Icon>}
      variant="generic1"
    />
  );
};

export default Search;
