// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { ItemType } from '../../FileExplorer/types/FileExplorerTypes';
import { FiltersType } from '../types/types';

export const filterFunctions = {
  assignment: {
    assigned: (item: ItemType) =>
      item.type === 'document'
        ? Object.values(item.assignment || {}).some(obj => obj?.name !== null)
        : true,
    unassigned: (item: ItemType) =>
      item.type === 'document'
        ? !Object.values(item.assignment || {}).some(obj => obj?.name !== null)
        : true,
    all: () => true,
  },
};

export const useFilters = (
  onChange: (filters: FiltersType) => void,
  filters: FiltersType
) => {
  const [t] = useTranslation('DocumentExplorer');

  const assignmentChoices = [
    {
      label: t('filters.assignment.assigned'),
      value: filterFunctions.assignment.assigned,
    },
    {
      label: t('filters.assignment.unassigned'),
      value: filterFunctions.assignment.unassigned,
    },
    {
      label: t('filters.assignment.all'),
      value: filterFunctions.assignment.all,
    },
  ];
  const assignmentValue = assignmentChoices.find(
    filter => filter.value === filters.assignment
  );

  return {
    assignment: (
      <Select
        value={assignmentValue}
        name="filter-assignment"
        isMulti={false}
        onChange={ev => {
          const value = ev.target.value.value;
          onChange({ ...filters, assignment: value });
        }}
        choices={assignmentChoices}
        generic={true}
      />
    ),
  };
};

export default useFilters;
