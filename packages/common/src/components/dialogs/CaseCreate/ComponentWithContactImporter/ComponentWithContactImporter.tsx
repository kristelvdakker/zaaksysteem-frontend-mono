// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
//@ts-ignore
import IconButton from '@material-ui/core/IconButton';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import ContactFinder from '@zaaksysteem/common/src/components/form/fields/ContactFinder/ContactFinder';
import ContactImporter from '../../ContactImporter/ContactImporter';
import { FormFieldComponentType } from '../../../form/types/Form2.types';
import { useStyles } from './ComponentWithContactImporter.styles';

const ComponentWithContactImporter: FormFieldComponentType<
  any,
  {
    caseMeta: {
      initiator_source: string;
      type_of_requestors: string[];
    };
  }
> = props => {
  const [t] = useTranslation('main');
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const { onChange, name, disabled, config } = props;
  const initiatorSource = config?.caseMeta?.initiator_source || '';
  const typeOfRequestors = config?.caseMeta?.type_of_requestors || [];

  const externalAllowed = () => {
    if (disabled) return false;
    return initiatorSource.indexOf('extern') > -1;
  };

  const handleSelectRow = ({ label, value }: ValueType<String>) => {
    onChange({
      target: {
        name,
        value: {
          label,
          value,
        },
      },
    } as React.ChangeEvent<any>);
    setOpen(false);
  };

  return (
    <div className={classes.wrapper}>
      <div className={classes.component}>
        <ContactFinder {...props} />
      </div>

      <div className={classes.button}>
        <Tooltip title={t('intake.caseCreate.requestor.import')}>
          <IconButton
            onClick={() => setOpen(true)}
            color="inherit"
            disabled={!externalAllowed()}
          >
            <Icon size="small">{iconNames.import}</Icon>
          </IconButton>
        </Tooltip>
      </div>

      {open && (
        <ContactImporter
          onSelectRow={handleSelectRow}
          onClose={() => setOpen(false)}
          typeOfRequestors={typeOfRequestors}
        />
      )}
    </div>
  );
};

export default ComponentWithContactImporter;
