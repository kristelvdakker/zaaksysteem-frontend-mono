// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import {
  Rule,
  hasValue,
  showFields,
  updateFields,
  transferValueAsConfig,
} from '@zaaksysteem/common/src/components/form/rules';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { CaseTypeOptionType } from '@zaaksysteem/common/src/components/form/fields/CaseTypeFinder/CaseTypeFinder.library';
import {
  getCaseCreateFormDefinition,
  CaseCreateFormValuesType,
} from './caseCreate.formDefinition';
import ComponentWithContactImporter from './ComponentWithContactImporter/ComponentWithContactImporter';

type CaseCreateDialogPropsType = {
  selectedDocumentUuid?: string;
  open: boolean;
  onClose: () => void;
};

const getRequestorFilter = ({
  data: { type_of_requestors },
}: CaseTypeOptionType) => {
  const availableRequestorTypes = [
    type_of_requestors.includes('medewerker') ? 'employee' : null,
    type_of_requestors.includes('niet_natuurlijk_persoon')
      ? 'organization'
      : null,
    type_of_requestors.includes('natuurlijk_persoon_na') ||
    type_of_requestors.includes('natuurlijk_persoon')
      ? 'person'
      : null,
  ].filter(Boolean);

  return { config: { filterTypes: availableRequestorTypes } };
};

const rules = [
  new Rule<CaseCreateFormValuesType>()
    .when('caseType', hasValue)
    .then(fields => {
      const caseType = (fields.find(
        (field: any) => field.name === 'caseType'
      ) as any).value;

      return fields
        .map(
          updateFields(['requestor'], {
            ...getRequestorFilter(caseType),
            disabled: false,
          })
        )
        .map(
          updateFields(['source'], {
            disabled: false,
          })
        );
    })
    .else(fields =>
      fields
        .map(
          updateFields(['requestor'], {
            value: null,
            disabled: true,
            readOnly: true,
          })
        )
        .map(
          updateFields(['source'], {
            value: null,
            disabled: true,
            readOnly: true,
          })
        )
    ),
  new Rule<CaseCreateFormValuesType>()
    .when(fields => (fields.requestor.value as any)?.contactType === 'employee')
    .then(showFields(['recipient']))
    .else(fields =>
      fields.map(
        updateFields(['recipient'], {
          hidden: true,
          value: null,
        })
      )
    ),
  new Rule()
    .when(() => true)
    .then(transferValueAsConfig('caseType', 'requestor', 'caseMeta')),
];

const getLegacyDocumentId = (uuid: string): Promise<number> => {
  return request('POST', '/api/file/get_ids', { file_uuids: [uuid] }).then(
    (data: { result: { [key: string]: number }[] }) => {
      return data.result[0][uuid] || 0;
    }
  );
};

export const CaseCreateDialog: React.ComponentType<CaseCreateDialogPropsType> = ({
  selectedDocumentUuid,
  open,
  onClose,
}) => {
  const [t] = useTranslation('main');
  const formDefinition = getCaseCreateFormDefinition(t);
  const [legacyDocumentId, setLegacyDocumentId] = React.useState(0);

  React.useEffect(() => {
    selectedDocumentUuid &&
      open &&
      getLegacyDocumentId(selectedDocumentUuid).then(id =>
        setLegacyDocumentId(id)
      );
  }, [selectedDocumentUuid, open]);

  const handleSubmit = (formValues: CaseCreateFormValuesType) => {
    const {
      caseType: {
        data: { id },
      },
      contactChannel: { value: contactChannelValue },
      requestor: { value: requestorId },
      recipient,
    } = formValues;

    const base = `/intern/aanvragen/${id}/`;
    window.top.location.href = buildUrl(base, {
      ontvanger: recipient ? recipient.value : '',
      aanvrager: requestorId,
      contactkanaal: contactChannelValue,
      document: legacyDocumentId,
    });
  };

  return (
    <React.Fragment>
      <FormDialog
        formDefinition={formDefinition}
        fieldComponents={{
          ContactImporter: ComponentWithContactImporter,
        }}
        title={t('intake.caseCreate.title')}
        saveLabel={t('intake.caseCreate.save')}
        icon="add"
        onClose={onClose}
        saving={false}
        scope="case-create"
        open={open}
        rules={rules}
        onSubmit={formValues => handleSubmit(formValues)}
      />
    </React.Fragment>
  );
};
