// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { SingleStepFormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { CaseTypeOptionType } from '@zaaksysteem/common/src/components/form/fields/CaseTypeFinder/CaseTypeFinder.library';

export type CaseCreateFormValuesType = {
  caseType: CaseTypeOptionType;
  requestor: ValueType<string>;
  recipient: ValueType<string>;
  contactChannel: ValueType<string>;
  searchType: any;
};

export const getCaseCreateFormDefinition: (
  t: i18next.TFunction
) => SingleStepFormDefinition<CaseCreateFormValuesType> = t => [
  {
    name: 'caseType',
    label: t('intake.caseCreate.caseType.label'),
    placeholder: t('intake.caseCreate.caseType.placeholder'),
    type: fieldTypes.CASE_TYPE_FINDER,
    value: null,
    required: true,
  },
  {
    name: 'requestor',
    label: t('intake.caseCreate.requestor.label'),
    placeholder: t('intake.caseCreate.requestor.placeholder'),
    type: 'ContactImporter',
    value: '',
    required: true,
    applyBackgroundColor: false,
  },
  {
    name: 'recipient',
    label: t('intake.caseCreate.recipient.label'),
    placeholder: t('intake.caseCreate.recipient.placeholder'),
    type: fieldTypes.CONTACT_FINDER,
    value: '',
    required: false,
    config: { filterTypes: ['person', 'organization'] },
  },
  {
    name: 'contactChannel',
    label: t('intake.caseCreate.contactChannel.label'),
    placeholder: t('intake.caseCreate.contactChannel.placeholder'),
    type: fieldTypes.SELECT,
    value: 'post',
    required: true,
    choices: [
      { value: 'behandelaar', label: 'Behandelaar' },
      { value: 'balie', label: 'Balie' },
      { value: 'telefoon', label: 'Telefoon' },
      { value: 'post', label: 'Post' },
      { value: 'email', label: 'Email' },
      { value: 'webformulier', label: 'Webformulier' },
      { value: 'sociale media', label: 'Sociale' },
      { value: 'externe applicatie', label: 'Externe applicatie' },
    ],
  },
];
