// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => ({
  dialogRoot: {
    width: 500,
    padding: 0,
  },
  wrapper: {
    '&:not(:last-child)': {
      marginBottom: 16,
    },
  },
}));
