// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { useTheme } from '@material-ui/core';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
//@ts-ignore
import { unique } from '@mintlab/kitchen-sink/source/unique';
//@ts-ignore
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogDivider,
  //@ts-ignore
} from '@mintlab/ui/App/Material/Dialog';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import {
  FormDefinition,
  FormValuesType,
  UseFormType,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { DialogTitlePropsType } from '@mintlab/ui/App/Material/Dialog/DialogTitle/DialogTitle';
import { useForm } from '../../form/hooks/useForm';
import { useStyles } from './FormDialog.style';

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'text'],
  secondaryPresets: ['default', 'text'],
});

export type FormDialogPropsType<Values> = Omit<
  UseFormType<Values>,
  'onSubmit'
> & {
  formDefinition: FormDefinition<Values>;
  onClose: () => void;
  title: string;
  onSubmit: (values: FormValuesType<Values>) => void;
  scope?: string;
  saveLabel?: string;
  icon?: DialogTitlePropsType['icon'];
  initializing?: boolean;
  saving?: boolean;
  open?: boolean;
};

function FormDialog<Values>({
  onSubmit,
  formDefinition,
  title,
  scope,
  saveLabel,
  icon,
  initializing = false,
  saving = false,
  validationMap,
  fieldComponents,
  rules,
  isInitialValid = false,
  onClose,
  open = true,
}: FormDialogPropsType<Values>) {
  const dialogEl = useRef(null);
  const [t] = useTranslation('common');
  const theme = useTheme<any>();
  const classes = useStyles(theme);

  const FormDialogContent = () => {
    const {
      fields,
      formik: { isValid, values },
    } = useForm({
      formDefinition: formDefinition,
      isInitialValid: isInitialValid,
      validationMap: validationMap,
      rules: rules,
      fieldComponents: fieldComponents,
    });

    return (
      <React.Fragment>
        <DialogContent padded={true}>
          {fields.map(
            ({ FieldComponent, key, type, suppressLabel, ...rest }) => {
              const props = cloneWithout(rest, 'definition', 'mode');

              return (
                <FormControlWrapper
                  {...props}
                  label={suppressLabel ? false : props.label}
                  compact={true}
                  className={classes.wrapper}
                  key={`${props.name}-formcontrol-wrapper`}
                >
                  <FieldComponent
                    {...props}
                    t={t}
                    containerRef={dialogEl.current}
                    submit={() => {
                      if (!isValid) return;
                      onSubmit(values);
                    }}
                  />
                </FormControlWrapper>
              );
            }
          )}
        </DialogContent>
        <DialogDivider />
        <DialogActions>
          {getDialogActions(
            {
              disabled: saving || !isValid,
              text: saveLabel ? saveLabel : t('dialog.save'),
              action() {
                onSubmit(values);
              },
            },
            {
              text: t('forms.cancel'),
              action: onClose,
            },
            scope
          )}
        </DialogActions>
      </React.Fragment>
    );
  };

  return (
    <Dialog
      classes={{ paper: classes.dialogRoot }}
      aria-label={title}
      open={open}
      onClose={onClose}
      ref={dialogEl}
      disableBackdropClick={true}
    >
      <DialogTitle
        elevated={true}
        id={unique()}
        title={title}
        onCloseClick={onClose}
        scope={scope}
        {...(icon && { icon })}
      />
      {initializing && <Loader />}
      {!initializing && <FormDialogContent />}
    </Dialog>
  );
}

export default FormDialog;
