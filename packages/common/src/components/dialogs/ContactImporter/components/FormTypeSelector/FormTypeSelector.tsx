// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
//@ts-ignore
import RadioGroup from '@mintlab/ui/App/Material/RadioGroup';
import FlatValueSelect from '@zaaksysteem/common/src/components/form/fields/FlatValueSelect';
import { StoreShapeType } from '../../types';
import { useFormTypeSelectorStyles } from './FormTypeSelector.styles';

type FormTypeSelectorPropsType = {
  onChangeFormType: (event: React.ChangeEvent) => void;
  onChangeInterface: (event: React.ChangeEvent) => void;
  t: i18next.TFunction;
} & Pick<
  StoreShapeType,
  'allowedFormTypes' | 'interfaces' | 'selectedFormType' | 'selectedInterface'
>;

const FormTypeSelector: React.ComponentType<FormTypeSelectorPropsType> = ({
  selectedFormType,
  onChangeFormType,
  onChangeInterface,
  interfaces,
  allowedFormTypes,
  selectedInterface,
  t,
}) => {
  const classes = useFormTypeSelectorStyles();
  const choices = [
    ...(allowedFormTypes.includes('person')
      ? [
          {
            label: t('ContactImporter:types.person'),
            value: 'person',
          },
        ]
      : []),
    ...(allowedFormTypes.includes('company')
      ? [
          {
            label: t('ContactImporter:types.company'),
            value: 'company',
          },
        ]
      : []),
  ];

  return (
    <div className={classes.formTypeSelector}>
      <RadioGroup
        choices={choices}
        name="formType"
        value={selectedFormType}
        scope="form-type-selector"
        onChange={onChangeFormType}
        classes={{
          root: classes.radioSelector,
        }}
      />
      {selectedFormType === 'person' &&
        allowedFormTypes.includes('person') &&
        interfaces.length > 1 && (
          <div className={classes.select}>
            <FlatValueSelect
              value={selectedInterface}
              choices={interfaces.map(interf => {
                return {
                  label: interf.name,
                  value: interf.id,
                };
              })}
              onChange={onChangeInterface}
            />
          </div>
        )}
    </div>
  );
};

export default FormTypeSelector;
