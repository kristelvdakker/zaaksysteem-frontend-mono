// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useFormTypeSelectorStyles = makeStyles(
  ({ mintlab: { greyscale, radius } }: Theme) => {
    return {
      formTypeSelector: {
        marginBottom: 28,
        flexWrap: 'wrap',
        display: 'flex',
        '&>*': {
          flexGrow: 0,
        },
        alignItems: 'center',
        height: 60,
      },
      radioSelector: {
        flexDirection: 'row',
      },
      select: {
        backgroundColor: greyscale.light,
        flex: 1,
        borderRadius: radius.defaultFormElement,
      },
    };
  }
);
