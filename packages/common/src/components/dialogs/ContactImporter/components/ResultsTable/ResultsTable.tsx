// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment } from 'react';
import * as i18next from 'i18next';
import { ColumnType } from '@mintlab/ui/App/Zaaksysteem/SortableTable/types/SortableTableTypes';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  AnyTableRowType,
  FormType,
  SelectRowType,
  PersonTableRowType,
} from '../../types';
import { useResultsTableStyles } from './ResultsTable.styles';
import { getWarnings } from './library';

const MAX_ROWS = 10;

type ResultsTablePropsType = {
  rows: AnyTableRowType[];
  onSelectRow: SelectRowType;
  searchType: FormType;
  t: i18next.TFunction;
};

type ColumnsType = {
  [key in FormType]: ColumnType[];
};

const ResultsTable: React.ComponentType<ResultsTablePropsType> = ({
  rows,
  onSelectRow,
  searchType,
  t,
}) => {
  const classes = useResultsTableStyles();
  const columns: ColumnsType = {
    person: [
      {
        name: 'gender',
        width: 30,
        label: '',
        disableSort: true,
        /* eslint-disable-next-line */
        cellRenderer: ({ rowData }: { rowData: PersonTableRowType }) => (
          <div>
            {rowData.gender.toLowerCase() === 'm' ? (
              <Icon
                size="small"
                classes={{
                  root: classes.male,
                }}
              >
                {iconNames.male}
              </Icon>
            ) : (
              <Icon
                size="small"
                classes={{
                  root: classes.female,
                }}
              >
                {iconNames.female}
              </Icon>
            )}
          </div>
        ),
      },
      {
        name: 'name',
        width: 200,
        label: t('ContactImporter:columns.name'),
        /* eslint-disable-next-line */
        cellRenderer: ({ rowData }: { rowData: PersonTableRowType }) => {
          return (
            <div className={classes.nameCell}>
              <span>{rowData.name}</span>
              {getWarnings({ rowData, t })}
            </div>
          );
        },
      },
      {
        name: 'dateOfBirth',
        width: 100,
        label: t('ContactImporter:columns.dateOfBirth'),
      },
      {
        name: 'address',
        width: 1,
        flexGrow: 1,
        label: t('ContactImporter:columns.address'),
      },
    ],
    company: [
      {
        name: 'cocNumber',
        width: 80,
        label: t('ContactImporter:columns.cocNumber'),
      },
      {
        name: 'cocLocationNumber',
        width: 100,
        label: t('ContactImporter:columns.cocLocationNumber'),
      },
      {
        name: 'name',
        width: 1,
        flexGrow: 1,
        label: t('ContactImporter:columns.companyName'),
      },
      {
        name: 'address',
        width: 1,
        flexGrow: 1,
        label: t('ContactImporter:columns.address'),
      },
    ],
  };

  return (
    <Fragment>
      {rows.length && rows.length >= MAX_ROWS ? (
        <div className={classes.maxRowsMessage}>
          {t('ContactImporter:maxRows', { rows: MAX_ROWS })}
        </div>
      ) : null}

      {rows ? (
        <SortableTable
          columns={columns[searchType]}
          rows={rows}
          onRowClick={onSelectRow}
          noRowsMessage={t('ContactImporter:noResults')}
        />
      ) : null}
    </Fragment>
  );
};

export default ResultsTable;
