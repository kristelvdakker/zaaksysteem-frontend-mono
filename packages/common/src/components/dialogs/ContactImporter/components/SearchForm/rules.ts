// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  Rule,
  setRequired,
  setOptional,
  hasValue,
} from '@zaaksysteem/common/src/components/form/rules';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { PersonFields, CompanyFields } from '../../types';

const allEmpty = (fields: AnyFormDefinitionField[]) => {
  const withValues = Object.values(fields).filter(field => hasValue(field));
  return !withValues.length;
};

export const personRules = [
  new Rule<PersonFields>()
    .when(fields => allEmpty(fields as any))
    .then(setRequired(['bsn', 'familyName', 'zipCode', 'streetNumber']))
    .else(setOptional(['bsn', 'familyName', 'zipCode', 'streetNumber'])),
  new Rule<PersonFields>()
    .when('bsn', field => hasValue(field))
    .then(setRequired(['bsn'])),
  new Rule<PersonFields>()
    .when('dateOfBirth', field => hasValue(field))
    .then(setRequired(['dateOfBirth', 'familyName'])),
  new Rule<PersonFields>()
    .when('familyName', field => hasValue(field))
    .then(setRequired(['familyName', 'zipCode', 'streetNumber'])),
  new Rule<PersonFields>()
    .when('zipCode', field => hasValue(field))
    .then(setRequired(['zipCode', 'streetNumber'])),
];

export const companyRules = [
  new Rule<CompanyFields>()
    .when(fields => allEmpty(fields as any))
    .then(
      setRequired([
        'cocNumber',
        'cocLocationNumber',
        'name',
        'zipCode',
        'number',
      ])
    )
    .else(
      setOptional([
        'cocNumber',
        'cocLocationNumber',
        'name',
        'zipCode',
        'number',
      ])
    ),
  new Rule<CompanyFields>()
    .when('cocNumber', field => hasValue(field))
    .then(setRequired(['cocNumber'])),
  new Rule<CompanyFields>()
    .when('cocLocationNumber', field => hasValue(field))
    .then(setRequired(['cocLocationNumber'])),
  new Rule<CompanyFields>()
    .when('street', field => hasValue(field))
    .then(setRequired(['street', 'zipCode', 'number'])),
  new Rule<CompanyFields>()
    .when('zipCode', field => hasValue(field))
    .then(setRequired(['zipCode', 'number'])),
];
