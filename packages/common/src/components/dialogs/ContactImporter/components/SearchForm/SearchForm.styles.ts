// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useSearchFormStyles = makeStyles(
  ({ palette: { coral } }: Theme) => {
    return {
      searchFormWrapper: {
        width: '100%',
        alignContent: 'flex-start',
        display: 'flex',
      },
      form: {
        width: '42%',
        display: 'flex',
        flexDirection: 'column',
      },
      results: {
        flex: 1,
        padding: '0px 20px 0px 20px',
      },
      searchBtn: {
        alignSelf: 'flex-end',
        width: 'min-content',
        marginTop: 8,
      },
      formControlWrapper: {
        '& >:first-child': {
          width: 150,
        },
      },
    };
  }
);
