// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { SingleStepFormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import * as i18next from 'i18next';
import { FormType, PersonFields, CompanyFields } from './types';

export const getFormDefinition = ({
  type,
  t,
}: {
  type: FormType;
  t: i18next.TFunction;
}):
  | SingleStepFormDefinition<PersonFields>
  | SingleStepFormDefinition<CompanyFields> => {
  if (type === 'person') {
    return [
      {
        name: 'bsn',
        label: t('ContactImporter:fields.bsn'),
        placeholder: t('ContactImporter:fields.bsn'),
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'dateOfBirth',
        label: t('ContactImporter:fields.dateOfBirth'),
        type: fieldTypes.KEYBOARDDATEPICKER,
        value: null,
        required: false,
      },
      {
        name: 'familyName',
        label: t('ContactImporter:fields.familyName'),
        placeholder: t('ContactImporter:fields.familyName'),
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'prefix',
        label: t('ContactImporter:fields.prefix'),
        placeholder: t('ContactImporter:fields.prefix'),
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'zipCode',
        label: t('ContactImporter:fields.zipCode'),
        placeholder: t('ContactImporter:fields.zipCode'),
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'streetNumber',
        label: t('ContactImporter:fields.streetNumber'),
        placeholder: t('ContactImporter:fields.streetNumber'),
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
    ];
  } else {
    return [
      {
        name: 'cocNumber',
        label: t('ContactImporter:fields.cocNumber'),
        placeholder: t('ContactImporter:fields.cocNumber'),
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'cocLocationNumber',
        label: t('ContactImporter:fields.cocLocationNumber'),
        placeholder: t('ContactImporter:fields.cocLocationNumber'),
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'company',
        label: t('ContactImporter:columns.companyName'),
        placeholder: t('ContactImporter:columns.companyName'),
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'street',
        label: t('ContactImporter:fields.street'),
        placeholder: t('ContactImporter:fields.street'),
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'zipCode',
        label: t('ContactImporter:fields.zipCode'),
        placeholder: t('ContactImporter:fields.zipCode'),
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'number',
        label: t('ContactImporter:fields.streetNumber'),
        placeholder: t('ContactImporter:fields.streetNumber'),
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
      {
        name: 'letter',
        label: 'Huisletter',
        placeholder: 'Huisletter',
        type: fieldTypes.TEXT,
        value: '',
        required: false,
      },
    ];
  }
};
