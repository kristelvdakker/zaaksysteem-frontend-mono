// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment, useRef, useEffect, useReducer } from 'react';
import { useTranslation } from 'react-i18next';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import ConfirmDialog from '@zaaksysteem/common/src/components/ConfirmDialog/ConfirmDialog';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogDivider,
  //@ts-ignore
} from '@mintlab/ui/App/Material/Dialog';
import Button from '@mintlab/ui/App/Material/Button';
//@ts-ignore
import { unique } from '@mintlab/kitchen-sink/source/unique';
import { useStyles } from './ContactImporter.styles';
//@ts-ignore
import { fetchSession, fetchModulesConfig } from './requests';
import { getInitialState, reducer } from './store/store';
import {
  setSessionData,
  setModulesConfig,
  initDefaults,
  setImportReferenceAction,
  setLoadingAction,
} from './store/actions';
import SearchForm from './components/SearchForm/SearchForm';
import { doRemoteSearch, getRow } from './library';
import locale from './locale';
import { remoteImport } from './requests';
import { AnyTypeFields, RowDataType } from './types';

type ContactImporterPropsType = {
  onSelectRow: (param: ValueType<String>) => void;
  onClose: () => void;
  typeOfRequestors: String[];
};

const ContactImporter: React.ComponentType<ContactImporterPropsType> = ({
  onSelectRow,
  onClose,
  typeOfRequestors,
}) => {
  const dialogEl = useRef(null);
  const classes = useStyles();
  const [t] = useTranslation();
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [state, dispatch] = useReducer(
    reducer,
    getInitialState({ typeOfRequestors })
  );

  const {
    initializing,
    loading,
    selectedInterface,
    selectedFormType,
    importReference,
  } = state;

  const busy = initializing || loading;

  useEffect(() => {
    async function initFunc() {
      dispatch(setSessionData(await fetchSession()));
      dispatch(setModulesConfig(await fetchModulesConfig()));
      dispatch(initDefaults());
    }
    initFunc();
  }, []);

  return (
    <Dialog
      aria-label={t('ContactImporter:importContact')}
      open={true}
      onClose={onClose}
      ref={dialogEl}
      disableBackdropClick={true}
      //@ts-ignore
      fullWidth={true}
      maxWidth="lg"
    >
      {ServerErrorDialog}
      <DialogTitle
        elevated={true}
        id={unique()}
        title={t('ContactImporter:importContact')}
        onCloseClick={onClose}
        scope={'external-contact'}
      />

      <DialogContent padded={true}>
        {initializing && <Loader />}
        {!initializing && (
          <div className={classes.wrapper}>
            <SearchForm
              key={selectedFormType}
              onSearch={async (fields: AnyTypeFields) => {
                doRemoteSearch({
                  fields,
                  state,
                  dispatch,
                  openServerErrorDialog,
                });
              }}
              onSelectRow={({ rowData }: RowDataType) =>
                dispatch(setImportReferenceAction(rowData.uuid))
              }
              dispatch={dispatch}
              state={state}
              t={t}
            />
          </div>
        )}
      </DialogContent>

      <ConfirmDialog
        open={Boolean(importReference)}
        onConfirm={async () => {
          const body = getRow({ state, uuid: state.importReference });
          if (!body) return;

          dispatch(setLoadingAction(true));
          dispatch(setImportReferenceAction(null));

          try {
            const result = await remoteImport({
              selectedInterface,
              body,
              selectedFormType,
            });
            const reference = result?.subject?.reference || '';
            onSelectRow({
              label: body.preview,
              value: reference,
            });
          } catch (error) {
            openServerErrorDialog(error);
          } finally {
            dispatch(setLoadingAction(false));
          }
        }}
        onClose={() => dispatch(setImportReferenceAction(null))}
        title={t('ContactImporter:import')}
        body={
          <div>
            {t('ContactImporter:confirm', {
              preview: getRow({
                state,
                uuid: importReference,
              })?.preview,
            })}
          </div>
        }
      />
      <Fragment>
        <DialogDivider />
        <DialogActions>
          <Button
            presets={['default', 'text']}
            action={onClose}
            disabled={busy}
          >
            {t('common:dialog.cancel')}
          </Button>
        </DialogActions>
      </Fragment>
    </Dialog>
  );
};

/* eslint-disable-next-line */
export default (props: ContactImporterPropsType) => (
  <I18nResourceBundle resource={locale} namespace="ContactImporter">
    <ContactImporter {...props} />
  </I18nResourceBundle>
);
