// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import { isSearchableInterface } from '../library';
import { StoreShapeType, AnyRecordType } from '../types';

type GetInitialStateType = (params: {
  typeOfRequestors: String[];
}) => StoreShapeType;

export const getInitialState: GetInitialStateType = ({ typeOfRequestors }) => ({
  loading: false,
  initializing: true,
  interfaces: [],
  capabilities: [],
  rows: null,
  allowedFormTypes: [],
  selectedFormType: 'company',
  selectedInterface: null,
  typeOfRequestors,
  importReference: null,
  importing: false,
});

/* eslint complexity: [2, 20] */
export const reducer = (state: StoreShapeType, action: any): StoreShapeType => {
  const { payload } = action;

  switch (action.type) {
    case 'setInitializing':
    case 'setLoading':
      return {
        ...state,
        loading: payload,
      };
    case 'setSessionData':
      return {
        ...state,
        capabilities: payload.logged_in_user.capabilities,
      };
    case 'setModulesConfig':
      return {
        ...state,
        interfaces: payload
          .map((interf: any) => ({
            uuid: interf.reference,
            id: interf.instance.id,
            name: interf.instance.name,
            config: interf.instance.interface_config,
          }))
          .filter((interf: any) =>
            isSearchableInterface(interf, state.capabilities)
          )
          .sort((sortA: any, sortB: any) => (sortA.id > sortB.id ? 1 : -1)),
      };
    case 'initDefaults': {
      // Allowed form types
      const { interfaces } = state;
      let allowedFormTypes = [];

      if (interfaces.length) {
        if (
          state.capabilities.includes('contact_search_extern') &&
          (state.typeOfRequestors.includes('natuurlijk_persoon') ||
            state.typeOfRequestors.includes('natuurlijk_persoon_na'))
        ) {
          allowedFormTypes.push('person');
        }
      }

      if (state.typeOfRequestors.includes('niet_natuurlijk_persoon')) {
        allowedFormTypes.push('company');
      }

      // Default selected interface
      const selectedFormType = allowedFormTypes.includes('person')
        ? 'person'
        : 'company';

      return {
        ...state,
        allowedFormTypes,
        selectedFormType,
        selectedInterface: interfaces.length === 0 ? null : interfaces[0].id,
        initializing: false,
      };
    }
    case 'setFormType': {
      return {
        ...state,
        selectedFormType: payload,
      };
    }
    case 'setSelectedInterface': {
      return {
        ...state,
        selectedInterface: payload,
      };
    }
    case 'setSearchResults':
      return {
        ...state,
        rows: payload.rows.map((thisRow: AnyRecordType) => ({
          ...thisRow,
          uuid: v4(),
        })),
      };
    case 'clearSearchResults':
      return {
        ...state,
        rows: [],
      };
    case 'setImportReference':
      return {
        ...state,
        importReference: payload,
      };
    case 'setImporting':
      return {
        ...state,
        importing: payload,
      };
    default:
      return state;
  }
};
