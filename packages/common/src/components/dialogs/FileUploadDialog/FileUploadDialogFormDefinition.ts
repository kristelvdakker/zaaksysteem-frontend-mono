// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { UPLOAD } from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
export const formDefinition = [
  {
    name: 'files',
    type: UPLOAD,
    value: [],
    required: true,
    label: 'FileLinkerDialog:label',
    placeholder: 'FileLinkerDialog:placeholder',
    format: 'file' as 'file',
    multiValue: true,
  },
];
