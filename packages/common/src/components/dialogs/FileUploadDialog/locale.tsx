// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const locale = {
  nl: {
    title: 'Voeg bestanden toe',
    save: 'Toevoegen',
    label: 'Upload een of meerdere bestanden',
  },
};

export default locale;
