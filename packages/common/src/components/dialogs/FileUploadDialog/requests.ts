// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import { APIDocument } from '@zaaksysteem/generated/types/APIDocument.types';
import { FileUploadDialogPropsType } from './FileUploadDialog';

type CreateDocumentsType = Pick<
  FileUploadDialogPropsType,
  'caseUUID' | 'directoryUUID'
> & {
  values: {
    key: string;
    value: string;
    name: string;
  }[];
};

export const createDocuments = async ({
  values,
  caseUUID,
  directoryUUID,
}: CreateDocumentsType): Promise<
  APIDocument.CreateDocumentFromFileResponseBody[]
> => {
  const requests = values.map((value: any) => {
    return request<APIDocument.CreateDocumentFromFileResponseBody>(
      'POST',
      '/api/v2/document/create_document_from_file',
      {
        file_uuid: value.value,
        ...(caseUUID && { case_uuid: caseUUID }),
        ...(directoryUUID && { directory_uuid: directoryUUID }),
      }
    );
  });

  return Promise.all(requests);
};
