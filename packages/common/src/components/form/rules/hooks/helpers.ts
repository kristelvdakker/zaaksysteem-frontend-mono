// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

//@ts-ignore
import differenceWith from 'lodash/differenceWith';
//@ts-ignore
import isEqual from 'lodash/isEqual';
import { executeRules } from '../library/executeRules';
import { FormDefinition } from '../../types/formDefinition.types';
import Rule from '../library/Rule';
import mapValuesToFormDefinition from '../../library/mapValuesToFormDefinition';
import getFormDefinitionWithoutValues from '../../library/getFormDefinitionWithoutValues';

export function getValuesDiff<FormShape = any>(
  values: Partial<FormShape>,
  newValues: Partial<FormShape>
): Partial<FormShape> {
  return Object.keys({ ...values, ...newValues }).reduce<Partial<FormShape>>(
    (acc, key) => {
      const formShapeKey = key as keyof FormShape;

      if (values[formShapeKey] !== newValues[formShapeKey]) {
        return {
          ...acc,
          [key]: newValues[formShapeKey],
        };
      }

      return acc;
    },
    {}
  );
}

export function isFormDefinitionUpdated<FormShape>(
  formDefinition: FormDefinition<FormShape>,
  newFormDefinition: FormDefinition<FormShape>
): boolean {
  const newFormDefinitionWithoutValues = getFormDefinitionWithoutValues<FormShape>(
    newFormDefinition
  );
  const formDefinitionWithoutValues = getFormDefinitionWithoutValues<FormShape>(
    formDefinition
  );

  const formDefinitionDiff = differenceWith(
    //@ts-ignore
    formDefinitionWithoutValues,
    newFormDefinitionWithoutValues,
    isEqual
  );

  return formDefinitionDiff.length > 0;
}

export function getFormDefinitionAfterRules<FormShape>(
  rules: Rule[],
  formDefinition: FormDefinition<FormShape>,
  values: FormShape | Partial<FormShape>
): FormDefinition<FormShape> {
  const formDefinitionWithValues = mapValuesToFormDefinition<FormShape>(
    values as Partial<FormShape>,
    formDefinition
  );

  return rules.length > 0
    ? executeRules(rules, formDefinitionWithValues)
    : formDefinitionWithValues;
}
