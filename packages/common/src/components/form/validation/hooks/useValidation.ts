// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useMemo, useState, useEffect } from 'react';
import * as yup from 'yup';
import { useTranslation } from 'react-i18next';
import {
  FormDefinition,
  FormValuesType,
  RegisterValidationForNamespaceType,
} from '../../types/formDefinition.types';
import createValidation from '../createValidation';
import { ValidationRule } from '../createValidationRule';
import { flattenValues } from '../library/flattenValues';
import validate from '../validate';

type UseValidationType<Values> = {
  formDefinition: FormDefinition<Values>;
  validationMap: {
    [key: string]: ValidationRule;
  };
  registerValidation?: RegisterValidationForNamespaceType;
  namespace?: string;
};

type UseValidationReturnType<Values> = {
  validate: (
    values: FormValuesType<Values>
  ) => Promise<{ [key: string]: string }>;
  schema: yup.ObjectSchema<{ [key: string]: yup.AnySchema }>;
  registerValidation: RegisterValidationForNamespaceType;
};

export function useValidation<Values = any>({
  formDefinition,
  validationMap,
  namespace,
  registerValidation,
}: UseValidationType<Values>) {
  const [t] = useTranslation('common');

  const [validationMapFromState, setValidationMap] = useState(validationMap);

  const schema = useMemo(() => {
    return createValidation({
      formDefinition: formDefinition,
      validationMap: validationMapFromState || {},
      t,
    });
  }, [formDefinition, validationMapFromState]);

  const registerValidationAfterChange = () => {
    if (registerValidation && namespace) {
      registerValidation(schema, namespace);
    }
  };

  const registerValidationForNamespace: RegisterValidationForNamespaceType = (
    schema,
    namespace
  ) => {
    setValidationMap({
      ...validationMapFromState,
      [namespace]: () => schema,
    });
  };

  const validateForm: UseValidationReturnType<Values>['validate'] = useMemo(() => {
    return async values => {
      const flattenedValues = flattenValues<Values>(
        values,
        formDefinition,
        validationMap
      );

      try {
        await validate(schema, flattenedValues);
      } catch (error) {
        const yupErrors = error as yup.ValidationError;

        const errors = yupErrors.inner.reduce<{
          [key: string]: string;
        }>((acc, error) => {
          const { message, path } = error;

          if (message && path && !acc[path]) {
            acc[path] = message;
          }
          return acc;
        }, {});

        if (errors) {
          return errors;
        }
      }

      return {};
    };
  }, [schema]);

  useEffect(registerValidationAfterChange, [validationMapFromState]);

  return {
    schema,
    validate: validateForm,
    registerValidation: registerValidationForNamespace,
  };
}

export default useValidation;
