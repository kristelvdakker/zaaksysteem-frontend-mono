// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useState, useEffect, useMemo } from 'react';
import { FormikProps } from 'formik';
import { get } from '@mintlab/kitchen-sink/source';
import {
  UseSubFormType,
  FormRendererFormField,
} from '../types/formDefinition.types';
import useRuleEngine from '../rules/hooks/useRuleEngine';
import useValidation from '../validation/hooks/useValidation';
import { useFormFields, UseFormFieldsReturnType } from './useFormFields';
import { useFormSteps, UseFormStepsReturnType } from './useFormSteps';

export type UseFormSubReturnType<FormShape> = {
  fields: UseFormFieldsReturnType<FormShape>;
  formik: FormikProps<FormShape>;
} & Omit<UseFormStepsReturnType<FormShape>, 'activeFields'>;

export function useSubForm<FormShape>({
  namespace,
  formDefinition,
  formik,
  registerValidation,
  validateForm,
  validationMap = {},
  fieldComponents = {},
  rules = [],
}: UseSubFormType<FormShape>): UseFormSubReturnType<FormShape> {
  const { setFieldValue, getFieldProps } = formik;

  const values = useMemo<FormShape | Partial<FormShape>>(() => {
    return get<FormShape, FormShape>(formik.values, namespace) || {};
  }, [formik.values]);

  const [formDefinitionFromState, setFormDefinition] = useState(formDefinition);

  const {
    schema,
    registerValidation: registerSubValidation,
  } = useValidation<FormShape>({
    formDefinition: formDefinitionFromState,
    validationMap: validationMap || {},
  });

  const [updatedFormDefinition, updatedValues] = useRuleEngine<FormShape>({
    values,
    rules,
    setFormDefinition,
    namespace,
    setValues: formik.setValues,
    setFieldValue: formik.setFieldValue,
    formDefinition: formDefinitionFromState,
  });

  const { activeFields, ...restFormSteps } = useFormSteps<FormShape>({
    formDefinition: updatedFormDefinition,
    errors: formik.errors,
    touched: formik.touched,
  });

  const fields = useFormFields<FormShape>({
    formik,
    namespace,
    validateForm,
    registerValidation: registerSubValidation,
    formDefinitionFields: activeFields,
    values: updatedValues,
    customFields: fieldComponents,
  });

  const registerValidationSchema = () => {
    registerValidation(schema, namespace);
  };

  const fieldHasNoValue = (
    field: FormRendererFormField<FormShape>
  ): boolean => {
    const fieldProps = getFieldProps(field.name);
    return !fieldProps.value;
  };

  const addFieldInitialValue = (
    field: FormRendererFormField<FormShape>
  ): FormRendererFormField<FormShape> => {
    const fieldValue = get(formik.values, field.name) || field.value || '';

    return {
      ...field,
      value: fieldValue,
    };
  };

  const setFieldsInitialValue = () => {
    fields
      .filter(fieldHasNoValue)
      .map(addFieldInitialValue)
      .forEach(({ name, value }) => setFieldValue(name, value));
  };

  useEffect(registerValidationSchema, [schema]);
  useEffect(setFieldsInitialValue, [activeFields]);
  useEffect(validateForm, [schema]);

  return { fields, formik, ...restFormSteps };
}
