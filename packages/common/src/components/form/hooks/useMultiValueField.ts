// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTranslation } from 'react-i18next';
import { createArraySchema } from '../validation/createSchema';
import { RegisterValidationForNamespaceType } from '../types/generic.types';
import {
  FormRendererFormField,
  FormFieldPropsType,
} from '../types/fieldComponent.types';
import { useNamespace } from './useNamespace';

type MultiFieldType<FormShape, Config, ValueType> = FormFieldPropsType<
  FormShape,
  Config,
  ValueType
> & {
  remove: () => void;
};

type UseMultiValueFieldReturnType<FormShape, ValueType> = {
  fields: MultiFieldType<FormShape, any, ValueType>[];
  add: (value: ValueType) => void;
};

export function useMultiValueField<FormShape, ValueType = unknown>(
  props: FormRendererFormField<FormShape, any, ValueType[]>
): UseMultiValueFieldReturnType<FormShape, ValueType> {
  const [t] = useTranslation('common');
  const {
    name,
    value,
    setFieldValue,
    setFieldTouched,
    definition,
    registerValidation,
    namespace,
  } = props;
  const { withNameSpace } = useNamespace(namespace);

  const values: ValueType[] = Array.isArray(value)
    ? value
    : ([value] as ValueType[]);

  const touchBaseField = () => setFieldTouched(`${name}.__base`, true);

  const add: UseMultiValueFieldReturnType<
    FormShape,
    ValueType
  >['add'] = value => {
    setFieldValue(name, [...values, value || null]);
    touchBaseField();
  };

  const remove = (valueToRemove: ValueType) => {
    setFieldValue(
      name,
      values.filter(value => value !== valueToRemove)
    );
    touchBaseField();
  };

  const registerArrayValidation: RegisterValidationForNamespaceType = schema => {
    const arraySchema = createArraySchema(definition, t);
    registerValidation((arraySchema as any).of(schema), withNameSpace(name));
  };

  const fields: MultiFieldType<FormShape, any, ValueType>[] = values.map(
    (fieldValue, index) => ({
      ...props,
      remove: () => remove(fieldValue),
      name: `${name}[${index}]` as any,
      value: fieldValue,
      registerValidation: registerArrayValidation,
    })
  );

  return {
    add,
    fields,
  };
}
