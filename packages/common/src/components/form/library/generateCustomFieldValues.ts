// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import {
  FormFieldPropsType,
  FormValuesType,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { RelationshipTypeType } from '@zaaksysteem/common/src/components/form/fields/Relationship/Relationship';

type ZsValueRelationDefault = {
  value: string;
  specifics: {
    relationship_type: string;
    metadata: {
      summary: string;
    };
  };
};

type ZsValueRelationDocument = {
  value: string;
  specifics: {
    relationship_type: string;
    metadata: {
      filename: string;
    };
  };
};

type ZsValueRelation = ZsValueRelationDefault | ZsValueRelationDocument | null;

type TransformersType = {
  [key: string]: (
    value: any | any[],
    field: FormFieldPropsType
  ) => ZsValueRelation;
};

const transformers: TransformersType = {
  relationship: (value, field) => {
    const relationshipType: RelationshipTypeType =
      field.config.relationshipType;

    switch (relationshipType) {
      case 'custom_object':
      case 'subject':
        return {
          value: value?.value,
          specifics: {
            relationship_type: relationshipType,
            metadata: {
              summary: value.label,
            },
          },
        };
      case 'document':
        return value?.value
          ? {
              value: value.value,
              specifics: {
                relationship_type: relationshipType,
                metadata: {
                  filename: value.label,
                },
              },
            }
          : null;
    }
  },
};

const generateCustomFieldValues = (
  values: FormValuesType,
  fields: FormFieldPropsType[]
) =>
  Object.entries(values).reduce((acc, [key, value]) => {
    const field = fields.find(field => field.name === key);

    if (!field || value === undefined) {
      return acc;
    }

    const transformedValue = Object.prototype.hasOwnProperty.call(
      transformers,
      field.type
    )
      ? transformers[field?.type](value, field)
      : { value };

    return {
      ...acc,
      [key]: transformedValue,
    };
  }, {});

export default generateCustomFieldValues;
