// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { FormikProps, FormikErrors, FormikTouched } from 'formik';
import {
  FormRendererFormField,
  FormRendererFieldComponentPropsType,
  NestedFormValue,
  FormValue,
  FormDefinition,
  MultiStepFormDefinition,
  AnyFormDefinitionField,
  WithNamespaceType,
} from '../types/formDefinition.types';

export function createField<Values>(fields: FormRendererFormField<Values>[]) {
  const Field: React.ComponentType<
    FormRendererFieldComponentPropsType<Values>
  > = ({ name, error }) => {
    const [{ FieldComponent, ...restProps }] = fields.filter(
      item => item.name === name
    );
    return (
      <React.Fragment>
        <FieldComponent {...restProps} error={error} />
      </React.Fragment>
    );
  };
  return Field;
}

export function getField<Values = any>(
  fields: FormRendererFormField<Values>[]
) {
  return (
    fieldName: keyof Values
  ): FormRendererFormField<Values> | undefined => {
    return fields.find(field => fieldName === field.name);
  };
}

export const setTouchedAndHandleChange = ({
  setFieldTouched,
  handleChange,
}: Pick<FormikProps<any>, 'handleChange' | 'setFieldTouched'>) => (
  event: React.ChangeEvent<any>
) => {
  const {
    target: { name },
  } = event;
  setFieldTouched(name, true);
  handleChange(event);
};

export function hasField<Values = any>(
  fields: FormRendererFormField<Values>[]
) {
  return (fieldName: keyof Values): boolean => {
    return fields.findIndex(field => fieldName === field.name) > -1;
  };
}

const flattenSingleValueField = (value: unknown, nestedKey: string = 'value') =>
  (value as NestedFormValue)[nestedKey]
    ? (value as NestedFormValue)[nestedKey].toString()
    : (value as FormValue).toString();

export const flattenField = (
  value: unknown | unknown[],
  nestedKey: string = 'value'
): string =>
  Array.isArray(value)
    ? value.map(item => flattenSingleValueField(item, nestedKey)).join(', ')
    : flattenSingleValueField(value, nestedKey);

export function isSteppedForm<FormShape>(
  formDefinition: FormDefinition<FormShape>
): formDefinition is MultiStepFormDefinition<FormShape> {
  return (
    typeof (formDefinition as MultiStepFormDefinition<FormShape>)[0]?.title !==
    'undefined'
  );
}

export function fieldsInNamespaceAreValid<FormShape>(
  namespace: string,
  errors: FormikErrors<FormShape>
) {
  return Object.keys(errors).every(key => key.startsWith(namespace) === false);
}

export function isStepValid<FormShape>(
  fields: AnyFormDefinitionField<FormShape>[],
  errors: FormikErrors<FormShape>,
  withNameSpace: WithNamespaceType<FormShape>
): boolean {
  return fields.every(field =>
    fieldsInNamespaceAreValid(withNameSpace(field.name), errors)
  );
}

export function isStepTouched<FormShape>(
  fields: AnyFormDefinitionField<FormShape>[],
  touched: FormikTouched<FormShape>
): boolean {
  return fields
    .filter(field => field.required)
    .every(field => typeof touched[field.name] !== 'undefined');
}

export function getActiveSteps<FormShape>(
  formDefinition: MultiStepFormDefinition<FormShape>
): MultiStepFormDefinition<FormShape> {
  return formDefinition.filter(
    ({ fields }) => fields.filter(field => !field.hidden).length > 0
  );
}
