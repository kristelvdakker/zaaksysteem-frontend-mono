// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICaseManagement } from '@zaaksysteem/generated';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  ReadonlyLinkContainerStyleSheet,
  iconStyleSheet,
} from './ReadonlyValuesContainer.styles';

const props = {
  style: { whiteSpace: 'pre' },
  tabIndex: 0,
} as const;

export const ReadonlyValuesContainer: React.ComponentType<{
  value: any;
}> = ({ value }) => {
  if (value || value === 0) {
    const normalizedValue = Array.isArray(value)
      ? value.map(val => val.toString()).join('\n')
      : value;
    return <div {...props}>{normalizedValue}</div>;
  } else {
    return <div {...props}>-</div>;
  }
};

export const ReadonlyLinkContainer: React.ComponentType<{
  value: any;
  type?: string;
}> = ({ value, type }) => {
  const classes = ReadonlyLinkContainerStyleSheet();
  const iconClasses = iconStyleSheet();

  const getUrl = (value: any, type: string) => {
    switch (type) {
      case 'subject':
        return buildUrl<APICaseManagement.GetCustomObjectTypeRequestParams>(
          '/redirect/contact_page',
          { uuid: value.value }
        );
      case 'custom_object':
        return `/main/object/${value.value}`;
      default:
        throw new Error('Unknown type: ' + type);
    }
  };

  if (type === 'document' && value) {
    return <ReadonlyValuesContainer value={value?.label} />;
  } else if (type && value) {
    return (
      <a
        {...props}
        href={getUrl(value, type)}
        target="_blank"
        rel="noopener noreferrer"
        className={classes.container}
      >
        {value.label}
        <Icon classes={iconClasses} size={18}>
          {iconNames.open_in_new}
        </Icon>
      </a>
    );
  } else if (value) {
    return (
      <a {...props} href={value}>
        {value}
      </a>
    );
  } else {
    return <div {...props}>-</div>;
  }
};
