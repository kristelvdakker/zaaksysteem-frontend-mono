// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { APICaseManagement } from '@zaaksysteem/generated';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { CustomFieldTypeType } from '@zaaksysteem/common/src/types/CustomFields';
import { IntegrationContextType } from '@mintlab/ui/types/MapIntegration';

type CustomObjectCustomFieldsType = { [k: string]: any };

type CustomObjectCustomFieldValue = {
  type: string;
  value: any;
  specifics: {
    metadata: {
      filename?: string;
      summary?: string;
    };
  };
};

const mapValue = (customField: CustomObjectCustomFieldValue) => {
  const { type, value, specifics } = customField;
  switch (type) {
    case 'relationship': {
      return {
        value,
        label: specifics.metadata.summary || specifics.metadata.filename,
        key: specifics.metadata.filename,
      };
    }

    default:
      return value;
  }
};

const mapChoices = (option: string) => ({ label: option, value: option });

const fieldTypesDict: Record<CustomFieldTypeType, string> = {
  text: fieldTypes.TEXT,
  richtext: fieldTypes.TEXT,
  textarea: fieldTypes.TEXTAREA,
  select: fieldTypes.FLATVALUE_SELECT,
  option: fieldTypes.RADIO_GROUP,
  date: fieldTypes.DATEPICKER,
  geojson: fieldTypes.GEOJSON_MAP_INPUT,
  relationship: fieldTypes.RELATIONSHIP,
  valuta: fieldTypes.CURRENCY,
  bankaccount: fieldTypes.IBAN,
  url: fieldTypes.WEB_ADDRESS,
  email: fieldTypes.EMAIL,
  address_v2: fieldTypes.ADDRESS,
  checkbox: fieldTypes.CHECKBOX_GROUP,
  numeric: fieldTypes.NUMERIC,
};

/* eslint complexity: [2, 9] */
const mapField = (
  customFieldValues: CustomObjectCustomFieldsType,
  readOnly?: Boolean,
  t?: i18next.TFunction,
  config?: any
) => ({
  custom_field_type,
  custom_field_specification,
  is_required,
  label,
  magic_string = 'undefined',
  options,
}: APICaseManagement.CustomObjectTypeCustomFieldDefinition) => {
  const value = customFieldValues[magic_string as CustomFieldTypeType];

  return {
    type: fieldTypesDict[custom_field_type as CustomFieldTypeType],
    name: magic_string,
    value: value ? mapValue(value) : undefined,
    config: {
      ...(custom_field_type === 'relationship'
        ? {
            relationshipType: custom_field_specification?.type,
            relationshipUuid: custom_field_specification?.uuid,
          }
        : {}),
      ...(config || {}),
    },
    label,
    required: is_required,
    choices: options.map(mapChoices),
    readOnly,
    applyBackgroundColor:
      !readOnly &&
      (custom_field_type as string) !== 'checkbox' &&
      custom_field_type !== 'option',
    ...(custom_field_type === 'relationship' ? { multiValue: false } : {}),
    ...(t && custom_field_type === 'date'
      ? { dateFormat: t('common:dates.dateFormat') }
      : {}),
  };
};

type GenerateCustomFieldFormDefinitionType = {
  customFieldsDefinition: APICaseManagement.CustomObjectTypeCustomFieldDefinition[];
  customFieldsValues: CustomObjectCustomFieldsType;
  readOnly?: Boolean;
  config?: { context?: IntegrationContextType };
  t?: i18next.TFunction;
};

const generateCustomFieldFormDefinition = ({
  customFieldsDefinition,
  customFieldsValues,
  readOnly,
  config,
  t,
}: GenerateCustomFieldFormDefinitionType): AnyFormDefinitionField[] =>
  customFieldsDefinition.map(mapField(customFieldsValues, readOnly, t, config));

export default generateCustomFieldFormDefinition;
