// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
type CustomFieldsType = {
  [key: string]: any;
};

const formatGetCaseCustomFields = (customFields: CustomFieldsType) =>
  Object.keys(customFields).reduce((acc, key) => {
    const type = customFields[key].type;
    const value = customFields[key].value;

    if (type === 'relationship') {
      return {
        ...acc,
        [key]: {
          type,
          value: value.id,
          specifics: {
            metadata: {
              summary: value.label,
            },
            relationship_type: value.type,
          },
        },
      };
    }

    return {
      ...acc,
      [key]: {
        value,
      },
    };
  }, {});

export default formatGetCaseCustomFields;
