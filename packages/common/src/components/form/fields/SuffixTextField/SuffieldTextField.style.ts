// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useStyles = makeStyles(({ palette: { common } }: Theme) => {
  return {
    wrapper: {
      display: 'flex',
    },
    suffix: {
      display: 'flex',
      paddingLeft: 20,
      backgroundColor: common.white,
      alignItems: 'center',
    },
  };
});
