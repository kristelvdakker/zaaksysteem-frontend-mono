// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
//@ts-ignore
import MultilineOption from '@mintlab/ui/App/Zaaksysteem/Select/Option/MultilineOption';
import { Select, ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { FormFieldComponentType } from '../../types/Form2.types';
import { fetchObjectChoices } from './ObjectFinder.library';

export const ObjectFinder: FormFieldComponentType<
  ValueType<string[]>,
  { relationshipUuid: string }
> = props => {
  const [input, setInput] = useState('');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const objectTypeUuid = props.config.relationshipUuid;

  return (
    <React.Fragment>
      <DataProvider
        autoProvide={input !== ''}
        provider={fetchObjectChoices(openServerErrorDialog)}
        providerArguments={[input, objectTypeUuid]}
      >
        {({ data, busy }) => {
          const normalizedChoices = data || undefined;

          return (
            <Select
              {...props}
              choices={normalizedChoices}
              isClearable={true}
              loading={busy}
              getChoices={setInput}
              components={{
                Option: MultilineOption,
              }}
              filterOption={() => true}
            />
          );
        }}
      </DataProvider>
      {ServerErrorDialog}
    </React.Fragment>
  );
};
