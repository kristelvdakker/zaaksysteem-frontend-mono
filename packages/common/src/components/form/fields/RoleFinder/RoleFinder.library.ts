// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICaseManagement } from '@zaaksysteem/generated';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { buildUrl } from '@mintlab/kitchen-sink/source';

export const fetchRoleChoices = (onError: OpenServerErrorDialogType) => async (
  roleUuid: string
) => {
  if (!roleUuid) return [];

  const body = await request<APICaseManagement.GetRolesResponseBody>(
    'GET',
    buildUrl<APICaseManagement.GetRolesRequestParams>(
      '/api/v2/cm/authorization/get_roles',
      {
        filter: {
          'relationships.parent.id': roleUuid,
        },
      }
    )
  ).catch(onError);

  return body
    ? body.data.map(role => ({
        label: role.attributes?.name || '',
        value: role.id,
      }))
    : [];
};
