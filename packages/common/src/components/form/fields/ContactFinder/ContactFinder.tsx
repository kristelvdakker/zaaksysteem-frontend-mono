// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import MultilineOption from '@mintlab/ui/App/Zaaksysteem/Select/Option/MultilineOption';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { Select } from '@mintlab/ui/App/Zaaksysteem/Select';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { FormFieldComponentType } from '../../types/Form2.types';
import { fetchContactChoices } from './ContactFinder.library';

const ContactFinder: FormFieldComponentType<ValueType<string>, any> = ({
  multiValue,
  config,
  value,
  ...restProps
}) => {
  const [input, setInput] = useState('');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const { choices } = restProps;

  return (
    <React.Fragment>
      <DataProvider
        providerArguments={[input]}
        autoProvide={input !== ''}
        provider={fetchContactChoices(config, openServerErrorDialog)}
      >
        {({ data, busy }) => {
          const normalizedChoices = data || choices;

          return (
            <Select
              {...restProps}
              value={value}
              choices={normalizedChoices}
              isClearable={true}
              loading={busy}
              isMulti={multiValue}
              getChoices={setInput}
              components={{
                ...restProps.components,
                Option: MultilineOption,
              }}
              filterOption={() => true}
              multiValueLabelIcon={config?.multiValueLabelIcon}
            />
          );
        }}
      </DataProvider>
      {ServerErrorDialog}
    </React.Fragment>
  );
};

export default ContactFinder;
