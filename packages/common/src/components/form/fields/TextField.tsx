// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import { TextField as UITextField } from '@mintlab/ui/App/Material/TextField';
import { ReadonlyValuesContainer } from '../library/ReadonlyValuesContainer';
import { FormFieldComponentType } from '../types/Form2.types';

export const TextField: FormFieldComponentType<string> = props => {
  const { value, readOnly } = props;

  return readOnly ? (
    <ReadonlyValuesContainer value={props.value} />
  ) : (
    <UITextField {...props} value={value || ''} />
  );
};
