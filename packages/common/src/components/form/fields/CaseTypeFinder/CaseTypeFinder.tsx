// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import { Select } from '@mintlab/ui/App/Zaaksysteem/Select';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { asArray } from '@mintlab/kitchen-sink/source';
import { FormFieldComponentType } from '../../types/Form2.types';
import {
  CaseTypeType,
  CaseTypeOptionType,
  fetchCaseType,
  fetchCaseTypeChoices,
} from './CaseTypeFinder.library';

export const CaseTypeFinder: FormFieldComponentType<
  ValueType<CaseTypeType>,
  any
> = ({ name, value, multiValue = false, config, ...restProps }) => {
  const [input, setInput] = React.useState('');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  const onChange = async (event: React.ChangeEvent<any>) => {
    const selectValue = asArray(event.target.value);

    const fetchFullCaseType = async (option: CaseTypeOptionType) => {
      if (option.fetched === false) {
        return await fetchCaseType(option.data.id, openServerErrorDialog).then(
          (fullCaseType: CaseTypeType) => ({
            data: fullCaseType,
            value: option.data.id,
            label: fullCaseType.name,
          })
        );
      }
      return option;
    };
    const results = await Promise.all(
      selectValue.map(async option => fetchFullCaseType(option))
    );

    restProps.onChange({
      ...event,
      target: {
        ...event.target,
        value: multiValue ? results : results[0],
      },
    });
  };

  return (
    <React.Fragment>
      <DataProvider
        providerArguments={[input]}
        autoProvide={input !== ''}
        provider={fetchCaseTypeChoices(openServerErrorDialog)}
      >
        {({ data, busy }) => {
          return (
            <Select
              {...restProps}
              value={value}
              name={name}
              choices={data || []}
              isClearable={true}
              getChoices={setInput}
              onChange={onChange}
              isSearchable={true}
              loading={busy}
              filterOption={option => Boolean(option)}
              isMulti={multiValue}
              multiValueLabelIcon={config?.multiValueLabelIcon}
            />
          );
        }}
      </DataProvider>
      {ServerErrorDialog}
    </React.Fragment>
  );
};
