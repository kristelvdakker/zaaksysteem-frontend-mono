// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import { TextField } from '@mintlab/ui/App/Material/TextField';
import { makeStyles } from '@material-ui/core/styles';
import { ReadonlyValuesContainer } from '../library/ReadonlyValuesContainer';
import { FormFieldComponentType } from '../types/Form2.types';

const useTextareaStyle = makeStyles(() => ({
  inputRoot: {
    paddingBottom: '0',
  },
  textarea: {
    resize: 'vertical',
  },
}));

export const Textarea: FormFieldComponentType<string> = ({
  readOnly,
  ...restProps
}) => {
  const classes = useTextareaStyle();
  return readOnly ? (
    <ReadonlyValuesContainer value={restProps.value} />
  ) : (
    <TextField
      {...restProps}
      isMultiline={true}
      classes={{ inputRoot: classes.inputRoot, input: classes.textarea }}
    />
  );
};

export default Textarea;
