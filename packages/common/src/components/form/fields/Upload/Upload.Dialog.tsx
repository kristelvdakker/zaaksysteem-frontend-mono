// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogDivider,
  //@ts-ignore
} from '@mintlab/ui/App/Material/Dialog';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import createDialogActions from '../../../dialogs/library/createDialogActions';
import { FormDefinition } from '../../types/formDefinition.types';
import { useForm } from '../../hooks/useForm';
import { FileObject } from './Upload.types';

export interface UploadDialogProps {
  open: boolean;
  scope?: string;
  onClose: Function;
  onConfirm: (files: FileObject[]) => void;
  classes: any;
  accept?: string[];
  multiValue: boolean;
  [key: string]: any;
}

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'text'],
  secondaryPresets: ['default', 'text'],
});

export const UploadDialog: React.ComponentType<UploadDialogProps> = ({
  open = false,
  scope,
  onClose,
  onConfirm,
  classes,
  accept,
  multiValue,
  ...rest
}) => {
  const [t] = useTranslation();
  const title = t('formRenderer:fileSelect.addFile');
  const formDefinition: FormDefinition = [
    {
      name: 'selectDialogAttachments',
      type: fieldTypes.UPLOAD,
      value: null,
      format: 'file',
      required: true,
      uploadDialog: false,
      multiValue,
      accept,
    },
  ];

  const {
    fields,
    formik: { values, isValid },
  } = useForm({ isInitialValid: true, formDefinition });

  const dialogActions = getDialogActions(
    {
      disabled: !isValid,
      text: t('common:forms.add'),
      action() {
        onConfirm(values.selectDialogAttachments as FileObject[]);
        onClose();
      },
    },
    {
      text: t('common:forms.cancel'),
      action: onClose,
    },
    'file-select-dialog'
  );

  return (
    <Dialog aria-label={title} open={open} {...rest} classes={classes}>
      <DialogTitle title={title} scope={scope} />
      <DialogContent padded={true}>
        {fields.map(({ FieldComponent, ...restProps }) => {
          return (
            <div key={restProps.key}>
              <FieldComponent {...restProps} />
            </div>
          );
        })}
      </DialogContent>
      <DialogDivider />
      <DialogActions>{dialogActions}</DialogActions>
    </Dialog>
  );
};

export default UploadDialog;
