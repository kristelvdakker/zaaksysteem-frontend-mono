// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useUploadStyle = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    list: {
      marginBottom: 20,
    },
    dialogRoot: {
      width: 500,
    },
    buttonRoot: {
      '&>*:first-child >*:first-child': {
        color: greyscale.darker,
      },
    },
  })
);
