// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Component } from 'react';
//@ts-ignore
import { FileSelect } from '@mintlab/ui/App/Zaaksysteem/FileSelect';
//@ts-ignore
import { unique } from '@mintlab/kitchen-sink/source';
import { STATUS_PENDING, STATUS_FAILED } from '../../constants/fileStatus';
import { ResponseBodyType, sendAsFile, sendAsDocument } from './Upload.library';
import { FileObject } from './Upload.types';

type UploadPropsType = {
  accept?: string[];
  selectInstructions: string;
  dragInstructions: string;
  dropInstructions: string;
  orLabel: string;
  value: any[];
  multiValue: boolean;
  list: React.ReactNode;
  addFiles: (files: FileObject[]) => void;
  setFileStatus: (key: string, properties: Partial<FileObject>) => void;
  fileType: 'file' | 'document';
};

class Upload extends Component<UploadPropsType> {
  render() {
    const {
      accept,
      selectInstructions,
      dragInstructions,
      dropInstructions,
      value = [],
      orLabel,
      multiValue = true,
      list = [],
    } = this.props;

    return (
      <FileSelect
        onDrop={this.handleDrop}
        accept={accept}
        dragInstructions={dragInstructions}
        dropInstructions={dropInstructions}
        selectInstructions={selectInstructions}
        orLabel={orLabel}
        hasFiles={value && value.length}
        multiValue={multiValue}
        fileList={list}
        error={this.error()}
      />
    );
  }

  fileCompleted(key: string, response: ResponseBodyType) {
    if (!response.data) return;
    const { setFileStatus } = this.props;
    setFileStatus(key, { value: response.data.id });
  }

  fileError(key: string) {
    const { setFileStatus } = this.props;
    setFileStatus(key, { status: STATUS_FAILED });
  }

  /**
   * @param {Array} acceptedFiles
   */
  handleDrop = (acceptedFiles: File[]) => {
    const { value, multiValue, addFiles, fileType } = this.props;

    if (!acceptedFiles || !acceptedFiles.length) return;
    if (!multiValue && value.length) return;

    const fileList = multiValue ? acceptedFiles : acceptedFiles.slice(0, 1);

    const fileObjects = fileList.map<FileObject>((file: File) => {
      const key = unique();

      (fileType === 'document' ? sendAsDocument : sendAsFile)(file)
        .then((response: ResponseBodyType) => this.fileCompleted(key, response))
        .catch(() => this.fileError(key));

      return {
        key,
        value: null,
        label: file.name,
        status: STATUS_PENDING,
      };
    });

    addFiles(fileObjects);
  };

  /**
   * @return {Boolean}
   */
  error() {
    const { value } = this.props;
    const failedFiles = value.filter(
      (thisFile: FileObject) => thisFile.status === STATUS_FAILED
    );
    return Boolean(failedFiles.length);
  }
}

export default Upload;
