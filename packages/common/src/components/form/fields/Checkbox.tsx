// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import MaterialCheckbox from '@mintlab/ui/App/Material/Checkbox';
//@ts-ignore
import { useTranslation } from 'react-i18next';
import { ReadonlyValuesContainer } from '../library/ReadonlyValuesContainer';
import { FormFieldComponentType } from '../types/Form2.types';

export const Checkbox: FormFieldComponentType<boolean> = ({
  readOnly,
  ...restProps
}) => {
  const [t] = useTranslation('common');

  return readOnly ? (
    <ReadonlyValuesContainer
      value={restProps.value ? t('forms.checkboxReadonlyChecked') : ''}
    />
  ) : (
    <MaterialCheckbox {...restProps} />
  );
};

export default Checkbox;
