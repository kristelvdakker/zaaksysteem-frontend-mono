// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { createDisplayName } from '../CaseRoleFinder/CaseRoleFinder.library';
import { CaseRequestorConfigType } from './CaseRequestor.types';

export const fetchRequestors = async (
  config: CaseRequestorConfigType,
  onError: OpenServerErrorDialogType
) => {
  const body = await request<APICaseManagement.GetSubjectRelationsResponseBody>(
    'GET',
    buildUrl<APICaseManagement.GetSubjectRelationsRequestParams>(
      '/api/v2/cm/case/get_subject_relations',
      {
        case_uuid: config.caseUuid,
        include: ['subject'],
      }
    )
  ).catch(onError);

  return body
    ? body?.data
        .map(role => ({
          role,
          subject:
            body.included &&
            body.included.filter(subject => {
              return (
                role.relationships.subject &&
                subject.id === role.relationships.subject.data.id
              );
            })[0],
        }))
        .sort((roleA, roleB) =>
          roleA.role.attributes.role < roleB.role.attributes.role ? -1 : 1
        )
        .filter(config.itemFilter || (() => true))
        .map(role => {
          return config.valueResolver
            ? config.valueResolver(role)
            : {
                value: role.role.id,
                label: createDisplayName(role),
              };
        })
    : [];
};
