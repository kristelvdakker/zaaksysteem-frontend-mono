// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import fecha from 'fecha';
//@ts-ignore
import { LoadableDatePicker } from '@mintlab/ui/App/Material/DatePicker';
import { ReadonlyValuesContainer } from '../library/ReadonlyValuesContainer';
import { FormFieldComponentType } from '../types/Form2.types';

const DatePicker: FormFieldComponentType<string> = props => {
  const { onClose, setFieldValue, name, readOnly, value, dateFormat } = props;
  const date =
    typeof value === 'string' && dateFormat
      ? fecha.format(new Date(value), dateFormat)
      : null;

  return readOnly ? (
    <ReadonlyValuesContainer value={date} />
  ) : (
    <LoadableDatePicker
      {...props}
      onClose={() => {
        if (onClose) {
          onClose();
        } else {
          setFieldValue(name, null);
        }
      }}
    />
  );
};

export default DatePicker;
