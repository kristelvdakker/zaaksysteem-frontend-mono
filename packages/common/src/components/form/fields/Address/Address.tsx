// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useSelector } from 'react-redux';
import { GeoMap } from '@mintlab/ui/App/External/GeoMap';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { Select } from '@mintlab/ui/App/Zaaksysteem/Select';
import { getBagPreferences } from '@zaaksysteem/common/src/store/session/session.selectors';
import { IntegrationContextType } from '@mintlab/ui/types/MapIntegration';
import { FormFieldComponentType } from '../../types/Form2.types';
import { ReadonlyValuesContainer } from '../../library/ReadonlyValuesContainer';
import { AddressValueType } from './Address.types';
import { fetchAddressChoices, fetchClosestAddress } from './Address.library';

export const AddressField: FormFieldComponentType<
  AddressValueType,
  { context: IntegrationContextType }
> = props => {
  const {
    value,
    name,
    onChange,
    readOnly,
    config: { context },
  } = props;
  const markerPosition = props.value?.geojson.features[0].geometry;
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [input, setInput] = React.useState('');
  const [preferredMunicipalities, preferredOnly] = useSelector(
    getBagPreferences
  );

  return (
    <React.Fragment>
      <DataProvider
        providerArguments={[
          input,
          (preferredMunicipalities || '').toString(),
          preferredOnly,
        ]}
        autoProvide={input !== ''}
        provider={fetchAddressChoices(openServerErrorDialog)}
      >
        {({ data, busy }) => {
          return readOnly ? (
            <div style={{ paddingBottom: 10 }}>
              <ReadonlyValuesContainer value={value?.address.full} />
            </div>
          ) : (
            <Select
              {...props}
              value={value ? { label: value.address.full, value } : null}
              choices={data || []}
              isClearable={true}
              loading={busy}
              getChoices={setInput}
              onChange={event =>
                onChange({
                  target: { name, value: event.target.value?.value || null },
                })
              }
            />
          );
        }}
      </DataProvider>
      <GeoMap
        canDrawFeatures={false}
        name={name}
        context={context}
        markerPosition={markerPosition}
        onClick={
          readOnly
            ? undefined
            : point =>
                fetchClosestAddress(point, openServerErrorDialog).then(
                  val => val && onChange({ target: { name, value: val } })
                )
        }
        geoFeature={null}
        minHeight={370}
      />
      {ServerErrorDialog}
    </React.Fragment>
  );
};
