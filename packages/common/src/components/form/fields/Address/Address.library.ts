// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { AddressValueType } from './Address.types';

export const fetchAddressChoices = (
  onError: OpenServerErrorDialogType
) => async (
  input: string,
  preferredMunicipalities: string,
  preferredOnly: boolean
): Promise<ValueType<AddressValueType>[]> => {
  const body = await request<{
    data: {
      geo_punt: {
        type: 'Point';
        coordinates: [number, number, number];
      };
      id: string;
      straatnaam: string;
      huisnummer_autocomplete: string;
      postcode: null;
      provincie: string;
      plaats: string;
    }[];
  }>(
    'GET',
    buildUrl('/zsnl_bag/bag/search', {
      search_string: input,
      bag_object_type: 'nummeraanduiding',
      prio_municipalities: preferredMunicipalities,
      prio_only: preferredOnly ? 1 : 0,
    })
  ).catch(onError);

  return body
    ? body.data.map(bagData => {
        const fullAddress = `${bagData.straatnaam} ${
          bagData.huisnummer_autocomplete
        }, ${bagData.postcode || ''} ${bagData.plaats}`.replace('  ', ' ');
        return {
          label: fullAddress,
          value: {
            bag: {
              id: bagData.id.split('-')[1],
              type: 'nummeraanduiding',
            },
            address: { full: fullAddress },
            geojson: {
              type: 'FeatureCollection',
              features: [
                {
                  type: 'Feature',
                  properties: {},
                  geometry: {
                    type: 'Point',
                    coordinates: [
                      bagData.geo_punt.coordinates[0],
                      bagData.geo_punt.coordinates[1],
                    ],
                  },
                },
              ],
            },
          },
        };
      })
    : [];
};

export const fetchClosestAddress = async (
  point: GeoJSON.Point,
  onError: OpenServerErrorDialogType
) => {
  const body = await request<{
    data: {
      geo_punt: {
        type: 'Point';
        coordinates: [number, number, number];
      };
      id: string;
      straatnaam: string;
      huisnummer_autocomplete: string;
      postcode: null;
      provincie: string;
      plaats: string;
    }[];
  }>(
    'GET',
    buildUrl('/zsnl_bag/bag/find-nearest', {
      latitude: point.coordinates[1],
      longitude: point.coordinates[0],
    })
  ).catch(onError);

  return (body || null)?.data.map<AddressValueType>(bagData => {
    const fullAddress = `${bagData.straatnaam} ${
      bagData.huisnummer_autocomplete
    }, ${bagData.postcode || ''} ${bagData.provincie}`.replace('  ', ' ');
    return {
      bag: { id: bagData.id.split('-')[1], type: 'nummeraanduiding' },
      address: { full: fullAddress },
      geojson: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {},
            geometry: {
              type: 'Point',
              coordinates: [
                bagData.geo_punt.coordinates[0],
                bagData.geo_punt.coordinates[1],
              ],
            },
          },
        ],
      },
    };
  })[0];
};
