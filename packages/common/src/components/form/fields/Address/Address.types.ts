// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type AddressValueType = {
  bag: {
    type: 'nummeraanduiding';
    id: string;
  };
  address: { full: string };
  geojson: {
    type: 'FeatureCollection';
    features: [
      {
        type: 'Feature';
        properties: {};
        geometry: {
          type: 'Point';
          coordinates: [number, number];
        };
      }
    ];
  };
};
