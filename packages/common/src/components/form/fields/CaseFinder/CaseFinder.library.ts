// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICommunication } from '@zaaksysteem/generated';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';

type CaseStatusType = 'new' | 'open' | 'stalled' | 'resolved';

export const fetchCaseChoices = (
  t: i18next.TFunction,
  onError: OpenServerErrorDialogType
) => async (
  input: string,
  statusFilter: CaseStatusType[]
): Promise<ValueType<string>[]> => {
  const body = await request<APICommunication.SearchCaseResponseBody>(
    'GET',
    buildUrl<APICommunication.SearchCaseRequestParams>(
      `/api/v2/communication/search_case`,
      {
        ...(statusFilter ? { filter: { status: statusFilter } } : {}),
        search_term: input,
        minimum_permission: 'read',
        limit: 25,
      }
    )
  ).catch(onError);

  return body
    ? body.data.map(
        ({
          id,
          attributes: { display_id, case_type_name, status, description },
        }) => {
          const label = `${display_id}: ${case_type_name}`;
          const subLabel = [t(`case.status.${status}`), description]
            .filter(item => item)
            .join(' - ');

          return {
            value: id,
            label,
            subLabel,
          };
        }
      )
    : [];
};
