// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classnames from 'classnames';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import { Select } from '@mintlab/ui/App/Zaaksysteem/Select';
import useServerErrorDialog, {
  OpenServerErrorDialogType,
} from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APIDocument } from '@zaaksysteem/generated';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { FormFieldComponentType } from '../../types/Form2.types';
import { useCaseDocumentFinderStyle } from './CaseDocumentFinder.style';

const fetchCaseDocumentChoices = (onError: OpenServerErrorDialogType) => async (
  caseUuid: string
) => {
  const body = await request<APIDocument.SearchDocumentResponseBody>(
    'GET',
    buildUrl<APIDocument.SearchDocumentRequestParams>(
      `/api/v2/documents/search_document`,
      {
        case_uuid: caseUuid,
      }
    )
  ).catch(onError);

  return body
    ? body.data
        .map(document => ({
          value: document.relationships.file.data.id,
          label: document.attributes.name,
        }))
        .sort((docA, docB) => docA.label.localeCompare(docB.label))
    : [];
};

const CaseDocumentFinder: FormFieldComponentType<
  ValueType<string>,
  { caseUuid: string }
> = ({ multiValue = true, config: { caseUuid }, ...restProps }) => {
  const classes = useCaseDocumentFinderStyle();
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  return (
    <React.Fragment>
      <DataProvider
        providerArguments={[caseUuid]}
        autoProvide={true}
        provider={fetchCaseDocumentChoices(openServerErrorDialog)}
      >
        {({ data, busy }) => {
          return (
            <div
              className={classnames(
                restProps.applyBackgroundColor && classes.withBackground
              )}
            >
              <Select
                {...restProps}
                choices={data || []}
                isClearable={true}
                loading={busy}
                isMulti={multiValue}
              />
            </div>
          );
        }}
      </DataProvider>
      {ServerErrorDialog}
    </React.Fragment>
  );
};

export default CaseDocumentFinder;
