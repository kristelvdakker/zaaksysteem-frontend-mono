// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxConstants } from '../../library/redux/ajax/createAjaxConstants';

export const SESSION_FETCH = createAjaxConstants('SESSION_FETCH');
export const SESSION_AUTH_LOGIN = 'SESSION:AUTH:LOGIN';
export const SESSION_AUTH_LOGOUT = 'SESSION:AUTH:LOGOUT';
