// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Middleware } from 'redux';
import { showDialog } from '@zaaksysteem/common/src/store/ui/dialog/dialog.actions';

/**
 * Handles request errors in a generic way.
 *
 * If the HTTP statuscode is 401, we redirect.
 *
 * If the response from the backend contains an error
 * array, we dispatch a Dialog action with an array of
 * translation keys, so we can display the most specific
 * error message possible.
 *
 * Else, we dispatch the dialog with a more generalized
 * translation key, based on the HTTP statuscode.
 *
 * If any of these keys cannot be found in the locale
 * file, the fallback key will be used.
 */
export interface ErrorMiddlewareOptionsType {
  errorDialogType: string;
  handleAuthenticationError: () => void;
}

export const errorMiddleware = ({
  errorDialogType,
  handleAuthenticationError,
}: ErrorMiddlewareOptionsType): Middleware => store => next => action => {
  const { dispatch } = store;

  const ajax = action?.ajax;
  const error = action?.error;

  const dispatchDialog = (errorCode: string) =>
    dispatch(
      showDialog({
        errorCode,
        dialogType: errorDialogType,
      })
    );

  if (ajax && error) {
    const { statusCode } = action;
    const { response } = action.payload;
    const { errors } = response;

    if (statusCode === 401) {
      handleAuthenticationError();
      return next(action);
    }

    if (errors) {
      const [firstError] = errors;
      const code = firstError.errorCode || firstError.code;
      dispatchDialog(code);

      return next(action);
    }

    dispatchDialog(statusCode);
  }

  return next(action);
};

export default errorMiddleware;
