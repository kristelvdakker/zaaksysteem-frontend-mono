// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { V2ServerErrorType } from '../../types/ServerError';
import { fetch } from './fetch';

type DataType = 'json' | 'formdata';
type MethodType = 'GET' | 'POST' | 'PUT' | 'DELETE';

export const request = async <T = any>(
  method: MethodType,
  url: string,
  body?: any,
  options: {
    type: DataType;
    cached: boolean;
  } = {
    type: 'json',
    cached: false,
  }
) => {
  const tryJSON = async (response: Response) => {
    try {
      return await response.json();
    } catch (error) {
      throw {
        response,
        errors: [{ code: 'invalid/syntax' }],
      };
    }
  };

  const handleError = async (
    response: Response
  ): Promise<V2ServerErrorType> => {
    const json = await tryJSON(response);

    if (/api\/v2/.test(response.url)) {
      throw {
        response,
        ...(json.errors && { errors: json.errors }),
      };
    } else {
      throw {
        response,
        ...(json.code && { errors: [{ code: json.code }] }),
        ...(json.result && { result: json.result }),
      };
    }
  };

  const response = await fetch(
    method,
    url,
    body,
    options.type,
    options.cached
  ).catch(handleError);
  const json: T = await tryJSON(response);

  return json;
};
