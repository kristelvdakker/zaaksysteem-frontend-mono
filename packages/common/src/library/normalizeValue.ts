// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { FlatFormValue } from '../components/form/types/formDefinition.types';

const hasNoValue = (value: any) =>
  typeof value === 'undefined' ||
  value === null ||
  JSON.stringify(value) === '{}';

export function normalizeValue(value: unknown): FlatFormValue {
  const normalizedValue =
    value && Object.prototype.hasOwnProperty.call(value, 'value')
      ? (value as any).value
      : value;

  return hasNoValue(normalizedValue) ? '' : normalizedValue;
}
