// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Reducer } from 'redux';
import { AjaxState, AJAX_STATE_INIT } from './createAjaxConstants';
import { AjaxAction } from './createAjaxAction';
export * from '@zaaksysteem/common/src/types/ActionWithPayload';

export interface AjaxStateShape {
  state: AjaxState;
}

type PartialAjaxState<S> = Omit<S, 'state'>;

export function makeInitialAjaxState<S>(data: PartialAjaxState<S>) {
  return {
    state: AJAX_STATE_INIT as AjaxState,
    ...data,
  };
}

export type StateFromReducer<T> = T extends Reducer<infer X, any> ? X : never;

export type AjaxResponseHandler<T, V> = (
  state: T & AjaxStateShape,
  action: AjaxAction<V>
) => T & AjaxStateShape;
