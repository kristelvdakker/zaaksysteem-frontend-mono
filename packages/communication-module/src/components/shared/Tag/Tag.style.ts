// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useTagStyle = makeStyles(
  ({ breakpoints, mintlab: { shadows } }: Theme) => ({
    wrapper: {
      backgroundColor: '#F5F7F7',
      color: '#637081',
      boxShadow: shadows.flat,
      fontWeight: 'bold',
      padding: '3px 6px',
      width: 'min-content',
      fontSize: 10,
      textTransform: 'uppercase',
      borderRadius: 18,
      whiteSpace: 'nowrap',
      [breakpoints.up('sm')]: {
        fontSize: 12,
        padding: '4px 8px',
      },
    },
  })
);
