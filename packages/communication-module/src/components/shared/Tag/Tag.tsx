// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { useTagStyle } from './Tag.style';

type TagPropsType = {
  style?: any;
};

const Tag: React.FunctionComponent<TagPropsType> = ({ children, style }) => {
  const classes = useTagStyle();
  return <span className={classNames(classes.wrapper, style)}>{children}</span>;
};

export default Tag;
