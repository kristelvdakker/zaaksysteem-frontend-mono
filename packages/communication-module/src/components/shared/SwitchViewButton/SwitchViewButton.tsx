// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Button } from '@mintlab/ui/App/Material/Button';
import { useSwitchViewButtonStyle } from './SwitchViewButton.style';

type SwitchViewButtonPropsType = {
  to: string;
};

const SwitchViewButton: React.FunctionComponent<SwitchViewButtonPropsType> = ({
  to,
}) => {
  const classes = useSwitchViewButtonStyle();
  const [t] = useTranslation('communication');
  const LinkComponent = React.forwardRef<HTMLAnchorElement>((props, ref) => (
    <Link to={to} innerRef={ref} {...props} />
  ));
  LinkComponent.displayName = 'SwitchViewButton';

  return (
    <div className={classes.wrapper}>
      <Button
        component={LinkComponent}
        presets={['default', 'medium']}
        icon="arrow_back"
      >
        {t('navigation.back')}
      </Button>
    </div>
  );
};

export default SwitchViewButton;
