// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import { preventDefaultAndCall } from '@mintlab/kitchen-sink/source';

type TopLinkProps = {
  style: string;
  href: string;
};

const handleClick = (event: any) => {
  preventDefaultAndCall(() => window.top.location.assign(event.target.href))(
    event
  );
};

const TopLink: React.FunctionComponent<TopLinkProps> = ({
  style,
  href,
  children,
}) => (
  <a className={style} onClick={handleClick} href={href}>
    {children}
  </a>
);

export default TopLink;
