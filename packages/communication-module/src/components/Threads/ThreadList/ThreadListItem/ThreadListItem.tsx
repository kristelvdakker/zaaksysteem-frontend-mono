// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { ReactElement } from 'react';
import classnames from 'classnames';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { Link } from 'react-router-dom';
import fecha from 'fecha';
import { useTranslation } from 'react-i18next';
import { addScopeAttribute } from '@mintlab/ui/App/library/addScope';
import { Render } from '@mintlab/ui/App/Abstract/Render';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Tag from '../../../shared/Tag/Tag';
import { useThreadListItemStyle } from './ThreadListItem.style';
import DirectionIcon from './DirectionIcon/DirectionIcon';

export type ThreadListItemPropsType = {
  id: string;
  summary: string;
  style: any;
  caseNumber?: number;
  selected: boolean;
  icon: ReactElement;
  title: string;
  subTitle?: string;
  typeTag?: string;
  hasAttachment?: boolean;
  direction?: 'root' | 'incoming' | 'outgoing';
  rootPath: string;
  date: Date;
  isUnread: boolean;
  scope?: string;
  showLinkToCase: boolean;
  numberOfMessages: number;
  isFailed?: boolean;
};

/* eslint complexity: [2, 8] */
const ThreadListItem: React.FunctionComponent<ThreadListItemPropsType> = props => {
  const {
    id,
    summary,
    style,
    caseNumber,
    selected,
    icon,
    title,
    subTitle,
    typeTag,
    hasAttachment,
    rootPath,
    direction,
    date,
    showLinkToCase,
    scope,
    numberOfMessages,
    isUnread,
    isFailed,
  } = props;
  const classes = useThreadListItemStyle(props);
  const [t] = useTranslation('communication');
  const caseNumberTag =
    showLinkToCase && caseNumber
      ? `${t('thread.tags.case')} ${caseNumber}`
      : undefined;
  const tags = [typeTag, caseNumberTag].filter(tag => tag);

  return (
    <Link
      to={`${rootPath}/view/${id}`}
      style={style}
      className={classnames(classes.wrapper, {
        [classes.selected]: selected,
        [classes.unread]: isUnread,
        [classes.failed]: isFailed,
      })}
      {...addScopeAttribute(scope, 'link', id)}
    >
      <div className={classes.type}>{icon}</div>
      <div className={classes.content}>
        <div className={classes.titleWrapper}>
          <p
            className={classnames(
              classes.contentTitle,
              classes.ellipsis,
              isUnread && classes.contentTitleUnread
            )}
          >
            {title}
          </p>
          {numberOfMessages > 1 && (
            <p className={classes.count}>
              <Tooltip
                title={t('thread.counterTooltip', {
                  count: numberOfMessages,
                })}
                placement="bottom"
              >
                <React.Fragment>{numberOfMessages}</React.Fragment>
              </Tooltip>
            </p>
          )}
        </div>
        {subTitle && (
          <p className={classnames(classes.contentSubTitle, classes.ellipsis)}>
            {subTitle}
          </p>
        )}
        <p className={classnames(classes.contentSummary, classes.ellipsis)}>
          {summary}
        </p>
      </div>
      <div className={classes.info}>
        <div className={classes.infoTop}>
          {hasAttachment && <Icon size="small">{iconNames.attachment}</Icon>}
          {direction && <DirectionIcon direction={direction} />}
          {<span>{fecha.format(date, t('thread.dateFormat'))}</span>}
        </div>
        <Render condition={Boolean(tags.length)}>
          <div className={classes.tags}>
            {tags.map(tag => (
              <Tag key={tag}>{tag}</Tag>
            ))}
          </div>
        </Render>
      </div>
    </Link>
  );
};

export default ThreadListItem;
