// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import withWidth, { WithWidth } from '@material-ui/core/withWidth';

export interface ThreadTypeIconPropsType extends WithWidth {
  type: string;
}

const ThreadTypeIcon: React.ComponentType<ThreadTypeIconPropsType> = ({
  type,
  width,
}) => {
  const iconSize = width === 'xs' ? 26 : 32;
  return <ZsIcon size={iconSize}>{type}</ZsIcon>;
};

export default withWidth()(ThreadTypeIcon);
