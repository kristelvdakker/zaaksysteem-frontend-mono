// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Button from '@mintlab/ui/App/Material/Button';
import TextField from '@mintlab/ui/App/Material/TextField';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import FlatValueSelect from '@zaaksysteem/common/src/components/form/fields/FlatValueSelect';
import {
  addScopeProp,
  addScopeAttribute,
  // @ts-ignore
} from '@mintlab/ui/App/library/addScope';
import ActionMenu, {
  ActionGroupsType,
} from '../../shared/ActionMenu/ActionMenu';
import { useThreadsHeaderStyle } from './ThreadsHeader.style';

export interface ThreadsHeaderPropsType {
  onFilterChange: (filter: string) => void;
  onSearchTermChange: (searchTerm: string) => void;
  onImportMessage: () => void;
  searchTerm: string;
  filter: string;
  rootPath: string;
  canCreate: boolean;
  canFilter: boolean;
  filterOptions: string[];
  canImportMessage: boolean;
  defaultCreateType: string;
}

/* eslint complexity: [2, 9] */
const ThreadsHeader: React.ComponentType<ThreadsHeaderPropsType> = ({
  rootPath,
  canCreate,
  canFilter,
  filter,
  searchTerm,
  onFilterChange,
  onSearchTermChange,
  filterOptions,
  canImportMessage,
  defaultCreateType,
  onImportMessage,
}) => {
  const classes = useThreadsHeaderStyle();
  const [t] = useTranslation(['communication']);
  const filterChoices = filterOptions.sort().map(type => ({
    label: t(`threadTypes.${type}`),
    value: type,
  }));
  const addThreadPath = `${rootPath}/new/${defaultCreateType}`;
  const LinkComponent = React.forwardRef<HTMLAnchorElement>(
    ({ children, ...restProps }, ref) => (
      <Link innerRef={ref} {...restProps} to={addThreadPath}>
        {children}
      </Link>
    )
  );
  LinkComponent.displayName = 'AddButtonLink';

  const actionButtons: ActionGroupsType = [
    [
      {
        action: onImportMessage,
        label: t('threads.actions.import'),
        condition: canImportMessage,
      },
    ],
  ];

  const clearTextField = () => onSearchTermChange('');

  const handleSearchTermChange = (event: React.ChangeEvent<HTMLInputElement>) =>
    onSearchTermChange(event.currentTarget.value);

  const handleFilterChange = (event: React.ChangeEvent<HTMLSelectElement>) =>
    onFilterChange(event.target.value);

  const displayFilter = canFilter && filterChoices.length > 1;
  const displayActionWrapper =
    canCreate || displayFilter || actionButtons.length > 0;

  return (
    <div className={classes.wrapper}>
      {displayActionWrapper && (
        <div className={classes.actionsWrapper}>
          {canCreate && (
            <div className={classes.buttonActionWrapper}>
              <Button
                component={LinkComponent}
                presets={['primary', 'medium']}
                icon="add"
                {...addScopeProp('add-thread')}
              >
                {t('forms.new')}
              </Button>
            </div>
          )}
          <div
            className={classes.filterActionWrapper}
            {...addScopeAttribute('filter-thread')}
          >
            {displayFilter && (
              <FlatValueSelect
                generic={true}
                value={filter}
                isClearable={true}
                isSearchable={false}
                placeholder="Filter"
                choices={filterChoices}
                onChange={handleFilterChange}
              />
            )}
          </div>
          {actionButtons.length > 0 && (
            <ActionMenu actions={actionButtons} scope={'threadList:header'} />
          )}
        </div>
      )}
      <div>
        <TextField
          variant="generic2"
          startAdornment={
            <div className={classes.searchIcon}>
              <Icon size="small" color="inherit">
                {iconNames.search}
              </Icon>
            </div>
          }
          placeholder={t('thread.searchPlaceholder')}
          onChange={handleSearchTermChange}
          value={searchTerm}
          {...(searchTerm && { closeAction: clearTextField })}
          {...addScopeProp('search-thread')}
        />
      </div>
    </div>
  );
};

export default ThreadsHeader;
