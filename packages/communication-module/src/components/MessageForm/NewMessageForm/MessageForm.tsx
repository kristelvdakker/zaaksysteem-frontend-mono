// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { RouteComponentProps } from 'react-router';
import PlusButtonSpaceWrapper from '@zaaksysteem/common/src/components/PlusButtonSpaceWrapper/PlusButtonSpaceWrapper';
//@ts-ignore
import ErrorBoundary from '@zaaksysteem/common/src/components/ErrorBoundary/ErrorBoundary';
import {
  TYPE_CONTACT_MOMENT,
  TYPE_NOTE,
  TYPE_EXTERNAL_MESSAGE,
} from '../../../library/communicationTypes.constants';
import SwitchViewButton from '../../shared/SwitchViewButton/SwitchViewButton';
import { TypeOfMessage, SubTypeOfMessage } from '../../../types/Message.types';
import ScrollWrapper from '../../shared/ScrollWrapper/ScrollWrapper';
import ContactMomentContainer from './ContactMoment/ContactMomentContainer';
import NoteContainer from './Note/NoteContainer';
import ExternalMessage from './ExternalMessage/ExternalMessage';
import { useMessageFormStyle } from './MessageForm.style';
import TabsContainer from './Tabs/TabsContainer';
import Tabs from './Tabs/Tabs';

const components = {
  [TYPE_CONTACT_MOMENT]: ContactMomentContainer,
  [TYPE_NOTE]: NoteContainer,
  [TYPE_EXTERNAL_MESSAGE]: ExternalMessage,
};

export interface MessageFormPropsType
  extends RouteComponentProps<{
    type: TypeOfMessage;
    subtype?: SubTypeOfMessage;
  }> {
  classes: any;
  width: string;
  rootPath: string;
  alwaysShowBackButton: boolean;
  canCreate: boolean;
  defaultSubType?: 'email' | 'pip';
  navigateTo: (url: string) => void;
}

const AddForm: React.FunctionComponent<MessageFormPropsType> = ({
  match,
  rootPath,
  width,
  alwaysShowBackButton,
  canCreate,
  defaultSubType,
  navigateTo,
}) => {
  const classes = useMessageFormStyle();
  const { type, subtype } = match.params;
  const ContainerElement = components[type];
  const showBackButton = alwaysShowBackButton || ['xs', 'sm'].includes(width);
  const ConnectedTab = TabsContainer(Tabs);
  const cancel = () => navigateTo(rootPath);

  if (!canCreate) {
    return null;
  }

  return (
    <React.Fragment>
      {showBackButton && <SwitchViewButton to={rootPath} />}
      <ScrollWrapper>
        <div className={classes.addFormWrapper}>
          <ConnectedTab rootPath={rootPath} type={type} />
          <ErrorBoundary>
            <PlusButtonSpaceWrapper>
              <ContainerElement
                cancel={cancel}
                rootPath={rootPath}
                type={type}
                subtype={subtype || defaultSubType}
              />
            </PlusButtonSpaceWrapper>
          </ErrorBoundary>
        </div>
      </ScrollWrapper>
    </React.Fragment>
  );
};

export default AddForm;
