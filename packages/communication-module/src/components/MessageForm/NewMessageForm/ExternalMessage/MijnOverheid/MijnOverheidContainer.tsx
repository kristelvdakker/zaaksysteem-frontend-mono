// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AJAX_STATE_PENDING } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import GenericMessageForm from '../../../GenericMessageForm/GenericMessageForm';
import { saveAction } from '../../../../../store/add/communication.add.actions';
import { CommunicationRootStateType } from '../../../../../store/communication.reducer';
import { GenericMessageFormPropsType } from '../../../GenericMessageForm/GenericMessageForm.types';
import mijnOverheidFormDefinition from './mijnOverheid.formDefinition';

type PropsFromStateType = Pick<
  GenericMessageFormPropsType,
  'formDefinition' | 'busy' | 'formName' | 'primaryButtonLabelKey'
>;

const mapStateToProps = (
  stateProps: CommunicationRootStateType
): PropsFromStateType => {
  const {
    communication: {
      add: { state },
      context: {
        capabilities: { canSelectCase },
        caseUuid = '',
        contactUuid,
      },
    },
  } = stateProps;

  const formDefinition = mijnOverheidFormDefinition.map(entry => {
    if (entry.name === 'case_uuid') {
      return canSelectCase
        ? {
            ...entry,
            contactUuid,
          }
        : {
            ...entry,
            value: caseUuid,
            hidden: true,
          };
    }
    return entry;
  });

  return {
    formDefinition,
    busy: state === AJAX_STATE_PENDING,
    formName: 'mijn-overheid',
    primaryButtonLabelKey: 'forms.send',
  };
};

type PropsFromDispatch = Pick<GenericMessageFormPropsType, 'save'>;

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatch => {
  return {
    save(payload) {
      const action = saveAction(payload);
      if (action !== null) {
        dispatch(action as any);
      }
    },
  };
};

export default connect<
  PropsFromStateType,
  PropsFromDispatch,
  Pick<GenericMessageFormPropsType, 'cancel'>,
  CommunicationRootStateType
>(
  mapStateToProps,
  mapDispatchToProps
)(GenericMessageForm);
