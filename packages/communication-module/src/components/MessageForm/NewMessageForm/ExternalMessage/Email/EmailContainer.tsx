// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AJAX_STATE_PENDING } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import {
  EmailExternalMessageForm,
  EmailExternalMessageFormProps,
} from '../../../EmailExternalMessageForm/EmailExternalMessageForm';
import {
  saveEmailMessage,
  setAddCommunicationPending,
} from '../../../../../store/add/communication.add.actions';
import { CommunicationRootStateType } from '../../../../../store/communication.reducer';
import { SaveMessageFormValuesType } from '../../../../../types/Message.types';
import { mapToParticipant } from '../../../../../store/library/participants';
import { resolveMagicStrings } from '../../../../../library/resolveMagicStrings';

type PropsFromStateType = Pick<
  EmailExternalMessageFormProps<SaveMessageFormValuesType>,
  'busy' | 'contactUuid' | 'caseUuid' | 'enablePreview' | 'htmlEmailTemplate'
>;

type PropsFromDispatchType = Pick<
  EmailExternalMessageFormProps<SaveMessageFormValuesType>,
  'save'
>;

type PropsFromOwnType = Pick<EmailExternalMessageFormProps, 'cancel'>;

const mapStateToProps = (
  stateProps: CommunicationRootStateType
): PropsFromStateType => {
  const {
    communication: {
      add: { state },
      context: { caseUuid, contactUuid, context },
    },
    case: {
      instance: { html_email_template },
    },
  } = stateProps;

  return {
    caseUuid,
    contactUuid,
    htmlEmailTemplate: html_email_template,
    enablePreview: context !== 'pip',
    busy: state === AJAX_STATE_PENDING,
  };
};

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatchType => {
  return {
    async save(values) {
      /*
       * --- START TEMPORARY CODE ---
       * This is temporary until Python can resolve magic string
       */
      dispatch(setAddCommunicationPending());

      const resolvedValues = await resolveMagicStrings<SaveMessageFormValuesType>(
        values,
        values.case_uuid
      );

      /* --- END TEMPORARY CODE --- */

      // Todo: Fix Nullable type in FormValues shape
      const { cc, to, bcc, ...restValues } = resolvedValues as any;
      const action = saveEmailMessage({
        ...restValues,
        message_type: 'email',
        to: to.map(mapToParticipant),
        cc: cc.map(mapToParticipant),
        bcc: bcc.map(mapToParticipant),
      });

      if (action !== null) {
        dispatch(action as any);
      }
    },
  };
};

export default connect<
  PropsFromStateType,
  PropsFromDispatchType,
  PropsFromOwnType,
  CommunicationRootStateType
>(
  mapStateToProps,
  mapDispatchToProps
)(EmailExternalMessageForm);
