// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';

import {
  TYPE_PIP_MESSAGE,
  TYPE_EMAIL,
  TYPE_MIJN_OVERHEID,
} from '../../../../library/communicationTypes.constants';
import TabsContainer from '../Tabs/TabsContainer';
import { SubTypeOfMessage } from '../../../../types/Message.types';
import { GenericMessageFormPropsType } from '../../GenericMessageForm/GenericMessageForm.types';
import PipMessageContainer from './Pip/PipContainer';
import EmailContainer from './Email/EmailContainer';
import MessageTabs from './MessageTabs/MessageTabs';
import MijnOverheidContainer from './MijnOverheid/MijnOverheidContainer';

export interface MessagePropsType
  extends Pick<GenericMessageFormPropsType, 'cancel'> {
  rootPath: string;
  subtype?: SubTypeOfMessage;
  [key: string]: any;
}

const ExternalMessage: React.ComponentType<MessagePropsType> = props => {
  const subtype = props.subtype || TYPE_PIP_MESSAGE;
  const { rootPath } = props;
  const components = {
    [TYPE_PIP_MESSAGE]: PipMessageContainer,
    [TYPE_EMAIL]: EmailContainer,
    [TYPE_MIJN_OVERHEID]: MijnOverheidContainer,
  };
  const ContainerElement = components[subtype];
  const ConnectedTabs = TabsContainer(MessageTabs);
  const tabs = <ConnectedTabs rootPath={rootPath} subtype={subtype} />;

  return (
    <React.Fragment>
      <ContainerElement {...props} top={tabs} />
    </React.Fragment>
  );
};

export default ExternalMessage;
