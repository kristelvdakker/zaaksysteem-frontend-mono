// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { CommunicationRootStateType } from '../../../store/communication.reducer';
import { ExternalMessageType } from '../../../types/Message.types';
import ReplyForm, { ReplyFormPropsType } from './ReplyMessageForm';

type PropsFromStateType = Pick<ReplyFormPropsType, 'type'>;

const mapStateToProps = ({
  communication: {
    thread: { messages },
  },
}: CommunicationRootStateType): PropsFromStateType => {
  if (!messages) {
    return {
      type: 'pip',
    };
  }

  const lastMessage = messages[messages.length - 1] as ExternalMessageType;
  return {
    type: lastMessage.messageType,
  };
};

export default connect<PropsFromStateType, {}, {}, CommunicationRootStateType>(
  mapStateToProps
)(ReplyForm);
