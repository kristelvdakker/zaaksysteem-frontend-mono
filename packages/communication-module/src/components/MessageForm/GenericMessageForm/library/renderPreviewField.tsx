// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { FormRendererFormField } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { flattenField } from '@zaaksysteem/common/src/components/form/library/formHelpers';
import { Body1 } from '@mintlab/ui/App/Material/Typography';
import { Render } from '@mintlab/ui/App/Abstract/Render';
import { GenericMessageFormPropsType } from '../GenericMessageForm.types';

type RenderFieldPropsType = Pick<GenericMessageFormPropsType, 'formName'> & {
  classes: any;
};

const getPlainTextValue = (value: unknown) => {
  if (!value || (Array.isArray(value) && value.length === 0)) {
    return '-';
  }

  return Array.isArray(value)
    ? value.map(item => flattenField(item)).join(', ')
    : flattenField(value);
};

export function renderPreviewField<Values>({ classes }: RenderFieldPropsType) {
  return function RenderFieldComponent({
    name,
    error,
    value,
    touched,
  }: FormRendererFormField<Values>) {
    return (
      <div key={name} className={classes.formRow}>
        <Body1 classes={{ root: classes.formValue }}>
          {getPlainTextValue(value)}
        </Body1>
        <Render condition={Boolean(touched) && Boolean(error)}>
          <div className={classes.error}>{error}</div>
        </Render>
      </div>
    );
  };
}
