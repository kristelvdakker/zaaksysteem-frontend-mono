// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classnames from 'classnames';
import { FormRendererFormField } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import { Render } from '@mintlab/ui/App/Abstract/Render';
import {
  CONTACT_FINDER,
  CASE_FINDER,
  CASE_SELECTOR,
} from '../../../../library/communicationFieldTypes.constants';
import { GenericMessageFormPropsType } from '../GenericMessageForm.types';

type RenderFieldPropsType = Pick<GenericMessageFormPropsType, 'formName'> & {
  classes: any;
};

export const renderField = ({ classes, formName }: RenderFieldPropsType) => {
  return function RenderFieldComponent({
    FieldComponent,
    name,
    error,
    touched,
    value,
    enablePreview = false,
    ...rest
  }: FormRendererFormField) {
    const props = {
      ...cloneWithout(rest, 'type', 'mode', 'classes'),
      compact: true,
      name,
      value,
      key: `${formName}-component-${name}`,
      scope: `${formName}-component-${name}`,
    };

    return (
      <div
        className={classnames(classes.formRow, {
          [classes.formRowBackground]:
            !rest.readOnly &&
            (rest.type === CONTACT_FINDER ||
              rest.type === CASE_FINDER ||
              rest.type === CASE_SELECTOR),
        })}
        key={props.key}
      >
        <FieldComponent {...props} error={error} />
        <Render condition={Boolean(touched) && Boolean(error)}>
          <div className={classes.error}>{error}</div>
        </Render>
      </div>
    );
  };
};
