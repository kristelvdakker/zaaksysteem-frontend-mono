// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect } from 'react';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import {
  FormRendererFormField,
  FormValuesType,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { resolveMagicStrings } from '../../../library/resolveMagicStrings';
import { useGenericMessageFormStyle } from './GenericMessageForm.style';
import { renderPreviewField } from './library/renderPreviewField';
import { GenericMessageFormPropsType } from './GenericMessageForm.types';

interface GenericMessageFormPreviewPropsType<Values = any>
  extends Pick<GenericMessageFormPropsType, 'mapPreviewValues'> {
  fields: FormRendererFormField<Values>[];
  values: FormValuesType<Values>;
  caseUuid: string;
}

export function GenericMessageFormPreview<Values = any>({
  values,
  fields,
  caseUuid,
  mapPreviewValues,
}: React.PropsWithChildren<
  GenericMessageFormPreviewPropsType<Values>
>): React.ReactElement {
  const classes = useGenericMessageFormStyle();
  const [
    resolvedValues,
    setResolvedValues,
  ] = useState<FormValuesType<Values> | null>(null);

  useEffect(() => {
    async function getPreviewValues() {
      const resolvedValues = await resolveMagicStrings(values, caseUuid);
      const mappedValues = mapPreviewValues
        ? mapPreviewValues(resolvedValues)
        : resolvedValues;
      setResolvedValues(mappedValues);
    }

    getPreviewValues();
  }, []);

  if (!resolvedValues) {
    return <Loader />;
  }

  const previewFields = fields.map(item => ({
    ...item,
    disabled: true,
    value: resolvedValues[item.name],
  }));

  return (
    <React.Fragment>
      {previewFields.map(
        renderPreviewField<Values>({ classes, formName: '' })
      )}
    </React.Fragment>
  );
}

export default GenericMessageFormPreview;
