// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { NestedFormValue } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import mapValuesToFormDefinition from '@zaaksysteem/common/src/components/form/library/mapValuesToFormDefinition';
import {
  transferValueAsConfig,
  Rule,
} from '@zaaksysteem/common/src/components/form/rules';
import GenericMessageForm from '../GenericMessageForm/GenericMessageForm';
import { SaveMessageFormValuesType } from '../../../types/Message.types';
import { GenericMessageFormPropsType } from '../GenericMessageForm/GenericMessageForm.types';
import { getEmailFormDefinition } from './emailExternalMessageForm.formDefinition';

export type EmailExternalMessageFormProps<
  Values = SaveMessageFormValuesType
> = Pick<
  GenericMessageFormPropsType<Values>,
  'save' | 'busy' | 'cancel' | 'top' | 'enablePreview'
> & {
  caseUuid?: string;
  htmlEmailTemplate?: string;
  contactUuid?: string;
  initialValues?: Partial<SaveMessageFormValuesType>;
  selectedRecipientType?: string;
};

const flattenEmail = (item: NestedFormValue): string => {
  if (item.label && item.label !== item.value) {
    return `${item.label} <${item.value}>`;
  }

  return item.value.toString();
};

const mapPreviewValues = (
  values: SaveMessageFormValuesType
): Omit<SaveMessageFormValuesType, 'to' | 'cc' | 'bcc'> & {
  to: string[];
  cc: string[];
  bcc: string[];
} => {
  const mappedValues = {
    ...values,
    to: values.to.map(flattenEmail),
    cc: values.cc.map(flattenEmail),
    bcc: values.bcc.map(flattenEmail),
  };

  return mappedValues;
};

export const EmailExternalMessageForm: React.ComponentType<EmailExternalMessageFormProps> = props => {
  const {
    caseUuid,
    contactUuid,
    initialValues,
    selectedRecipientType,
    htmlEmailTemplate,
  } = props;
  const [t] = useTranslation('communication');
  const htmlEmailTemplateDescription = htmlEmailTemplate
    ? t('addFields.htmlEmailTemplateDescription', {
        htmlEmailTemplate,
      })
    : undefined;
  const formDefinition = useMemo(() => {
    return getEmailFormDefinition({
      caseUuid,
      contactUuid,
      selectedRecipientType,
      htmlEmailTemplateDescription,
    });
  }, [caseUuid, contactUuid, selectedRecipientType]);
  const formDefinitionWithValues = useMemo(() => {
    return initialValues
      ? mapValuesToFormDefinition<SaveMessageFormValuesType>(
          initialValues,
          formDefinition
        )
      : formDefinition;
  }, []);

  return (
    <GenericMessageForm
      {...props}
      mapPreviewValues={mapPreviewValues}
      primaryButtonLabelKey={t('forms.send')}
      formName="email-form"
      formDefinition={formDefinitionWithValues}
      rules={[
        new Rule()
          .when(() => true)
          .then(transferValueAsConfig('case_uuid', 'to', 'caseUuid')),
      ]}
    />
  );
};
