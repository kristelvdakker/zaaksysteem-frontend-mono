// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { useTranslation } from 'react-i18next';
import { FormikValues } from 'formik';
import { AddThreadToCaseValuesType } from '../../../types/Thread.types';

export type AddThreadToCaseDialogPropsType = {
  onSubmit: (caseUuid: string, threadUuid: string) => void;
  [key: string]: any;
};

const AddThreadToCaseDialog: React.FunctionComponent<AddThreadToCaseDialogPropsType> = ({
  threadUuid,
  onSubmit,
  onClose,
}) => {
  const [t] = useTranslation('communication');

  const formDefinition: FormDefinition<AddThreadToCaseValuesType> = [
    {
      name: 'case_uuid',
      type: fieldTypes.CASE_FINDER,
      value: null,
      required: true,
      filter: {
        status: ['new', 'open', 'stalled'],
      },
      placeholder: t('dialogs.addThreadToCase.select'),
      label: t('dialogs.addThreadToCase.select'),
    },
  ];

  const handleOnSubmit = (values: FormikValues) =>
    onSubmit(values.case_uuid, threadUuid);

  return (
    <FormDialog
      formDefinition={formDefinition}
      onSubmit={handleOnSubmit}
      onClose={onClose}
      title={t('dialogs.addThreadToCase.title')}
      scope="case-select-dialog"
    />
  );
};

export default AddThreadToCaseDialog;
