// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import classnames from 'classnames';
import { Route, Switch } from 'react-router-dom';
import Render from '@mintlab/ui/App/Abstract/Render';
import withWidth from '@material-ui/core/withWidth';
import { useCommunicationStyle } from './Communication.style';
import ThreadsContainer from './Threads/ThreadsContainer';
import ThreadContainer from './Thread/ThreadContainer';
import MessageFormContainer from './MessageForm/NewMessageForm/MessageFormContainer';
import ThreadPlaceholder from './ThreadPlaceholder/ThreadPlaceholder';

export interface CommunicationPropsType {
  width: string;
  rootPath: string;
  allowSplitScreen: boolean;
  requestor?: string;
}

const Communication: React.ComponentType<CommunicationPropsType> = ({
  width,
  rootPath,
  allowSplitScreen,
}) => {
  const [t] = useTranslation('communication');
  const classes = useCommunicationStyle();
  const threadListBreakpoint = allowSplitScreen
    ? ['md', 'lg', 'xl'].includes(width)
    : false;

  return (
    <div
      className={classnames(
        classes.wrapper,
        allowSplitScreen && classes.wrapperResponsive
      )}
    >
      <Route
        path={`${rootPath}/(view|new)?/:identifier?`}
        render={({ match, ...restRouteProps }) => {
          const renderThreadList =
            !match.params.identifier ||
            (match.params.identifier && threadListBreakpoint);

          return (
            <Render condition={renderThreadList}>
              <div className={classes.threadListWrapper}>
                <ThreadsContainer {...restRouteProps} match={match} />
              </div>
            </Render>
          );
        }}
      />

      <Switch>
        <Route
          path={`${rootPath}/new/:type/:subtype?`}
          render={props => (
            <div className={classes.contentOuterWrapper}>
              <MessageFormContainer
                {...props}
                classes={classes}
                alwaysShowBackButton={allowSplitScreen === false}
                width={width}
              />
            </div>
          )}
        />
        <Route
          path={`${rootPath}/view/:threadId`}
          render={props => (
            <div className={classes.contentOuterWrapper}>
              <ThreadContainer {...props} rootPath={rootPath} width={width} />
            </div>
          )}
        />
        <Route
          path={`${rootPath}`}
          render={() => (
            <Render condition={threadListBreakpoint}>
              <div className={classes.contentOuterWrapper}>
                <ThreadPlaceholder>{t('placeholder')}</ThreadPlaceholder>
              </div>
            </Render>
          )}
        />
      </Switch>
    </div>
  );
};

export default withWidth()(Communication);
