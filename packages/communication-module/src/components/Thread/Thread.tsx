// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { RouteComponentProps } from 'react-router';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import PlusButtonSpaceWrapper from '@zaaksysteem/common/src/components/PlusButtonSpaceWrapper/PlusButtonSpaceWrapper';
//@ts-ignore
import ErrorBoundary from '@zaaksysteem/common/src/components/ErrorBoundary/ErrorBoundary';
import { get } from '@mintlab/kitchen-sink/source';
import SwitchViewButton from '../shared/SwitchViewButton/SwitchViewButton';
import {
  TYPE_CONTACT_MOMENT,
  TYPE_NOTE,
  TYPE_PIP_MESSAGE,
  TYPE_EMAIL,
} from '../../library/communicationTypes.constants';
import ReplyFormContainer from '../MessageForm/ReplyMessageForm/ReplyMessageFormContainer';
import { AnyMessageType, ExternalMessageType } from '../../types/Message.types';
import ScrollWrapper from '../shared/ScrollWrapper/ScrollWrapper';
import ThreadPlaceholder from '../ThreadPlaceholder/ThreadPlaceholder';
import { CommunicationContextContextType } from '../../types/Context.types';
import { isMessageUnread } from '../../store/thread/library/isUnread';
import ContactMomentMessage from './Message/ContactMomentMessage';
import NoteMessage from './Message/NoteMessage';
import PipMessageMessage from './Message/PipMessage';
import EmailMessage from './Message/EmailMessage';
import ContactMomentThreadTitle from './ThreadTitle/ContactMomentThreadTitle';
import NoteThreadTitle from './ThreadTitle/NoteThreadTitle';
import PipMessageThreadTitle from './ThreadTitle/PipMessageThreadTitle';
import EmailThreadTitle, {
  EmailThreadTitlePropsType,
} from './ThreadTitle/EmailThreadTitle';
import { useThreadStyles } from './Thread.style';

const messageElement: {
  [key: string]: any;
} = {
  [TYPE_CONTACT_MOMENT]: ContactMomentMessage,
  [TYPE_NOTE]: NoteMessage,
  [TYPE_PIP_MESSAGE]: PipMessageMessage,
  [TYPE_EMAIL]: EmailMessage,
};

const titleElement: {
  [key: string]: any;
} = {
  [TYPE_CONTACT_MOMENT]: ContactMomentThreadTitle,
  [TYPE_NOTE]: NoteThreadTitle,
  [TYPE_PIP_MESSAGE]: PipMessageThreadTitle,
  [TYPE_EMAIL]: EmailThreadTitle,
};

export interface ThreadPropsType
  extends RouteComponentProps<{ threadId: string }>,
    Pick<EmailThreadTitlePropsType, 'canAddThreadToCase'> {
  fetchThread: (threadId: string) => void;
  messages: AnyMessageType[];
  rootPath: string;
  width: string;
  busy: boolean;
  showLinkToCase: boolean;
  caseRelation?: {
    id: string;
    caseNumber: string;
    status: string;
  };
  alwaysShowBackButton: boolean;
  isReplyable: boolean;
  canSendReply: boolean;
  context: CommunicationContextContextType;
  showAddThreadToCaseDialog: (threadUuid: string) => void;
}

/* eslint-disable complexity */
const Thread: React.FunctionComponent<ThreadPropsType> = ({
  context,
  fetchThread,
  messages,
  match,
  rootPath,
  width,
  caseRelation,
  showLinkToCase,
  busy,
  alwaysShowBackButton,
  showAddThreadToCaseDialog,
  canAddThreadToCase,
  isReplyable,
  canSendReply,
}) => {
  const [t] = useTranslation('communication');
  const threadId = match.params.threadId;
  const showBackButton = alwaysShowBackButton || ['xs', 'sm'].includes(width);
  const classes = useThreadStyles();

  useEffect(() => {
    fetchThread(threadId);
  }, [threadId]);

  if (!messages || busy) {
    return <Loader delay={200} />;
  }

  if (messages.length === 0) {
    return <ThreadPlaceholder>{t('thread.noContent')}</ThreadPlaceholder>;
  }

  const [firstMessage] = messages;
  const TitleComponent = (firstMessage as ExternalMessageType).messageType
    ? titleElement[(firstMessage as ExternalMessageType).messageType]
    : titleElement[firstMessage.type];
  const caseNumber = get(caseRelation, 'caseNumber');

  return (
    <ErrorBoundary>
      {showBackButton && <SwitchViewButton to={rootPath} />}
      <ScrollWrapper>
        <div className={classes.threadWrapper}>
          <PlusButtonSpaceWrapper>
            <div className={classes.threadTitle}>
              <TitleComponent
                message={firstMessage}
                context={context}
                caseNumber={caseNumber}
                showLinkToCase={showLinkToCase}
                showAddThreadToCaseDialog={() =>
                  showAddThreadToCaseDialog(threadId)
                }
                canAddThreadToCase={canAddThreadToCase}
              />
            </div>
            <div className={classes.threadItemsWrapper}>
              {messages.map((message, index) => {
                const MessageComponent = (firstMessage as ExternalMessageType)
                  .messageType
                  ? messageElement[
                      (firstMessage as ExternalMessageType).messageType
                    ]
                  : messageElement[firstMessage.type];

                return (
                  <MessageComponent
                    key={message.id}
                    message={message}
                    isUnread={isMessageUnread(context, message)}
                    expanded={index === messages.length - 1}
                  />
                );
              })}
            </div>
            <div className={classes.replyForm}>
              {/* note: caseRelation check is only because typescript doesn't understand
              that canSendReply is only true when there is a caseRelation*/}
              {isReplyable && canSendReply && caseRelation && (
                <ReplyFormContainer caseUuid={caseRelation.id} />
              )}
              {isReplyable && !canSendReply && (
                <span>{t('replyForm.placeholder')}</span>
              )}
            </div>
          </PlusButtonSpaceWrapper>
        </div>
      </ScrollWrapper>
    </ErrorBoundary>
  );
};

export default Thread;
