// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useThreadStyles = makeStyles(
  ({ mintlab: { greyscale }, breakpoints }: Theme) => ({
    threadWrapper: {
      width: '100%',
      maxWidth: 1024,
      margin: '0 auto',
    },
    threadTitle: {
      padding: 20,
      paddingBottom: 20,
      paddingTop: 30,
      [breakpoints.up('sm')]: {
        paddingLeft: 80,
      },
    },
    messageWrapper: {
      marginBottom: '100px',
    },
    replyForm: {
      padding: 20,
      [breakpoints.up('sm')]: {
        padding: 30,
        paddingLeft: 80,
      },
    },
    threadItemsWrapper: {
      padding: 20,
      [breakpoints.up('sm')]: {
        padding: 30,
      },
      '&>*': {
        paddingBottom: 35,
      },
      '&>*:not(:first-child)': {
        paddingTop: 35,
      },
      '&>*:after': {
        content: '""',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        height: 1,
        background: greyscale.dark,
        [breakpoints.up('sm')]: {
          left: 50,
        },
      },
    },
  })
);
