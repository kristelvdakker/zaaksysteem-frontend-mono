// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { RouteComponentProps } from 'react-router';
import { AJAX_STATE_VALID } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { showDialog } from '@zaaksysteem/common/src/store/ui/dialog/dialog.actions';
import { fetchThreadAction } from '../../store/thread/communication.thread.actions';
import { CommunicationRootStateType } from '../../store/communication.reducer';
import { ADD_THREAD_TO_CASE_DIALOG } from '../Dialogs/dialog.constants';
import { ExternalMessageType } from '../../types/Message.types';
import { threadNotRelatedOrRelatedCaseNotResolvedSelector } from '../../store/selectors/threadNotRelatedOrRelatedCaseNotResolvedSelector';
import {
  TYPE_PIP_MESSAGE,
  TYPE_EXTERNAL_MESSAGE,
} from '../../library/communicationTypes.constants';
import Thread, { ThreadPropsType } from './Thread';

type PropsFromStateType = Pick<
  ThreadPropsType,
  | 'messages'
  | 'busy'
  | 'alwaysShowBackButton'
  | 'caseRelation'
  | 'showLinkToCase'
  | 'context'
  | 'canAddThreadToCase'
  | 'canSendReply'
  | 'isReplyable'
>;

/* eslint complexity: [2, 7] */
const mapStateToProps = (
  stateProps: CommunicationRootStateType
): PropsFromStateType => {
  const {
    communication: {
      thread: { messages, state, caseRelation },
      context: {
        capabilities: {
          allowSplitScreen,
          canSelectCase,
          canAddThreadToCase,
          canCreatePipMessage,
          canCreateEmail,
        },
        context,
      },
    },
  } = stateProps;

  if (!messages || messages.length === 0) {
    return {
      context,
      messages: [],
      busy: false,
      alwaysShowBackButton: false,
      showLinkToCase: false,
      canAddThreadToCase: false,
      isReplyable: false,
      canSendReply: false,
    };
  }

  const firstMessage = messages[0];
  const isReplyable = firstMessage.type === TYPE_EXTERNAL_MESSAGE;
  const canReplyOnMessageType =
    (firstMessage as ExternalMessageType).messageType === TYPE_PIP_MESSAGE
      ? canCreatePipMessage
      : canCreateEmail;
  const canSendReply =
    canReplyOnMessageType &&
    Boolean(caseRelation) &&
    threadNotRelatedOrRelatedCaseNotResolvedSelector(stateProps);

  return {
    context,
    messages,
    busy: state !== AJAX_STATE_VALID,
    alwaysShowBackButton: allowSplitScreen === false,
    caseRelation: caseRelation ? caseRelation : undefined,
    showLinkToCase: canSelectCase,
    canAddThreadToCase,
    isReplyable,
    canSendReply,
  };
};

type PropsFromDispatchType = Pick<
  ThreadPropsType,
  'fetchThread' | 'showAddThreadToCaseDialog'
>;

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatchType => ({
  fetchThread(payload: string) {
    const action = fetchThreadAction(payload);

    dispatch(action as any);
  },
  showAddThreadToCaseDialog(threadUuid: string) {
    const action = showDialog({
      dialogType: ADD_THREAD_TO_CASE_DIALOG,
      threadUuid,
    });

    dispatch(action as any);
  },
});

const ThreadContainer = connect<
  PropsFromStateType,
  PropsFromDispatchType,
  RouteComponentProps<any>,
  CommunicationRootStateType
>(
  mapStateToProps,
  mapDispatchToProps
)(Thread);

export default ThreadContainer;
