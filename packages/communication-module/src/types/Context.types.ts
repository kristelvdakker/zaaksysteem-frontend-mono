// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type CommunicationContextCapabilitiesType = {
  allowSplitScreen: boolean;
  canAddAttachmentToCase: boolean;
  canAddSourceFileToCase: boolean;
  canAddThreadToCase: boolean;
  canCreateContactMoment: boolean;
  canCreateNote: boolean;
  canCreatePipMessage: boolean;
  canCreateEmail: boolean;
  canCreateMijnOverheid: boolean;
  canDeleteMessage: boolean;
  canSelectContact: boolean;
  canSelectCase: boolean;
  canImportMessage: boolean;
  canFilter: boolean;
  canOpenPDFPreview: boolean;
};

export type CommunicationContextContextType =
  | 'pip'
  | 'case'
  | 'contact'
  | 'inbox';

export interface CommunicationContextType {
  capabilities: CommunicationContextCapabilitiesType;
  context: CommunicationContextContextType;
  caseUuid?: string;
  contactUuid?: string;
  contactName?: string;
  rootPath: string;
}
