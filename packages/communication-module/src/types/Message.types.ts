// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICommunication } from '@zaaksysteem/generated';
import { NestedFormValue } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';

type Direction = 'incoming' | 'outgoing';

export type TypeOfMessage = 'contact_moment' | 'note' | 'external_message';
export type SubTypeOfMessage = 'pip' | 'email';

type NameObject = {
  name: string;
  type: 'person' | 'organization' | 'employee';
};

export type AttachmentType = {
  download?: {
    url: string;
  };
  preview?: {
    url: string;
    contentType?: string;
  };
  id: string;
  name: string;
  size: number;
};

export type ParticipantType = {
  email: string;
  name: string;
  uuid?: string;
};

export type ExternalMessageParticipantsType = {
  from?: ParticipantType;
  to: ParticipantType[];
  cc: ParticipantType[];
  bcc: ParticipantType[];
};

export interface MessageType {
  id: string;
  type: TypeOfMessage;
  messageType: undefined;
  content: string;
  createdDate: string;
  summary: string;
  threadUuid: string;
  readPip: undefined;
  readEmployee: undefined;
}

export interface ContactMomentMessageType extends MessageType {
  channel: string;
  direction: Direction;
  recipient: NameObject;
  sender: NameObject;
}

export interface NoteMessageType extends MessageType {
  sender: NameObject;
}

export interface ExternalMessageType
  extends Omit<MessageType, 'readEmployee' | 'readPip' | 'messageType'> {
  subject: string;
  attachments: AttachmentType[];
  participants?: ExternalMessageParticipantsType;
  messageType: SubTypeOfMessage;
  sender: NameObject | null;
  hasSourceFile?: boolean;
  readPip: string | null;
  readEmployee: string | null;
  failureReason: string | undefined;
}

export type AnyMessageType =
  | ContactMomentMessageType
  | NoteMessageType
  | ExternalMessageType;

export type CaseChoiceType = {
  value: string;
  label: string;
  subLabel: string | null;
};

export type SaveMessageFormValuesType = {
  thread_uuid?: string;
  content: string;
  subject: string;
  customHtmlTemplate: string;
  case_uuid: string;
  to: NestedFormValue[];
  cc: NestedFormValue[];
  bcc: NestedFormValue[];
  attachments: NestedFormValue | NestedFormValue[];
  requestor: NestedFormValue;
};

export type SaveContactMomentFormValuesType = Pick<
  APICommunication.CreateContactMomentRequestBody,
  'content' | 'channel' | 'direction' | 'case_uuid'
> & { contact_uuid: NestedFormValue };

export type SaveNoteFormValuesType = Pick<
  APICommunication.CreateNoteRequestBody,
  'content'
> &
  ({ case_uuid: string } | { contact_uuid: string });

export type ImportMessageFormFields = { file_uuid: NestedFormValue };
