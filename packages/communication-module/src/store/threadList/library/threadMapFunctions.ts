// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICommunication } from '@zaaksysteem/generated';
import {
  ThreadType,
  AnyLastMessageType,
  LastMessageType,
} from '../../../types/Thread.types';

const getCaseNumber = (thread: APICommunication.MessageThreadEntity) =>
  thread.relationships && thread.relationships.case
    ? thread.relationships.case.data.attributes.display_id
    : undefined;

export const mapThreadProperties = (
  thread: APICommunication.MessageThreadEntity
): ThreadType => {
  const {
    id,
    attributes: { thread_type, last_message },
    meta,
  } = thread;
  const { slug, created } = last_message;
  const {
    number_of_messages,
    unread_employee_count,
    unread_pip_count,
    attachment_count,
  } = meta;

  return {
    id,
    type: thread_type,
    summary: slug,
    date: new Date(created),
    numberOfMessages: number_of_messages,
    tag: '',
    caseNumber: getCaseNumber(thread),
    lastMessage: mapLastMessageType(thread),
    unreadEmployee:
      typeof unread_employee_count !== 'undefined' && unread_employee_count > 0,
    unreadPip: typeof unread_pip_count !== 'undefined' && unread_pip_count > 0,
    hasAttachment: attachment_count > 0,
  };
};

const mapLastMessageType = (
  thread: APICommunication.MessageThreadEntity
): AnyLastMessageType => {
  const lastMessage = thread.attributes.last_message;
  const genericProperties: LastMessageType = {
    createdByName: lastMessage.created_name,
  };
  switch (lastMessage.message_type) {
    case 'contact_moment':
      return {
        ...genericProperties,
        type: lastMessage.message_type,
        channel: lastMessage.channel,
        direction: lastMessage.direction,
        withName: lastMessage.recipient_name,
      };

    case 'email':
      return {
        ...genericProperties,
        type: lastMessage.message_type,
        subject: lastMessage.subject,
        failureReason: lastMessage.failure_reason,
      };

    case 'note':
      return { ...genericProperties, type: lastMessage.message_type };

    case 'pip_message':
      return {
        ...genericProperties,
        type: lastMessage.message_type,
        subject: lastMessage.subject,
      };
  }
};
