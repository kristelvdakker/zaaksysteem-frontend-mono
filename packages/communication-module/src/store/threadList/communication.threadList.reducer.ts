// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { handleAjaxStateChange } from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import { Reducer } from 'redux';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { APICommunication } from '@zaaksysteem/generated';
import { ThreadType } from '../../types/Thread.types';
import { THREAD_LIST_FETCH } from './communication.threadList.constants';
import { mapThreadProperties } from './library/threadMapFunctions';

export interface CommunicationThreadListState {
  state: AjaxState;
  threadList: ThreadType[];
}

const initialState: CommunicationThreadListState = {
  state: AJAX_STATE_INIT,
  threadList: [],
};

const handleFetchSuccess = (
  state: CommunicationThreadListState,
  action: AjaxAction<APICommunication.ThreadListResponseBody>
): CommunicationThreadListState => {
  const { response } = action.payload;

  if (!response.data) {
    return state;
  }

  return {
    ...state,
    threadList: response.data.map<ThreadType>(thread =>
      mapThreadProperties(thread)
    ),
  };
};

export const threadList: Reducer<CommunicationThreadListState, AjaxAction> = (
  state = initialState,
  action
) => {
  const { type } = action;
  const handleFetchThreadListStateChange = handleAjaxStateChange(
    THREAD_LIST_FETCH
  );

  switch (type) {
    case THREAD_LIST_FETCH.PENDING:
    case THREAD_LIST_FETCH.ERROR:
      return handleFetchThreadListStateChange(state, action);
    case THREAD_LIST_FETCH.SUCCESS:
      return handleFetchThreadListStateChange(
        handleFetchSuccess(
          state,
          action as AjaxAction<APICommunication.ThreadListResponseBody>
        ),
        action
      );
    default:
      return state;
  }
};

export default threadList;
