// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { buildUrl, ENCODED_NULL_BYTE } from '@mintlab/kitchen-sink/source';
import { APICommunication } from '@zaaksysteem/generated';
import { ThunkActionWithPayload } from '@zaaksysteem/common/src/types/ActionWithPayload';
import { CommunicationRootStateType } from '../communication.reducer';
import { CommunicationContextContextType } from '../../types/Context.types';
import { THREAD_LIST_FETCH } from './communication.threadList.constants';

const fetchThreadListAjaxAction = createAjaxAction(THREAD_LIST_FETCH);

type GetUrlParametersOptions = {
  context: CommunicationContextContextType;
  caseUuid?: string;
  contactUuid?: string;
};

const getFilterParameters = ({
  caseUuid,
  contactUuid,
  context,
}: GetUrlParametersOptions): APICommunication.ThreadListRequestParams => {
  switch (context) {
    case 'case':
      return {
        filter: {
          case_uuid: caseUuid,
        },
      };

    case 'contact':
      return {
        filter: {
          contact_uuid: contactUuid,
        },
      };

    case 'pip':
      return {};

    case 'inbox':
      return {
        filter: {
          case_uuid: ENCODED_NULL_BYTE,
          contact_uuid: ENCODED_NULL_BYTE,
          message_types: 'external',
        },
      };
  }
};

/**
 * @return {Function}
 */
export const fetchThreadListAction = (): ThunkActionWithPayload<
  CommunicationRootStateType,
  {}
> => (dispatch, getState) => {
  const {
    communication: {
      context: { caseUuid, contactUuid, context },
    },
  } = getState();
  const url = buildUrl<APICommunication.ThreadListRequestParams>(
    '/api/v2/communication/get_thread_list',
    getFilterParameters({
      caseUuid,
      contactUuid,
      context,
    })
  );

  return fetchThreadListAjaxAction({
    url,
    method: 'GET',
  })(dispatch);
};
