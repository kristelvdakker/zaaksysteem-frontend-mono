// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  ExternalMessageParticipantsType,
  ParticipantType,
} from '../../types/Message.types';
import { createReplyParticipants } from './participants';

describe('Participant transformation', () => {
  const participants: ExternalMessageParticipantsType = {
    from: { email: 'from' },
    to: [{ email: 'to' }, { email: 'sender' }],
    cc: [{ email: 'cc' }],
    bcc: [{ email: 'bcc' }],
  };
  const sender: ParticipantType = {
    email: 'sender',
  };

  test('Should remove previous `from` and add it to `to` field', () => {
    const result = createReplyParticipants(participants);
    expect(result.from).toBeUndefined();
    expect(result.to).toContain(participants.from);
  });

  test('Should add sender to `from` field', () => {
    const result = createReplyParticipants(participants, sender);
    expect(result.from).toEqual(sender);
  });

  test('Should remove sender from `to` field', () => {
    const result = createReplyParticipants(participants, sender);
    expect(result.to).toEqual([{ email: 'from' }, { email: 'to' }]);
  });

  test('Should not touch `cc` and `bcc` fields', () => {
    const result = createReplyParticipants(participants, sender);
    expect(result.cc).toEqual(participants.cc);
    expect(result.bcc).toEqual(participants.bcc);
  });
});
