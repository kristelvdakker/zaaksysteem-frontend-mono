// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { CommunicationRootStateType } from '../communication.reducer';
import { messageSelector } from './messageSelector';

export const messageSubtypeSelector = (messageUuid: string) => (
  state: CommunicationRootStateType
) => {
  return messageSelector(messageUuid)(state)?.messageType;
};
