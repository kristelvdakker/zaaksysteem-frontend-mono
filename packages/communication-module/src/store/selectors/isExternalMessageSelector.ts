// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { CommunicationRootStateType } from '../communication.reducer';
import { messageSelector } from './messageSelector';

export const isExternalMessageSelector = (messageUuid: string) => (
  state: CommunicationRootStateType
) => {
  const message = messageSelector(messageUuid)(state);

  if (message) {
    return message?.type === 'external_message';
  } else {
    return false;
  }
};
