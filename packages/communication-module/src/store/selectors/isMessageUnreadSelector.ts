// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { CommunicationRootStateType } from '../communication.reducer';
import { isMessageUnread } from '../thread/library/isUnread';
import { messageSelector } from './messageSelector';

export const isMessageUnreadSelector = (messageUuid: string) => (
  state: CommunicationRootStateType
) => {
  const {
    communication: {
      context: { context },
    },
  } = state;

  const message = messageSelector(messageUuid)(state);

  return isMessageUnread(context, message);
};
