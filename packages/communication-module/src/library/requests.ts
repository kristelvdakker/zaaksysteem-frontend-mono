// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICommunication } from '@zaaksysteem/generated';

export const searchContact = (
  keyword: string
): Promise<APICommunication.SearchContactResponseBody> =>
  request<APICommunication.SearchContactResponseBody>(
    'GET',
    buildUrl<APICommunication.SearchContactRequestParams>(
      `/api/v2/communication/search_contact`,
      {
        keyword,
      }
    )
  ).catch(response => Promise.reject(response));

export const fetchCases = (
  contact_uuid: string
): Promise<APICommunication.GetCaseListForContactResponseBody> =>
  request<APICommunication.GetCaseListForContactResponseBody>(
    'GET',
    buildUrl<APICommunication.GetCaseListForContactRequestParams>(
      `/api/v2/communication/get_case_list_for_contact`,
      {
        contact_uuid,
      }
    )
  ).catch(response => Promise.reject(response));
