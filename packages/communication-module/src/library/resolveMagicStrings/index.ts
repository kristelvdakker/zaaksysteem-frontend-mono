// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { FormValuesType } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { request } from '@zaaksysteem/common/src/library/request/request';
import flattenValues from './flattenValues';
import { FlatFormValues } from './resolveMagicStrings.types';
import { getValuesToResolve } from './getValuesToResolve';
import { restoreNestedValues } from './restoreNestedValues';

async function resolveValues<Values>(
  values: FlatFormValues<Values>,
  caseUuid: string
): Promise<FlatFormValues<Values>> {
  const response = await request('POST', '/api/mail/preview', {
    case_uuid: caseUuid,
    ...values,
  });
  return Object.entries(response).reduce<Partial<FlatFormValues<Values>>>(
    (acc, [name, value]) => ({
      ...acc,
      ...(value ? { [name]: value } : {}),
    }),
    {}
  );
}

export async function resolveMagicStrings<Values = any>(
  values: FormValuesType<Values>,
  caseUuid: string
): Promise<FormValuesType<Values>> {
  // Flatten nested structure to prepare sending it to the preview API
  const flattenedValues = flattenValues<Values>(values);
  // Gets all values that contain a magic string
  const valuesToResolve = getValuesToResolve(flattenedValues);
  // Resolve the new values from the API
  const resolvedValues = await resolveValues(valuesToResolve, caseUuid);
  // Restore the original nested structure
  const restoredValues = restoreNestedValues<Values>(resolvedValues);
  // Add all resolved values to the original values
  const mergedValues = {
    ...values,
    ...restoredValues,
  };

  return mergedValues;
}
