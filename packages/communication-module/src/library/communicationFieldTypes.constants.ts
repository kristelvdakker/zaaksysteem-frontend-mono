// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const CONTACT_FINDER = 'contactFinder';
export const CASE_FINDER = 'caseFinder';
export const CASE_SELECTOR = 'caseSelector';
export const UPLOAD = 'file';
export const EMAIL_RECIPIENT = 'emailRecipient';
