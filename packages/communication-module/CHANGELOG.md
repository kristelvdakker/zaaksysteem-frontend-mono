# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.24.29](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.28...@zaaksysteem/communication-module@1.24.29) (2020-12-21)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.28](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.27...@zaaksysteem/communication-module@1.24.28) (2020-12-18)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.27](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.26...@zaaksysteem/communication-module@1.24.27) (2020-12-18)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.26](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.25...@zaaksysteem/communication-module@1.24.26) (2020-12-17)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.25](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.24...@zaaksysteem/communication-module@1.24.25) (2020-12-10)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.24](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.23...@zaaksysteem/communication-module@1.24.24) (2020-12-04)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.23](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.22...@zaaksysteem/communication-module@1.24.23) (2020-11-25)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.22](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.21...@zaaksysteem/communication-module@1.24.22) (2020-11-20)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.21](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.20...@zaaksysteem/communication-module@1.24.21) (2020-11-19)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.20](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.19...@zaaksysteem/communication-module@1.24.20) (2020-11-18)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.19](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.18...@zaaksysteem/communication-module@1.24.19) (2020-11-11)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.18](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.17...@zaaksysteem/communication-module@1.24.18) (2020-11-11)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.17](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.16...@zaaksysteem/communication-module@1.24.17) (2020-11-11)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.16](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.15...@zaaksysteem/communication-module@1.24.16) (2020-10-30)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.15](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.14...@zaaksysteem/communication-module@1.24.15) (2020-10-26)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.14](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.13...@zaaksysteem/communication-module@1.24.14) (2020-10-26)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.13](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.12...@zaaksysteem/communication-module@1.24.13) (2020-10-15)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.12](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.11...@zaaksysteem/communication-module@1.24.12) (2020-10-15)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.11](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.10...@zaaksysteem/communication-module@1.24.11) (2020-10-15)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.9...@zaaksysteem/communication-module@1.24.10) (2020-10-08)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.8...@zaaksysteem/communication-module@1.24.9) (2020-10-06)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.7...@zaaksysteem/communication-module@1.24.8) (2020-10-02)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.6...@zaaksysteem/communication-module@1.24.7) (2020-09-30)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.5...@zaaksysteem/communication-module@1.24.6) (2020-09-30)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.4...@zaaksysteem/communication-module@1.24.5) (2020-09-29)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.3...@zaaksysteem/communication-module@1.24.4) (2020-09-25)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.2...@zaaksysteem/communication-module@1.24.3) (2020-09-23)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.1...@zaaksysteem/communication-module@1.24.2) (2020-09-18)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.24.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.24.0...@zaaksysteem/communication-module@1.24.1) (2020-09-17)

**Note:** Version bump only for package @zaaksysteem/communication-module





# [1.24.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.47...@zaaksysteem/communication-module@1.24.0) (2020-09-17)


### Features

* **PIP:** MINTY-3636 - Make prefilled case uuids visible as read-only ([f2eba03](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f2eba03))





## [1.23.47](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.46...@zaaksysteem/communication-module@1.23.47) (2020-09-16)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.46](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.45...@zaaksysteem/communication-module@1.23.46) (2020-09-16)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.45](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.44...@zaaksysteem/communication-module@1.23.45) (2020-09-15)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.44](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.43...@zaaksysteem/communication-module@1.23.44) (2020-09-15)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.43](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.42...@zaaksysteem/communication-module@1.23.43) (2020-09-04)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.42](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.41...@zaaksysteem/communication-module@1.23.42) (2020-09-03)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.41](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.40...@zaaksysteem/communication-module@1.23.41) (2020-09-03)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.40](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.39...@zaaksysteem/communication-module@1.23.40) (2020-09-03)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.39](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.38...@zaaksysteem/communication-module@1.23.39) (2020-09-03)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.38](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.37...@zaaksysteem/communication-module@1.23.38) (2020-09-01)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.37](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.36...@zaaksysteem/communication-module@1.23.37) (2020-09-01)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.36](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.35...@zaaksysteem/communication-module@1.23.36) (2020-09-01)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.35](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.34...@zaaksysteem/communication-module@1.23.35) (2020-08-27)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.34](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.33...@zaaksysteem/communication-module@1.23.34) (2020-08-21)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.33](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.32...@zaaksysteem/communication-module@1.23.33) (2020-08-20)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.32](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.31...@zaaksysteem/communication-module@1.23.32) (2020-08-19)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.31](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.30...@zaaksysteem/communication-module@1.23.31) (2020-08-17)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.30](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.25...@zaaksysteem/communication-module@1.23.30) (2020-08-11)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.29](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.25...@zaaksysteem/communication-module@1.23.29) (2020-08-07)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.28](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.25...@zaaksysteem/communication-module@1.23.28) (2020-08-07)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.27](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.25...@zaaksysteem/communication-module@1.23.27) (2020-08-06)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.26](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.25...@zaaksysteem/communication-module@1.23.26) (2020-08-06)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.25](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.24...@zaaksysteem/communication-module@1.23.25) (2020-08-06)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.24](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.23...@zaaksysteem/communication-module@1.23.24) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.23](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.22...@zaaksysteem/communication-module@1.23.23) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.22](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.21...@zaaksysteem/communication-module@1.23.22) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.21](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.20...@zaaksysteem/communication-module@1.23.21) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.20](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.19...@zaaksysteem/communication-module@1.23.20) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.19](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.18...@zaaksysteem/communication-module@1.23.19) (2020-07-17)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.18](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.17...@zaaksysteem/communication-module@1.23.18) (2020-07-15)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.17](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.16...@zaaksysteem/communication-module@1.23.17) (2020-07-15)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.16](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.15...@zaaksysteem/communication-module@1.23.16) (2020-07-15)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.15](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.14...@zaaksysteem/communication-module@1.23.15) (2020-07-14)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.14](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.13...@zaaksysteem/communication-module@1.23.14) (2020-07-09)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.13](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.12...@zaaksysteem/communication-module@1.23.13) (2020-07-09)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.12](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.11...@zaaksysteem/communication-module@1.23.12) (2020-07-02)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.11](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.10...@zaaksysteem/communication-module@1.23.11) (2020-06-30)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.9...@zaaksysteem/communication-module@1.23.10) (2020-06-26)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.8...@zaaksysteem/communication-module@1.23.9) (2020-06-25)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.7...@zaaksysteem/communication-module@1.23.8) (2020-06-24)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.6...@zaaksysteem/communication-module@1.23.7) (2020-06-23)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.5...@zaaksysteem/communication-module@1.23.6) (2020-06-18)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.4...@zaaksysteem/communication-module@1.23.5) (2020-06-17)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.3...@zaaksysteem/communication-module@1.23.4) (2020-06-12)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.2...@zaaksysteem/communication-module@1.23.3) (2020-06-11)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.1...@zaaksysteem/communication-module@1.23.2) (2020-06-11)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.23.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.23.0...@zaaksysteem/communication-module@1.23.1) (2020-06-09)

**Note:** Version bump only for package @zaaksysteem/communication-module





# [1.23.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.22.14...@zaaksysteem/communication-module@1.23.0) (2020-06-05)


### Features

* **Communication:** Disallow messages of type 'pip' to be deleted ([c13156b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c13156b))





## [1.22.14](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.22.13...@zaaksysteem/communication-module@1.22.14) (2020-06-05)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.22.13](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.22.12...@zaaksysteem/communication-module@1.22.13) (2020-06-03)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.22.12](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.22.11...@zaaksysteem/communication-module@1.22.12) (2020-05-29)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.22.11](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.22.10...@zaaksysteem/communication-module@1.22.11) (2020-05-28)

**Note:** Version bump only for package @zaaksysteem/communication-module





## [1.22.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.22.9...@zaaksysteem/communication-module@1.22.10) (2020-05-27)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.22.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.22.8...@zaaksysteem/communication-module@1.22.9) (2020-05-19)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.22.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.22.7...@zaaksysteem/communication-module@1.22.8) (2020-05-14)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.22.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.22.6...@zaaksysteem/communication-module@1.22.7) (2020-05-14)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.22.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.22.5...@zaaksysteem/communication-module@1.22.6) (2020-05-13)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.22.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.22.4...@zaaksysteem/communication-module@1.22.5) (2020-05-12)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.22.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.22.3...@zaaksysteem/communication-module@1.22.4) (2020-05-12)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.22.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.22.2...@zaaksysteem/communication-module@1.22.3) (2020-05-12)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.22.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.22.1...@zaaksysteem/communication-module@1.22.2) (2020-05-11)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.22.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.22.0...@zaaksysteem/communication-module@1.22.1) (2020-05-01)

**Note:** Version bump only for package @zaaksysteem/communication-module

# [1.22.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.21.4...@zaaksysteem/communication-module@1.22.0) (2020-05-01)

### Features

- **CreateObject:** MINTY-3472 - add Rights step to Object Create with associated components ([8699e78](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8699e78))

## [1.21.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.21.3...@zaaksysteem/communication-module@1.21.4) (2020-04-30)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.21.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.21.2...@zaaksysteem/communication-module@1.21.3) (2020-04-30)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.21.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.21.1...@zaaksysteem/communication-module@1.21.2) (2020-04-30)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.21.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.21.0...@zaaksysteem/communication-module@1.21.1) (2020-04-28)

**Note:** Version bump only for package @zaaksysteem/communication-module

# [1.21.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.20.0...@zaaksysteem/communication-module@1.21.0) (2020-04-20)

### Features

- **CommunicationModule:** MINTY-3790 Send filename when creating external message ([b72104d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b72104d))

# [1.20.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.19.9...@zaaksysteem/communication-module@1.20.0) (2020-04-16)

### Features

- **Communication:** MINTY-3645 - change endpoint Email to document ([27c1d6a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/27c1d6a))

## [1.19.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.19.8...@zaaksysteem/communication-module@1.19.9) (2020-04-16)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.19.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.19.7...@zaaksysteem/communication-module@1.19.8) (2020-04-16)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.19.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.19.6...@zaaksysteem/communication-module@1.19.7) (2020-04-16)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.19.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.19.5...@zaaksysteem/communication-module@1.19.6) (2020-04-14)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.19.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.19.4...@zaaksysteem/communication-module@1.19.5) (2020-04-10)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.19.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.19.3...@zaaksysteem/communication-module@1.19.4) (2020-04-09)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.19.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.19.2...@zaaksysteem/communication-module@1.19.3) (2020-04-07)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.19.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.19.1...@zaaksysteem/communication-module@1.19.2) (2020-04-06)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.19.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.19.0...@zaaksysteem/communication-module@1.19.1) (2020-04-03)

**Note:** Version bump only for package @zaaksysteem/communication-module

# [1.19.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.18.12...@zaaksysteem/communication-module@1.19.0) (2020-04-02)

### Features

- **Communication:** MINTY-3411 - save Emails to PDF in Documents ([08046ce](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/08046ce))
- **Communication:** MINTY-3432 - add attachment PDF preview ([5bebd92](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5bebd92))

## [1.18.12](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.18.11...@zaaksysteem/communication-module@1.18.12) (2020-04-01)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.18.11](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.18.10...@zaaksysteem/communication-module@1.18.11) (2020-04-01)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.18.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.18.9...@zaaksysteem/communication-module@1.18.10) (2020-04-01)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.18.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.18.8...@zaaksysteem/communication-module@1.18.9) (2020-03-31)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.18.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.18.7...@zaaksysteem/communication-module@1.18.8) (2020-03-31)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.18.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.18.6...@zaaksysteem/communication-module@1.18.7) (2020-03-30)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.18.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.18.5...@zaaksysteem/communication-module@1.18.6) (2020-03-30)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.18.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.18.4...@zaaksysteem/communication-module@1.18.5) (2020-03-30)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.18.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.18.3...@zaaksysteem/communication-module@1.18.4) (2020-03-27)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.18.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.18.2...@zaaksysteem/communication-module@1.18.3) (2020-03-20)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.18.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.18.1...@zaaksysteem/communication-module@1.18.2) (2020-03-19)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.18.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.18.0...@zaaksysteem/communication-module@1.18.1) (2020-03-19)

**Note:** Version bump only for package @zaaksysteem/communication-module

# [1.18.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.17.1...@zaaksysteem/communication-module@1.18.0) (2020-03-17)

### Features

- **CaseRequestor:** MINTY-3297 Use nested form value instead of flat so you have access to the label ([3eccfa3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3eccfa3))
- **MagicStrings:** MINTY-3297 Add util to call the Perl preview endpoint with the Python data structure ([594f2c1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/594f2c1))
- **MagicStrings:** MINTY-3297 Temporary resolve magic strings before saving the message ([032aaa0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/032aaa0))
- **MessagePreview:** MINTY-3297 Add GenericFormMessagePreview component to display messages that contain magic strings ([13dce0f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/13dce0f))
- **MessagePreview:** MINTY-3297 Enable preview based on context ([9318f77](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9318f77))

## [1.17.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.17.0...@zaaksysteem/communication-module@1.17.1) (2020-03-17)

**Note:** Version bump only for package @zaaksysteem/communication-module

# [1.17.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.16.0...@zaaksysteem/communication-module@1.17.0) (2020-03-05)

### Features

- **DocumentPreview:** MINTY-2960 Add case document preview to dev dashboard ([0f6fb22](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0f6fb22))
- **DocumentPreview:** MINTY-2960 Add DocumentPreview component ([ef352ac](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ef352ac))

# [1.16.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.15.2...@zaaksysteem/communication-module@1.16.0) (2020-03-03)

### Features

- **Communication:** MINTY-3190 Add visual indication in thread that message is unread ([d675d65](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d675d65))

## [1.15.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.15.1...@zaaksysteem/communication-module@1.15.2) (2020-02-24)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.15.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.15.0...@zaaksysteem/communication-module@1.15.1) (2020-02-21)

**Note:** Version bump only for package @zaaksysteem/communication-module

# [1.15.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.14.5...@zaaksysteem/communication-module@1.15.0) (2020-02-17)

### Features

- **MultiValueText:** MINTY-3076 Add entered value on blur ([5c098f8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5c098f8))

## [1.14.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.14.4...@zaaksysteem/communication-module@1.14.5) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.14.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.14.3...@zaaksysteem/communication-module@1.14.4) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.14.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.14.2...@zaaksysteem/communication-module@1.14.3) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.14.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.14.1...@zaaksysteem/communication-module@1.14.2) (2020-02-10)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.14.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.14.0...@zaaksysteem/communication-module@1.14.1) (2020-02-07)

**Note:** Version bump only for package @zaaksysteem/communication-module

# [1.14.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.13.0...@zaaksysteem/communication-module@1.14.0) (2020-02-06)

### Features

- **Communication:** MINTY-2955 Expand unread messages in thread and mark unread messages as read ([f157214](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f157214))
- **Communication:** MINTY-2955 Reload the threadList after marking messages as read ([d23190e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d23190e))
- **Communication:** MINTY-2955 Show read/unread status of thread messages ([9e8ec85](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9e8ec85))

# [1.13.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.12.1...@zaaksysteem/communication-module@1.13.0) (2020-02-06)

### Features

- **Communication:** MINTY-2962 Add button to toggle ReplyForm ([5b1d327](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5b1d327))
- **Communication:** MINTY-2962 Extract Pip form and reuse the same form for create and reply ([42848aa](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/42848aa))

## [1.12.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.12.0...@zaaksysteem/communication-module@1.12.1) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/communication-module

# [1.12.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.11.0...@zaaksysteem/communication-module@1.12.0) (2020-01-30)

### Features

- **CM:** Divide email recipients into to, cc, bcc groups ([284bb46](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/284bb46))

# [1.11.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.10.1...@zaaksysteem/communication-module@1.11.0) (2020-01-30)

### Features

- **communication:** Add email to label of contact search field ([64d48fb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/64d48fb))

## [1.10.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.10.0...@zaaksysteem/communication-module@1.10.1) (2020-01-23)

**Note:** Version bump only for package @zaaksysteem/communication-module

# [1.10.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.9.0...@zaaksysteem/communication-module@1.10.0) (2020-01-23)

### Features

- **Communication:** MINTY-2658 Add CaseRequestor form field which fetches te current case requestor ([8af717c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8af717c))

# [1.9.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.8.0...@zaaksysteem/communication-module@1.9.0) (2020-01-22)

### Bug Fixes

- **CaseDocumentSelect:** Add ConfigType of CaseDocumentSelect ([1c48eb2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1c48eb2))

### Features

- **CaseDocumentSelect:** Autoprovide case documents instead of searching by keyword ([1cab1eb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1cab1eb))
- **MessageForm:** Replace upload ability with ability to select case documents in case context ([e1d1f92](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e1d1f92))

# [1.8.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.7.0...@zaaksysteem/communication-module@1.8.0) (2020-01-14)

### Features

- **Communication:** MINTY-2704 Add `gemachtigde` option to email recipient selector ([b2a204a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b2a204a))

# [1.7.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.6.0...@zaaksysteem/communication-module@1.7.0) (2020-01-14)

### Features

- **Communication:** MINTY-2658 Set `betrokkene` as default option when sending an email ([fed8cfb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/fed8cfb))

## [1.6.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.6.0...@zaaksysteem/communication-module@1.6.1) (2020-01-13)

**Note:** Version bump only for package @zaaksysteem/communication-module

# [1.6.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.5.0...@zaaksysteem/communication-module@1.6.0) (2020-01-13)

### Features

- **Communication:** MINTY-2886 Change default entry point for new items to `contactmoment` ([57abd6f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/57abd6f))
- **Communication:** MINTY-2886 Change order for new items to match default `contactmoment` ([6e294cf](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6e294cf))

# [1.5.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.4.0...@zaaksysteem/communication-module@1.5.0) (2020-01-10)

### Features

- **Communication:** MINTY-2658 Remove 'aanvrager' when it is a preset case requestor ([8b72fcd](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8b72fcd))
- **Communication:** MINTY-2665 Use `ContactFinder` component to search for email recipients ([c0a4ad6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c0a4ad6))
- **Communication:** MINTY-2692 Add role selection to email recipient form ([2122179](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2122179))
- **Communication:** MINTY-2693 Add EmailRecipient selection component ([c67d8e9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c67d8e9))

# [1.4.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.3.0...@zaaksysteem/communication-module@1.4.0) (2020-01-09)

### Bug Fixes

- fix merge conflicts ([39dfe26](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/39dfe26))

### Features

- **Tasks:** Add the basic setup for the task module ([ecc9d4c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ecc9d4c))
- **Tasks:** MINTY-1575 - add basic form fields, DatePicker ([07ed5e5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/07ed5e5))
- **Tasks:** MINTY-2695 - rewrite internal logic to immediate responding to user actions ([c0f74c2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c0f74c2))

# [1.3.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.2.4...@zaaksysteem/communication-module@1.3.0) (2020-01-09)

### Bug Fixes

- **AddForm:** Filter out resolved cases from case_search ([7470a2e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7470a2e))
- **Communication:** change casing to pass eslint ([68efc5e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/68efc5e))
- **Communication:** MINTY-2262 - allow e-mail addresses over multiple lines ([db44093](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/db44093))
- **Message:** Fix some alignments within the message header ([4f29f35](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4f29f35))
- **MessageForm:** Disable case selector for contact moment from contact view ([5195393](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5195393))
- **Thread:** Fix search case API to new version as filter-status ([85aff79](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/85aff79))
- **Thread:** Remove unused prop ([7b3f2e9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7b3f2e9))
- **ThreadList:** Sort threadList on creation date last_message ([b32522a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b32522a))
- **UI:** Fix aria-labels on snackbar and dialogs ([775a287](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/775a287))

### Features

- **AddThread:** Filter out resolved cases ([7f31864](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7f31864))
- **Case:** Allow status of case to determine capabilities of communication module ([1a7a781](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1a7a781))
- **CaseFinder:** Display the case status of the case results ([5cbfeb0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5cbfeb0))
- **Communication:** Allow reply form to scale up with content ([0d34d84](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0d34d84))
- **Communication:** MINTY-1798 Add `canImportMessage` capability ([c10c88e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c10c88e))
- **Communication:** MINTY-1798 Add dialog to import message ([b4543aa](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b4543aa))
- **Communication:** MINTY-1798 Add saving state to import dialog ([f30c765](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f30c765))
- **Communication:** MINTY-1798 Allow selecting `.eml` and `.mail` extensions too ([223d1c6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/223d1c6))
- **Communication:** MINTY-1925 Add message menu with delete action, uses `useConfirmDialog` to ask for confirmation ([33bdccb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/33bdccb))
- **Communication:** MINTY-1925 Implement delete message action and trigger refresh using redux middleware ([4d15e36](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4d15e36))
- **Communication:** MINTY-1925 Make deleting a message controllable via a capability ([704ee17](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/704ee17))
- **Communication:** MINTY-2034 Call `/get_thread_list` with correct params according to context of communication-module ([7fef0f2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7fef0f2))
- **Communication:** MINTY-2659 Add recipient to PIP external message form ([23deed1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/23deed1))
- **Communication:** MINTY-2664 Implement icon which indicates mulitple messages in the thread ([4a50cdb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4a50cdb))
- **Communication:** Replace omitStatus with a filter for multiple statuses ([f2f8e95](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f2f8e95))
- **Forms:** MINTY-2323 - add generic FormDialog component and implement throughout ([7f46465](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7f46465))
- **Message:** Allow source files of messages to be added to the case documents ([6819f60](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6819f60))
- **Message:** Make deletion of messages depended on case status ([c1968df](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c1968df))
- **Thread:** Allow actions on case-messages to be performed outside of case-context ([2608cbc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2608cbc))
- **Thread:** Allow threads to be linked to cases from main view ([44a8a51](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/44a8a51))
- **Thread:** Disable reply when case is closed ([05d56b8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/05d56b8))
- **ThreadList:** Add tooltip to message counter ([78965a4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/78965a4))

## [1.2.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.2.3...@zaaksysteem/communication-module@1.2.4) (2019-12-19)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.2.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.2.2...@zaaksysteem/communication-module@1.2.3) (2019-11-21)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.2.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.2.1...@zaaksysteem/communication-module@1.2.2) (2019-10-29)

**Note:** Version bump only for package @zaaksysteem/communication-module

## [1.2.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.2.0...@zaaksysteem/communication-module@1.2.1) (2019-10-25)

**Note:** Version bump only for package @zaaksysteem/communication-module

# [1.2.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/communication-module@1.1.0...@zaaksysteem/communication-module@1.2.0) (2019-10-22)

### Features

- **Communication:** Add temporary text for send_to_pip button ([db5f3f2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/db5f3f2))

# 1.1.0 (2019-10-17)

### Bug Fixes

- add empty attachments to save, remove Document domain from generated ([8cbda28](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8cbda28))
- change tag color, check for internalUrl in Details view ([89f58c3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/89f58c3))
- **ThreadPlaceholder:** Fix positioning of fallback when there are no messages ([15e77fb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/15e77fb))

### Features

- **Communciation:** MINTY-1790 Add communication module on `/main/customer-contact` route ([c90005c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c90005c))
- **Communication:** Add actionMenus to threadList, thread and message ([665d4b7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/665d4b7))
- **Communication:** MINTY-1790 Add contacts and customer-contact to main dashboard for testing purposes ([fb27a9a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/fb27a9a))
- **Theme:** adjust some theming for better contrast ([33d10c6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/33d10c6))
- **Thread:** Add attachments view with empty click ([2b613ed](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2b613ed))
