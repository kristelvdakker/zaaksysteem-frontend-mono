// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withStyles } from '@material-ui/styles';
import IconButton from '@material-ui/core/IconButton';
import { addScopeAttribute } from '../../library/addScope';
import Icon from '../../Material/Icon/Icon';
import { closeIndicatorStylesheet } from './CloseIndicator.style';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Function} [props.action]
 * @param {boolean} [props.disabled=false]
 * @return {ReactElement}
 */
const CloseIndicator = ({ classes, action, disabled = false, scope }) => (
  <IconButton
    onClick={action}
    color="inherit"
    classes={classes}
    disabled={disabled}
    {...addScopeAttribute(scope, 'close-button')}
  >
    <Icon size="extraSmall">close</Icon>
  </IconButton>
);

export default withStyles(closeIndicatorStylesheet)(CloseIndicator);
