// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withStyles } from '@material-ui/styles';
import Icon from '../../../Material/Icon';
import { infoIconStyleSheet } from './InfoIcon.style';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @return {ReactElement}
 */
export const InfoIcon = ({ classes }) => (
  <Icon classes={classes}>help_outline</Icon>
);

export default withStyles(infoIconStyleSheet)(InfoIcon);
