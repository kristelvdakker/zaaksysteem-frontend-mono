// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment } from 'react';
import classNames from 'classnames';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { addScopeAttribute } from '../../library/addScope';
import { Subtitle1, Caption } from '../../Material/Typography';
import Render from '../../Abstract/Render/Render';
import InfoIcon from './library/InfoIcon';
import ErrorLabel from './library/ErrorLabel';
import { useFormControlWrapperStylesheet } from './FormControlWrapper.style';

/**
 * Wraps children (usually an input component) with:
 *
 * - A title label
 * - A help tooltip (if provided)
 * - A hint label (if provided)
 * - An error label (if provided)
 *
 * Displays this in either a wide (3 column, default) or
 * compact (stacked) layout.
 */

export type FormControlWrapperPropsType = {
  error?: string;
  help?: string;
  hint?: string;
  label?: string;
  required?: boolean;
  compact?: boolean;
  scope?: string;
  touched?: boolean;
  disabled?: boolean;
  applyBackgroundColor?: boolean;
  className?: string;
  readOnly?: boolean;
};

// eslint-disable-next-line complexity
export const FormControlWrapper: React.ComponentType<FormControlWrapperPropsType> = ({
  children,
  error,
  help,
  scope,
  hint = '',
  label = '',
  required = false,
  disabled = false,
  compact = false,
  touched = false,
  applyBackgroundColor = null,
  readOnly = false,
  className,
}) => {
  const classes = useFormControlWrapperStylesheet();
  const applyBackgroundColorDefaulted =
    applyBackgroundColor ||
    (applyBackgroundColor === null && readOnly === false);

  const elHelp = (
    <Render condition={Boolean(help)}>
      <div className={classes.help}>
        <Tooltip title={help as string} type="info">
          <InfoIcon />
        </Tooltip>
      </div>
    </Render>
  );

  const elLabel = label && (
    <div
      className={classNames({
        [classes.labelCompact]: compact,
        [classes.label]: !compact,
        [classes.disabled]: disabled,
      })}
    >
      <Subtitle1>
        {label}
        <Render condition={required}>
          <sup>﹡</sup>
        </Render>
      </Subtitle1>
    </div>
  );

  const elHint = (
    <Render condition={Boolean(hint)}>
      <div className={classes.hint}>
        <Caption
          classes={{
            root: classes.hint,
          }}
        >
          {hint}
        </Caption>
      </div>
    </Render>
  );

  const elError = (
    <Render condition={Boolean(error && touched)}>
      <div className={classes.error}>
        <ErrorLabel label={error as string} />
      </div>
    </Render>
  );

  const elControl = (
    <div
      className={classNames({
        [classes.controlBackgroundColor]: applyBackgroundColorDefaulted,
        [classes.control]: !readOnly,
        [classes.controlReadonly]: readOnly,
      })}
    >
      {children}
    </div>
  );

  const wrap = (content: React.ReactNode) => (
    <div
      className={classNames(classes.wrapper, className)}
      {...addScopeAttribute(scope, 'formControlWrapper')}
    >
      {content}
    </div>
  );

  if (compact) {
    return wrap(
      <Fragment>
        {elLabel}
        {elHelp}
        {elHint}
        {elControl}
        {elError}
      </Fragment>
    );
  }

  return wrap(
    <Fragment>
      {elLabel && (
        <div className={classes.colLabels}>
          {elLabel} {elHint}
        </div>
      )}
      <div className={classes.colContent}>
        {elControl}
        {elError}
      </div>
      <div className={classes.colHelp}>{elHelp}</div>
    </Fragment>
  );
};

export default FormControlWrapper;
