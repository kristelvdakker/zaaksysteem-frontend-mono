// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

const marginBottom = '6px';
const paddingTop = '8px';

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const useFormControlWrapperStylesheet = makeStyles(
  ({ mintlab: { greyscale, radius } }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexWrap: 'wrap',
      width: '100%',
    },
    labelCompact: {
      marginBottom,
      flex: '1 auto',
    },
    label: {
      flex: '1 0 auto',
      marginBottom,
    },
    help: {
      width: '50px',
      '& > div': {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '4px',
      },
    },
    error: {
      width: '100%',
    },
    hint: {
      width: '100%',
      color: greyscale.evenDarker,
      marginBottom,
    },
    control: {
      width: '100%',
      marginBottom,
      borderRadius: radius.defaultFormElement,
    },
    controlReadonly: {
      width: '100%',
      paddingTop: 10,
      borderRadius: radius.defaultFormElement,
    },
    controlBackgroundColor: {
      backgroundColor: greyscale.light,
    },
    colLabels: {
      width: '250px',
      marginRight: '10px',
      paddingTop,
    },
    colContent: {
      flex: '1 0 auto',
    },
    colHelp: {
      paddingTop,
    },
    disabled: {
      color: greyscale.darker,
    },
  })
);
