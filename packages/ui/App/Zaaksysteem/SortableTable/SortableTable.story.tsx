// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { withStyles } from '@material-ui/styles';
import { React, stories } from '../../story';
import SortableTable from './SortableTable';
import { RowType, ColumnType } from './types/SortableTableTypes';

interface StoryRowType extends RowType {
  column1: string;
  column2: string;
  column3: string;
}

const generateRows = (): StoryRowType[] => {
  const rows = [];
  for (let num = 1; num < 100; num++) {
    rows.push({
      uuid: `${num}`,
      name: `column ${num}`,
      column1: `First column, row ${num}`,
      column2:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus venenatis gravida dui, id sagittis nibh euismod malesuada. Sed eget nibh neque. Phasellus ut tortor nisl. Donec imperdiet posuere justo, efficitur viverra tellus sollicitudin et. Nam nec nulla in metus vehicula hendrerit. Praesent consectetur rutrum ipsum, id volutpat odio vehicula id. Maecenas turpis eros, posuere ut vulputate vitae, semper eget purus. Integer nulla ligula, auctor eget risus a, ullamcorper aliquam elit. Nunc hendrerit sagittis euismod. Donec egestas neque non mauris cursus tristique. Duis nec tortor a velit tincidunt egestas. Phasellus vitae ex efficitur, bibendum magna at, placerat odio. Morbi ac est libero. Ut finibus urna quis pellentesque iaculis.',
      column3: `Third column, row ${num}`,
    });
  }
  return rows;
};

const columns: ColumnType[] = [
  {
    name: 'column1',
    width: 200,
    label: 'Column 1',
  },
  {
    name: 'column2',
    width: 1,
    flexGrow: 1,
    label: 'Column 2',
  },
  {
    name: 'column3',
    width: 250,
    label: 'Column 3',
  },
];

const styles = ({ typography }: any) => ({
  wrapper: {
    fontFamily: typography.fontFamily,
    width: '100%',
    height: '500px',
  },
});

const Wrapper = ({ children }: { children: React.ReactChild }) =>
  React.createElement(
    withStyles(styles)(({ classes }: any) => (
      <div className={classes.wrapper}>{children}</div>
    ))
  );

const NO_ROWS_MESSAGE = 'There are no rows to display.';

stories(module, __dirname, {
  'Fixed row heights'() {
    return (
      <Wrapper>
        <SortableTable
          rows={generateRows()}
          columns={columns}
          noRowsMessage={NO_ROWS_MESSAGE}
          loading={false}
          rowHeight={50}
          onRowDoubleClick={() => {}}
        />
      </Wrapper>
    );
  },
  'Dynamic row heights'() {
    return (
      <Wrapper>
        <SortableTable
          rows={generateRows()}
          columns={columns}
          noRowsMessage={NO_ROWS_MESSAGE}
          loading={false}
          onRowDoubleClick={() => {}}
        />
      </Wrapper>
    );
  },
  'No rows'() {
    return (
      <Wrapper>
        <SortableTable
          rows={[]}
          columns={columns}
          noRowsMessage={NO_ROWS_MESSAGE}
          loading={false}
          rowHeight={50}
          onRowDoubleClick={() => {}}
        />
      </Wrapper>
    );
  },
});
