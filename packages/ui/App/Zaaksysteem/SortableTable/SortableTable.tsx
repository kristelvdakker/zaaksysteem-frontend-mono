// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import classNames from 'classnames';
import {
  AutoSizer,
  Column,
  Table,
  SortDirection,
  TableHeaderProps,
  CellMeasurerCache,
  CellMeasurer,
} from 'react-virtualized';
import 'react-virtualized/styles.css';
import { SortDirectionType } from 'react-virtualized';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
// @ts-ignore
import Checkbox from '@mintlab/ui/App/Material/Checkbox';
import { useSortableTableStyles } from './SortableTable.style';
import {
  SortableTablePropsType,
  ColumnType,
  RowType,
  CellRendererPropsType,
} from './types/SortableTableTypes';

const DEFAULT_ROW_HEIGHT = 45;
const HEADER_HEIGHT = 50;

/**
 * Sortable Table component, which takes arrays of rows and columns,
 * and generates a Virtualized table.
 *
 * Basic support for:
 * - sorting on column content
 * - custom cellrenderers
 * - responsive behaviour by showing/hiding columns
 * - dynamic or fixed row height
 * - checkboxes for selecting
 */
const SortableTable: React.ComponentType<SortableTablePropsType> = ({
  rows,
  columns,
  onRowDoubleClick = () => {},
  onRowClick,
  noRowsMessage,
  loading = false,
  rowHeight,
  selectable = false,
  styles,
}) => {
  const [sortBy, setSortBy] = useState(getDefaultSorting(columns));
  const [sortDirection, setSortDirection] = useState(
    SortDirection.ASC as SortDirectionType
  );
  const SortableTableStyles = useSortableTableStyles();
  const classes = styles || SortableTableStyles;
  const rowHeightPropOrDefault = rowHeight || DEFAULT_ROW_HEIGHT;
  const cache = new CellMeasurerCache({
    defaultHeight: rowHeightPropOrDefault,
    minHeight: rowHeightPropOrDefault,
    fixedHeight: Boolean(rowHeight),
    fixedWidth: false,
  });

  const headerRenderer = ({ dataKey, label, sortBy }: TableHeaderProps) => {
    const sortHeader = sortBy === dataKey;
    return (
      <div
        className={classNames(classes.tableCell, classes.tableCellHeader, {
          [classes.sortHeader]: sortHeader,
        })}
      >
        {label}
        {sortHeader ? (
          <Icon
            size="extraSmall"
            classes={{
              root:
                sortDirection === SortDirection.ASC
                  ? classes.sortAsc
                  : classes.sortDesc,
            }}
          >
            {iconNames.arrow_back}
          </Icon>
        ) : null}
      </div>
    );
  };

  const tableCellRenderer = (props: CellRendererPropsType) => {
    const { dataKey, rowData, parent, columnIndex, rowIndex } = props;
    const column = getVisibleColumns(props.parent.props.width).find(
      (col: ColumnType) => col.name === dataKey
    );

    return (
      <CellMeasurer
        cache={cache}
        columnIndex={columnIndex}
        key={dataKey}
        parent={parent}
        rowIndex={rowIndex}
      >
        <div
          className={classNames(classes.tableCell, {
            [classes.tableCellFixedHeight]: Boolean(rowHeight),
            [classes.tableCellFirst]: columnIndex === 0,
          })}
        >
          {column && column.cellRenderer
            ? column.cellRenderer(props)
            : rowData[dataKey]}
        </div>
      </CellMeasurer>
    );
  };

  const rowClassName = ({ index, rowData }: any) => {
    const row = rows[index];

    return classNames(classes.flexContainer, classes.tableRow, {
      [classes.tableHeader]: index === -1,
      [classes.tableRowHover]: index >= 0,
      [classes.rowSelected]: row?.selected === true,
    });
  };

  /* eslint complexity: [2, 8] */
  const getSortedRows = () => {
    if (!sortBy) return rows;

    const sorted = rows.sort((sortA, sortB) => {
      const noValue = (first: RowType, second: RowType) => !first || !second;

      if (noValue(sortA, sortB)) return 0;

      const valA = (sortA as any)[sortBy];
      const valB = (sortB as any)[sortBy];

      if (noValue(valA, valB)) return 0;

      if (Number.isInteger(valA)) {
        return valA > valB ? 1 : -1;
      }

      if (valA instanceof Date) {
        return valA.getTime() > valB.getTime() ? 1 : -1;
      }

      return valA.localeCompare(valB);
    });

    return sortDirection === SortDirection.DESC ? sorted.reverse() : sorted;
  };

  const getVisibleColumns = (width: number): ColumnType[] => {
    const base = selectable
      ? [
          {
            label: '',
            name: 'checked',
            width: 40,
            disableSort: true,
            /* eslint-disable-next-line */
            cellRenderer: ({ rowData }: any) => (
              <div className={classes.selectCell}>
                {typeof rowData.selected !== 'undefined' ? (
                  <Checkbox
                    checked={rowData.selected}
                    name={`checkbox-${rowData.uuid}`}
                    color={'primary'}
                  />
                ) : null}
              </div>
            ),
          },
        ]
      : [];

    return [
      ...base,
      ...columns.filter(
        (thisColumn: ColumnType) =>
          !thisColumn.showFromWidth || thisColumn.showFromWidth <= width
      ),
    ];
  };

  const sortedRows = getSortedRows();

  return (
    <AutoSizer>
      {table => {
        return (
          <React.Fragment>
            {loading ? (
              <Loader className={classes.loader} />
            ) : sortedRows.length ? (
              <Table
                height={table.height}
                width={table.width}
                headerHeight={HEADER_HEIGHT}
                rowHeight={cache.rowHeight}
                rowCount={rows.length}
                rowGetter={({ index }) => sortedRows[index]}
                rowClassName={rowClassName}
                onRowDoubleClick={onRowDoubleClick}
                onRowClick={onRowClick}
                sort={({ sortBy, sortDirection }) => {
                  setSortBy(sortBy);
                  setSortDirection(sortDirection);
                }}
                sortBy={sortBy}
                sortDirection={sortDirection}
              >
                {getVisibleColumns(table.width).map(
                  ({ name, width, cellRenderer, ...rest }: ColumnType) => (
                    <Column
                      key={name}
                      width={width}
                      dataKey={name}
                      headerRenderer={headerRenderer}
                      cellRenderer={tableCellRenderer}
                      {...rest}
                    />
                  )
                )}
              </Table>
            ) : (
              <div
                style={{ width: table.width }}
                className={classes.noRowsMessage}
              >
                {noRowsMessage}
              </div>
            )}
          </React.Fragment>
        );
      }}
    </AutoSizer>
  );
};

const getDefaultSorting = (columns: ColumnType[]) => {
  const [defaultColumn] = columns.filter(thisColumn => thisColumn.defaultSort);
  return defaultColumn?.name || '';
};

export default SortableTable;
