// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import ReactSelectCreatable from 'react-select/lib/Creatable';
import { useTheme } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';
import defaultFilterOptions from '../library/filterOption';
import { BaseSelectPropsType } from '../types/BaseSelectPropsType';
import { subcomponents } from '../library/subcomponents';
import { useSelectBase } from '../hooks/useSelectBase';
import { createSharedStyleSheet } from './Shared.style';

/* eslint complexity: [2, 8] */
export const CreatableSelect: React.ComponentType<BaseSelectPropsType<any>> = ({
  autoLoad,
  disabled,
  error,
  focus,
  hasInitialChoices,
  isClearable,
  value,
  getChoices,
  filterOption,
  formatCreateLabel,
  name,
  placeholder,
  translations,
  styles,
  components,
  onBlur,
  onChange,
  onKeyDown,
  createOnBlur,
  isMulti,
  choices,
  multiValueLabelIcon,
}) => {
  const theme = useTheme<Theme>();

  const {
    focusState,
    defaultedTranslations: { creatable, create },
    eventHandlers,
  } = useSelectBase({
    autoLoad: Boolean(autoLoad),
    hasInitialChoices: Boolean(hasInitialChoices),
    focus: Boolean(focus),
    name,
    translations,
    getChoices,
    eventType: 'creatable',
    onBlur,
    onChange,
    onKeyDown,
    isMulti,
    value,
    choices,
  });
  const createLabel = (input: string) => [create, ' ', input].join('');

  return (
    <ReactSelectCreatable
      {...eventHandlers}
      id={name}
      inputId={`creatable-select-${name}`}
      onBlur={(event: any) => {
        const nextValue = event.target.value;
        if (nextValue && createOnBlur) {
          const itemToSet = {
            label: nextValue,
            value: nextValue,
          };

          if (Array.isArray(value)) {
            eventHandlers.onChange([...value, itemToSet]);
          } else {
            eventHandlers.onChange([value, itemToSet]);
          }
        }
        eventHandlers.onBlur();
      }}
      error={Boolean(error)}
      isDisabled={disabled}
      isMulti={isMulti === undefined ? true : isMulti}
      isClearable={isClearable}
      components={{
        ...subcomponents,
        DropdownIndicator: null,
        ...(components || {}),
      }}
      multiValueLabelIcon={multiValueLabelIcon}
      value={value}
      placeholder={placeholder || creatable}
      formatCreateLabel={formatCreateLabel || createLabel}
      noOptionsMessage={() => null}
      filterOption={filterOption || defaultFilterOptions}
      styles={
        styles ||
        createSharedStyleSheet({
          theme,
          error,
          focus: focusState,
        })
      }
    />
  );
};
