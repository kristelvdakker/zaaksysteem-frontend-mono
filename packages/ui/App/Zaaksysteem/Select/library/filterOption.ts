// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

//@ts-ignore
import { stripDiacritics } from 'react-select/lib/diacritics';
import { ValueType } from '../types/ValueType';

const filterOption = <T>(option: ValueType<T>, input: string) => {
  if (!input) {
    return true;
  }
  //@ts-ignore
  const { label, alternativeLabels } = option.data;
  const labels = [label].concat(alternativeLabels).filter(item => item);

  const clean = (value: any) =>
    stripDiacritics(value.toString().trim().toLowerCase());

  return labels.some(thisLabel => clean(thisLabel).includes(clean(input)));
};

export default filterOption;
