// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Suspense } from 'react';
import { Loader } from '../Loader';
import { BaseSelectPropsType } from './types/BaseSelectPropsType';

const LazySelect = React.lazy(
  () =>
    import(
      // https://webpack.js.org/api/module-methods/#import
      /* webpackChunkName: "ui.select" */
      './library/SelectStrategy'
    )
);

export const Select = (props: BaseSelectPropsType<any>) => (
  <Suspense fallback={<Loader />}>
    <LazySelect {...props} />
  </Suspense>
);

export default Select;
