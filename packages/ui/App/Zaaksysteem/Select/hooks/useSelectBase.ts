// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useState, useEffect } from 'react';
import { useDebouncedCallback } from 'use-debounce';
import { asArray } from '@mintlab/kitchen-sink/source';
import { TranslationsType as Translations } from '../types/TranslationsType';

export const DELAY = 400;
export const MIN_CHARACTERS = 3;

const defaultTranslations: Translations = {
  'form:choose': 'Select…',
  'form:loading': 'Loading…',
  'form:beginTyping': 'Begin typing to search…',
  'form:creatable': 'Typ, en <ENTER> om te bevestigen.',
  'form:create': 'Aanmaken:',
};

export const useSelectBase = ({
  autoLoad,
  hasInitialChoices,
  focus,
  name,
  translations,
  getChoices,
  eventType,
  onBlur,
  onChange,
  onKeyDown,
  value,
  isMulti = false,
  choices,
}: {
  autoLoad?: boolean;
  hasInitialChoices?: boolean;
  focus?: boolean;
  name?: string;
  translations?: Translations;
  getChoices?: (input: string) => void;
  eventType: string;
  onBlur?: (event: any) => void;
  onChange?: (event: any) => void;
  onKeyDown?: (event: any) => void;
  value?: any;
  isMulti?: boolean;
  choices?: any;
}) => {
  const [focusState, setFocus] = useState(focus);

  const [getChoicesDebounced] = useDebouncedCallback((input: string) => {
    getChoices &&
      !autoLoad &&
      !hasInitialChoices &&
      isValidInput(input) &&
      getChoices(input);
  }, DELAY);

  useEffect(() => {
    autoLoad && getChoices && getChoices('');
  }, []);

  const defaultedTranslations = translations || defaultTranslations;

  return {
    normalizedValue: getNormalizedInputValue({ value, isMulti, choices }),
    focusState,
    defaultedTranslations: {
      choose: defaultedTranslations['form:choose'],
      loadingMessage: defaultedTranslations['form:loading'],
      beginTyping: defaultedTranslations['form:beginTyping'],
      creatable: defaultedTranslations['form:creatable'],
      create: defaultedTranslations['form:create'],
    },
    eventHandlers: {
      onInputChange: getChoicesDebounced,
      onKeyDown: (keyDownEvent: any) => {
        const { key, keyCode, charCode } = keyDownEvent;
        onKeyDown &&
          onKeyDown({
            key,
            keyCode,
            charCode,
            name,
            type: eventType,
          });

        keyDownEvent.stopPropagation();
      },
      onChange: (value: any) => {
        onChange &&
          onChange({
            target: {
              name,
              value,
              type: eventType,
            },
          });
      },
      onBlur: () => {
        setFocus(false);
        onBlur &&
          onBlur({
            target: {
              name,
            },
          });
      },

      onFocus: () => {
        setFocus(true);
      },
    },
  };
};

const isValidInput = (input: string) => {
  return Boolean(input) && input.length >= MIN_CHARACTERS;
};

const getNormalizedInputValue = ({
  value,
  choices,
  isMulti,
}: {
  value: any;
  choices: any;
  isMulti: boolean;
}) => {
  if (!value || (Array.isArray(value) && !value.length)) return value;

  const parsedValues = asArray(value).map((val: any) => {
    if (typeof val === 'object') return val;
    if (!choices || !asArray(choices).length) return val;

    const foundChoice = choices.find((choice: any) => choice.value === val);
    return foundChoice ? foundChoice : val;
  });

  return isMulti ? parsedValues : parsedValues[0];
};
