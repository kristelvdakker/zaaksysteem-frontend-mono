// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type ValueType<T, D = unknown> = {
  label: string | React.ReactNode;
  alternativeLabels?: string[];
  value: T;
  data?: D;
};
