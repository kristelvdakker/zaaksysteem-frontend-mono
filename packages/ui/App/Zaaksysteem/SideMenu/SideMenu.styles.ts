// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useSideMenuStyles = makeStyles(
  ({
    palette: { primary },
    shape: { borderRadius },
    mintlab: { greyscale, shadows },
  }: Theme) => ({
    root: {},
    button: {
      borderRadius,
    },
    selected: {
      '&&': {
        color: primary.main,
        backgroundColor: greyscale.dark,
      },
    },
    iconSelected: {
      color: primary.main,
    },
  })
);
