// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { useSideMenuStyles } from './SideMenu.styles';

export type SideMenuItemType = {
  label: string;
  ariaLabel?: string;
  component?: any;
  selected?: boolean;
  href?: string;
  icon?: React.ReactElement;
  [key: string]: any;
};

export type SideMenuPropsType = {
  items: SideMenuItemType[];
};

export const SideMenu: React.ComponentType<SideMenuPropsType> = ({ items }) => {
  const classes = useSideMenuStyles();

  return (
    <List component="nav" role="navigation">
      {items.map(
        (
          { component, icon, label, selected, href }: SideMenuItemType,
          index
        ) => (
          <ListItem
            button
            role="button"
            href={href}
            key={index}
            component={component}
            selected={selected}
            classes={{
              root: classes.root,
              button: classes.button,
              selected: classes.selected,
            }}
          >
            {icon && (
              <ListItemIcon
                classes={{ root: selected ? classes.iconSelected : undefined }}
              >
                {icon}
              </ListItemIcon>
            )}
            <ListItemText>{label}</ListItemText>
          </ListItem>
        )
      )}
    </List>
  );
};
