// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * Style Sheet with theme access for the {@link Layout} component.
 * See the CSS file for the autoprefixed CSS grid.
 *
 * @param {Object} theme
 * @return {JSS}
 */
export const layoutStyleSheet = theme => ({
  root: {
    height: '90%',
    background: theme.mintlab.greyscale.lighter,
    '& *::-ms-clear': {
      display: 'none',
    },
  },
  content: {},
  drawerAndContentContainer: {
    display: 'flex',
    height: '100%',
  },
});
