// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories, action, select, boolean } from '../../story';
import Card from '../../Material/Card';
import { TextField } from '../../Material/TextField';
import * as classes from './Layout.story.css';
import Layout from '.';

const [p1, p2, p3] = [
  `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
   magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
   consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
   Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
  `Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem
   aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo
   enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui
   ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur,
   adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat
   voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut
   aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil
   molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?`,
  '[blank]',
];

const Card1 = () => (
  <Card title="Home">
    <p>{p1}</p>
    <p>{p2}</p>
    <p>{p1}</p>
    <p>{p2}</p>
    <p>{p1}</p>
  </Card>
);

const Card2 = () => (
  <Card title="Help">
    <p>{p2}</p>
  </Card>
);

const Card3 = () => (
  <Card title="Alarm">
    <p>{p3}</p>
  </Card>
);

const pages = [Card1, Card2, Card3];

const stores = {
  Default: {
    active: 0,
    isDrawerOpen: false,
  },
};

const Search = () => <TextField />;

stories(
  module,
  __dirname,
  {
    Default({ store, active, isDrawerOpen }) {
      const drawer = {
        primary: [
          {
            icon: 'home',
            label: 'Home',
          },
          {
            icon: 'help',
            label: 'Help',
          },
          {
            icon: 'alarm',
            label: 'Alarm',
          },
        ].map((value, isActive) => ({
          ...value,
          action: () =>
            store.set({
              active: isActive,
            }),
        })),
        secondary: [
          {
            href: 'https://example.org',
            icon: 'sync',
            label: 'Switch naar behandelen',
          },
          {
            href: 'https://example.org',
            icon: 'alarm',
            label: 'New tab example',
            target: '_blank',
          },
        ],
        about: {
          action: action('About'),
          label: 'Versie 3.2.1',
        },
      };

      const userActions = [
        {
          action: action('Log out'),
          icon: 'power_settings_new',
          label: 'Log out',
        },
      ];

      const banners = {
        primary: {
          label: 'Fusce auctor, orci eget convallis tincidunt.',
          variant: 'primary',
        },
        secondary: {
          label: 'Fusce auctor, orci eget convallis tincidunt.',
          variant: 'secondary',
          primary: {
            action: action('Primary action'),
            label: 'Primary label',
          },
          secondary: {
            action: action('Secondary action'),
            icon: 'close',
          },
        },
        review: {
          label: 'Nunc placerat nec nibh bibendum dignissim.',
          variant: 'review',
          primary: {
            action: action('Primary action'),
            label: 'Primary label',
          },
          secondary: [
            {
              action: action('Secondary action'),
              label: 'First item',
              icon: 'search',
            },
            {
              action: action('Secondary action'),
              label: 'Second item',
              icon: 'favorite',
            },
          ],
        },
        danger: {
          label: 'Fusce auctor, orci eget convallis tincidunt.',
          variant: 'danger',
        },
      };

      const bannerType = select(
        'Notification banner',
        ['none', 'primary', 'secondary', 'review', 'danger'],
        'none'
      );
      const bannerProp =
        bannerType === 'none' ? {} : { [bannerType]: banners[bannerType] };

      const searchComponent = boolean('Search bar') ? <Search /> : null;

      return (
        <div
          style={{
            position: 'fixed',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
          }}
        >
          <Layout
            searchComponent={searchComponent}
            active={active}
            appBar={<div>Hello, Layout!</div>}
            classes={{
              content: classes.content,
            }}
            customer="The Minions"
            drawer={drawer}
            identity="Zaaksysteem.nl Demo"
            isDrawerOpen={isDrawerOpen}
            menuLabel="Main menu"
            toggleDrawer={() =>
              store.set({
                isDrawerOpen: !isDrawerOpen,
              })
            }
            userName="Story User Name"
            userLabel="User menu"
            userActions={userActions}
            banners={bannerProp}
            scope="story"
          >
            {React.createElement(pages[active])}
          </Layout>
        </div>
      );
    },
  },
  stores
);
