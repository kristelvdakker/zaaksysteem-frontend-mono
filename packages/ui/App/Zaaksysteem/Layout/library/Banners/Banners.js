// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Render from '../../../../Abstract/Render';
import Banner from '../../../Banner/Banner/Banner';
import { addScopeAttribute, addScopeProp } from '../../../../library/addScope';

const { values } = Object;

/**
 * @param {String} className
 * @param {Object} banners
 *    An object, where the keys represents a banner's identifier,
 *    and the value is a valid set of Banner props.
 * @return {ReactElement}
 */
export const Banners = ({ className, banners, scope }) => {
  const bannersList = values(banners);

  return (
    <Render condition={bannersList.length}>
      <div className={className} {...addScopeAttribute(scope, 'banners')}>
        {bannersList &&
          bannersList.map((bannerProps, index) => (
            <Banner
              key={index}
              {...bannerProps}
              {...addScopeProp(scope, 'banners', index.toString())}
            />
          ))}
      </div>
    </Render>
  );
};

export default Banners;
