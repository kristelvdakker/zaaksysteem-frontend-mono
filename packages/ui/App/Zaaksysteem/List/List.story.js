// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/* eslint-disable */
import { React, stories } from '../../story';
import { SortableList, useListStyle } from '.';

const stores = {
  Sortable: {
    value: [
      { name: 'helium', id: 'He' },
      { name: 'neon', id: 'Ne' },
      { name: 'argon', id: 'Ar' },
      { name: 'krypton', id: 'Kr' },
      { name: 'xenon', id: 'Xe' },
      { name: 'radon', id: 'Rn' },
    ],
  },
};

stories(
  module,
  __dirname,
  {
    Sortable({ store, value }) {
      return (
        <div>
          <SortableList
            renderItem={({ id, name }) => {
              return (
                <div>
                  <h3 style={{ display: 'inline', marginRight: 25 }}>{id}</h3>
                  <span style={{ textTransform: 'capitalize' }}>{name}</span>
                </div>
              );
            }}
            getId={item => item.id}
            onReorder={nextValue => store.set({ value: nextValue })}
            value={value}
          />
          <strong>the nobility</strong>
        </div>
      );
    },
  },
  stores
);
