// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Component, createElement } from 'react';
import Clone from '../Clone';
import { Loader } from '../../Zaaksysteem/Loader';

const { isArray } = Array;
const { keys } = Object;

export const DEFAULT_ID_ERROR = 'Invalid resource ID';

/**
 * Declarative resource component.
 *
 * Depends on {@link Clone} and {@link Loader}
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Abstract/Resource
 * @see /npm-mintlab-ui/documentation/consumer/manual/Resource.html
 *
 * @reactProps {ReactElement} children
 *   A component that renders the resolved resource(s).
 * @reactProps {Array<string>|string} id
 *   Resource ID specification
 * @reactProps {string} idError
 *   Invalid id error object message
 * @reactProps {string} [loader]
 *   Loader type
 * @reactProps {Object} [payload={}]
 *   Resource payload
 * @reactProps {Function} resolve
 *   Resource resolver
 * @reactProps {Function} [wrapper]
 *   Custom wrapper component that is instantiated with
 *   the resolved Loader instance as `children` prop
 */
export class Resource extends Component {
  /**
   * @ignore
   */
  static get defaultProps() {
    return {
      idError: 'Invalid resource ID',
      payload: {},
    };
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#componentdidmount
   */
  componentDidMount() {
    const { resolve, id, idError } = this.props;

    if (!this.isIdValueValid()) {
      throw new Error(idError);
    }

    resolve(id);
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#unsafe_componentwillreceiveprops
   * @todo refactor deprecated lifecycle method
   *
   * @param {Object} nextProps
   */
  UNSAFE_componentWillReceiveProps(nextProps) {
    const { resolve } = this.props;
    const { id } = nextProps;

    if (this.hasResourceIdChanged(id)) {
      resolve(id);
    }
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  render() {
    const { children, loader, payload, wrapper } = this.props;

    if (this.isCurrentResourceResolved()) {
      return createElement(Clone, {
        children,
        resource: payload,
      });
    }

    const loaderInstance = createElement(Loader, {
      type: loader,
    });

    if (wrapper) {
      return createElement(wrapper, {
        children: loaderInstance,
      });
    }

    return loaderInstance;
  }

  /**
   * Get a normalized version of the resource ID for comparison.
   *
   * @param {string|Array} id
   * @return {string}
   */
  getIdAsString(id) {
    if (isArray(id)) {
      return id.sort().join();
    }

    return id;
  }

  /**
   * @param {string|Array} nextId
   * @return {boolean}
   */
  hasResourceIdChanged(nextId) {
    const { id } = this.props;

    return this.getIdAsString(id) !== this.getIdAsString(nextId);
  }

  /**
   * @return {boolean}
   */
  isCurrentResourceResolved() {
    const { payload } = this.props;

    return keys(payload).every(key => payload[key] !== null);
  }

  /**
   * @param {string} value
   * @return {boolean}
   */
  hasNoComma(value) {
    const expression = /^[^,]+$/;

    return expression.test(value);
  }

  /**
   * @param {*} value
   * @return {boolean}
   */
  isValidIdString(value) {
    if (typeof value === 'string') {
      return this.hasNoComma(value);
    }

    return false;
  }

  /**
   * @return {boolean}
   */
  isIdValueValid() {
    const { id } = this.props;

    if (isArray(id)) {
      return id.every(this.isValidIdString, this);
    }

    return this.isValidIdString(id);
  }
}

export default Resource;
