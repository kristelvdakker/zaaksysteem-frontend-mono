// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
// eslint-disable-next-line import/no-unresolved
import toJson from 'enzyme-to-json';
// eslint-disable-next-line import/no-unresolved
import { shallow } from 'enzyme';
import Resource from './Resource';

describe('The `Resource` component', () => {
  test('renders correctly', () => {
    const component = shallow(
      <Resource
        id="foo"
        payload={{
          foo: null,
        }}
        resolve={() => {}}
      >
        <div />
      </Resource>
    );

    expect(toJson(component)).toMatchSnapshot();
  });
});
