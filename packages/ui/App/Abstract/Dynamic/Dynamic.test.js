// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
// eslint-disable-next-line import/no-unresolved
import { shallow } from 'enzyme';
import Dynamic from '.';

/**
 * @test {Dynamic}
 */
describe('The `Dynamic` component', () => {
  test('creates an HTML element if the `component` property is a string', () => {
    const wrapper = shallow(<Dynamic component="h1">TEST</Dynamic>);
    const actual = wrapper.html();
    const expected = '<h1>TEST</h1>';

    expect(actual).toBe(expected);
  });

  test('creates a component if the `component` property is a function', () => {
    const Foo = ({ barf, children }) => <code title={barf}>{children}</code>;
    const wrapper = shallow(
      <Dynamic component={Foo} barf="Yes">
        TEST
      </Dynamic>
    );
    const actual = wrapper.html();
    const expected = '<code title="Yes">TEST</code>';

    expect(actual).toBe(expected);
  });
});
