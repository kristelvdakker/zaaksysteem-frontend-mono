// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories, boolean, text } from '../../story';
import { DataProvider } from './DataProvider';

const SECOND_IN_MS = 1000;

const mockedDataResponse = (value = '') => {
  return new Promise(resolve => {
    const data = ['aap', 'noot', 'mies'].map(item => `${item}${value}`);
    setTimeout(() => resolve(data), SECOND_IN_MS);
  });
};

/* eslint complexity: [2, 5] */
stories(module, __dirname, {
  'Auto call provider': function AutoCallProvider() {
    return (
      <div>
        <DataProvider provider={mockedDataResponse} autoProvide={true}>
          {({ data, busy }) => {
            if (busy) {
              return 'Loading...';
            }

            return (
              <ul>
                {data && data.map((item, index) => <li key={index}>{item}</li>)}
              </ul>
            );
          }}
        </DataProvider>
      </div>
    );
  },

  'Manual call provider': function ManualCallProvider() {
    return (
      <div>
        <DataProvider provider={mockedDataResponse} autoProvide={false}>
          {({ data, busy, provide }) => {
            if (busy) {
              return 'Loading...';
            }

            if (data === null) {
              return (
                <button type="button" onClick={provide}>
                  Load
                </button>
              );
            }

            return (
              <ul>
                {data.map((item, index) => (
                  <li key={index}>{item}</li>
                ))}
              </ul>
            );
          }}
        </DataProvider>
      </div>
    );
  },

  'Provider options': function ProviderOptions() {
    const providerOptions = text('Value passed to provider', '');
    return (
      <div>
        <DataProvider
          provider={mockedDataResponse}
          autoProvide={true}
          providerArguments={providerOptions}
        >
          {({ data, busy, provide }) => {
            if (busy) {
              return 'Loading...';
            }

            if (data === null) {
              return (
                <button type="button" onClick={provide}>
                  Load
                </button>
              );
            }

            return (
              <ul>
                {data.map((item, index) => (
                  <li key={index}>{item}</li>
                ))}
              </ul>
            );
          }}
        </DataProvider>
      </div>
    );
  },

  'Error handling': function ErrorHandling() {
    const shouldReject = boolean('Provider fails', true);
    const provider = () =>
      new Promise((resolve, reject) => {
        setTimeout(() => {
          if (shouldReject) {
            reject('Some error has occurred');
          } else {
            resolve(['aap', 'noot', 'mies']);
          }
        }, SECOND_IN_MS);
      });

    return (
      <div>
        <DataProvider provider={provider} autoProvide={false}>
          {({ data, error, busy, provide }) => {
            if (busy) {
              return 'Loading...';
            }

            if (error) {
              return (
                <div>
                  <p>{error}</p>
                  <button type="button" onClick={provide}>
                    Retry
                  </button>
                </div>
              );
            }

            if (data === null) {
              return (
                <button type="button" onClick={provide}>
                  Load
                </button>
              );
            }

            return (
              <div>
                <ul>
                  {data.map((item, index) => (
                    <li key={index}>{item}</li>
                  ))}
                </ul>
                <button type="button" onClick={provide}>
                  Reload
                </button>
              </div>
            );
          }}
        </DataProvider>
      </div>
    );
  },
});
