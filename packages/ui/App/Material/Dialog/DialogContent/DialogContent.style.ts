// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useDialogContentStyles = makeStyles(({ typography }: Theme) => ({
  root: {
    ...typography.body2,
    padding: 0,
  },
  padded: {
    ...typography.body2,
    padding: '0 24px 16px',
  },
}));
