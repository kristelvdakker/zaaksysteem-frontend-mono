// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { makeStyles } from '@material-ui/core/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useButtonStyle = makeStyles(
  ({ palette: { common, primary, secondary } }: Theme) => ({
    contained: {
      color: common.black,
      background: common.white,
      '&:hover': {
        background: common.white,
      },
    },
    containedPrimary: {
      color: primary.main,
      background: common.white,
    },
    containedSecondary: {
      color: secondary.main,
      background: common.white,
    },
  })
);
