// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment } from 'react';
import IconButton from '@material-ui/core/IconButton';
import MuiButton from '@material-ui/core/Button';
import { addScopeAttribute } from '../../library/addScope';
import { CustomPalette } from '../MaterialUiThemeProvider/library/CustomPalette';
import Icon, { IconSizeType, IconNameType } from '../Icon';
import {
  parseAction,
  parsePresets,
  ButtonPresetType,
  ButtonVariantType,
  ButtonColorType,
} from './library/utilities';
import { useButtonStyle } from './Button.style';

const SemiContainedButton = (properties: any) => (
  <MuiButton {...properties} variant="contained" />
);

type ButtonPropsType = {
  action?: (ev: React.MouseEvent) => any;
  onClick?: (ev: React.MouseEvent) => any;
  component?:
    | React.ReactElement
    | React.ForwardRefExoticComponent<any>
    | string;
  disabled?: boolean;
  label?: string;
  presets?: ButtonPresetType[];
  scope?: string;
  icon?: IconNameType;
  iconSize?: number | IconSizeType;
  color?: ButtonColorType;
  variant?: ButtonVariantType;
  className?: string;
  classes?: Record<string, string>;
  title?: string;
  fullWidth?: boolean;
};

export const Button: React.ComponentType<ButtonPropsType> = ({
  action,
  children,
  disabled = false,
  icon,
  iconSize = 'extraSmall',
  label,
  presets = [],
  scope,
  classes = {},
  ...rest
}) => {
  const defaultClasses = useButtonStyle();
  const props = { ...parsePresets(presets) };
  const { color, variant } = props;

  function createInstance(instanceProps: any) {
    const compact = {
      icon() {
        return (
          //@ts-ignore
          <IconButton
            aria-label={label}
            color={instanceProps.color}
            disabled={disabled}
            onClick={action}
            {...addScopeAttribute(scope, 'button')}
            {...rest}
          >
            <Icon size={instanceProps.size}>{children as any}</Icon>
          </IconButton>
        );
      },
    };

    if (Object.prototype.hasOwnProperty.call(compact, variant)) {
      //@ts-ignore
      return compact[variant]();
    }

    function getChildren() {
      if (icon) {
        return (
          <Fragment>
            <Icon size={iconSize}>{icon}</Icon>
            {children}
          </Fragment>
        );
      }

      return children;
    }

    const baseProps = {
      ...parseAction(action),
      ...instanceProps,
      children: getChildren(),
      classes,
      disabled,
      ...rest,
    };

    if (variant === 'semiContained') {
      return (
        <SemiContainedButton
          {...baseProps}
          {...addScopeAttribute(scope, 'button')}
          classes={{ ...classes, ...defaultClasses }}
        />
      );
    }

    return <MuiButton {...baseProps} {...addScopeAttribute(scope, 'button')} />;
  }

  if (color === 'review') {
    return (
      <CustomPalette>
        {createInstance({
          ...props,
          color: 'primary',
        })}
      </CustomPalette>
    );
  }

  if (color === 'danger') {
    return (
      <CustomPalette>
        {createInstance({
          ...props,
          color: 'secondary',
        })}
      </CustomPalette>
    );
  }

  return createInstance(props);
};

export default Button;
