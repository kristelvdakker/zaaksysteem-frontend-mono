// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import CssBaseline from '@material-ui/core/CssBaseline';
import { withStyles, withTheme } from '@material-ui/styles';
import * as moduleUnderTest from './default-exports';

/**
 * @test {Exports}
 */
describe('The `default-exports` module', () => {
  test('re-exports the Material-UI `CssBaseline` Component', () => {
    const actual = moduleUnderTest.CssBaseline;
    const asserted = CssBaseline;

    expect(actual).toBe(asserted);
  });

  test('re-exports the Material-UI `withStyles` HoC', () => {
    const actual = moduleUnderTest.withStyles;
    const asserted = withStyles;

    expect(actual).toBe(asserted);
  });

  test('re-exports the Material-UI `withTheme` HoC', () => {
    const actual = moduleUnderTest.withTheme;
    const asserted = withTheme;

    expect(actual).toBe(asserted);
  });
});
