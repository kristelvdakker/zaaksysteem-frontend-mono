// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { BreadcrumbItemType } from '../Breadcrumbs.types';

const ZERO = 0;

const biggerThan = (num: number) => (value: number): boolean => value > num;

/**
 * @param {Array<Object>} items
 * @param {Number} maxItems
 * @return {Array<Object>}
 */
export const getItemsToRender = (
  items: BreadcrumbItemType[],
  maxItems: number
): BreadcrumbItemType[] => {
  if (items.length >= maxItems) {
    const isLastTwoItems = biggerThan(items.length - maxItems);
    const [first, second, ...rest] = items.filter(
      (item, index) => index === ZERO || isLastTwoItems(index)
    );

    return [
      first,
      {
        ...second,
        label: '...',
      },
      ...rest,
    ];
  }

  return items;
};
