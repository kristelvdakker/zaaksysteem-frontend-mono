// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useTooltipStyles = makeStyles(
  ({ palette: { primary, common, error }, mintlab: { radius } }: Theme) => ({
    wrapper: {
      width: '100%',
      '&:hover': {
        cursor: 'pointer',
      },
    },
    all: {
      padding: '8px',
      maxWidth: '300px',
      borderRadius: radius.toolTip,
      fontSize: '13px',
    },
    popper: {
      opacity: 1,
    },
    default: {
      backgroundColor: common.black,
    },
    error: {
      backgroundColor: error.dark,
    },
    info: {
      backgroundColor: primary.lightest,
      color: primary.main,
    },
  })
);
