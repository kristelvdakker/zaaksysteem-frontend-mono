// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withStyles } from '@material-ui/styles';
import Render from '../../../Abstract/Render';
import { pageButtonStyleSheet } from '../Pagination.style';
import IconButton from './PaginationActionsIconButton';
import { getButtonConfig } from './functions';

const FIRSTPAGE = 0;
/**
 * The page navigation, consistent of:
 * - buttons to step X back,
 * - buttons of surrounding plus current page,
 * - buttons to step X forward
 *
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Number} props.page
 * @param {Number} props.pageCount
 * @param {Function} props.onChangePage
 * @return {ReactElement}
 */
export const TablePaginationActions = props => {
  const { classes, page, pageCount, onChangePage, scope } = props;
  const getButtons = name =>
    getButtonConfig(
      name,
      page,
      pageCount,
      classes,
      scope
    ).map((button, key) => (
      <IconButton
        key={key}
        button={button}
        onChangePage={onChangePage}
        page={page}
        pageCount={pageCount}
      />
    ));

  return (
    <div className={classes.pageButtonsWrapper}>
      <Render condition={page !== FIRSTPAGE}>
        <div>{getButtons('stepBackButtons')}</div>
      </Render>
      <div className={classes.pageJumpButtons}>
        {getButtons('pageJumpButtons')}
      </div>
      <div>{getButtons('stepForwardButtons')}</div>
    </div>
  );
};

export default withStyles(pageButtonStyleSheet, {
  withTheme: true,
})(TablePaginationActions);
