// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import TextField from './TextField';
import { useFormStyles } from './styles/TextField.form.style';
import { useGeneric1Styles } from './styles/TextField.generic1.style';
import { useGeneric2Styles } from './styles/TextField.generic2.style';
import { formatTypes } from './TextField.library';
import { EntrypointPropsType } from './TextField.types';

const TextFieldWrapper: React.FunctionComponent<EntrypointPropsType> = props => {
  const { classes, formatType, error } = props;
  const variant = props.variant || 'form';
  const formStyles = useFormStyles();
  const generic1Styles = useGeneric1Styles();
  const generic2Styles = useGeneric2Styles();

  const getClasses = () => {
    if (variant === 'form') {
      return formStyles;
    } else if (variant === 'generic1') {
      return generic1Styles;
    } else if (variant === 'generic2') {
      return generic2Styles;
    } else if (classes) {
      return classes;
    }

    return formStyles;
  };

  //@ts-ignore
  const formatTypeProp = formatTypes[formatType] || formatType || null;

  return (
    <TextField
      {...cloneWithout(props, 'formatType', 'variant', 'classes', 'error')}
      classes={getClasses()}
      disableUnderline={variant !== 'form'}
      error={Boolean(error)}
      {...(formatTypeProp && { formatType: formatTypeProp })}
    />
  );
};

export default TextFieldWrapper;
export { TextFieldWrapper as TextField };
