// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { useState } from 'react';
import classNames from 'classnames';
import MuiTextField from '@material-ui/core/TextField';
//@ts-ignore
import { callOrNothingAtAll, unique } from '@mintlab/kitchen-sink/source';
import { addScopeAttribute } from '../../library/addScope';
import {
  getStartAdornment,
  getEndAdornment,
  NumberFormatWrapper,
} from './TextField.library';
import { TextFieldPropsType } from './TextField.types';

const DEFAULT_ROWS = 3;

/* eslint-disable complexity */
const TextField: React.FunctionComponent<TextFieldPropsType> = ({
  classes,
  disabled = false,
  type = 'text',
  error = false,
  placeholder,
  name,
  required = false,
  rows = DEFAULT_ROWS,
  rowsMax = 999,
  value,
  onChange,
  onKeyDown,
  onKeyPress,
  scope,
  InputProps,
  inputProps,
  isMultiline = false,
  autoFocus = false,
  readOnly = false,
  onBlur,
  submit,
  startAdornment,
  endAdornment,
  closeAction,
  disableUnderline = false,
  formatType,
  displayType = 'input',
}) => {
  const [focus, setFocus] = useState(false);

  const handleFocus = () => {
    setFocus(true);
  };

  const handleBlur = (event: React.ChangeEvent) => {
    setFocus(false);
    callOrNothingAtAll(onBlur, [event]);
  };

  const handleKeyPress = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (onKeyPress) return onKeyPress(event);

    if (submit && event.key.toLowerCase() === 'enter' && !isMultiline) {
      submit(event);
    }
  };

  return (
    <MuiTextField
      error={error}
      type={type}
      value={value}
      onChange={onChange}
      onFocus={handleFocus}
      onBlur={handleBlur}
      onKeyPress={handleKeyPress}
      onKeyDown={onKeyDown}
      disabled={disabled}
      id={unique()}
      placeholder={placeholder}
      name={name}
      required={required}
      multiline={isMultiline}
      rows={rows}
      rowsMax={rowsMax}
      // eslint-disable-next-line
      autoFocus={autoFocus}
      classes={{
        root: classNames(
          readOnly ? classes?.formControlReadonly : classes?.formControl,
          {
            [classes?.focus as any]: focus,
          }
        ),
      }}
      InputProps={{
        readOnly,
        disableUnderline,
        classes: {
          root: classNames(classes?.inputRoot, {
            [classes?.inputRootError as any]: Boolean(error),
            [classes?.inputRootErrorFocus as any]: Boolean(error) && focus,
          }),
          input: classNames(classes?.input, {
            [classes?.inputError as any]: Boolean(error),
          }),
        },
        ...(startAdornment && {
          startAdornment: getStartAdornment({
            focusClass: classes?.focus,
            startAdornment,
            startAdornmentClass: classes?.startAdornment,
            focus,
          }),
        }),
        ...(Boolean(closeAction || endAdornment) && {
          endAdornment: getEndAdornment({
            focusClass: classes?.focus,
            closeAction,
            endAdornment,
            endAdornmentClass: classes?.endAdornment,
            focus,
          }),
        }),
        ...(formatType && { inputComponent: NumberFormatWrapper as any }),
        ...InputProps,
      }}
      inputProps={{
        ...addScopeAttribute(scope, 'text-field-input'),
        ...(formatType && { formatType, displayType }),
        ...(inputProps || {}),
      }}
      {...addScopeAttribute(scope, 'text-field')}
    />
  );
};

export default TextField;
