// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useDatePickerStyleSheet = makeStyles(({ palette }) => ({
  wrapper: {
    width: '100%',
  },
  inputRoot: {
    padding: 10,
  },
  adornment: {
    color: palette.thundercloud.lightest,
  },
}));
