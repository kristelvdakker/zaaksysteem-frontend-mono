// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import nlLocale from 'date-fns/locale/nl';
import DateFnsUtils from '@date-io/date-fns';
import {
  KeyboardDatePicker as MUIKeyboardDatePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
//@ts-ignore
import { callOrNothingAtAll } from '@mintlab/kitchen-sink/source';
import { useDatePickerStyleSheet } from './DatePicker.style';

const KeyboardDatePicker = ({
  value,
  onChange,
  onClose,
  name,
  format = 'dd-MM-yyyy',
  disabled = false,
  placeholder = 'DD-MM-JJJJ',
}) => {
  const classes = useDatePickerStyleSheet();

  const handleClose = event => {
    event.stopPropagation();
    event.preventDefault();
    callOrNothingAtAll(onClose);
  };

  const handleChange = newValue => {
    const callback = val =>
      callOrNothingAtAll(onChange, () => [
        {
          target: {
            name,
            value: val,
          },
        },
      ]);

    if (!newValue) return callback(null);
    if (isNaN(newValue.getTime())) return;
    return callback(newValue);
  };

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={nlLocale}>
      <MUIKeyboardDatePicker
        name={name}
        disabled={disabled}
        value={value}
        onChange={handleChange}
        onAccept={handleChange}
        onClose={handleClose}
        format={format}
        autoOk={true}
        placeholder={placeholder}
        InputProps={{
          disableUnderline: true,
          classes: {
            root: classes.inputRoot,
          },
        }}
        classes={{
          root: classes.wrapper,
        }}
        label=""
      />
    </MuiPickersUtilsProvider>
  );
};

export default KeyboardDatePicker;
