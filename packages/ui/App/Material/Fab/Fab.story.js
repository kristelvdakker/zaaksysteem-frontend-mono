// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories } from '../../story';
import Fab from '.';

stories(module, __dirname, {
  Default() {
    return (
      <Fab
        aria-label={'ariaLabel'}
        color="primary"
        action={() => {}}
        scope={'fab'}
      >
        add
      </Fab>
    );
  },
});
