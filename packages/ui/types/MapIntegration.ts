// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

type IntegrationData = {
  object: {
    lastModified?: string;
    objectTypeVersionUuid?: string;
    registrationDate?: string;
    status?: string;
    title?: string;
    uuid: string;
    versionIndependentUuid?: string;
  } | null;
  objectType: {
    catalog_folder_id?: number;
    date_created?: string;
    date_deleted?: string;
    external_reference?: string;
    is_active_version?: boolean;
    last_modified?: string;
    name?: string;
    status?: string;
    title?: string;
    version?: number;
    version_independent_uuid?: string;
  } | null;
  case: {
    assignee: null;
    date_of_registration: string;
    date_target: string;
    id: string;
    parent_uuid: string;
    phase: string;
  } | null;
  caseType: {
    id: string;
    title: string;
    trigger: string;
  };
  contact: {
    uuid: string;
    address: string;
    location_address: any;
    geojson: {
      readonly type: 'FeatureCollection';
      readonly features: readonly [
        {
          readonly type: 'Feature';
          readonly properties: {};
          readonly geometry: {
            readonly type: 'Point';
            readonly coordinates: [number, number] | null;
          };
        }
      ];
    };
  } | null;
  contactType: string;
};

export type LatLng = [number, number];
export type Geojson = GeoJSON.GeoJsonObject;

export type IntegrationContextType =
  | {
      type: 'ContactMap';
      data: Pick<IntegrationData, 'contact' | 'contactType'>;
    }
  | { type: 'ObjectMap'; data: Pick<IntegrationData, 'object' | 'objectType'> }
  | { type: 'ObjectView'; data: Pick<IntegrationData, 'object' | 'objectType'> }
  | { type: 'CaseForm'; data: Pick<IntegrationData, 'case' | 'caseType'> }
  | {
      type: 'CaseObjectForm';
      data: Pick<
        IntegrationData,
        'case' | 'caseType' | 'object' | 'objectType'
      >;
    }
  | { type: 'CaseRegistrationForm'; data: Pick<IntegrationData, 'caseType'> }
  | { type: 'CaseMap'; data: Pick<IntegrationData, 'case' | 'caseType'> }
  | { type: 'AdvancedSearchMap'; data: null };

type CurrentVersion = 5;
type EventOfMessage<T> = { data: T };
type Message<TypeName, Value> = {
  type: TypeName;
  name: string;
  version: CurrentVersion;
  value: Value;
};

export type MapInitType = {
  initialFeature: GeoJSON.GeoJsonObject | null;
  wmsLayers: any[];
  canDrawFeatures: boolean;
  canSelectLayers: boolean;
  center: LatLng;
  context: IntegrationContextType;
};

type InitMessageType = Message<'init', MapInitType>;
type InitEventType = EventOfMessage<InitMessageType>;

type SetFeatureMessageType = Message<
  'setFeature',
  GeoJSON.GeoJsonObject | null
>;
type SetFeatureEventType = EventOfMessage<SetFeatureMessageType>;

type SetMarkerMessageType = Message<'setMarker', GeoJSON.Point | null>;
type SetMarkerEventType = EventOfMessage<SetMarkerMessageType>;

type FeatureChangeMessageType = Message<
  'featureChange',
  GeoJSON.GeoJsonObject | null
>;
type FeatureChangeEventType = EventOfMessage<FeatureChangeMessageType>;

type ClickMessageType = Message<'click', GeoJSON.Point>;
type ClickEventType = EventOfMessage<ClickMessageType>;

export type ExternalMapEventType =
  | InitEventType
  | SetFeatureEventType
  | SetMarkerEventType;

export type ExternalMapMessageType =
  | FeatureChangeMessageType
  | ClickMessageType;

export type WrapperMapMessageType =
  | SetFeatureMessageType
  | InitMessageType
  | SetMarkerMessageType;

export type WrapperMapEventType = FeatureChangeEventType | ClickEventType;

export type PopupFeatureProp = {
  title: { href?: string; text: string };
  rows: { text: string; title: string }[];
};
