// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const test = require('tape');
const { unique } = require('./unique');

test('unique()', assert => {
  {
    const foo = unique();
    const bar = unique();

    const actual = foo !== bar;
    const expected = true;
    const message = 'generates unique values';

    assert.equal(actual, expected, message);
  }
  {
    const prefix = 'FOO';
    const actual = unique(prefix).startsWith(prefix);
    const expected = true;
    const message = 'generates unique values that can be prefixed';

    assert.equal(actual, expected, message);
  }
  assert.end();
});
