// Generated on: Wed Aug 05 2020 16:35:23 GMT+0200 (Central European Summer Time)
// Environment used: https://development.zaaksysteem.nl
//
// This file was automatically generated. DO NOT MODIFY IT BY HAND.
// Instead, rerun generation of Document domain.

/* eslint-disable */
export namespace APIDocument {
  export interface GetDocumentRequestParams {
    document_uuid: string;
    integrity?: 0 | 1;
    [k: string]: any;
  }

  export interface GetDocumentResponseBody {
    data?: {
      type: string;
      id: string;
      meta: {
        last_modified_datetime?: string;
        created_datetime?: string;
        document_number?: number;
        [k: string]: any;
      };
      relationships: {
        /**
         * Case this document is related to
         */
        case?: {
          data?: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          meta?: {
            display_number?: number;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Directory this document is part of (map / folder / directory)
         */
        directory?: {
          data: {
            id: string;
            type: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Subject making the last change to this document
         */
        modified_by?: {
          data?: {
            id: string;
            type: string;
            [k: string]: any;
          };
          meta?: {
            display_name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Subject making the last change to this document
         */
        locked_by?: {
          data: {
            id: string;
            type: string;
            [k: string]: any;
          };
          meta?: {
            display_name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Subject created this document
         */
        created_by: {
          data: {
            id: string;
            type: string;
            [k: string]: any;
          };
          meta?: {
            display_name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * File this document is related to
         */
        file: {
          data: {
            id: string;
            type: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
      };
      attributes: {
        /**
         * Basename of the file.
         */
        basename?: string;
        /**
         * Extension of the file.
         */
        extension?: string;
        /**
         * Fullname of the file with extension
         */
        name: string;
        /**
         * Mime type of the file
         */
        mimetype: string;
        /**
         * Size of the file
         */
        filesize: number;
        /**
         * md5 of the file
         */
        md5: string;
        /**
         * contains the archive properties of the file.
         */
        archive: {
          type?: 'danslist2018';
          is_archivable?: boolean;
          [k: string]: any;
        };
        /**
         * contains the security properties of the file.
         */
        security: {
          virus_status?: 'pending' | 'ok';
          [k: string]: any;
        };
        /**
         * Accepted status of the document
         */
        accepted: boolean;
        /**
         * Version number, an increasing value of 1-9
         */
        current_version?: number;
        /**
         * Status of the document
         */
        status?: {
          state?: 'pending' | 'active' | 'rejected' | 'deleted';
          pending_type?: 'filename_exists' | 'no_permission';
          /**
           * In case of rejected, this is the reason why
           */
          reason?: string;
          [k: string]: any;
        };
        lock?: {
          /**
           * (e.g. locked) since
           */
          since?: string;
          [k: string]: any;
        };
        metadata?: {
          status?: string;
          origin?: 'Uitgaand' | 'Intern' | 'Inkomend';
          origin_date?: string;
          appearance?: string;
          confidentiality?:
            | 'Openbaar'
            | 'Beperkt openbaar'
            | 'Intern'
            | 'Zaakvertrouwelijk'
            | 'Vertrouwelijk'
            | 'Confidentieel'
            | 'Geheim'
            | 'Zeer geheim';
          structure?: string;
          document_category?:
            | 'Aangifte'
            | 'Aanmaning'
            | 'Aanmelding'
            | 'Aanvraag'
            | 'Advies'
            | 'Afbeelding'
            | 'Afmelding'
            | 'Afspraak'
            | 'Agenda'
            | 'Akte'
            | 'Bankgarantie'
            | 'Begroting'
            | 'Bekendmaking'
            | 'Beleidsdocument'
            | 'Benoeming'
            | 'Berekening'
            | 'Beroepschrift'
            | 'Beschikking'
            | 'Besluit'
            | 'Besluitenlijst'
            | 'Bestek'
            | 'Bestemmingsplan'
            | 'Betaalafspraak'
            | 'Betalingsherinnering'
            | 'Bevestiging'
            | 'Bezwaarschrift'
            | 'Brochure'
            | 'Catalogus'
            | 'Checklist'
            | 'Circulaire'
            | 'Declaratie'
            | 'Dwangbevel'
            | 'Factuur'
            | 'Film'
            | 'Foto'
            | 'Garantiebewijs'
            | 'Geluidsfragment'
            | 'Gespreksverslag'
            | 'Gids'
            | 'Grafiek'
            | 'Herinnering'
            | 'Identificatiebewijs'
            | 'Kaart'
            | 'Kennisgeving'
            | 'Klacht'
            | 'Lastgeving'
            | 'Mededeling'
            | 'Melding'
            | 'Norm'
            | 'Nota'
            | 'Notitie'
            | 'Offerte'
            | 'Ontvangstbevestiging'
            | 'Ontwerp'
            | 'Opdracht'
            | 'Overeenkomst'
            | 'Pakket Van Eisen'
            | 'Persbericht'
            | 'Plan'
            | 'Plan Van Aanpak'
            | 'Polis'
            | 'Procesbeschrijving'
            | 'Proces-verbaal'
            | 'Rapport'
            | 'Regeling'
            | 'Register'
            | 'Rooster'
            | 'Ruimtelijk plan'
            | 'Sollicitatiebrief'
            | 'Statistische opgave'
            | 'Taxatierapport'
            | 'Technische tekening'
            | 'Tekening'
            | 'Uitnodiging'
            | 'Uitspraak'
            | 'Uittreksel'
            | 'Vergaderverslag'
            | 'Vergunning'
            | 'Verklaring'
            | 'Verordening'
            | 'Verslag'
            | 'Verslag van bevindingen'
            | 'Verspreidingslijst'
            | 'Verweerschrift'
            | 'Verzoek'
            | 'Verzoekschrift'
            | 'Voordracht'
            | 'Voorschrift'
            | 'Voorstel'
            | 'Wet'
            | 'Zienswijze';
          pronom_format?: string;
          description?: string;
          [k: string]: any;
        };
        publish?: {
          status?: boolean;
          destinations?: string[];
          [k: string]: any;
        };
        /**
         * List of tags, in case of zaaksysteem: magic strings
         */
        tags?: string[];
        /**
         * true if the integrity of file is verified, else false. Shown in response only if query_parameter 'integrity' is set.
         */
        integrity_check_successful?: boolean | null;
      };
    };
    [k: string]: any;
  }

  export interface CreateDocumentResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateDocumentRequestBody {
    document_file: string;
    document_uuid: string;
    case_uuid: string;
    directory_uuid: string;
    magic_strings?: string[];
    [k: string]: any;
  }

  export interface CreateFileResponseBody {
    data?: {
      type?: 'file';
      id?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateFileRequestBody {
    file: string;
    file_uuid?: string;
    [k: string]: any;
  }

  export interface CreateDocumentFromAttachmentResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateDocumentFromAttachmentRequestBody {
    attachment_uuid: string;
  }

  export interface CreateDocumentFromMessageResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateDocumentFromMessageRequestBody {
    /**
     * The message UUID.
     */
    message_uuid: string;
    /**
     * The output format of the document, original or pdf
     */
    output_format?: 'original' | 'pdf';
  }

  export interface SearchDocumentRequestParams {
    case_uuid?: string;
    keyword?: string;
    [k: string]: any;
  }

  export interface SearchDocumentResponseBody {
    data: {
      type: string;
      id: string;
      meta: {
        last_modified_datetime?: string;
        created_datetime?: string;
        document_number?: number;
        [k: string]: any;
      };
      relationships: {
        /**
         * Case this document is related to
         */
        case?: {
          data?: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          meta?: {
            display_number?: number;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Directory this document is part of (map / folder / directory)
         */
        directory?: {
          data: {
            id: string;
            type: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Subject making the last change to this document
         */
        modified_by?: {
          data?: {
            id: string;
            type: string;
            [k: string]: any;
          };
          meta?: {
            display_name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Subject making the last change to this document
         */
        locked_by?: {
          data: {
            id: string;
            type: string;
            [k: string]: any;
          };
          meta?: {
            display_name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Subject created this document
         */
        created_by: {
          data: {
            id: string;
            type: string;
            [k: string]: any;
          };
          meta?: {
            display_name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * File this document is related to
         */
        file: {
          data: {
            id: string;
            type: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
      };
      attributes: {
        /**
         * Basename of the file.
         */
        basename?: string;
        /**
         * Extension of the file.
         */
        extension?: string;
        /**
         * Fullname of the file with extension
         */
        name: string;
        /**
         * Mime type of the file
         */
        mimetype: string;
        /**
         * Size of the file
         */
        filesize: number;
        /**
         * md5 of the file
         */
        md5: string;
        /**
         * contains the archive properties of the file.
         */
        archive: {
          type?: 'danslist2018';
          is_archivable?: boolean;
          [k: string]: any;
        };
        /**
         * contains the security properties of the file.
         */
        security: {
          virus_status?: 'pending' | 'ok';
          [k: string]: any;
        };
        /**
         * Accepted status of the document
         */
        accepted: boolean;
        /**
         * Version number, an increasing value of 1-9
         */
        current_version?: number;
        /**
         * Status of the document
         */
        status?: {
          state?: 'pending' | 'active' | 'rejected' | 'deleted';
          pending_type?: 'filename_exists' | 'no_permission';
          /**
           * In case of rejected, this is the reason why
           */
          reason?: string;
          [k: string]: any;
        };
        lock?: {
          /**
           * (e.g. locked) since
           */
          since?: string;
          [k: string]: any;
        };
        metadata?: {
          status?: string;
          origin?: 'Uitgaand' | 'Intern' | 'Inkomend';
          origin_date?: string;
          appearance?: string;
          confidentiality?:
            | 'Openbaar'
            | 'Beperkt openbaar'
            | 'Intern'
            | 'Zaakvertrouwelijk'
            | 'Vertrouwelijk'
            | 'Confidentieel'
            | 'Geheim'
            | 'Zeer geheim';
          structure?: string;
          document_category?:
            | 'Aangifte'
            | 'Aanmaning'
            | 'Aanmelding'
            | 'Aanvraag'
            | 'Advies'
            | 'Afbeelding'
            | 'Afmelding'
            | 'Afspraak'
            | 'Agenda'
            | 'Akte'
            | 'Bankgarantie'
            | 'Begroting'
            | 'Bekendmaking'
            | 'Beleidsdocument'
            | 'Benoeming'
            | 'Berekening'
            | 'Beroepschrift'
            | 'Beschikking'
            | 'Besluit'
            | 'Besluitenlijst'
            | 'Bestek'
            | 'Bestemmingsplan'
            | 'Betaalafspraak'
            | 'Betalingsherinnering'
            | 'Bevestiging'
            | 'Bezwaarschrift'
            | 'Brochure'
            | 'Catalogus'
            | 'Checklist'
            | 'Circulaire'
            | 'Declaratie'
            | 'Dwangbevel'
            | 'Factuur'
            | 'Film'
            | 'Foto'
            | 'Garantiebewijs'
            | 'Geluidsfragment'
            | 'Gespreksverslag'
            | 'Gids'
            | 'Grafiek'
            | 'Herinnering'
            | 'Identificatiebewijs'
            | 'Kaart'
            | 'Kennisgeving'
            | 'Klacht'
            | 'Lastgeving'
            | 'Mededeling'
            | 'Melding'
            | 'Norm'
            | 'Nota'
            | 'Notitie'
            | 'Offerte'
            | 'Ontvangstbevestiging'
            | 'Ontwerp'
            | 'Opdracht'
            | 'Overeenkomst'
            | 'Pakket Van Eisen'
            | 'Persbericht'
            | 'Plan'
            | 'Plan Van Aanpak'
            | 'Polis'
            | 'Procesbeschrijving'
            | 'Proces-verbaal'
            | 'Rapport'
            | 'Regeling'
            | 'Register'
            | 'Rooster'
            | 'Ruimtelijk plan'
            | 'Sollicitatiebrief'
            | 'Statistische opgave'
            | 'Taxatierapport'
            | 'Technische tekening'
            | 'Tekening'
            | 'Uitnodiging'
            | 'Uitspraak'
            | 'Uittreksel'
            | 'Vergaderverslag'
            | 'Vergunning'
            | 'Verklaring'
            | 'Verordening'
            | 'Verslag'
            | 'Verslag van bevindingen'
            | 'Verspreidingslijst'
            | 'Verweerschrift'
            | 'Verzoek'
            | 'Verzoekschrift'
            | 'Voordracht'
            | 'Voorschrift'
            | 'Voorstel'
            | 'Wet'
            | 'Zienswijze';
          pronom_format?: string;
          description?: string;
          [k: string]: any;
        };
        publish?: {
          status?: boolean;
          destinations?: string[];
          [k: string]: any;
        };
        /**
         * List of tags, in case of zaaksysteem: magic strings
         */
        tags?: string[];
        /**
         * true if the integrity of file is verified, else false. Shown in response only if query_parameter 'integrity' is set.
         */
        integrity_check_successful?: boolean | null;
      };
    }[];
    [k: string]: any;
  }

  export interface GetDirectoryEntriesForCaseRequestParams {
    case_uuid: string;
    filter?: {
      'relationships.parent.id'?: string;
      fulltext?: string;
      [k: string]: any;
    };
    no_empty_folders?: '0' | '1';
    [k: string]: any;
  }

  export interface GetDirectoryEntriesForCaseResponseBody {
    data?: {
      links: {
        download?: {
          /**
           * Download link for document.
           */
          href: string;
        };
        preview?: {
          /**
           * preview link for document.
           */
          href: string;
          meta: {
            'content-type'?: string;
            [k: string]: any;
          };
        };
        thumbnail?: {
          /**
           * thumbnail link for document.
           */
          href: string;
          meta: {
            'content-type'?: string;
            [k: string]: any;
          };
        };
      };
      type: string;
      id: string;
      attributes: {
        /**
         * Name of the document_entry
         */
        name: string;
        /**
         * Type of document entry
         */
        entry_type: 'document' | 'directory';
        /**
         * Description about directory entry. Valid only for document.
         */
        description: string | null;
        /**
         * Extension of directory_entry. Valid only for document.
         */
        extension: string | null;
        /**
         * Mimetype of directory_entry. Valid only for document
         */
        mimetype: string | null;
        /**
         * Accepted status of directory_entry. Valid only for document.
         */
        accepted: boolean | null;
        /**
         * Table id for document in file table. Valid only for document.
         */
        document_number?: number;
        /**
         * Last modified date time. Valid only for document.
         */
        last_modified_date_time: string | null;
        /**
         * Optional rejection reason for document after case assignment
         */
        rejection_reason: string | null;
        /**
         * Optional display name of person who gave a rejection_reason
         */
        rejected_by_display_name: string | null;
        assignment: {
          employee?: {
            /**
             * Assigned employee name
             */
            name?: string | null;
            [k: string]: any;
          };
          role?: {
            /**
             * Assigned role name
             */
            name?: string | null;
            [k: string]: any;
          };
          department?: {
            /**
             * Assigned department name
             */
            name?: string | null;
            [k: string]: any;
          };
          [k: string]: any;
        };
      };
      relationships: {
        /**
         * Reference to the directory object when directory_entry is of type directory.
         */
        directory?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Reference to the document object when directory_entry is of type document.
         */
        document?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Case this directory_entry is related to
         */
        case?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          meta: {
            display_number?: number;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Parent directory this document_entry.
         */
        parent?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          meta: {
            display_name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Subject making the last change to this document
         */
        modified_by?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          meta: {
            display_name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
      };
    }[];
    included?: {
      type: string;
      id: string;
      attributes: {
        name: string;
        [k: string]: any;
      };
      relationships: {
        /**
         * Parent directory.
         */
        parent?: {
          data?: {
            id?: string | null;
            type?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
      };
    }[];
    [k: string]: any;
  }

  export interface CreateDocumentFromFileResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateDocumentFromFileRequestBody {
    /**
     * UUID of the filestore to which the document is linked.
     */
    file_uuid: string;
    /**
     * UUID of the case to which the document is linked.
     */
    case_uuid?: string;
    /**
     * Indicates whether the generated document should show up on the intake
     */
    skip_intake?: boolean;
    /**
     * UUID of the document to be created
     */
    document_uuid?: string;
    /**
     * UUID of the directory in which the document is created.
     */
    directory_uuid?: string;
    /**
     * Magic strings associated with document.
     */
    magic_strings?: string[];
  }

  export interface DownloadDocumentRequestParams {
    id: string;
    [k: string]: any;
  }

  export interface GetDirectoryEntriesForIntakeRequestParams {
    filter?: {
      /**
       * Search through document intakes with fulltext
       */
      fulltext?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetDirectoryEntriesForIntakeResponseBody {
    data?: {
      links: {
        download?: {
          /**
           * Download link for document.
           */
          href: string;
        };
        preview?: {
          /**
           * preview link for document.
           */
          href: string;
          meta: {
            'content-type'?: string;
            [k: string]: any;
          };
        };
        thumbnail?: {
          /**
           * thumbnail link for document.
           */
          href: string;
          meta: {
            'content-type'?: string;
            [k: string]: any;
          };
        };
      };
      type: string;
      id: string;
      attributes: {
        /**
         * Name of the document_entry
         */
        name: string;
        /**
         * Type of document entry
         */
        entry_type: 'document' | 'directory';
        /**
         * Description about directory entry. Valid only for document.
         */
        description: string | null;
        /**
         * Extension of directory_entry. Valid only for document.
         */
        extension: string | null;
        /**
         * Mimetype of directory_entry. Valid only for document
         */
        mimetype: string | null;
        /**
         * Accepted status of directory_entry. Valid only for document.
         */
        accepted: boolean | null;
        /**
         * Table id for document in file table. Valid only for document.
         */
        document_number?: number;
        /**
         * Last modified date time. Valid only for document.
         */
        last_modified_date_time: string | null;
        /**
         * Optional rejection reason for document after case assignment
         */
        rejection_reason: string | null;
        /**
         * Optional display name of person who gave a rejection_reason
         */
        rejected_by_display_name: string | null;
        assignment: {
          employee?: {
            /**
             * Assigned employee name
             */
            name?: string | null;
            [k: string]: any;
          };
          role?: {
            /**
             * Assigned role name
             */
            name?: string | null;
            [k: string]: any;
          };
          department?: {
            /**
             * Assigned department name
             */
            name?: string | null;
            [k: string]: any;
          };
          [k: string]: any;
        };
      };
      relationships: {
        /**
         * Reference to the directory object when directory_entry is of type directory.
         */
        directory?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Reference to the document object when directory_entry is of type document.
         */
        document?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Case this directory_entry is related to
         */
        case?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          meta: {
            display_number?: number;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Parent directory this document_entry.
         */
        parent?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          meta: {
            display_name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Subject making the last change to this document
         */
        modified_by?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          meta: {
            display_name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
      };
    }[];
    included?: {
      type: string;
      id: string;
      attributes: {
        name: string;
        [k: string]: any;
      };
      relationships: {
        /**
         * Parent directory.
         */
        parent?: {
          data?: {
            id?: string | null;
            type?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
      };
    }[];
    [k: string]: any;
  }

  export interface PreviewDocumentRequestParams {
    id: string;
    [k: string]: any;
  }

  export interface GetDocumentLabelsRequestParams {
    case_uuid: string;
    [k: string]: any;
  }

  export interface GetDocumentLabelsResponseBody {
    data?: {
      type: string;
      id: string;
      attributes: {
        name: string;
        [k: string]: any;
      };
    }[];
    [k: string]: any;
  }

  export interface AddDocumentToCaseResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface AddDocumentToCaseRequestBody {
    /**
     * Internal identifier of the document
     */
    document_uuid: string;
    /**
     * Internal identifier the case to which the document is attached.
     */
    case_uuid: string;
    /**
     * List of internal identifiers of document_labels/magic_strings used to label the document.
     */
    document_label_uuids?: string[];
  }

  export interface UpdateDocumentResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface UpdateDocumentRequestBody {
    /**
     * Internal identifier of the document
     */
    document_uuid: string;
    /**
     * New basename for the document
     */
    basename?: string | null;
    /**
     * New description for the document.
     */
    description?: string | null;
    /**
     * New category for the document
     */
    document_category?:
      | 'Aangifte'
      | 'Aanmaning'
      | 'Aanmelding'
      | 'Aanvraag'
      | 'Advies'
      | 'Afbeelding'
      | 'Afmelding'
      | 'Afspraak'
      | 'Agenda'
      | 'Akte'
      | 'Bankgarantie'
      | 'Begroting'
      | 'Bekendmaking'
      | 'Beleidsdocument'
      | 'Benoeming'
      | 'Berekening'
      | 'Beroepschrift'
      | 'Beschikking'
      | 'Besluit'
      | 'Besluitenlijst'
      | 'Bestek'
      | 'Bestemmingsplan'
      | 'Betaalafspraak'
      | 'Betalingsherinnering'
      | 'Bevestiging'
      | 'Bezwaarschrift'
      | 'Brochure'
      | 'Catalogus'
      | 'Checklist'
      | 'Circulaire'
      | 'Declaratie'
      | 'Dwangbevel'
      | 'Factuur'
      | 'Film'
      | 'Foto'
      | 'Garantiebewijs'
      | 'Geluidsfragment'
      | 'Gespreksverslag'
      | 'Gids'
      | 'Grafiek'
      | 'Herinnering'
      | 'Identificatiebewijs'
      | 'Kaart'
      | 'Kennisgeving'
      | 'Klacht'
      | 'Lastgeving'
      | 'Mededeling'
      | 'Melding'
      | 'Norm'
      | 'Nota'
      | 'Notitie'
      | 'Offerte'
      | 'Ontvangstbevestiging'
      | 'Ontwerp'
      | 'Opdracht'
      | 'Overeenkomst'
      | 'Pakket Van Eisen'
      | 'Persbericht'
      | 'Plan'
      | 'Plan Van Aanpak'
      | 'Polis'
      | 'Procesbeschrijving'
      | 'Proces-verbaal'
      | 'Rapport'
      | 'Regeling'
      | 'Register'
      | 'Rooster'
      | 'Ruimtelijk plan'
      | 'Sollicitatiebrief'
      | 'Statistische opgave'
      | 'Taxatierapport'
      | 'Technische tekening'
      | 'Tekening'
      | 'Uitnodiging'
      | 'Uitspraak'
      | 'Uittreksel'
      | 'Vergaderverslag'
      | 'Vergunning'
      | 'Verklaring'
      | 'Verordening'
      | 'Verslag'
      | 'Verslag van bevindingen'
      | 'Verspreidingslijst'
      | 'Verweerschrift'
      | 'Verzoek'
      | 'Verzoekschrift'
      | 'Voordracht'
      | 'Voorschrift'
      | 'Voorstel'
      | 'Wet'
      | 'Zienswijze';
    /**
     * New origin/direction for the document
     */
    origin?: 'Uitgaand' | 'Intern' | 'Inkomend';
    /**
     * New origin date for the document.
     */
    origin_date?: string | null;
    /**
     * New confidentiality for the document
     */
    confidentiality?:
      | 'Openbaar'
      | 'Beperkt openbaar'
      | 'Intern'
      | 'Zaakvertrouwelijk'
      | 'Vertrouwelijk'
      | 'Confidentieel'
      | 'Geheim'
      | 'Zeer geheim';
  }

  export interface DeleteDocumentResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteDocumentRequestBody {
    /**
     * Internal identifier of the document
     */
    document_uuid: string;
    /**
     * Optional human readable reason for deletion
     */
    reason?: string;
  }

  export interface AssignDocumentToUserResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface AssignDocumentToUserRequestBody {
    /**
     * Internal identifier of the document
     */
    document_uuid: string;
    /**
     * Internal identifier of the user
     */
    user_uuid: string;
  }

  export interface AssignDocumentToRoleResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface AssignDocumentToRoleRequestBody {
    /**
     * Internal identifier of the document
     */
    document_uuid: string;
    /**
     * Internal identifier of the group
     */
    group_uuid: string;
    /**
     * Internal identifier of the role
     */
    role_uuid: string;
  }

  export interface CreateDirectoryResponseBody {
    data?: {
      directory_uuid?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateDirectoryRequestBody {
    /**
     * The Case UUID
     */
    case_uuid: string;
    /**
     * The name of the new directory.
     */
    name: string;
    /**
     * Optional UUID of the parent directory.
     */
    parent_uuid?: string;
    /**
     * Optional UUID of the directory to be created.
     */
    directory_uuid?: string;
    [k: string]: any;
  }

  export interface ThumbnailDocumentRequestParams {
    id: string;
    [k: string]: any;
  }

  export interface DocumentEntity {
    type: string;
    id: string;
    meta: {
      last_modified_datetime?: string;
      created_datetime?: string;
      document_number?: number;
      [k: string]: any;
    };
    relationships: {
      /**
       * Case this document is related to
       */
      case?: {
        data?: {
          id?: string;
          type?: string;
          [k: string]: any;
        };
        meta?: {
          display_number?: number;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Directory this document is part of (map / folder / directory)
       */
      directory?: {
        data: {
          id: string;
          type: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Subject making the last change to this document
       */
      modified_by?: {
        data?: {
          id: string;
          type: string;
          [k: string]: any;
        };
        meta?: {
          display_name?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Subject making the last change to this document
       */
      locked_by?: {
        data: {
          id: string;
          type: string;
          [k: string]: any;
        };
        meta?: {
          display_name?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Subject created this document
       */
      created_by: {
        data: {
          id: string;
          type: string;
          [k: string]: any;
        };
        meta?: {
          display_name?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * File this document is related to
       */
      file: {
        data: {
          id: string;
          type: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
    };
    attributes: {
      /**
       * Basename of the file.
       */
      basename?: string;
      /**
       * Extension of the file.
       */
      extension?: string;
      /**
       * Fullname of the file with extension
       */
      name: string;
      /**
       * Mime type of the file
       */
      mimetype: string;
      /**
       * Size of the file
       */
      filesize: number;
      /**
       * md5 of the file
       */
      md5: string;
      /**
       * contains the archive properties of the file.
       */
      archive: {
        type?: 'danslist2018';
        is_archivable?: boolean;
        [k: string]: any;
      };
      /**
       * contains the security properties of the file.
       */
      security: {
        virus_status?: 'pending' | 'ok';
        [k: string]: any;
      };
      /**
       * Accepted status of the document
       */
      accepted: boolean;
      /**
       * Version number, an increasing value of 1-9
       */
      current_version?: number;
      /**
       * Status of the document
       */
      status?: {
        state?: 'pending' | 'active' | 'rejected' | 'deleted';
        pending_type?: 'filename_exists' | 'no_permission';
        /**
         * In case of rejected, this is the reason why
         */
        reason?: string;
        [k: string]: any;
      };
      lock?: {
        /**
         * (e.g. locked) since
         */
        since?: string;
        [k: string]: any;
      };
      metadata?: {
        status?: string;
        origin?: 'Uitgaand' | 'Intern' | 'Inkomend';
        origin_date?: string;
        appearance?: string;
        confidentiality?:
          | 'Openbaar'
          | 'Beperkt openbaar'
          | 'Intern'
          | 'Zaakvertrouwelijk'
          | 'Vertrouwelijk'
          | 'Confidentieel'
          | 'Geheim'
          | 'Zeer geheim';
        structure?: string;
        document_category?:
          | 'Aangifte'
          | 'Aanmaning'
          | 'Aanmelding'
          | 'Aanvraag'
          | 'Advies'
          | 'Afbeelding'
          | 'Afmelding'
          | 'Afspraak'
          | 'Agenda'
          | 'Akte'
          | 'Bankgarantie'
          | 'Begroting'
          | 'Bekendmaking'
          | 'Beleidsdocument'
          | 'Benoeming'
          | 'Berekening'
          | 'Beroepschrift'
          | 'Beschikking'
          | 'Besluit'
          | 'Besluitenlijst'
          | 'Bestek'
          | 'Bestemmingsplan'
          | 'Betaalafspraak'
          | 'Betalingsherinnering'
          | 'Bevestiging'
          | 'Bezwaarschrift'
          | 'Brochure'
          | 'Catalogus'
          | 'Checklist'
          | 'Circulaire'
          | 'Declaratie'
          | 'Dwangbevel'
          | 'Factuur'
          | 'Film'
          | 'Foto'
          | 'Garantiebewijs'
          | 'Geluidsfragment'
          | 'Gespreksverslag'
          | 'Gids'
          | 'Grafiek'
          | 'Herinnering'
          | 'Identificatiebewijs'
          | 'Kaart'
          | 'Kennisgeving'
          | 'Klacht'
          | 'Lastgeving'
          | 'Mededeling'
          | 'Melding'
          | 'Norm'
          | 'Nota'
          | 'Notitie'
          | 'Offerte'
          | 'Ontvangstbevestiging'
          | 'Ontwerp'
          | 'Opdracht'
          | 'Overeenkomst'
          | 'Pakket Van Eisen'
          | 'Persbericht'
          | 'Plan'
          | 'Plan Van Aanpak'
          | 'Polis'
          | 'Procesbeschrijving'
          | 'Proces-verbaal'
          | 'Rapport'
          | 'Regeling'
          | 'Register'
          | 'Rooster'
          | 'Ruimtelijk plan'
          | 'Sollicitatiebrief'
          | 'Statistische opgave'
          | 'Taxatierapport'
          | 'Technische tekening'
          | 'Tekening'
          | 'Uitnodiging'
          | 'Uitspraak'
          | 'Uittreksel'
          | 'Vergaderverslag'
          | 'Vergunning'
          | 'Verklaring'
          | 'Verordening'
          | 'Verslag'
          | 'Verslag van bevindingen'
          | 'Verspreidingslijst'
          | 'Verweerschrift'
          | 'Verzoek'
          | 'Verzoekschrift'
          | 'Voordracht'
          | 'Voorschrift'
          | 'Voorstel'
          | 'Wet'
          | 'Zienswijze';
        pronom_format?: string;
        description?: string;
        [k: string]: any;
      };
      publish?: {
        status?: boolean;
        destinations?: string[];
        [k: string]: any;
      };
      /**
       * List of tags, in case of zaaksysteem: magic strings
       */
      tags?: string[];
      /**
       * true if the integrity of file is verified, else false. Shown in response only if query_parameter 'integrity' is set.
       */
      integrity_check_successful?: boolean | null;
    };
  }

  export interface FileEntity {
    type?: string;
    id?: string;
    meta?: {
      last_modified_datetime?: string;
      created_datetime?: string;
      summary?: string;
      table_id?: number;
      [k: string]: any;
    };
    relationships?: {
      /**
       * Document this storage item links to, if available
       */
      document?: {
        [k: string]: any;
      };
      /**
       * Related thumbnail
       */
      thumbnail?: {
        [k: string]: any;
      };
      /**
       * Subject created this entry in the file
       */
      created_by?: {
        [k: string]: any;
      };
      [k: string]: any;
    };
    attributes?: {
      /**
       * Fullname of the file with extension
       */
      name?: string;
      /**
       * Name without extension
       */
      basename?: string;
      /**
       * Extension of this file
       */
      extension?: {
        [k: string]: any;
      };
      hash?: {
        type?: 'md5';
        value?: string;
        [k: string]: any;
      };
      archive?: {
        type?: 'danslist2018';
        is_archivable?: boolean;
        [k: string]: any;
      };
      security?: {
        virus_status?: 'pending' | 'ok';
        [k: string]: any;
      };
      filesize?: number;
      mimetype?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DirectoryEntryEntity {
    links: {
      download?: {
        /**
         * Download link for document.
         */
        href: string;
      };
      preview?: {
        /**
         * preview link for document.
         */
        href: string;
        meta: {
          'content-type'?: string;
          [k: string]: any;
        };
      };
      thumbnail?: {
        /**
         * thumbnail link for document.
         */
        href: string;
        meta: {
          'content-type'?: string;
          [k: string]: any;
        };
      };
    };
    type: string;
    id: string;
    attributes: {
      /**
       * Name of the document_entry
       */
      name: string;
      /**
       * Type of document entry
       */
      entry_type: 'document' | 'directory';
      /**
       * Description about directory entry. Valid only for document.
       */
      description: string | null;
      /**
       * Extension of directory_entry. Valid only for document.
       */
      extension: string | null;
      /**
       * Mimetype of directory_entry. Valid only for document
       */
      mimetype: string | null;
      /**
       * Accepted status of directory_entry. Valid only for document.
       */
      accepted: boolean | null;
      /**
       * Table id for document in file table. Valid only for document.
       */
      document_number?: number;
      /**
       * Last modified date time. Valid only for document.
       */
      last_modified_date_time: string | null;
      /**
       * Optional rejection reason for document after case assignment
       */
      rejection_reason: string | null;
      /**
       * Optional display name of person who gave a rejection_reason
       */
      rejected_by_display_name: string | null;
      assignment: {
        employee?: {
          /**
           * Assigned employee name
           */
          name?: string | null;
          [k: string]: any;
        };
        role?: {
          /**
           * Assigned role name
           */
          name?: string | null;
          [k: string]: any;
        };
        department?: {
          /**
           * Assigned department name
           */
          name?: string | null;
          [k: string]: any;
        };
        [k: string]: any;
      };
    };
    relationships: {
      /**
       * Reference to the directory object when directory_entry is of type directory.
       */
      directory?: {
        data: {
          id?: string;
          type?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Reference to the document object when directory_entry is of type document.
       */
      document?: {
        data: {
          id?: string;
          type?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Case this directory_entry is related to
       */
      case?: {
        data: {
          id?: string;
          type?: string;
          [k: string]: any;
        };
        meta: {
          display_number?: number;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Parent directory this document_entry.
       */
      parent?: {
        data: {
          id?: string;
          type?: string;
          [k: string]: any;
        };
        meta: {
          display_name?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Subject making the last change to this document
       */
      modified_by?: {
        data: {
          id?: string;
          type?: string;
          [k: string]: any;
        };
        meta: {
          display_name?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
    };
  }

  export interface DirectoryEntity {
    type: string;
    id: string;
    attributes: {
      name: string;
      [k: string]: any;
    };
    relationships: {
      /**
       * Parent directory.
       */
      parent?: {
        data?: {
          id?: string | null;
          type?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
    };
  }

  export interface DocumentLabelEntity {
    type: string;
    id: string;
    attributes: {
      name: string;
      [k: string]: any;
    };
  }
}
