// Generated on: Mon Dec 07 2020 16:51:44 GMT+0100 (Central European Standard Time)
// Environment used: https://development.zaaksysteem.nl
//
// This file was automatically generated. DO NOT MODIFY IT BY HAND.
// Instead, rerun generation of CaseManagement domain.

/* eslint-disable */
export namespace APICaseManagement {
  export type GetDepartmentsResponseBody = ({
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    links?: {
      self?: string;
      prev?: string;
      next?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }) & {
    data: {
      type: 'department';
      meta?: {
        summary: string;
        [k: string]: any;
      };
      id: string;
      attributes?: {
        name: string;
        description?: string;
        [k: string]: any;
      };
      relationships?: {
        parent?: {
          type: 'department';
          id: string;
          meta?: {
            summary: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    [k: string]: any;
  };

  export interface GetRolesRequestParams {
    filter?: {
      'relationships.parent.id'?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export type GetRolesResponseBody = ({
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    links?: {
      self?: string;
      prev?: string;
      next?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }) & {
    data: {
      type: 'role';
      id: string;
      attributes?: {
        name: string;
        description?: string;
        [k: string]: any;
      };
      relationships?: {
        parent?: {
          type: 'department';
          id: string;
          meta?: {
            summary: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    [k: string]: any;
  };

  export interface GetCaseRequestParams {
    case_uuid: string;
    [k: string]: any;
  }

  export type GetCaseResponseBody = {
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    data?: {
      data?: {
        type?: string;
        id?: string;
        meta?: {
          last_modified_datetime?: string;
          created_datetime?: string;
          summary?: string;
          [k: string]: any;
        };
        attributes?: {
          /**
           * The public case number of this case
           */
          number?: number;
          /**
           * Short slug (description) of this case
           */
          summary?: string;
          /**
           * Short slug (description) of this case, for public use
           */
          public_summary?: string;
          /**
           * Defines whether this case is closed, open or suspended
           */
          status?: {
            name?: 'new' | 'open' | 'resolved' | 'stalled';
            /**
             * When prematurely closed or stalled, this is the reason
             */
            reason?: string;
            /**
             * When stalled: the since date
             */
            since?: string;
            /**
             * When stalled: the until date
             */
            until?: string;
            [k: string]: any;
          };
          /**
           * The date this case was registered
           */
          registration_date?: string;
          /**
           * Date when this case should be resolved
           */
          target_completion_date?: string;
          /**
           * Date when this case was resolved
           */
          completion_date?: string;
          /**
           * Date when this case is due for destruction
           */
          destruction_date?: string;
          /**
           * The phase and milestone this case is currently in
           */
          phase?: {
            label?: string;
            milestone_label?: string;
            sequence?: number;
            next_sequence?: number;
            [k: string]: any;
          };
          /**
           * Information about the result of this case
           */
          result?: {
            result?: string;
            result_id?: number;
            archival_attributes?: {
              state?: 'vernietigen' | 'overdragen';
              selection_list?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * In which way this case ended in creation
           */
          contactchannel?:
            | 'behandelaar'
            | 'balie'
            | 'telefoon'
            | 'post'
            | 'email'
            | 'webformulier'
            | 'sociale media';
          /**
           * Specifies the payment information of a case
           */
          payment?: {
            amount?: number;
            status?: 'success' | 'failed' | 'pending' | 'offline';
            [k: string]: any;
          };
          /**
           * All user defined properties for this case
           */
          custom_fields?: {
            [k: string]: any;
          };
          /**
           * The confidentiality of this case, which defines the authorization path to use
           */
          confidentiality?: {
            mapped?: string;
            original?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        relationships?: {
          /**
           * Contains the subject (employee) who handles this case
           */
          assignee?: {
            data?: {
              type?: 'employee';
              id?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * Contains the coordinator (employee) who handles this case
           */
          coordinator?: {
            data?: {
              type?: 'employee';
              id?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * Contains the requestor (employee/company/person) who handles this case
           */
          requestor?: {
            data?: {
              type?: 'employee' | 'person' | 'organization';
              id?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * Department this case is currently assigned to
           */
          department?: {
            data?: {
              type?: 'department';
              id?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * Location this case references to
           */
          location?: {
            data?: {
              type?: 'location';
              id?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * Subject this case references to (most of the time the requestor of the case
           */
          subjects?: {
            data?: {
              type?: 'employee' | 'person' | 'organization';
              id?: string;
              meta?: {
                role?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * Case type this case descended from
           */
          casetype?: {
            data?: {
              type?: 'casetype';
              id?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * List of related cases
           */
          related_cases?: {
            data?: {
              type?: 'case';
              id?: string;
              [k: string]: any;
            }[];
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Case relates to, e.g. a subject
         */
        relates_to?: {
          data?: {
            type?: 'subject';
            id?: string;
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  };

  export interface SetCompletionDateResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface SetCompletionDateRequestBody {
    target_date?: string;
    case_uuid: string;
    [k: string]: any;
  }

  export interface SetTargetCompletionDateResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface SetTargetCompletionDateRequestBody {
    target_date: string;
    case_uuid: string;
    [k: string]: any;
  }

  export interface SetRegistrationDateResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface SetRegistrationDateRequestBody {
    target_date: string;
    case_uuid: string;
    [k: string]: any;
  }

  export interface AssignCaseToSelfResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface AssignCaseToSelfRequestBody {
    case_uuid: string;
    [k: string]: any;
  }

  export interface ChangeCaseCoordinatorResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface ChangeCaseCoordinatorRequestBody {
    case_uuid: string;
    coordinator_uuid: string;
    [k: string]: any;
  }

  export interface AssignCaseToUserResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface AssignCaseToUserRequestBody {
    /**
     * UUID of the case to be updated
     */
    case_uuid: string;
    /**
     * UUID of the user to be assigned to case.
     */
    user_uuid?: string;
    [k: string]: any;
  }

  export interface AssignCaseToDepartmentResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface AssignCaseToDepartmentRequestBody {
    /**
     * UUID of the case to be updated
     */
    case_uuid: string;
    /**
     * UUID of the department to be assigned to case.
     */
    department_uuid?: string;
    /**
     * UUID of the role to be assigned to case.
     */
    role_uuid?: string;
    [k: string]: any;
  }

  export interface PauseCaseResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface PauseCaseRequestBody {
    case_uuid: string;
    suspension_reason: string;
    suspension_term_value?: number | string;
    suspension_term_type:
      | 'weeks'
      | 'work_days'
      | 'calendar_days'
      | 'fixed_date'
      | 'indefinite';
    [k: string]: any;
  }

  export interface ResumeCaseResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface ResumeCaseRequestBody {
    case_uuid: string;
    resume_reason: string;
    stalled_since_date: number;
    stalled_until_date: number;
    [k: string]: any;
  }

  export interface CreateCaseResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateCaseRequestBody {
    /**
     * UUID of the case to be created. If case_uuid field is missing, then back-end will generate one
     */
    case_uuid?: string;
    /**
     * UUID of the Casetype the case descended from.
     */
    case_type_version_uuid: string;
    /**
     * In which way the case ended in creation
     */
    contact_channel:
      | 'behandelaar'
      | 'balie'
      | 'telefoon'
      | 'post'
      | 'email'
      | 'webformulier'
      | 'sociale media';
    /**
     * Requestor (employee/organization/person) of case
     */
    requestor: {
      /**
       * Type of the requestor(employee/organization/person)
       */
      type?: 'employee' | 'person' | 'organization';
      /**
       * UUID of the requestor
       */
      id?: string;
      [k: string]: any;
    };
    /**
     * Confidentiality for the case.
     */
    confidentiality?: 'public' | 'internal' | 'confidential';
    /**
     * Custom fields for the case.
     */
    custom_fields?: {
      [k: string]: (
        | number
        | string
        | string[]
        | {
            [k: string]: any;
          }
      )[];
    };
    assignment?:
      | {
          role?: {
            id: string;
            type: 'role';
          };
          department?: {
            id: string;
            type: 'department';
          };
        }
      | {
          employee?: {
            id: string;
            type: 'employee';
            use_employee_department?: boolean;
            send_email_notification?: boolean;
          };
        };
    /**
     * Contact information for case
     */
    contact_information?: {
      /**
       * Mobile number of requestor
       */
      mobile_number?: string;
      /**
       * Phone number of requestor
       */
      phone_number?: string;
      /**
       * Email address for the requestor
       */
      email?: string;
    };
    options?: {
      allow_missing_required_fields?: boolean;
    };
  }

  export interface CreateCaseRelationRequestParams {
    uuid1: string;
    uuid2: string;
    [k: string]: any;
  }

  export interface CreateCaseRelationResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetCaseTypeVersionRequestParams {
    version_uuid: string;
    [k: string]: any;
  }

  export interface GetCaseTypeVersionResponseBody {
    data?: {
      type: string;
      id: string;
      meta: {
        last_modified: string;
        created: string;
        summary: string;
        is_eligible_for_case_creation: boolean;
      };
      attributes: {
        /**
         * Indicates that this casetype is a 'mother' of child casetypes
         */
        is_parent: boolean;
        /**
         * Fullname of the casetype
         */
        name: string;
        /**
         * Userdefined identification of this casetype
         */
        identification: string;
        /**
         * Describes this casetype in detail
         */
        description: string;
        /**
         * List of tags defining this casetype, used for faster searching
         */
        tags: string;
        /**
         * A templating line of text which defines the summary of a generated case
         */
        case_summary: string;
        /**
         * A templating line of text which defines the summary of a generated case which we use on public communication
         */
        case_public_summary: string;
        settings: {
          custom_webform: string;
          reuse_casedata: boolean;
          enable_webform: boolean;
          enable_online_payment: boolean;
          enable_manual_payment: boolean;
          require_email_on_webform: boolean;
          require_phonenumber_on_webform: boolean;
          require_mobilenumber_on_webform: boolean;
          disable_captcha_for_predefined_requestor: boolean;
          show_confidentiality: boolean;
          show_contact_info: boolean;
          enable_subject_relations_on_form: boolean;
          open_case_on_create: boolean;
          enable_allocation_on_form: boolean;
          disable_pip_for_requestor: boolean;
          lock_registration_phase: boolean;
          enable_queue_for_changes_from_other_subjects: boolean;
          check_acl_on_allocation: boolean;
          api_can_transition: boolean;
          is_public: boolean;
          list_of_default_folders: {
            parent?: string;
            name?: string;
            [k: string]: any;
          }[];
          text_confirmation_message_title: string;
          text_public_confirmation_message: string;
          payment: {
            assignee: {
              amount: number;
            };
            frontdesk: {
              amount: number;
            };
            phone: {
              amount: number;
            };
            mail: {
              amount: number;
            };
            email: {
              amount: number;
            };
            webform: {
              amount: number;
            };
          };
        };
        /**
         * Extra information about this casetype for archiving and other purposes
         */
        metadata: {
          /**
           * Whether postponing a case is a possibility
           */
          may_postpone: boolean;
          /**
           * Whether extending the duration of a case is a possibility
           */
          may_extend: boolean;
          /**
           * Amount of days a case can be extended
           */
          extension_period: number;
          /**
           * Amount of days a case can be adjourned
           */
          adjourn_period: number;
          /**
           * Link to the e-form of this casetype
           */
          'e-webform': string;
          process_description: string;
          /**
           * Motivation of this casetype (NL: Aanleiding)
           */
          motivation: string;
          /**
           * Purpose of this casetype (NL: Doel)
           */
          purpose: string;
          /**
           * Classification for archiving (NL: Archiefclassificatiecode)
           */
          archive_classification_code: string;
          /**
           * level of confidentiality (NL: Vertrouwelijkheidsaanduiding)
           */
          designation_of_confidentiality:
            | 'Openbaar'
            | 'Beperkt openbaar'
            | 'Intern'
            | 'Zaakvertrouwelijk'
            | 'Vertrouwelijk'
            | 'Confidentieel'
            | 'Geheim'
            | 'Zeer geheim';
          /**
           * The person, team, department or other subject responsible (NL: Verantwoordelijke)
           */
          responsible_subject: string;
          /**
           * The relationship between this casetype and a political reason (NL: Verantwoordingsrelatie)
           */
          responsible_relationship: string;
          /**
           * Whether its possible to object and appeal to this casetype
           */
          possibility_for_objection_and_appeal: boolean;
          /**
           * Whether a decision needs to be published
           */
          publication: boolean;
          /**
           * The text used for publishing decisions of this casetype
           */
          publication_text: string;
          bag: boolean;
          /**
           * Will cases from this casetype automatically turn into a positive result for the citizen when this case is not finished in time.
           */
          lex_silencio_positivo: boolean;
          /**
           * Whether a penalty is required (NL: Wet dwangsom)
           */
          penalty_law: boolean;
          /**
           * Whether the 'NL: Wet Publiekrechtelijke Beperkingen' applies
           */
          wkpb_applies: boolean;
          /**
           * The legal basis (NL: Wettelijke grondslag) of this casetype
           */
          legal_basis: string;
          /**
           * The legal basis (NL: Lokale grondslag) of this casetype
           */
          local_basis: string;
        };
        initiator_type:
          | 'aangaan'
          | 'aangeven'
          | 'aanmelden'
          | 'aanschrijven'
          | 'aanvragen'
          | 'afkopen'
          | 'afmelden'
          | 'indienen'
          | 'inschrijven'
          | 'melden'
          | 'ontvangen'
          | 'opstellen'
          | 'opzeggen'
          | 'registreren'
          | 'reserveren'
          | 'starten'
          | 'stellen'
          | 'uitvoeren'
          | 'vaststellen'
          | 'versturen'
          | 'voordragen'
          | 'vragen';
        initiator_source: 'internal' | 'external' | 'all';
        terms: {
          /**
           * The maximum duration of a case according to the law
           */
          lead_time_legal: {
            type: 'werkdagen' | 'weken' | 'kalenderdagen' | 'einddatum';
            /**
             * The value of 'lead_time_legal' a case may take
             */
            value: string | number;
          };
          /**
           * The maximum duration of a case we would like
           */
          lead_time_service: {
            type: 'werkdagen' | 'weken' | 'kalenderdagen' | 'einddatum';
            /**
             * The value of 'lead_time_service' a case may take
             */
            value: string | number;
          };
        };
        requestor: {
          type_of_requestors: (
            | 'niet_natuurlijk_persoon'
            | 'preset_client'
            | 'natuurlijk_persoon_na'
            | 'natuurlijk_persoon'
            | 'medewerker'
          )[];
          predefined_requestor?: string;
          use_for_correspondence: boolean;
        };
        relates_to?: {
          subject?: {
            type_of_requestors?: string[];
            predefined_requestor?: string;
            [k: string]: any;
          };
          location?: {
            location_from?: 'requestor' | 'attribute';
            [k: string]: any;
          };
          [k: string]: any;
        };
        phases: {
          name?: string;
          milestone?: number;
          allocation?: {
            department?: {
              id?: number;
              name?: string;
              [k: string]: any;
            };
            role?: {
              id?: number;
              name?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          custom_fields?: {
            name: string;
            public_name: string;
            title: string;
            title_multiple: string;
            description: string;
            is_required: boolean;
            external_description: string;
            publish_on: {
              name: string;
            }[];
            requestor_can_change_from_pip: boolean;
            is_hidden_field: boolean;
            edit_authorizations: {
              [k: string]: any;
            }[];
            enable_skip_of_queue: boolean;
            sensitive_data: boolean;
            field_magic_string: string;
            field_type: 'text' | 'option' | 'checkbox';
            field_options: string[];
            default_value: string;
            referential: boolean;
            date_field_limit: {
              start?: {
                active: boolean;
                interval_type: 'days' | 'weeks' | 'months' | 'years';
                interval: number;
                during: 'pre' | 'post';
                refernce: 'current';
              };
              end?: {
                active: boolean;
                interval_type: 'days' | 'weeks' | 'months' | 'years';
                interval: number;
                during: 'pre' | 'post';
                refernce: 'current';
              };
            };
          }[];
          rules?: {
            label?: string;
            conditional_type?: 'AND' | 'OR';
            when?: {
              condition?: string;
              values?: string[];
              immediate?: boolean;
              [k: string]: any;
            };
            then?: {
              action?: string;
              attribute?: string;
              [k: string]: any;
            };
            else?: {
              action?: string;
              attribute?: string;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          document_templates?: {
            related_document_template_element?: string;
            description?: string;
            generate_on_transition?: boolean;
            /**
             * Add to this custom field of type 'document'
             */
            add_to_case_document?: string;
            storage_format?: 'odt' | 'pdf' | 'docx';
            [k: string]: any;
          }[];
          cases?: {
            type_of_relation?:
              | 'deelzaak'
              | 'gerelateerd'
              | 'vervolgzaak'
              | 'vervolgzaak_datum';
            related_casetype_element?: string;
            allocation?: {
              department?: {
                id?: number;
                name?: string;
                [k: string]: any;
              };
              role?: {
                id?: number;
                name?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            requestor?: {
              requestor_type?:
                | 'aanvrager'
                | 'betrokkene'
                | 'anders'
                | 'behandelaar'
                | 'ontvanger';
              related_role?: string;
              /**
               * Not implemented yet
               */
              custom_subject?: {
                subject_type?: 'person' | 'organization';
                id?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            /**
             * Phase number before which this case must be resolved
             */
            resolve_before_phase?: number;
            copy_custom_fields_from_parent?: boolean;
            start_on_transition?: boolean;
            /**
             * Automatically open the case (in behandeling)
             */
            open_case_on_create?: boolean;
            /**
             * Not implemented yet
             */
            related_subject?: {
              subject?: {
                subject_type?: 'person' | 'organization';
                id?: string;
                [k: string]: any;
              };
              role?: string;
              magic_string_prefix?: string;
              /**
               * Authorized to view this case on the PIP (NL: gemachtigde)
               */
              authorized_subject?: boolean;
              send_confirmation_message?: boolean;
              [k: string]: any;
            };
            show_in_pip?: boolean;
            label_in_pip?: string;
            [k: string]: any;
          }[];
          emails?: {
            related_email_element?: string;
            recipient?: {
              recipient_type?:
                | 'requestor'
                | 'assignee'
                | 'coordinator'
                | 'employee'
                | 'authorized_subject'
                | 'related_subject'
                | 'custom';
              id?: string;
              role?: string;
              emailaddress?: string;
              [k: string]: any;
            };
            cc?: string;
            bcc?: string;
            send_on_transition?: boolean;
            [k: string]: any;
          }[];
          subjects?: {
            subject_type?: 'person' | 'employee';
            id?: string;
            role?: string;
            magic_string_prefix?: string;
            authorized_subject?: boolean;
            send_confirmation_message?: boolean;
            [k: string]: any;
          }[];
          results?: {
            title?: string;
            is_default_value?: boolean;
            result?: string;
            archival_attributes?: {
              state?: 'vernietigen' | 'overdragen';
              selection_list?: string;
              selection_list_source_date?: string;
              selection_list_end_date?: string;
              activate_period_of_preservation?: boolean;
              type_of_archiving?: 'vernietigen';
              period_of_preservation?: string;
              archiving_procedure?: 'Afhandeling';
              description?: string;
              selection_list_number?: string;
              process_type_number?: string;
              process_type_name?: string;
              process_type_description?: string;
              process_type_object?: string;
              process_type_generic?: 'generic' | 'specific';
              origin?: string;
              process_term?: 'A' | 'B' | 'C' | 'D' | 'E';
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          checklist_items?: string[];
          [k: string]: any;
        }[];
        authorizations?: {
          [k: string]: any;
        }[];
        changelog?: {
          summary?: string;
          changes?: {
            [k: string]: any;
          }[];
          [k: string]: any;
        };
      };
      relationships?: {
        /**
         * In which folder this casetype resides
         */
        catalogue_folder?: {
          data?: {
            type?: 'folder';
            id?: string;
            meta?: {
              name?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Related parent casetype
         */
        parent?: {
          data?: {
            type?: 'casetype';
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * List of child casetypes of this mother casetype
         */
        children?: {
          data?: {
            type?: 'casetype';
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
    };
    [k: string]: any;
  }

  export interface GetCaseTypeActiveVersionRequestParams {
    case_type_uuid: string;
    [k: string]: any;
  }

  export interface GetCaseTypeActiveVersionResponseBody {
    data?: {
      type: string;
      id: string;
      meta: {
        last_modified: string;
        created: string;
        summary: string;
        is_eligible_for_case_creation: boolean;
      };
      attributes: {
        /**
         * Indicates that this casetype is a 'mother' of child casetypes
         */
        is_parent: boolean;
        /**
         * Fullname of the casetype
         */
        name: string;
        /**
         * Userdefined identification of this casetype
         */
        identification: string;
        /**
         * Describes this casetype in detail
         */
        description: string;
        /**
         * List of tags defining this casetype, used for faster searching
         */
        tags: string;
        /**
         * A templating line of text which defines the summary of a generated case
         */
        case_summary: string;
        /**
         * A templating line of text which defines the summary of a generated case which we use on public communication
         */
        case_public_summary: string;
        settings: {
          custom_webform: string;
          reuse_casedata: boolean;
          enable_webform: boolean;
          enable_online_payment: boolean;
          enable_manual_payment: boolean;
          require_email_on_webform: boolean;
          require_phonenumber_on_webform: boolean;
          require_mobilenumber_on_webform: boolean;
          disable_captcha_for_predefined_requestor: boolean;
          show_confidentiality: boolean;
          show_contact_info: boolean;
          enable_subject_relations_on_form: boolean;
          open_case_on_create: boolean;
          enable_allocation_on_form: boolean;
          disable_pip_for_requestor: boolean;
          lock_registration_phase: boolean;
          enable_queue_for_changes_from_other_subjects: boolean;
          check_acl_on_allocation: boolean;
          api_can_transition: boolean;
          is_public: boolean;
          list_of_default_folders: {
            parent?: string;
            name?: string;
            [k: string]: any;
          }[];
          text_confirmation_message_title: string;
          text_public_confirmation_message: string;
          payment: {
            assignee: {
              amount: number;
            };
            frontdesk: {
              amount: number;
            };
            phone: {
              amount: number;
            };
            mail: {
              amount: number;
            };
            email: {
              amount: number;
            };
            webform: {
              amount: number;
            };
          };
        };
        /**
         * Extra information about this casetype for archiving and other purposes
         */
        metadata: {
          /**
           * Whether postponing a case is a possibility
           */
          may_postpone: boolean;
          /**
           * Whether extending the duration of a case is a possibility
           */
          may_extend: boolean;
          /**
           * Amount of days a case can be extended
           */
          extension_period: number;
          /**
           * Amount of days a case can be adjourned
           */
          adjourn_period: number;
          /**
           * Link to the e-form of this casetype
           */
          'e-webform': string;
          process_description: string;
          /**
           * Motivation of this casetype (NL: Aanleiding)
           */
          motivation: string;
          /**
           * Purpose of this casetype (NL: Doel)
           */
          purpose: string;
          /**
           * Classification for archiving (NL: Archiefclassificatiecode)
           */
          archive_classification_code: string;
          /**
           * level of confidentiality (NL: Vertrouwelijkheidsaanduiding)
           */
          designation_of_confidentiality:
            | 'Openbaar'
            | 'Beperkt openbaar'
            | 'Intern'
            | 'Zaakvertrouwelijk'
            | 'Vertrouwelijk'
            | 'Confidentieel'
            | 'Geheim'
            | 'Zeer geheim';
          /**
           * The person, team, department or other subject responsible (NL: Verantwoordelijke)
           */
          responsible_subject: string;
          /**
           * The relationship between this casetype and a political reason (NL: Verantwoordingsrelatie)
           */
          responsible_relationship: string;
          /**
           * Whether its possible to object and appeal to this casetype
           */
          possibility_for_objection_and_appeal: boolean;
          /**
           * Whether a decision needs to be published
           */
          publication: boolean;
          /**
           * The text used for publishing decisions of this casetype
           */
          publication_text: string;
          bag: boolean;
          /**
           * Will cases from this casetype automatically turn into a positive result for the citizen when this case is not finished in time.
           */
          lex_silencio_positivo: boolean;
          /**
           * Whether a penalty is required (NL: Wet dwangsom)
           */
          penalty_law: boolean;
          /**
           * Whether the 'NL: Wet Publiekrechtelijke Beperkingen' applies
           */
          wkpb_applies: boolean;
          /**
           * The legal basis (NL: Wettelijke grondslag) of this casetype
           */
          legal_basis: string;
          /**
           * The legal basis (NL: Lokale grondslag) of this casetype
           */
          local_basis: string;
        };
        initiator_type:
          | 'aangaan'
          | 'aangeven'
          | 'aanmelden'
          | 'aanschrijven'
          | 'aanvragen'
          | 'afkopen'
          | 'afmelden'
          | 'indienen'
          | 'inschrijven'
          | 'melden'
          | 'ontvangen'
          | 'opstellen'
          | 'opzeggen'
          | 'registreren'
          | 'reserveren'
          | 'starten'
          | 'stellen'
          | 'uitvoeren'
          | 'vaststellen'
          | 'versturen'
          | 'voordragen'
          | 'vragen';
        initiator_source: 'internal' | 'external' | 'all';
        terms: {
          /**
           * The maximum duration of a case according to the law
           */
          lead_time_legal: {
            type: 'werkdagen' | 'weken' | 'kalenderdagen' | 'einddatum';
            /**
             * The value of 'lead_time_legal' a case may take
             */
            value: string | number;
          };
          /**
           * The maximum duration of a case we would like
           */
          lead_time_service: {
            type: 'werkdagen' | 'weken' | 'kalenderdagen' | 'einddatum';
            /**
             * The value of 'lead_time_service' a case may take
             */
            value: string | number;
          };
        };
        requestor: {
          type_of_requestors: (
            | 'niet_natuurlijk_persoon'
            | 'preset_client'
            | 'natuurlijk_persoon_na'
            | 'natuurlijk_persoon'
            | 'medewerker'
          )[];
          predefined_requestor?: string;
          use_for_correspondence: boolean;
        };
        relates_to?: {
          subject?: {
            type_of_requestors?: string[];
            predefined_requestor?: string;
            [k: string]: any;
          };
          location?: {
            location_from?: 'requestor' | 'attribute';
            [k: string]: any;
          };
          [k: string]: any;
        };
        phases: {
          name?: string;
          milestone?: number;
          allocation?: {
            department?: {
              id?: number;
              name?: string;
              [k: string]: any;
            };
            role?: {
              id?: number;
              name?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          custom_fields?: {
            name: string;
            public_name: string;
            title: string;
            title_multiple: string;
            description: string;
            is_required: boolean;
            external_description: string;
            publish_on: {
              name: string;
            }[];
            requestor_can_change_from_pip: boolean;
            is_hidden_field: boolean;
            edit_authorizations: {
              [k: string]: any;
            }[];
            enable_skip_of_queue: boolean;
            sensitive_data: boolean;
            field_magic_string: string;
            field_type: 'text' | 'option' | 'checkbox';
            field_options: string[];
            default_value: string;
            referential: boolean;
            date_field_limit: {
              start?: {
                active: boolean;
                interval_type: 'days' | 'weeks' | 'months' | 'years';
                interval: number;
                during: 'pre' | 'post';
                refernce: 'current';
              };
              end?: {
                active: boolean;
                interval_type: 'days' | 'weeks' | 'months' | 'years';
                interval: number;
                during: 'pre' | 'post';
                refernce: 'current';
              };
            };
          }[];
          rules?: {
            label?: string;
            conditional_type?: 'AND' | 'OR';
            when?: {
              condition?: string;
              values?: string[];
              immediate?: boolean;
              [k: string]: any;
            };
            then?: {
              action?: string;
              attribute?: string;
              [k: string]: any;
            };
            else?: {
              action?: string;
              attribute?: string;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          document_templates?: {
            related_document_template_element?: string;
            description?: string;
            generate_on_transition?: boolean;
            /**
             * Add to this custom field of type 'document'
             */
            add_to_case_document?: string;
            storage_format?: 'odt' | 'pdf' | 'docx';
            [k: string]: any;
          }[];
          cases?: {
            type_of_relation?:
              | 'deelzaak'
              | 'gerelateerd'
              | 'vervolgzaak'
              | 'vervolgzaak_datum';
            related_casetype_element?: string;
            allocation?: {
              department?: {
                id?: number;
                name?: string;
                [k: string]: any;
              };
              role?: {
                id?: number;
                name?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            requestor?: {
              requestor_type?:
                | 'aanvrager'
                | 'betrokkene'
                | 'anders'
                | 'behandelaar'
                | 'ontvanger';
              related_role?: string;
              /**
               * Not implemented yet
               */
              custom_subject?: {
                subject_type?: 'person' | 'organization';
                id?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            /**
             * Phase number before which this case must be resolved
             */
            resolve_before_phase?: number;
            copy_custom_fields_from_parent?: boolean;
            start_on_transition?: boolean;
            /**
             * Automatically open the case (in behandeling)
             */
            open_case_on_create?: boolean;
            /**
             * Not implemented yet
             */
            related_subject?: {
              subject?: {
                subject_type?: 'person' | 'organization';
                id?: string;
                [k: string]: any;
              };
              role?: string;
              magic_string_prefix?: string;
              /**
               * Authorized to view this case on the PIP (NL: gemachtigde)
               */
              authorized_subject?: boolean;
              send_confirmation_message?: boolean;
              [k: string]: any;
            };
            show_in_pip?: boolean;
            label_in_pip?: string;
            [k: string]: any;
          }[];
          emails?: {
            related_email_element?: string;
            recipient?: {
              recipient_type?:
                | 'requestor'
                | 'assignee'
                | 'coordinator'
                | 'employee'
                | 'authorized_subject'
                | 'related_subject'
                | 'custom';
              id?: string;
              role?: string;
              emailaddress?: string;
              [k: string]: any;
            };
            cc?: string;
            bcc?: string;
            send_on_transition?: boolean;
            [k: string]: any;
          }[];
          subjects?: {
            subject_type?: 'person' | 'employee';
            id?: string;
            role?: string;
            magic_string_prefix?: string;
            authorized_subject?: boolean;
            send_confirmation_message?: boolean;
            [k: string]: any;
          }[];
          results?: {
            title?: string;
            is_default_value?: boolean;
            result?: string;
            archival_attributes?: {
              state?: 'vernietigen' | 'overdragen';
              selection_list?: string;
              selection_list_source_date?: string;
              selection_list_end_date?: string;
              activate_period_of_preservation?: boolean;
              type_of_archiving?: 'vernietigen';
              period_of_preservation?: string;
              archiving_procedure?: 'Afhandeling';
              description?: string;
              selection_list_number?: string;
              process_type_number?: string;
              process_type_name?: string;
              process_type_description?: string;
              process_type_object?: string;
              process_type_generic?: 'generic' | 'specific';
              origin?: string;
              process_term?: 'A' | 'B' | 'C' | 'D' | 'E';
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          checklist_items?: string[];
          [k: string]: any;
        }[];
        authorizations?: {
          [k: string]: any;
        }[];
        changelog?: {
          summary?: string;
          changes?: {
            [k: string]: any;
          }[];
          [k: string]: any;
        };
      };
      relationships?: {
        /**
         * In which folder this casetype resides
         */
        catalogue_folder?: {
          data?: {
            type?: 'folder';
            id?: string;
            meta?: {
              name?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Related parent casetype
         */
        parent?: {
          data?: {
            type?: 'casetype';
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * List of child casetypes of this mother casetype
         */
        children?: {
          data?: {
            type?: 'casetype';
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
    };
    [k: string]: any;
  }

  export interface SetSubjectRelatedCustomObjectResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export type SetSubjectRelatedCustomObjectRequestBody = {
    /**
     * UUID of the custom object to link. If this is set to `null`, the subject/custom object link will be broken.
     */
    custom_object_uuid: string;
    [k: string]: any;
  } & (
    | {
        /**
         * UUID of an employee to link a custom_object to.
         */
        employee_uuid: string;
        [k: string]: any;
      }
    | {
        /**
         * UUID of an organization to link a custom_object to.
         */
        organization_uuid: string;
        [k: string]: any;
      }
    | {
        /**
         * UUID of a person to link a custom_object to.
         */
        person_uuid: string;
        [k: string]: any;
      }
  );

  export interface GetSubjectRequestParams {
    employee_uuid?: string;
    organization_uuid?: string;
    person_uuid?: string;
    [k: string]: any;
  }

  export type GetSubjectResponseBody = {
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    data?:
      | ({
          type?: string;
          id?: string;
          meta?: {
            last_modified_datetime?: string;
            created_datetime?: string;
            summary?: string;
            [k: string]: any;
          };
          attributes?: {
            /**
             * Tells us if this is a cached subject
             */
            is_cache?: boolean;
            /**
             * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
             */
            summary?: string;
            contact_information: {
              email: string | null;
              phone_number: string | null;
              mobile_number: string | null;
              internal_note?: string;
            };
          };
          [k: string]: any;
        } & {
          type: string;
          id: string;
          attributes: {
            /**
             * The name, for example changed after marriage
             */
            surname?: string;
            first_name?: string;
            /**
             * The prefix of the surname, the 'DE' in 'A. DE boer'
             */
            surname_prefix?: string;
            initials?: string;
            [k: string]: any;
          };
          relationships?: {
            related_custom_object?: {
              links: {
                self: string;
              };
              data: {
                type: 'custom_object';
                id: string;
              };
            };
            department?: {
              data?: {
                id?: string;
                type?: string;
                meta?: {
                  name?: string;
                  [k: string]: any;
                };
              };
            };
            roles?: {
              data?: {
                id?: string;
                type?: string;
                meta?: {
                  name?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              }[];
              [k: string]: any;
            };
          };
        })
      | ({
          type?: string;
          id?: string;
          meta?: {
            last_modified_datetime?: string;
            created_datetime?: string;
            summary?: string;
            [k: string]: any;
          };
          attributes?: {
            /**
             * Tells us if this is a cached subject
             */
            is_cache?: boolean;
            /**
             * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
             */
            summary?: string;
            contact_information: {
              email: string | null;
              phone_number: string | null;
              mobile_number: string | null;
              internal_note?: string;
            };
          };
          [k: string]: any;
        } & {
          type: string;
          id: string;
          attributes: {
            /**
             * The name, for example changed after marriage
             */
            surname?: string;
            first_names?: string;
            /**
             * The prefix of the surname, the 'DE' in 'A. DE boer'
             */
            surname_prefix?: string;
            initials?: string;
            /**
             * List of statuses for this person, e.g.: [secret deceased]
             */
            status?: string[];
            date_of_birth?: string;
            date_of_death?: string;
            /**
             * In dutch, the geslachtsnaam
             */
            family_name?: string;
            gender?: 'Male' | 'Female';
            noble_title?: string;
            /**
             * Burgerservicenumber in dutch
             */
            personal_number?: string;
            /**
             * Another 'hidden' identifier in the dutch systems, called a_nummer
             */
            personal_a_number?: string;
            /**
             * Flag to check if person lives in the municipality
             */
            inside_municipality?: boolean;
            /**
             * Flag to check if person has correspondence_address
             */
            has_correspondence_address?: boolean;
            residence_address?: {
              street?: string;
              street_number?: number;
              street_number_suffix?: string;
              street_number_letter?: string;
              zipcode?: string;
              city?: string;
              foreign_address_line1?: string;
              foreign_address_line2?: string;
              foreign_address_line3?: string;
              country?: string;
              municipality?: string;
            };
            correspondence_address?: {
              street?: string;
              street_number?: number;
              street_number_suffix?: string;
              street_number_letter?: string;
              zipcode?: string;
              city?: string;
              foreign_address_line1?: string;
              foreign_address_line2?: string;
              foreign_address_line3?: string;
              country?: string;
              municipality?: string;
            };
            [k: string]: any;
          };
          relationships?: {
            related_custom_object?: {
              links: {
                self: string;
              };
              data: {
                type: 'custom_object';
                id: string;
              };
            };
          };
        })
      | ({
          type?: string;
          id?: string;
          meta?: {
            last_modified_datetime?: string;
            created_datetime?: string;
            summary?: string;
            [k: string]: any;
          };
          attributes?: {
            /**
             * Tells us if this is a cached subject
             */
            is_cache?: boolean;
            /**
             * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
             */
            summary?: string;
            contact_information: {
              email: string | null;
              phone_number: string | null;
              mobile_number: string | null;
              internal_note?: string;
            };
          };
          [k: string]: any;
        } & {
          type: string;
          id: string;
          attributes: {
            /**
             * Name of the organization
             */
            name?: string;
            /**
             * The KVKNumber of this organization, chamber of commerce identifier
             */
            coc_number?: string;
            /**
             * The location number at the chamber of commerce (vestigingsnummer)
             */
            coc_location_number?: string;
            organization_type?: (
              | 'Eenmanszaak'
              | 'Eenmanszaak met meer dan één eigenaar'
              | 'N.V./B.V. in oprichting op A-formulier'
              | 'Rederij'
              | 'Maatschap'
              | 'Vennootschap onder firma'
              | 'N.V/B.V. in oprichting op B-formulier'
              | 'Commanditaire vennootschap met een beherend vennoot'
              | 'Commanditaire vennootschap met meer dan één beherende vennoot'
              | 'N.V./B.V. in oprichting op D-formulier'
              | 'Rechtspersoon in oprichting'
              | 'Besloten vennootschap met gewone structuur'
              | 'Besloten vennootschap blijkens statuten structuurvennootschap'
              | 'Naamloze vennootschap met gewone structuur'
              | 'Naamloze vennootschap blijkens statuten structuurvennootschap'
              | 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal'
              | 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal blijkens statuten structuurvennootschap'
              | 'Europese naamloze vennootschap (SE) met gewone structuur'
              | 'Europese naamloze vennootschap (SE) blijkens statuten structuurvennootschap'
              | 'Coöperatie U.A. met gewone structuur'
              | 'Coöperatie U.A. blijkens statuten structuurcoöperatie'
              | 'Coöperatie W.A. met gewone structuur'
              | 'Coöperatie W.A. blijkens statuten structuurcoöperatie'
              | 'Coöperatie B.A. met gewone structuur'
              | 'Coöperatie B.A. blijkens statuten structuurcoöperatie'
              | 'Vereniging van eigenaars'
              | 'Vereniging met volledige rechtsbevoegdheid'
              | 'Vereniging met beperkte rechtsbevoegdheid'
              | 'Kerkgenootschap'
              | 'Stichting'
              | 'Onderlinge waarborgmaatschappij U.A. met gewone structuur'
              | 'Onderlinge waarborgmaatschappij U.A. blijkens statuten structuuronderlinge'
              | 'Onderlinge waarborgmaatschappij W.A. met gewone structuur'
              | 'Onderlinge waarborgmaatschappij W.A. blijkens statuten structuuronderlinge'
              | 'Onderlinge waarborgmaatschappij B.A. met gewone structuur'
              | 'Onderlinge waarborgmaatschappij B.A. blijkens statuten structuuronderlinge'
              | 'Publiekrechtelijke rechtspersoon'
              | 'Privaatrechtelijke rechtspersoon'
              | 'Buitenlandse rechtsvorm met hoofdvestiging in Nederland'
              | 'Nevenvest. met hoofdvest. in buitenl.'
              | 'Europees economisch samenwerkingsverband'
              | 'Buitenl. EG-venn. met onderneming in Nederland'
              | 'Buitenl. EG-venn. met hoofdnederzetting in Nederland'
              | 'Buitenl. op EG-venn. lijkende venn. met onderneming in Nederland'
              | 'Buitenl. op EG-venn. lijkende venn. met hoofdnederzetting in Nederland'
              | 'Coöperatie'
              | 'Vereniging'
            )[];
            location_address?: {
              street?: string;
              street_number?: number;
              street_number_suffix?: string;
              street_number_letter?: string;
              zipcode?: string;
              city?: string;
              foreign_address_line1?: string;
              foreign_address_line2?: string;
              foreign_address_line3?: string;
              country?: string;
              municipality?: string;
            };
            /**
             * Flag to check if organization has correspondence address
             */
            has_correspondence_address?: boolean;
            correspondence_address?: {
              street?: string;
              street_number?: number;
              street_number_suffix?: string;
              street_number_letter?: string;
              zipcode?: string;
              city?: string;
              foreign_address_line1?: string;
              foreign_address_line2?: string;
              foreign_address_line3?: string;
              country?: string;
              municipality?: string;
            };
            [k: string]: any;
          };
          relationships?: {
            related_custom_object?: {
              links: {
                self: string;
              };
              data: {
                type: 'custom_object';
                id: string;
              };
            };
          };
        });
    [k: string]: any;
  };

  export interface GetRelatedObjectsForSubjectRequestParams {
    subject_uuid: string;
    [k: string]: any;
  }

  export interface GetRelatedObjectsForSubjectResponseBody {
    data: {
      attributes: {
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetRelatedSubjectsForSubjectRequestParams {
    subject_uuid: string;
    [k: string]: any;
  }

  export interface GetRelatedSubjectsForSubjectResponseBody {
    data: {
      attributes: {
        /**
         * Roles of the subject
         */
        roles?: {
          /**
           * Pydantic based Entity object
           *
           * Entity object based on pydantic and the "new way" of creating entities in
           * our minty platform. It has the same functionality as the EntityBase object,
           * but does not depend on it. Migrationpaths are unsure.
           */
          data: {
            attributes: {
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        /**
         * An enumeration.
         */
        type: 'person' | 'organization' | 'employee';
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetSubjectRelationsRequestParams {
    case_uuid: string;
    include?: 'subject'[];
    [k: string]: any;
  }

  export type GetSubjectRelationsResponseBody = ({
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    links?: {
      self?: string;
      prev?: string;
      next?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }) & {
    data: {
      type: string;
      id: string;
      attributes: {
        role: string;
        magic_string_prefix: string;
        /**
         * flag to check if the subject_relation is pip_authorized. Only if the subject_relation is of type person/organization
         */
        authorized: boolean;
        /**
         * permission of subject_relations. Only if subject_relation is of type employee
         */
        permission: 'none' | 'search' | 'read' | 'write';
        /**
         * flag that indicates whether this is a 'preset' subject relation
         */
        is_preset_client: boolean;
        [k: string]: any;
      };
      relationships: {
        subject: {
          data: {
            type?: string;
            id?: string;
            [k: string]: any;
          };
          meta?: {
            name?: string;
            [k: string]: any;
          };
          links?: {
            links?: {
              self: string;
              [k: string]: any;
            };
            [k: string]: any;
          } & {
            related?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        case: {
          data: {
            type?: string;
            id?: string;
            [k: string]: any;
          };
          meta?: {
            name?: string;
            [k: string]: any;
          };
          links?: {
            links?: {
              self: string;
              [k: string]: any;
            };
            [k: string]: any;
          } & {
            related?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      links: {
        links?: {
          self: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
    }[];
    included?: (
      | ({
          type?: string;
          id?: string;
          meta?: {
            last_modified_datetime?: string;
            created_datetime?: string;
            summary?: string;
            [k: string]: any;
          };
          attributes?: {
            /**
             * Tells us if this is a cached subject
             */
            is_cache?: boolean;
            /**
             * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
             */
            summary?: string;
            contact_information: {
              email: string | null;
              phone_number: string | null;
              mobile_number: string | null;
              internal_note?: string;
            };
          };
          [k: string]: any;
        } & {
          type: string;
          id: string;
          attributes: {
            /**
             * The name, for example changed after marriage
             */
            surname?: string;
            first_names?: string;
            /**
             * The prefix of the surname, the 'DE' in 'A. DE boer'
             */
            surname_prefix?: string;
            initials?: string;
            /**
             * List of statuses for this person, e.g.: [secret deceased]
             */
            status?: string[];
            date_of_birth?: string;
            date_of_death?: string;
            /**
             * In dutch, the geslachtsnaam
             */
            family_name?: string;
            gender?: 'Male' | 'Female';
            noble_title?: string;
            /**
             * Burgerservicenumber in dutch
             */
            personal_number?: string;
            /**
             * Another 'hidden' identifier in the dutch systems, called a_nummer
             */
            personal_a_number?: string;
            /**
             * Flag to check if person lives in the municipality
             */
            inside_municipality?: boolean;
            /**
             * Flag to check if person has correspondence_address
             */
            has_correspondence_address?: boolean;
            residence_address?: {
              street?: string;
              street_number?: number;
              street_number_suffix?: string;
              street_number_letter?: string;
              zipcode?: string;
              city?: string;
              foreign_address_line1?: string;
              foreign_address_line2?: string;
              foreign_address_line3?: string;
              country?: string;
              municipality?: string;
            };
            correspondence_address?: {
              street?: string;
              street_number?: number;
              street_number_suffix?: string;
              street_number_letter?: string;
              zipcode?: string;
              city?: string;
              foreign_address_line1?: string;
              foreign_address_line2?: string;
              foreign_address_line3?: string;
              country?: string;
              municipality?: string;
            };
            [k: string]: any;
          };
          relationships?: {
            related_custom_object?: {
              links: {
                self: string;
              };
              data: {
                type: 'custom_object';
                id: string;
              };
            };
          };
        })
      | ({
          type?: string;
          id?: string;
          meta?: {
            last_modified_datetime?: string;
            created_datetime?: string;
            summary?: string;
            [k: string]: any;
          };
          attributes?: {
            /**
             * Tells us if this is a cached subject
             */
            is_cache?: boolean;
            /**
             * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
             */
            summary?: string;
            contact_information: {
              email: string | null;
              phone_number: string | null;
              mobile_number: string | null;
              internal_note?: string;
            };
          };
          [k: string]: any;
        } & {
          type: string;
          id: string;
          attributes: {
            /**
             * The name, for example changed after marriage
             */
            surname?: string;
            first_name?: string;
            /**
             * The prefix of the surname, the 'DE' in 'A. DE boer'
             */
            surname_prefix?: string;
            initials?: string;
            [k: string]: any;
          };
          relationships?: {
            related_custom_object?: {
              links: {
                self: string;
              };
              data: {
                type: 'custom_object';
                id: string;
              };
            };
            department?: {
              data?: {
                id?: string;
                type?: string;
                meta?: {
                  name?: string;
                  [k: string]: any;
                };
              };
            };
            roles?: {
              data?: {
                id?: string;
                type?: string;
                meta?: {
                  name?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              }[];
              [k: string]: any;
            };
          };
        })
      | ({
          type?: string;
          id?: string;
          meta?: {
            last_modified_datetime?: string;
            created_datetime?: string;
            summary?: string;
            [k: string]: any;
          };
          attributes?: {
            /**
             * Tells us if this is a cached subject
             */
            is_cache?: boolean;
            /**
             * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
             */
            summary?: string;
            contact_information: {
              email: string | null;
              phone_number: string | null;
              mobile_number: string | null;
              internal_note?: string;
            };
          };
          [k: string]: any;
        } & {
          type: string;
          id: string;
          attributes: {
            /**
             * Name of the organization
             */
            name?: string;
            /**
             * The KVKNumber of this organization, chamber of commerce identifier
             */
            coc_number?: string;
            /**
             * The location number at the chamber of commerce (vestigingsnummer)
             */
            coc_location_number?: string;
            organization_type?: (
              | 'Eenmanszaak'
              | 'Eenmanszaak met meer dan één eigenaar'
              | 'N.V./B.V. in oprichting op A-formulier'
              | 'Rederij'
              | 'Maatschap'
              | 'Vennootschap onder firma'
              | 'N.V/B.V. in oprichting op B-formulier'
              | 'Commanditaire vennootschap met een beherend vennoot'
              | 'Commanditaire vennootschap met meer dan één beherende vennoot'
              | 'N.V./B.V. in oprichting op D-formulier'
              | 'Rechtspersoon in oprichting'
              | 'Besloten vennootschap met gewone structuur'
              | 'Besloten vennootschap blijkens statuten structuurvennootschap'
              | 'Naamloze vennootschap met gewone structuur'
              | 'Naamloze vennootschap blijkens statuten structuurvennootschap'
              | 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal'
              | 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal blijkens statuten structuurvennootschap'
              | 'Europese naamloze vennootschap (SE) met gewone structuur'
              | 'Europese naamloze vennootschap (SE) blijkens statuten structuurvennootschap'
              | 'Coöperatie U.A. met gewone structuur'
              | 'Coöperatie U.A. blijkens statuten structuurcoöperatie'
              | 'Coöperatie W.A. met gewone structuur'
              | 'Coöperatie W.A. blijkens statuten structuurcoöperatie'
              | 'Coöperatie B.A. met gewone structuur'
              | 'Coöperatie B.A. blijkens statuten structuurcoöperatie'
              | 'Vereniging van eigenaars'
              | 'Vereniging met volledige rechtsbevoegdheid'
              | 'Vereniging met beperkte rechtsbevoegdheid'
              | 'Kerkgenootschap'
              | 'Stichting'
              | 'Onderlinge waarborgmaatschappij U.A. met gewone structuur'
              | 'Onderlinge waarborgmaatschappij U.A. blijkens statuten structuuronderlinge'
              | 'Onderlinge waarborgmaatschappij W.A. met gewone structuur'
              | 'Onderlinge waarborgmaatschappij W.A. blijkens statuten structuuronderlinge'
              | 'Onderlinge waarborgmaatschappij B.A. met gewone structuur'
              | 'Onderlinge waarborgmaatschappij B.A. blijkens statuten structuuronderlinge'
              | 'Publiekrechtelijke rechtspersoon'
              | 'Privaatrechtelijke rechtspersoon'
              | 'Buitenlandse rechtsvorm met hoofdvestiging in Nederland'
              | 'Nevenvest. met hoofdvest. in buitenl.'
              | 'Europees economisch samenwerkingsverband'
              | 'Buitenl. EG-venn. met onderneming in Nederland'
              | 'Buitenl. EG-venn. met hoofdnederzetting in Nederland'
              | 'Buitenl. op EG-venn. lijkende venn. met onderneming in Nederland'
              | 'Buitenl. op EG-venn. lijkende venn. met hoofdnederzetting in Nederland'
              | 'Coöperatie'
              | 'Vereniging'
            )[];
            location_address?: {
              street?: string;
              street_number?: number;
              street_number_suffix?: string;
              street_number_letter?: string;
              zipcode?: string;
              city?: string;
              foreign_address_line1?: string;
              foreign_address_line2?: string;
              foreign_address_line3?: string;
              country?: string;
              municipality?: string;
            };
            /**
             * Flag to check if organization has correspondence address
             */
            has_correspondence_address?: boolean;
            correspondence_address?: {
              street?: string;
              street_number?: number;
              street_number_suffix?: string;
              street_number_letter?: string;
              zipcode?: string;
              city?: string;
              foreign_address_line1?: string;
              foreign_address_line2?: string;
              foreign_address_line3?: string;
              country?: string;
              municipality?: string;
            };
            [k: string]: any;
          };
          relationships?: {
            related_custom_object?: {
              links: {
                self: string;
              };
              data: {
                type: 'custom_object';
                id: string;
              };
            };
          };
        })
    )[];
    [k: string]: any;
  };

  export interface CreateSubjectRelationResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateSubjectRelationRequestBody {
    /**
     * UUID of the case.
     */
    case_uuid: string;
    subject: {
      type: 'employee' | 'organization' | 'person';
      id: string;
    };
    /**
     * Magic string prefix for new related_subject
     */
    magic_string_prefix: string;
    /**
     * Role for new related_subject.
     */
    role: string;
    /**
     * Flag to set pip_authorization for subject_relation. Can set only if subject_relation is of type person/organization.
     */
    authorized?: boolean;
    /**
     * Flag to check if confirmation email needed to be send to the subject.
     */
    send_confirmation_email?: boolean;
    /**
     * Permission of subject_relation. Only if subject_relation is of type employee.
     */
    permission?: 'none' | 'search' | 'read' | 'write';
  }

  export interface GetCaseRelationsRequestParams {
    case_uuid: string;
    include?: 'this_case' | 'other_case' | 'this_case,other_case';
    [k: string]: any;
  }

  export type GetCaseRelationsResponseBody = ({
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    links?: {
      self?: string;
      prev?: string;
      next?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }) & {
    data?: {
      type?: string;
      id?: string;
      attributes?: {
        blocks_deletion?: boolean;
        owner_uuid?: string;
        sequence_number?: number;
        relation_type?: 'related_case' | 'child_case' | 'parent_case';
        [k: string]: any;
      };
      relationships?: {
        this_case?: {
          data?: {
            type?: string;
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        other_case?: {
          data?: {
            type?: string;
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    included?: {
      type?: string;
      id?: string;
      meta?: {
        summary?: string;
        display_id?: number;
        [k: string]: any;
      };
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    [k: string]: any;
  };

  export interface UpdateSubjectRelationResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface UpdateSubjectRelationRequestBody {
    /**
     * UUID of the subject_relation.
     */
    relation_uuid: string;
    /**
     * New magic string for subject_relation
     */
    magic_string_prefix: string;
    /**
     * New role for subject_relation
     */
    role: string;
    /**
     * Flag to set the pip authorization of subject_relation.Can set only if subject_relation is of type person/organization.
     */
    authorized?: boolean;
    /**
     * New permission for subject_relation. Can set only if the subject_relation is of type employee.
     */
    permission?: 'none' | 'search' | 'read' | 'write';
  }

  export interface GetTaskListRequestParams {
    filter?: {
      'relationships.case.id'?: string;
      'attributes.phase'?: number;
      'attributes.completed'?: boolean;
      'relationships.assignee.id'?: string;
      'relationships.department.id'?: string;
    };
    [k: string]: any;
  }

  export type GetTaskListResponseBody = ({
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    links?: {
      self?: string;
      prev?: string;
      next?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }) & {
    data?: {
      type: string;
      id: string;
      meta: {
        is_editable: boolean;
        can_set_completion: boolean;
      };
      attributes: {
        completed: boolean;
        description: string;
        title: string;
        due_date: string;
        phase: number;
      };
      relationships?: {
        case: {
          data: {
            id: string;
            type: 'case';
          };
          meta?: {
            phase?: number;
            display_number?: number;
            [k: string]: any;
          };
        };
        case_type?: {
          data: {
            id: string;
            type: 'case_type';
          };
          meta: {
            display_name: string;
          };
        };
        department?: {
          data: {
            id: string;
            type: 'department';
          };
          meta: {
            display_name: string;
          };
        };
        assignee?: {
          data: {
            id: string;
            type: 'employee';
            meta: {
              display_name: string;
            };
          };
        };
      };
      links?: {
        links?: {
          self: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
    }[];
    [k: string]: any;
  };

  export interface CreateTaskResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateTaskRequestBody {
    case_uuid: string;
    title: string;
    task_uuid: string;
    phase: number;
    [k: string]: any;
  }

  export interface DeleteTaskResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteTaskRequestBody {
    task_uuid: string;
    [k: string]: any;
  }

  export interface UpdateTaskResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface UpdateTaskRequestBody {
    task_uuid: string;
    description?: string;
    title: string;
    due_date: string | null;
    assignee: string | null;
    [k: string]: any;
  }

  export interface SetTaskCompletionResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface SetTaskCompletionRequestBody {
    task_uuid: string;
    completed: boolean;
    [k: string]: any;
  }

  export interface DeleteSubjectRelationResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteSubjectRelationRequestBody {
    /**
     * UUID of the subject_relation.
     */
    relation_uuid: string;
  }

  export interface ReorderCaseRelationRequestParams {
    relation_uuid: string;
    case_uuid: string;
    new_index: string;
    [k: string]: any;
  }

  export interface ReorderCaseRelationResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteCaseRelationResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteCaseRelationRequestBody {
    /**
     * UUID of the case relation to be deleted.
     */
    relation_uuid: string;
    /**
     * UUID of the case to be affected.
     */
    case_uuid: string;
  }

  export interface CreateCustomObjectTypeResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateCustomObjectTypeRequestBody {
    /**
     * Name of this custom_object_type
     */
    name: string;
    /**
     * Internal identifier of this specific custom_object_type version
     */
    uuid: string;
    /**
     * Title for the created custom_object_types
     */
    title?: string;
    /**
     * The current status of this custom_object_type, either 'active' or 'offline'
     */
    status?: 'active' | 'offline';
    /**
     * The catalog folder UUID to store the new custom object in. None will store in root.
     */
    catalog_folder_uuid?: string;
    /**
     * Custom field definition pointing to configured custom fields
     */
    custom_field_definition?: {
      /**
       * List of custom fields
       */
      custom_fields?: {
        /**
         * Label of this field
         */
        label: string;
        /**
         * Name of this field in the catalog
         */
        name: string;
        /**
         * Indicates whether this field is a required field
         */
        is_required?: boolean;
        /**
         * Indicates whether this field is a hidden field, or 'system field'
         */
        is_hidden_field?: boolean;
        /**
         * Description of this field for internal purposes
         */
        description?: string;
        /**
         * Description of this field for public purposes
         */
        external_description?: string;
        /**
         * Type of this custom field
         */
        custom_field_type:
          | 'bankaccount'
          | 'date'
          | 'email'
          | 'geojson'
          | 'option'
          | 'relationship'
          | 'richtext'
          | 'select'
          | 'text'
          | 'textarea'
          | 'url'
          | 'valuta'
          | 'address_v2'
          | 'numeric'
          | 'checkbox';
        /**
         * The magic string of this field
         */
        magic_string?: string;
        /**
         * UUID referencing the field in our attribute catalog
         */
        attribute_uuid?: string;
        /**
         * The CustomField specific attributes for this field
         */
        custom_field_specification?: {
          /**
           * Flag which indicates wether the Geolocation(s) of this custom_field is used in the map of custom_object
           */
          use_on_map?: boolean;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      [k: string]: any;
    };
    /**
     * Authorization settings for this object type
     */
    authorization_definition?: {
      /**
       * Optional list of authorizations for this object
       */
      authorizations?: {
        /**
         * Set of permissions
         */
        authorization?: 'read' | 'readwrite' | 'admin';
        /**
         * Role name
         */
        role: {
          /**
           * Unique identifier for this role
           */
          uuid: string;
          /**
           * Name of the role
           */
          name?: string;
          [k: string]: any;
        };
        /**
         * Group name
         */
        department?: {
          /**
           * Unique identifier for this department
           */
          uuid: string;
          /**
           * Name of the department
           */
          name?: string;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      [k: string]: any;
    };
    /**
     * Relationship settings for this object type
     */
    relationship_definition?: {
      /**
       * Optional list of relationships for this object
       */
      relationships?: {
        /**
         * Name of this relationship in the catalog
         */
        name?: string;
        /**
         * UUID of related Custom Object Type
         */
        custom_object_type_uuid?: string;
        /**
         * UUID of related Case Type
         */
        case_type_uuid?: string;
        /**
         * Description of this field for internal purposes
         */
        description?: string;
        /**
         * Description of this field for public purposes
         */
        external_description?: string;
        /**
         * Indicate that this relationship is required on creation of this object
         */
        is_required?: boolean;
        [k: string]: any;
      }[];
      [k: string]: any;
    };
  }

  export interface UpdateCustomObjectTypeRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export interface UpdateCustomObjectTypeResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface UpdateCustomObjectTypeRequestBody {
    /**
     * Name of this custom_object_type
     */
    name?: string;
    /**
     * Internal identifier of this specific custom_object_type version
     */
    uuid: string;
    /**
     * Title for the created custom_object_types
     */
    title?: string;
    /**
     * The current status of this custom_object_type, either 'active' or 'offline'
     */
    status?: 'active' | 'offline';
    /**
     * Custom field definition pointing to configured custom fields
     */
    custom_field_definition?: {
      /**
       * List of custom fields
       */
      custom_fields?: {
        /**
         * Label of this field
         */
        label: string;
        /**
         * Name of this field in the catalog
         */
        name: string;
        /**
         * Indicates whether this field is a required field
         */
        is_required?: boolean;
        /**
         * Indicates whether this field is a hidden field, or 'system field'
         */
        is_hidden_field?: boolean;
        /**
         * Description of this field for internal purposes
         */
        description?: string;
        /**
         * Description of this field for public purposes
         */
        external_description?: string;
        /**
         * Type of this custom field
         */
        custom_field_type:
          | 'bankaccount'
          | 'date'
          | 'email'
          | 'geojson'
          | 'option'
          | 'relationship'
          | 'richtext'
          | 'select'
          | 'text'
          | 'textarea'
          | 'url'
          | 'valuta'
          | 'address_v2'
          | 'numeric'
          | 'checkbox';
        /**
         * The magic string of this field
         */
        magic_string?: string;
        /**
         * UUID referencing the field in our attribute catalog
         */
        attribute_uuid?: string;
        /**
         * The CustomField specific attributes for this field
         */
        custom_field_specification?: {
          /**
           * Flag which indicates wether the Geolocation(s) of this custom_field is used in the map of custom_object
           */
          use_on_map?: boolean;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      [k: string]: any;
    };
    /**
     * Authorization settings for this object type
     */
    authorization_definition?: {
      /**
       * Optional list of authorizations for this object
       */
      authorizations?: {
        /**
         * Set of permissions
         */
        authorization?: 'read' | 'readwrite' | 'admin';
        /**
         * Role name
         */
        role: {
          /**
           * Unique identifier for this role
           */
          uuid: string;
          /**
           * Name of the role
           */
          name?: string;
          [k: string]: any;
        };
        /**
         * Group name
         */
        department?: {
          /**
           * Unique identifier for this department
           */
          uuid: string;
          /**
           * Name of the department
           */
          name?: string;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      [k: string]: any;
    };
    /**
     * Relationship settings for this object type
     */
    relationship_definition?: {
      /**
       * Optional list of relationships for this object
       */
      relationships?: {
        /**
         * Name of this relationship in the catalog
         */
        name?: string;
        /**
         * UUID of related Custom Object Type
         */
        custom_object_type_uuid?: string;
        /**
         * UUID of related Case Type
         */
        case_type_uuid?: string;
        /**
         * Description of this field for internal purposes
         */
        description?: string;
        /**
         * Description of this field for public purposes
         */
        external_description?: string;
        /**
         * Indicate that this relationship is required on creation of this object
         */
        is_required?: boolean;
        [k: string]: any;
      }[];
      [k: string]: any;
    };
  }

  export interface DeleteCustomObjectTypeResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export type DeleteCustomObjectTypeRequestBody = {
    [k: string]: any;
  };

  export interface CreateCustomObjectResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateCustomObjectRequestBody {
    /**
     * Internal identifier of this specific custom object version
     */
    uuid: string;
    /**
     * The current status of this custom object
     */
    status?: 'active' | 'inactive' | 'draft';
    /**
     * Key-value pair of custom fields
     */
    custom_fields: {
      [k: string]: any;
    };
    /**
     * Archiving metadata for this object
     */
    archive_metadata?: {
      /**
       * Archival status for this custom object
       */
      status?: 'archived' | 'to destroy' | 'to preserve';
      /**
       * Archival ground for this custom object
       */
      ground?: string;
      /**
       * Archival retention for this custom object
       */
      retention?: number;
      [k: string]: any;
    };
    /**
     * Internal identifier of the custom object type
     */
    custom_object_type_uuid?: string;
    related_to?: {
      case?: string[];
      [k: string]: any;
    };
  }

  export interface UpdateCustomObjectRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export interface UpdateCustomObjectResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface UpdateCustomObjectRequestBody {
    /**
     * Internal identifier of this specific custom object version
     */
    uuid: string;
    /**
     * The current status of this custom object
     */
    status?: 'active' | 'inactive' | 'draft';
    /**
     * Key-value pair of custom fields
     */
    custom_fields?: {
      [k: string]: any;
    };
    /**
     * Archiving metadata for this object
     */
    archive_metadata?: {
      /**
       * Archival status for this custom object
       */
      status?: 'archived' | 'to destroy' | 'to preserve';
      /**
       * Archival ground for this custom object
       */
      ground?: string;
      /**
       * Archival retention for this custom object
       */
      retention?: number;
      [k: string]: any;
    };
    /**
     * Internal identifier of the custom object type
     */
    custom_object_type_uuid?: string;
  }

  export interface RelateCustomObjectToRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export interface RelateCustomObjectToResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface RelateCustomObjectToRequestBody {
    cases?: string[];
  }

  export interface UnrelateCustomObjectFromRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export interface UnrelateCustomObjectFromResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface UnrelateCustomObjectFromRequestBody {
    cases?: string[];
  }

  export interface GetCustomObjectTypeRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export interface GetCustomObjectTypeResponseBody {
    /**
     * Definition of an object
     */
    data: {
      attributes: {
        /**
         * Auditlog for this Custom Object Type Version
         */
        audit_log: {
          /**
           * Description of this change
           */
          description: string;
          /**
           * Components affected by this change
           */
          updated_components: (
            | 'attributes'
            | 'authorizations'
            | 'custom_fields'
            | 'relationships'
          )[];
          [k: string]: any;
        };
        /**
         * Authorization settings for this custom object type
         */
        authorization_definition?: {
          /**
           * Optional list of authorizations for this object
           */
          authorizations?: {
            /**
             * An enumeration.
             */
            authorization: 'read' | 'readwrite' | 'admin';
            /**
             * Group name
             */
            department: {
              /**
               * Name of the department
               */
              name: string;
              /**
               * Unique identifier for this department
               */
              uuid: string;
              [k: string]: any;
            };
            /**
             * Role name
             */
            role: {
              /**
               * Name of the role
               */
              name: string;
              /**
               * Unique identifier for this role
               */
              uuid: string;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * Integer of folder in our elements catalog
         */
        catalog_folder_id?: number;
        /**
         * Definition of the related custom fields
         */
        custom_field_definition?: {
          /**
           * List of custom fields
           */
          custom_fields?: {
            /**
             * UUID referencing the field in our attribute catalog
             */
            attribute_uuid?: string;
            /**
             * Specification around a certain custom field type
             */
            custom_field_specification?:
              | {
                  /**
                   * Name of the relationship
                   */
                  name?: string;
                  /**
                   * An enumeration.
                   */
                  type: 'case' | 'custom_object' | 'document' | 'subject';
                  /**
                   * Use the GeoLocation(s) and Addresse(s) of relationship(custom_object) on the map of custom_object.
                   */
                  use_on_map?: boolean;
                  /**
                   * UUID of the relationship type
                   */
                  uuid?: string;
                  [k: string]: any;
                }
              | {
                  /**
                   * Use the address_v2 or geolocation on the map of custom_object.
                   */
                  use_on_map?: boolean;
                  [k: string]: any;
                };
            /**
             * An enumeration.
             */
            custom_field_type?:
              | 'relationship'
              | 'text'
              | 'option'
              | 'select'
              | 'textarea'
              | 'richtext'
              | 'checkbox'
              | 'date'
              | 'geojson'
              | 'valuta'
              | 'email'
              | 'url'
              | 'bankaccount'
              | 'address_v2'
              | 'numeric';
            /**
             * Description of this field for internal purposes
             */
            description?: string;
            /**
             * Description of this field for public purposes
             */
            external_description?: string;
            /**
             * Indicates whether this field is a hidden field, or 'system field'
             */
            is_hidden_field?: boolean;
            /**
             * Indicates whether this field is a required field
             */
            is_required?: boolean;
            /**
             * Label of this field
             */
            label: string;
            /**
             * The magic string of this field
             */
            magic_string?: string;
            /**
             * Name of this field in the catalog
             */
            name: string;
            /**
             * List of possible options for this custom field
             */
            options?: string[];
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * Date this object got created
         */
        date_created: string;
        /**
         * Deleted date for this object
         */
        date_deleted?: string;
        /**
         * An external reference for the object
         */
        external_reference?: string;
        /**
         * Indicates whether this entity is the latest version
         */
        is_active_version: boolean;
        /**
         * Last modified date for this object
         */
        last_modified: string;
        /**
         * Name of this object type
         */
        name: string;
        /**
         * Relationship settings for this custom object type
         */
        relationship_definition?: {
          /**
           * Optional list of relationships for this object
           */
          relationships?: {
            /**
             * UUID of related Case Type
             */
            case_type_uuid?: string;
            /**
             * UUID of related Custom Object Type
             */
            custom_object_type_uuid?: string;
            /**
             * Description of this field for internal purposes
             */
            description?: string;
            /**
             * Description of this field for public purposes
             */
            external_description?: string;
            /**
             * Indicate that this relationship is required on creation of this object
             */
            is_required?: boolean;
            /**
             * Name of this relationship
             */
            name: string;
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * An enumeration.
         */
        status?: 'active' | 'inactive';
        /**
         * Title for the created object types
         */
        title?: string;
        /**
         * The current version of this object type'
         */
        version: number;
        /**
         * The version independent UUID for this object
         */
        version_independent_uuid?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        latest_version?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetPersistentCustomObjectTypeRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export interface GetPersistentCustomObjectTypeResponseBody {
    /**
     * Definition of an object
     */
    data: {
      attributes: {
        /**
         * Auditlog for this Custom Object Type Version
         */
        audit_log: {
          /**
           * Description of this change
           */
          description: string;
          /**
           * Components affected by this change
           */
          updated_components: (
            | 'attributes'
            | 'authorizations'
            | 'custom_fields'
            | 'relationships'
          )[];
          [k: string]: any;
        };
        /**
         * Authorization settings for this custom object type
         */
        authorization_definition?: {
          /**
           * Optional list of authorizations for this object
           */
          authorizations?: {
            /**
             * An enumeration.
             */
            authorization: 'read' | 'readwrite' | 'admin';
            /**
             * Group name
             */
            department: {
              /**
               * Name of the department
               */
              name: string;
              /**
               * Unique identifier for this department
               */
              uuid: string;
              [k: string]: any;
            };
            /**
             * Role name
             */
            role: {
              /**
               * Name of the role
               */
              name: string;
              /**
               * Unique identifier for this role
               */
              uuid: string;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * Integer of folder in our elements catalog
         */
        catalog_folder_id?: number;
        /**
         * Definition of the related custom fields
         */
        custom_field_definition?: {
          /**
           * List of custom fields
           */
          custom_fields?: {
            /**
             * UUID referencing the field in our attribute catalog
             */
            attribute_uuid?: string;
            /**
             * Specification around a certain custom field type
             */
            custom_field_specification?:
              | {
                  /**
                   * Name of the relationship
                   */
                  name?: string;
                  /**
                   * An enumeration.
                   */
                  type: 'case' | 'custom_object' | 'document' | 'subject';
                  /**
                   * Use the GeoLocation(s) and Addresse(s) of relationship(custom_object) on the map of custom_object.
                   */
                  use_on_map?: boolean;
                  /**
                   * UUID of the relationship type
                   */
                  uuid?: string;
                  [k: string]: any;
                }
              | {
                  /**
                   * Use the address_v2 or geolocation on the map of custom_object.
                   */
                  use_on_map?: boolean;
                  [k: string]: any;
                };
            /**
             * An enumeration.
             */
            custom_field_type?:
              | 'relationship'
              | 'text'
              | 'option'
              | 'select'
              | 'textarea'
              | 'richtext'
              | 'checkbox'
              | 'date'
              | 'geojson'
              | 'valuta'
              | 'email'
              | 'url'
              | 'bankaccount'
              | 'address_v2'
              | 'numeric';
            /**
             * Description of this field for internal purposes
             */
            description?: string;
            /**
             * Description of this field for public purposes
             */
            external_description?: string;
            /**
             * Indicates whether this field is a hidden field, or 'system field'
             */
            is_hidden_field?: boolean;
            /**
             * Indicates whether this field is a required field
             */
            is_required?: boolean;
            /**
             * Label of this field
             */
            label: string;
            /**
             * The magic string of this field
             */
            magic_string?: string;
            /**
             * Name of this field in the catalog
             */
            name: string;
            /**
             * List of possible options for this custom field
             */
            options?: string[];
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * Date this object got created
         */
        date_created: string;
        /**
         * Deleted date for this object
         */
        date_deleted?: string;
        /**
         * An external reference for the object
         */
        external_reference?: string;
        /**
         * Indicates whether this entity is the latest version
         */
        is_active_version: boolean;
        /**
         * Last modified date for this object
         */
        last_modified: string;
        /**
         * Name of this object type
         */
        name: string;
        /**
         * Relationship settings for this custom object type
         */
        relationship_definition?: {
          /**
           * Optional list of relationships for this object
           */
          relationships?: {
            /**
             * UUID of related Case Type
             */
            case_type_uuid?: string;
            /**
             * UUID of related Custom Object Type
             */
            custom_object_type_uuid?: string;
            /**
             * Description of this field for internal purposes
             */
            description?: string;
            /**
             * Description of this field for public purposes
             */
            external_description?: string;
            /**
             * Indicate that this relationship is required on creation of this object
             */
            is_required?: boolean;
            /**
             * Name of this relationship
             */
            name: string;
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * An enumeration.
         */
        status?: 'active' | 'inactive';
        /**
         * Title for the created object types
         */
        title?: string;
        /**
         * The current version of this object type'
         */
        version: number;
        /**
         * The version independent UUID for this object
         */
        version_independent_uuid?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        latest_version?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetCustomObjectRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export type GetCustomObjectResponseBody = {
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    /**
     * Content of a user defined Custom Object
     */
    data: {
      attributes: {
        /**
         * Archiving metadata for this object
         */
        archive_metadata: {
          /**
           * Archival ground for this custom object
           */
          ground?: string;
          /**
           * Archival retention for this custom object
           */
          retention?: number;
          /**
           * An enumeration.
           */
          status?: 'archived' | 'to destroy' | 'to preserve';
          [k: string]: any;
        };
        /**
         * Key-value pair of custom fields
         */
        custom_fields?: {
          [k: string]: any;
        };
        /**
         * Date this custom object got created
         */
        date_created: string;
        /**
         * Deleted date for this custom object
         */
        date_deleted?: string;
        /**
         * External reference this specific object
         */
        external_reference?: string;
        /**
         * Indicates whether this entity is the latest version
         */
        is_active_version: boolean;
        /**
         * Last modified date for this custom object
         */
        last_modified: string;
        /**
         * Name of the related object type
         */
        name: string;
        /**
         * An enumeration.
         */
        status?: 'active' | 'inactive' | 'draft';
        /**
         * Title of this specific object
         */
        title: string;
        /**
         * The current version of this custom object
         */
        version: number;
        /**
         * The version independent UUID for this object
         */
        version_independent_uuid?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        cases?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        custom_object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        custom_objects?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        documents?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        subjects?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  };

  export interface GetCustomObjectsRequestParams {
    filter: string;
    [k: string]: any;
  }

  export interface GetCustomObjectsResponseBody {
    data: {
      attributes: {
        /**
         * Archiving metadata for this object
         */
        archive_metadata: {
          /**
           * Archival ground for this custom object
           */
          ground?: string;
          /**
           * Archival retention for this custom object
           */
          retention?: number;
          /**
           * An enumeration.
           */
          status?: 'archived' | 'to destroy' | 'to preserve';
          [k: string]: any;
        };
        /**
         * Key-value pair of custom fields
         */
        custom_fields?: {
          [k: string]: any;
        };
        /**
         * Date this custom object got created
         */
        date_created: string;
        /**
         * Deleted date for this custom object
         */
        date_deleted?: string;
        /**
         * External reference this specific object
         */
        external_reference?: string;
        /**
         * Indicates whether this entity is the latest version
         */
        is_active_version: boolean;
        /**
         * Last modified date for this custom object
         */
        last_modified: string;
        /**
         * Name of the related object type
         */
        name: string;
        /**
         * An enumeration.
         */
        status?: 'active' | 'inactive' | 'draft';
        /**
         * Title of this specific object
         */
        title: string;
        /**
         * The current version of this custom object
         */
        version: number;
        /**
         * The version independent UUID for this object
         */
        version_independent_uuid?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        cases?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        custom_object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        custom_objects?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        documents?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        subjects?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface SearchRequestParams {
    keyword: string;
    max_results?: number;
    max_results_per_type?: number;
    type: (
      | 'case'
      | 'custom_object'
      | 'custom_object_type'
      | 'document'
      | 'employee'
      | 'object_v1'
      | 'organization'
      | 'person'
      | 'case_type'
    )[];
    filter?: string;
    [k: string]: any;
  }

  export interface SearchResponseBody {
    data: {
      attributes: {
        /**
         * Description of the object represented by this search result
         */
        description?: string;
        /**
         * Type of the object represented by this search result
         */
        result_type: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        case?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        custom_object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetRelatedCasesRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export interface GetRelatedCasesResponseBody {
    data: {
      attributes: {
        /**
         * Case ID
         */
        number: number;
        /**
         * Progress status of the case
         */
        progress: number;
        /**
         * Result of the case
         */
        result?: string;
        /**
         * An enumeration.
         */
        status: 'new' | 'open' | 'stalled' | 'resolved';
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        assignee?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        case_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetRelatedObjectsRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export interface GetRelatedObjectsResponseBody {
    data: {
      attributes: {
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export type GetCustomObjectEventLogsResponseBody = ({
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    links?: {
      self?: string;
      prev?: string;
      next?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }) &
    (({
      meta: {
        api_version: number;
        [k: string]: any;
      };
      [k: string]: any;
    } & {
      links?: {
        self?: string;
        prev?: string;
        next?: string;
        [k: string]: any;
      };
      [k: string]: any;
    }) & {
      data: {
        id?: string;
        type?: string;
        meta?: {
          /**
           * Describing summary of this object
           */
          summary?: string;
          [k: string]: any;
        };
        attributes?: {
          id: number;
          type: string;
          date: string;
          user: string;
          description: string;
        };
        links?: {
          links?: {
            self: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      [k: string]: any;
    });

  export interface DeleteCustomObjectResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteCustomObjectRequestBody {
    uuid: string;
    [k: string]: any;
  }

  export interface GetRelatedSubjectsRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export interface GetRelatedSubjectsResponseBody {
    data: {
      attributes: {
        /**
         * Roles of the subject
         */
        roles?: {
          /**
           * Pydantic based Entity object
           *
           * Entity object based on pydantic and the "new way" of creating entities in
           * our minty platform. It has the same functionality as the EntityBase object,
           * but does not depend on it. Migrationpaths are unsure.
           */
          data: {
            attributes: {
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        /**
         * An enumeration.
         */
        type: 'person' | 'organization' | 'employee';
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface RelatedCustomObject {
    links: {
      self: string;
    };
    data: {
      type: 'custom_object';
      id: string;
    };
  }

  export interface CaseEntity {
    data?: {
      type?: string;
      id?: string;
      meta?: {
        last_modified_datetime?: string;
        created_datetime?: string;
        summary?: string;
        [k: string]: any;
      };
      attributes?: {
        /**
         * The public case number of this case
         */
        number?: number;
        /**
         * Short slug (description) of this case
         */
        summary?: string;
        /**
         * Short slug (description) of this case, for public use
         */
        public_summary?: string;
        /**
         * Defines whether this case is closed, open or suspended
         */
        status?: {
          name?: 'new' | 'open' | 'resolved' | 'stalled';
          /**
           * When prematurely closed or stalled, this is the reason
           */
          reason?: string;
          /**
           * When stalled: the since date
           */
          since?: string;
          /**
           * When stalled: the until date
           */
          until?: string;
          [k: string]: any;
        };
        /**
         * The date this case was registered
         */
        registration_date?: string;
        /**
         * Date when this case should be resolved
         */
        target_completion_date?: string;
        /**
         * Date when this case was resolved
         */
        completion_date?: string;
        /**
         * Date when this case is due for destruction
         */
        destruction_date?: string;
        /**
         * The phase and milestone this case is currently in
         */
        phase?: {
          label?: string;
          milestone_label?: string;
          sequence?: number;
          next_sequence?: number;
          [k: string]: any;
        };
        /**
         * Information about the result of this case
         */
        result?: {
          result?: string;
          result_id?: number;
          archival_attributes?: {
            state?: 'vernietigen' | 'overdragen';
            selection_list?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * In which way this case ended in creation
         */
        contactchannel?:
          | 'behandelaar'
          | 'balie'
          | 'telefoon'
          | 'post'
          | 'email'
          | 'webformulier'
          | 'sociale media';
        /**
         * Specifies the payment information of a case
         */
        payment?: {
          amount?: number;
          status?: 'success' | 'failed' | 'pending' | 'offline';
          [k: string]: any;
        };
        /**
         * All user defined properties for this case
         */
        custom_fields?: {
          [k: string]: any;
        };
        /**
         * The confidentiality of this case, which defines the authorization path to use
         */
        confidentiality?: {
          mapped?: string;
          original?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      relationships?: {
        /**
         * Contains the subject (employee) who handles this case
         */
        assignee?: {
          data?: {
            type?: 'employee';
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Contains the coordinator (employee) who handles this case
         */
        coordinator?: {
          data?: {
            type?: 'employee';
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Contains the requestor (employee/company/person) who handles this case
         */
        requestor?: {
          data?: {
            type?: 'employee' | 'person' | 'organization';
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Department this case is currently assigned to
         */
        department?: {
          data?: {
            type?: 'department';
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Location this case references to
         */
        location?: {
          data?: {
            type?: 'location';
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Subject this case references to (most of the time the requestor of the case
         */
        subjects?: {
          data?: {
            type?: 'employee' | 'person' | 'organization';
            id?: string;
            meta?: {
              role?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Case type this case descended from
         */
        casetype?: {
          data?: {
            type?: 'casetype';
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * List of related cases
         */
        related_cases?: {
          data?: {
            type?: 'case';
            id?: string;
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Case relates to, e.g. a subject
       */
      relates_to?: {
        data?: {
          type?: 'subject';
          id?: string;
          [k: string]: any;
        }[];
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export type Subject =
    | ({
        type?: string;
        id?: string;
        meta?: {
          last_modified_datetime?: string;
          created_datetime?: string;
          summary?: string;
          [k: string]: any;
        };
        attributes?: {
          /**
           * Tells us if this is a cached subject
           */
          is_cache?: boolean;
          /**
           * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
           */
          summary?: string;
          contact_information: {
            email: string | null;
            phone_number: string | null;
            mobile_number: string | null;
            internal_note?: string;
          };
        };
        [k: string]: any;
      } & {
        type: string;
        id: string;
        attributes: {
          /**
           * The name, for example changed after marriage
           */
          surname?: string;
          first_name?: string;
          /**
           * The prefix of the surname, the 'DE' in 'A. DE boer'
           */
          surname_prefix?: string;
          initials?: string;
          [k: string]: any;
        };
        relationships?: {
          related_custom_object?: {
            links: {
              self: string;
            };
            data: {
              type: 'custom_object';
              id: string;
            };
          };
          department?: {
            data?: {
              id?: string;
              type?: string;
              meta?: {
                name?: string;
                [k: string]: any;
              };
            };
          };
          roles?: {
            data?: {
              id?: string;
              type?: string;
              meta?: {
                name?: string;
                [k: string]: any;
              };
              [k: string]: any;
            }[];
            [k: string]: any;
          };
        };
      })
    | ({
        type?: string;
        id?: string;
        meta?: {
          last_modified_datetime?: string;
          created_datetime?: string;
          summary?: string;
          [k: string]: any;
        };
        attributes?: {
          /**
           * Tells us if this is a cached subject
           */
          is_cache?: boolean;
          /**
           * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
           */
          summary?: string;
          contact_information: {
            email: string | null;
            phone_number: string | null;
            mobile_number: string | null;
            internal_note?: string;
          };
        };
        [k: string]: any;
      } & {
        type: string;
        id: string;
        attributes: {
          /**
           * The name, for example changed after marriage
           */
          surname?: string;
          first_names?: string;
          /**
           * The prefix of the surname, the 'DE' in 'A. DE boer'
           */
          surname_prefix?: string;
          initials?: string;
          /**
           * List of statuses for this person, e.g.: [secret deceased]
           */
          status?: string[];
          date_of_birth?: string;
          date_of_death?: string;
          /**
           * In dutch, the geslachtsnaam
           */
          family_name?: string;
          gender?: 'Male' | 'Female';
          noble_title?: string;
          /**
           * Burgerservicenumber in dutch
           */
          personal_number?: string;
          /**
           * Another 'hidden' identifier in the dutch systems, called a_nummer
           */
          personal_a_number?: string;
          /**
           * Flag to check if person lives in the municipality
           */
          inside_municipality?: boolean;
          /**
           * Flag to check if person has correspondence_address
           */
          has_correspondence_address?: boolean;
          residence_address?: {
            street?: string;
            street_number?: number;
            street_number_suffix?: string;
            street_number_letter?: string;
            zipcode?: string;
            city?: string;
            foreign_address_line1?: string;
            foreign_address_line2?: string;
            foreign_address_line3?: string;
            country?: string;
            municipality?: string;
          };
          correspondence_address?: {
            street?: string;
            street_number?: number;
            street_number_suffix?: string;
            street_number_letter?: string;
            zipcode?: string;
            city?: string;
            foreign_address_line1?: string;
            foreign_address_line2?: string;
            foreign_address_line3?: string;
            country?: string;
            municipality?: string;
          };
          [k: string]: any;
        };
        relationships?: {
          related_custom_object?: {
            links: {
              self: string;
            };
            data: {
              type: 'custom_object';
              id: string;
            };
          };
        };
      })
    | ({
        type?: string;
        id?: string;
        meta?: {
          last_modified_datetime?: string;
          created_datetime?: string;
          summary?: string;
          [k: string]: any;
        };
        attributes?: {
          /**
           * Tells us if this is a cached subject
           */
          is_cache?: boolean;
          /**
           * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
           */
          summary?: string;
          contact_information: {
            email: string | null;
            phone_number: string | null;
            mobile_number: string | null;
            internal_note?: string;
          };
        };
        [k: string]: any;
      } & {
        type: string;
        id: string;
        attributes: {
          /**
           * Name of the organization
           */
          name?: string;
          /**
           * The KVKNumber of this organization, chamber of commerce identifier
           */
          coc_number?: string;
          /**
           * The location number at the chamber of commerce (vestigingsnummer)
           */
          coc_location_number?: string;
          organization_type?: (
            | 'Eenmanszaak'
            | 'Eenmanszaak met meer dan één eigenaar'
            | 'N.V./B.V. in oprichting op A-formulier'
            | 'Rederij'
            | 'Maatschap'
            | 'Vennootschap onder firma'
            | 'N.V/B.V. in oprichting op B-formulier'
            | 'Commanditaire vennootschap met een beherend vennoot'
            | 'Commanditaire vennootschap met meer dan één beherende vennoot'
            | 'N.V./B.V. in oprichting op D-formulier'
            | 'Rechtspersoon in oprichting'
            | 'Besloten vennootschap met gewone structuur'
            | 'Besloten vennootschap blijkens statuten structuurvennootschap'
            | 'Naamloze vennootschap met gewone structuur'
            | 'Naamloze vennootschap blijkens statuten structuurvennootschap'
            | 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal'
            | 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal blijkens statuten structuurvennootschap'
            | 'Europese naamloze vennootschap (SE) met gewone structuur'
            | 'Europese naamloze vennootschap (SE) blijkens statuten structuurvennootschap'
            | 'Coöperatie U.A. met gewone structuur'
            | 'Coöperatie U.A. blijkens statuten structuurcoöperatie'
            | 'Coöperatie W.A. met gewone structuur'
            | 'Coöperatie W.A. blijkens statuten structuurcoöperatie'
            | 'Coöperatie B.A. met gewone structuur'
            | 'Coöperatie B.A. blijkens statuten structuurcoöperatie'
            | 'Vereniging van eigenaars'
            | 'Vereniging met volledige rechtsbevoegdheid'
            | 'Vereniging met beperkte rechtsbevoegdheid'
            | 'Kerkgenootschap'
            | 'Stichting'
            | 'Onderlinge waarborgmaatschappij U.A. met gewone structuur'
            | 'Onderlinge waarborgmaatschappij U.A. blijkens statuten structuuronderlinge'
            | 'Onderlinge waarborgmaatschappij W.A. met gewone structuur'
            | 'Onderlinge waarborgmaatschappij W.A. blijkens statuten structuuronderlinge'
            | 'Onderlinge waarborgmaatschappij B.A. met gewone structuur'
            | 'Onderlinge waarborgmaatschappij B.A. blijkens statuten structuuronderlinge'
            | 'Publiekrechtelijke rechtspersoon'
            | 'Privaatrechtelijke rechtspersoon'
            | 'Buitenlandse rechtsvorm met hoofdvestiging in Nederland'
            | 'Nevenvest. met hoofdvest. in buitenl.'
            | 'Europees economisch samenwerkingsverband'
            | 'Buitenl. EG-venn. met onderneming in Nederland'
            | 'Buitenl. EG-venn. met hoofdnederzetting in Nederland'
            | 'Buitenl. op EG-venn. lijkende venn. met onderneming in Nederland'
            | 'Buitenl. op EG-venn. lijkende venn. met hoofdnederzetting in Nederland'
            | 'Coöperatie'
            | 'Vereniging'
          )[];
          location_address?: {
            street?: string;
            street_number?: number;
            street_number_suffix?: string;
            street_number_letter?: string;
            zipcode?: string;
            city?: string;
            foreign_address_line1?: string;
            foreign_address_line2?: string;
            foreign_address_line3?: string;
            country?: string;
            municipality?: string;
          };
          /**
           * Flag to check if organization has correspondence address
           */
          has_correspondence_address?: boolean;
          correspondence_address?: {
            street?: string;
            street_number?: number;
            street_number_suffix?: string;
            street_number_letter?: string;
            zipcode?: string;
            city?: string;
            foreign_address_line1?: string;
            foreign_address_line2?: string;
            foreign_address_line3?: string;
            country?: string;
            municipality?: string;
          };
          [k: string]: any;
        };
        relationships?: {
          related_custom_object?: {
            links: {
              self: string;
            };
            data: {
              type: 'custom_object';
              id: string;
            };
          };
        };
      });

  export interface SubjectBase {
    type?: string;
    id?: string;
    meta?: {
      last_modified_datetime?: string;
      created_datetime?: string;
      summary?: string;
      [k: string]: any;
    };
    attributes?: {
      /**
       * Tells us if this is a cached subject
       */
      is_cache?: boolean;
      /**
       * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
       */
      summary?: string;
      contact_information: {
        email: string | null;
        phone_number: string | null;
        mobile_number: string | null;
        internal_note?: string;
      };
    };
    [k: string]: any;
  }

  export type EmployeeEntity = {
    type?: string;
    id?: string;
    meta?: {
      last_modified_datetime?: string;
      created_datetime?: string;
      summary?: string;
      [k: string]: any;
    };
    attributes?: {
      /**
       * Tells us if this is a cached subject
       */
      is_cache?: boolean;
      /**
       * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
       */
      summary?: string;
      contact_information: {
        email: string | null;
        phone_number: string | null;
        mobile_number: string | null;
        internal_note?: string;
      };
    };
    [k: string]: any;
  } & {
    type: string;
    id: string;
    attributes: {
      /**
       * The name, for example changed after marriage
       */
      surname?: string;
      first_name?: string;
      /**
       * The prefix of the surname, the 'DE' in 'A. DE boer'
       */
      surname_prefix?: string;
      initials?: string;
      [k: string]: any;
    };
    relationships?: {
      related_custom_object?: {
        links: {
          self: string;
        };
        data: {
          type: 'custom_object';
          id: string;
        };
      };
      department?: {
        data?: {
          id?: string;
          type?: string;
          meta?: {
            name?: string;
            [k: string]: any;
          };
        };
      };
      roles?: {
        data?: {
          id?: string;
          type?: string;
          meta?: {
            name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        [k: string]: any;
      };
    };
  };

  export type PersonEntity = {
    type?: string;
    id?: string;
    meta?: {
      last_modified_datetime?: string;
      created_datetime?: string;
      summary?: string;
      [k: string]: any;
    };
    attributes?: {
      /**
       * Tells us if this is a cached subject
       */
      is_cache?: boolean;
      /**
       * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
       */
      summary?: string;
      contact_information: {
        email: string | null;
        phone_number: string | null;
        mobile_number: string | null;
        internal_note?: string;
      };
    };
    [k: string]: any;
  } & {
    type: string;
    id: string;
    attributes: {
      /**
       * The name, for example changed after marriage
       */
      surname?: string;
      first_names?: string;
      /**
       * The prefix of the surname, the 'DE' in 'A. DE boer'
       */
      surname_prefix?: string;
      initials?: string;
      /**
       * List of statuses for this person, e.g.: [secret deceased]
       */
      status?: string[];
      date_of_birth?: string;
      date_of_death?: string;
      /**
       * In dutch, the geslachtsnaam
       */
      family_name?: string;
      gender?: 'Male' | 'Female';
      noble_title?: string;
      /**
       * Burgerservicenumber in dutch
       */
      personal_number?: string;
      /**
       * Another 'hidden' identifier in the dutch systems, called a_nummer
       */
      personal_a_number?: string;
      /**
       * Flag to check if person lives in the municipality
       */
      inside_municipality?: boolean;
      /**
       * Flag to check if person has correspondence_address
       */
      has_correspondence_address?: boolean;
      residence_address?: {
        street?: string;
        street_number?: number;
        street_number_suffix?: string;
        street_number_letter?: string;
        zipcode?: string;
        city?: string;
        foreign_address_line1?: string;
        foreign_address_line2?: string;
        foreign_address_line3?: string;
        country?: string;
        municipality?: string;
      };
      correspondence_address?: {
        street?: string;
        street_number?: number;
        street_number_suffix?: string;
        street_number_letter?: string;
        zipcode?: string;
        city?: string;
        foreign_address_line1?: string;
        foreign_address_line2?: string;
        foreign_address_line3?: string;
        country?: string;
        municipality?: string;
      };
      [k: string]: any;
    };
    relationships?: {
      related_custom_object?: {
        links: {
          self: string;
        };
        data: {
          type: 'custom_object';
          id: string;
        };
      };
    };
  };

  export type OrganizationEntity = {
    type?: string;
    id?: string;
    meta?: {
      last_modified_datetime?: string;
      created_datetime?: string;
      summary?: string;
      [k: string]: any;
    };
    attributes?: {
      /**
       * Tells us if this is a cached subject
       */
      is_cache?: boolean;
      /**
       * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
       */
      summary?: string;
      contact_information: {
        email: string | null;
        phone_number: string | null;
        mobile_number: string | null;
        internal_note?: string;
      };
    };
    [k: string]: any;
  } & {
    type: string;
    id: string;
    attributes: {
      /**
       * Name of the organization
       */
      name?: string;
      /**
       * The KVKNumber of this organization, chamber of commerce identifier
       */
      coc_number?: string;
      /**
       * The location number at the chamber of commerce (vestigingsnummer)
       */
      coc_location_number?: string;
      organization_type?: (
        | 'Eenmanszaak'
        | 'Eenmanszaak met meer dan één eigenaar'
        | 'N.V./B.V. in oprichting op A-formulier'
        | 'Rederij'
        | 'Maatschap'
        | 'Vennootschap onder firma'
        | 'N.V/B.V. in oprichting op B-formulier'
        | 'Commanditaire vennootschap met een beherend vennoot'
        | 'Commanditaire vennootschap met meer dan één beherende vennoot'
        | 'N.V./B.V. in oprichting op D-formulier'
        | 'Rechtspersoon in oprichting'
        | 'Besloten vennootschap met gewone structuur'
        | 'Besloten vennootschap blijkens statuten structuurvennootschap'
        | 'Naamloze vennootschap met gewone structuur'
        | 'Naamloze vennootschap blijkens statuten structuurvennootschap'
        | 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal'
        | 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal blijkens statuten structuurvennootschap'
        | 'Europese naamloze vennootschap (SE) met gewone structuur'
        | 'Europese naamloze vennootschap (SE) blijkens statuten structuurvennootschap'
        | 'Coöperatie U.A. met gewone structuur'
        | 'Coöperatie U.A. blijkens statuten structuurcoöperatie'
        | 'Coöperatie W.A. met gewone structuur'
        | 'Coöperatie W.A. blijkens statuten structuurcoöperatie'
        | 'Coöperatie B.A. met gewone structuur'
        | 'Coöperatie B.A. blijkens statuten structuurcoöperatie'
        | 'Vereniging van eigenaars'
        | 'Vereniging met volledige rechtsbevoegdheid'
        | 'Vereniging met beperkte rechtsbevoegdheid'
        | 'Kerkgenootschap'
        | 'Stichting'
        | 'Onderlinge waarborgmaatschappij U.A. met gewone structuur'
        | 'Onderlinge waarborgmaatschappij U.A. blijkens statuten structuuronderlinge'
        | 'Onderlinge waarborgmaatschappij W.A. met gewone structuur'
        | 'Onderlinge waarborgmaatschappij W.A. blijkens statuten structuuronderlinge'
        | 'Onderlinge waarborgmaatschappij B.A. met gewone structuur'
        | 'Onderlinge waarborgmaatschappij B.A. blijkens statuten structuuronderlinge'
        | 'Publiekrechtelijke rechtspersoon'
        | 'Privaatrechtelijke rechtspersoon'
        | 'Buitenlandse rechtsvorm met hoofdvestiging in Nederland'
        | 'Nevenvest. met hoofdvest. in buitenl.'
        | 'Europees economisch samenwerkingsverband'
        | 'Buitenl. EG-venn. met onderneming in Nederland'
        | 'Buitenl. EG-venn. met hoofdnederzetting in Nederland'
        | 'Buitenl. op EG-venn. lijkende venn. met onderneming in Nederland'
        | 'Buitenl. op EG-venn. lijkende venn. met hoofdnederzetting in Nederland'
        | 'Coöperatie'
        | 'Vereniging'
      )[];
      location_address?: {
        street?: string;
        street_number?: number;
        street_number_suffix?: string;
        street_number_letter?: string;
        zipcode?: string;
        city?: string;
        foreign_address_line1?: string;
        foreign_address_line2?: string;
        foreign_address_line3?: string;
        country?: string;
        municipality?: string;
      };
      /**
       * Flag to check if organization has correspondence address
       */
      has_correspondence_address?: boolean;
      correspondence_address?: {
        street?: string;
        street_number?: number;
        street_number_suffix?: string;
        street_number_letter?: string;
        zipcode?: string;
        city?: string;
        foreign_address_line1?: string;
        foreign_address_line2?: string;
        foreign_address_line3?: string;
        country?: string;
        municipality?: string;
      };
      [k: string]: any;
    };
    relationships?: {
      related_custom_object?: {
        links: {
          self: string;
        };
        data: {
          type: 'custom_object';
          id: string;
        };
      };
    };
  };

  export interface AddressObject {
    street?: string;
    street_number?: number;
    street_number_suffix?: string;
    street_number_letter?: string;
    zipcode?: string;
    city?: string;
    foreign_address_line1?: string;
    foreign_address_line2?: string;
    foreign_address_line3?: string;
    country?: string;
    municipality?: string;
  }

  export interface AllocationObject {
    department?: {
      id?: number;
      name?: string;
      [k: string]: any;
    };
    role?: {
      id?: number;
      name?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export type LegalEntityList = (
    | 'Eenmanszaak'
    | 'Eenmanszaak met meer dan één eigenaar'
    | 'N.V./B.V. in oprichting op A-formulier'
    | 'Rederij'
    | 'Maatschap'
    | 'Vennootschap onder firma'
    | 'N.V/B.V. in oprichting op B-formulier'
    | 'Commanditaire vennootschap met een beherend vennoot'
    | 'Commanditaire vennootschap met meer dan één beherende vennoot'
    | 'N.V./B.V. in oprichting op D-formulier'
    | 'Rechtspersoon in oprichting'
    | 'Besloten vennootschap met gewone structuur'
    | 'Besloten vennootschap blijkens statuten structuurvennootschap'
    | 'Naamloze vennootschap met gewone structuur'
    | 'Naamloze vennootschap blijkens statuten structuurvennootschap'
    | 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal'
    | 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal blijkens statuten structuurvennootschap'
    | 'Europese naamloze vennootschap (SE) met gewone structuur'
    | 'Europese naamloze vennootschap (SE) blijkens statuten structuurvennootschap'
    | 'Coöperatie U.A. met gewone structuur'
    | 'Coöperatie U.A. blijkens statuten structuurcoöperatie'
    | 'Coöperatie W.A. met gewone structuur'
    | 'Coöperatie W.A. blijkens statuten structuurcoöperatie'
    | 'Coöperatie B.A. met gewone structuur'
    | 'Coöperatie B.A. blijkens statuten structuurcoöperatie'
    | 'Vereniging van eigenaars'
    | 'Vereniging met volledige rechtsbevoegdheid'
    | 'Vereniging met beperkte rechtsbevoegdheid'
    | 'Kerkgenootschap'
    | 'Stichting'
    | 'Onderlinge waarborgmaatschappij U.A. met gewone structuur'
    | 'Onderlinge waarborgmaatschappij U.A. blijkens statuten structuuronderlinge'
    | 'Onderlinge waarborgmaatschappij W.A. met gewone structuur'
    | 'Onderlinge waarborgmaatschappij W.A. blijkens statuten structuuronderlinge'
    | 'Onderlinge waarborgmaatschappij B.A. met gewone structuur'
    | 'Onderlinge waarborgmaatschappij B.A. blijkens statuten structuuronderlinge'
    | 'Publiekrechtelijke rechtspersoon'
    | 'Privaatrechtelijke rechtspersoon'
    | 'Buitenlandse rechtsvorm met hoofdvestiging in Nederland'
    | 'Nevenvest. met hoofdvest. in buitenl.'
    | 'Europees economisch samenwerkingsverband'
    | 'Buitenl. EG-venn. met onderneming in Nederland'
    | 'Buitenl. EG-venn. met hoofdnederzetting in Nederland'
    | 'Buitenl. op EG-venn. lijkende venn. met onderneming in Nederland'
    | 'Buitenl. op EG-venn. lijkende venn. met hoofdnederzetting in Nederland'
    | 'Coöperatie'
    | 'Vereniging'
  )[];

  export interface CasetypeEntity {
    type: string;
    id: string;
    meta: {
      last_modified: string;
      created: string;
      summary: string;
      is_eligible_for_case_creation: boolean;
    };
    attributes: {
      /**
       * Indicates that this casetype is a 'mother' of child casetypes
       */
      is_parent: boolean;
      /**
       * Fullname of the casetype
       */
      name: string;
      /**
       * Userdefined identification of this casetype
       */
      identification: string;
      /**
       * Describes this casetype in detail
       */
      description: string;
      /**
       * List of tags defining this casetype, used for faster searching
       */
      tags: string;
      /**
       * A templating line of text which defines the summary of a generated case
       */
      case_summary: string;
      /**
       * A templating line of text which defines the summary of a generated case which we use on public communication
       */
      case_public_summary: string;
      settings: {
        custom_webform: string;
        reuse_casedata: boolean;
        enable_webform: boolean;
        enable_online_payment: boolean;
        enable_manual_payment: boolean;
        require_email_on_webform: boolean;
        require_phonenumber_on_webform: boolean;
        require_mobilenumber_on_webform: boolean;
        disable_captcha_for_predefined_requestor: boolean;
        show_confidentiality: boolean;
        show_contact_info: boolean;
        enable_subject_relations_on_form: boolean;
        open_case_on_create: boolean;
        enable_allocation_on_form: boolean;
        disable_pip_for_requestor: boolean;
        lock_registration_phase: boolean;
        enable_queue_for_changes_from_other_subjects: boolean;
        check_acl_on_allocation: boolean;
        api_can_transition: boolean;
        is_public: boolean;
        list_of_default_folders: {
          parent?: string;
          name?: string;
          [k: string]: any;
        }[];
        text_confirmation_message_title: string;
        text_public_confirmation_message: string;
        payment: {
          assignee: {
            amount: number;
          };
          frontdesk: {
            amount: number;
          };
          phone: {
            amount: number;
          };
          mail: {
            amount: number;
          };
          email: {
            amount: number;
          };
          webform: {
            amount: number;
          };
        };
      };
      /**
       * Extra information about this casetype for archiving and other purposes
       */
      metadata: {
        /**
         * Whether postponing a case is a possibility
         */
        may_postpone: boolean;
        /**
         * Whether extending the duration of a case is a possibility
         */
        may_extend: boolean;
        /**
         * Amount of days a case can be extended
         */
        extension_period: number;
        /**
         * Amount of days a case can be adjourned
         */
        adjourn_period: number;
        /**
         * Link to the e-form of this casetype
         */
        'e-webform': string;
        process_description: string;
        /**
         * Motivation of this casetype (NL: Aanleiding)
         */
        motivation: string;
        /**
         * Purpose of this casetype (NL: Doel)
         */
        purpose: string;
        /**
         * Classification for archiving (NL: Archiefclassificatiecode)
         */
        archive_classification_code: string;
        /**
         * level of confidentiality (NL: Vertrouwelijkheidsaanduiding)
         */
        designation_of_confidentiality:
          | 'Openbaar'
          | 'Beperkt openbaar'
          | 'Intern'
          | 'Zaakvertrouwelijk'
          | 'Vertrouwelijk'
          | 'Confidentieel'
          | 'Geheim'
          | 'Zeer geheim';
        /**
         * The person, team, department or other subject responsible (NL: Verantwoordelijke)
         */
        responsible_subject: string;
        /**
         * The relationship between this casetype and a political reason (NL: Verantwoordingsrelatie)
         */
        responsible_relationship: string;
        /**
         * Whether its possible to object and appeal to this casetype
         */
        possibility_for_objection_and_appeal: boolean;
        /**
         * Whether a decision needs to be published
         */
        publication: boolean;
        /**
         * The text used for publishing decisions of this casetype
         */
        publication_text: string;
        bag: boolean;
        /**
         * Will cases from this casetype automatically turn into a positive result for the citizen when this case is not finished in time.
         */
        lex_silencio_positivo: boolean;
        /**
         * Whether a penalty is required (NL: Wet dwangsom)
         */
        penalty_law: boolean;
        /**
         * Whether the 'NL: Wet Publiekrechtelijke Beperkingen' applies
         */
        wkpb_applies: boolean;
        /**
         * The legal basis (NL: Wettelijke grondslag) of this casetype
         */
        legal_basis: string;
        /**
         * The legal basis (NL: Lokale grondslag) of this casetype
         */
        local_basis: string;
      };
      initiator_type:
        | 'aangaan'
        | 'aangeven'
        | 'aanmelden'
        | 'aanschrijven'
        | 'aanvragen'
        | 'afkopen'
        | 'afmelden'
        | 'indienen'
        | 'inschrijven'
        | 'melden'
        | 'ontvangen'
        | 'opstellen'
        | 'opzeggen'
        | 'registreren'
        | 'reserveren'
        | 'starten'
        | 'stellen'
        | 'uitvoeren'
        | 'vaststellen'
        | 'versturen'
        | 'voordragen'
        | 'vragen';
      initiator_source: 'internal' | 'external' | 'all';
      terms: {
        /**
         * The maximum duration of a case according to the law
         */
        lead_time_legal: {
          type: 'werkdagen' | 'weken' | 'kalenderdagen' | 'einddatum';
          /**
           * The value of 'lead_time_legal' a case may take
           */
          value: string | number;
        };
        /**
         * The maximum duration of a case we would like
         */
        lead_time_service: {
          type: 'werkdagen' | 'weken' | 'kalenderdagen' | 'einddatum';
          /**
           * The value of 'lead_time_service' a case may take
           */
          value: string | number;
        };
      };
      requestor: {
        type_of_requestors: (
          | 'niet_natuurlijk_persoon'
          | 'preset_client'
          | 'natuurlijk_persoon_na'
          | 'natuurlijk_persoon'
          | 'medewerker'
        )[];
        predefined_requestor?: string;
        use_for_correspondence: boolean;
      };
      relates_to?: {
        subject?: {
          type_of_requestors?: string[];
          predefined_requestor?: string;
          [k: string]: any;
        };
        location?: {
          location_from?: 'requestor' | 'attribute';
          [k: string]: any;
        };
        [k: string]: any;
      };
      phases: {
        name?: string;
        milestone?: number;
        allocation?: {
          department?: {
            id?: number;
            name?: string;
            [k: string]: any;
          };
          role?: {
            id?: number;
            name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        custom_fields?: {
          name: string;
          public_name: string;
          title: string;
          title_multiple: string;
          description: string;
          is_required: boolean;
          external_description: string;
          publish_on: {
            name: string;
          }[];
          requestor_can_change_from_pip: boolean;
          is_hidden_field: boolean;
          edit_authorizations: {
            [k: string]: any;
          }[];
          enable_skip_of_queue: boolean;
          sensitive_data: boolean;
          field_magic_string: string;
          field_type: 'text' | 'option' | 'checkbox';
          field_options: string[];
          default_value: string;
          referential: boolean;
          date_field_limit: {
            start?: {
              active: boolean;
              interval_type: 'days' | 'weeks' | 'months' | 'years';
              interval: number;
              during: 'pre' | 'post';
              refernce: 'current';
            };
            end?: {
              active: boolean;
              interval_type: 'days' | 'weeks' | 'months' | 'years';
              interval: number;
              during: 'pre' | 'post';
              refernce: 'current';
            };
          };
        }[];
        rules?: {
          label?: string;
          conditional_type?: 'AND' | 'OR';
          when?: {
            condition?: string;
            values?: string[];
            immediate?: boolean;
            [k: string]: any;
          };
          then?: {
            action?: string;
            attribute?: string;
            [k: string]: any;
          };
          else?: {
            action?: string;
            attribute?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        document_templates?: {
          related_document_template_element?: string;
          description?: string;
          generate_on_transition?: boolean;
          /**
           * Add to this custom field of type 'document'
           */
          add_to_case_document?: string;
          storage_format?: 'odt' | 'pdf' | 'docx';
          [k: string]: any;
        }[];
        cases?: {
          type_of_relation?:
            | 'deelzaak'
            | 'gerelateerd'
            | 'vervolgzaak'
            | 'vervolgzaak_datum';
          related_casetype_element?: string;
          allocation?: {
            department?: {
              id?: number;
              name?: string;
              [k: string]: any;
            };
            role?: {
              id?: number;
              name?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          requestor?: {
            requestor_type?:
              | 'aanvrager'
              | 'betrokkene'
              | 'anders'
              | 'behandelaar'
              | 'ontvanger';
            related_role?: string;
            /**
             * Not implemented yet
             */
            custom_subject?: {
              subject_type?: 'person' | 'organization';
              id?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * Phase number before which this case must be resolved
           */
          resolve_before_phase?: number;
          copy_custom_fields_from_parent?: boolean;
          start_on_transition?: boolean;
          /**
           * Automatically open the case (in behandeling)
           */
          open_case_on_create?: boolean;
          /**
           * Not implemented yet
           */
          related_subject?: {
            subject?: {
              subject_type?: 'person' | 'organization';
              id?: string;
              [k: string]: any;
            };
            role?: string;
            magic_string_prefix?: string;
            /**
             * Authorized to view this case on the PIP (NL: gemachtigde)
             */
            authorized_subject?: boolean;
            send_confirmation_message?: boolean;
            [k: string]: any;
          };
          show_in_pip?: boolean;
          label_in_pip?: string;
          [k: string]: any;
        }[];
        emails?: {
          related_email_element?: string;
          recipient?: {
            recipient_type?:
              | 'requestor'
              | 'assignee'
              | 'coordinator'
              | 'employee'
              | 'authorized_subject'
              | 'related_subject'
              | 'custom';
            id?: string;
            role?: string;
            emailaddress?: string;
            [k: string]: any;
          };
          cc?: string;
          bcc?: string;
          send_on_transition?: boolean;
          [k: string]: any;
        }[];
        subjects?: {
          subject_type?: 'person' | 'employee';
          id?: string;
          role?: string;
          magic_string_prefix?: string;
          authorized_subject?: boolean;
          send_confirmation_message?: boolean;
          [k: string]: any;
        }[];
        results?: {
          title?: string;
          is_default_value?: boolean;
          result?: string;
          archival_attributes?: {
            state?: 'vernietigen' | 'overdragen';
            selection_list?: string;
            selection_list_source_date?: string;
            selection_list_end_date?: string;
            activate_period_of_preservation?: boolean;
            type_of_archiving?: 'vernietigen';
            period_of_preservation?: string;
            archiving_procedure?: 'Afhandeling';
            description?: string;
            selection_list_number?: string;
            process_type_number?: string;
            process_type_name?: string;
            process_type_description?: string;
            process_type_object?: string;
            process_type_generic?: 'generic' | 'specific';
            origin?: string;
            process_term?: 'A' | 'B' | 'C' | 'D' | 'E';
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        checklist_items?: string[];
        [k: string]: any;
      }[];
      authorizations?: {
        [k: string]: any;
      }[];
      changelog?: {
        summary?: string;
        changes?: {
          [k: string]: any;
        }[];
        [k: string]: any;
      };
    };
    relationships?: {
      /**
       * In which folder this casetype resides
       */
      catalogue_folder?: {
        data?: {
          type?: 'folder';
          id?: string;
          meta?: {
            name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Related parent casetype
       */
      parent?: {
        data?: {
          type?: 'casetype';
          id?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * List of child casetypes of this mother casetype
       */
      children?: {
        data?: {
          type?: 'casetype';
          id?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
  }

  export interface JsonAPILinks {
    links?: {
      self: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  /**
   * Department users can be a part of
   */
  export interface DepartmentEntity {
    type: 'department';
    meta?: {
      summary: string;
      [k: string]: any;
    };
    id: string;
    attributes?: {
      name: string;
      description?: string;
      [k: string]: any;
    };
    relationships?: {
      parent?: {
        type: 'department';
        id: string;
        meta?: {
          summary: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  /**
   * Role users of the system can have
   */
  export interface RoleEntity {
    type: 'role';
    id: string;
    attributes?: {
      name: string;
      description?: string;
      [k: string]: any;
    };
    relationships?: {
      parent?: {
        type: 'department';
        id: string;
        meta?: {
          summary: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface RelationshipObject {
    data: {
      type?: string;
      id?: string;
      [k: string]: any;
    };
    meta?: {
      name?: string;
      [k: string]: any;
    };
    links?: {
      links?: {
        self: string;
        [k: string]: any;
      };
      [k: string]: any;
    } & {
      related?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface SubjectRelationEntity {
    type: string;
    id: string;
    attributes: {
      role: string;
      magic_string_prefix: string;
      /**
       * flag to check if the subject_relation is pip_authorized. Only if the subject_relation is of type person/organization
       */
      authorized: boolean;
      /**
       * permission of subject_relations. Only if subject_relation is of type employee
       */
      permission: 'none' | 'search' | 'read' | 'write';
      /**
       * flag that indicates whether this is a 'preset' subject relation
       */
      is_preset_client: boolean;
      [k: string]: any;
    };
    relationships: {
      subject: {
        data: {
          type?: string;
          id?: string;
          [k: string]: any;
        };
        meta?: {
          name?: string;
          [k: string]: any;
        };
        links?: {
          links?: {
            self: string;
            [k: string]: any;
          };
          [k: string]: any;
        } & {
          related?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      case: {
        data: {
          type?: string;
          id?: string;
          [k: string]: any;
        };
        meta?: {
          name?: string;
          [k: string]: any;
        };
        links?: {
          links?: {
            self: string;
            [k: string]: any;
          };
          [k: string]: any;
        } & {
          related?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    links: {
      links?: {
        self: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
  }

  export interface TaskEntity {
    type: string;
    id: string;
    meta: {
      is_editable: boolean;
      can_set_completion: boolean;
    };
    attributes: {
      completed: boolean;
      description: string;
      title: string;
      due_date: string;
      phase: number;
    };
    relationships?: {
      case: {
        data: {
          id: string;
          type: 'case';
        };
        meta?: {
          phase?: number;
          display_number?: number;
          [k: string]: any;
        };
      };
      case_type?: {
        data: {
          id: string;
          type: 'case_type';
        };
        meta: {
          display_name: string;
        };
      };
      department?: {
        data: {
          id: string;
          type: 'department';
        };
        meta: {
          display_name: string;
        };
      };
      assignee?: {
        data: {
          id: string;
          type: 'employee';
          meta: {
            display_name: string;
          };
        };
      };
    };
    links?: {
      links?: {
        self: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
  }

  /**
   * Definition of an object
   */
  export interface CustomObjectType {
    type: string;
    id: string;
    meta: {
      /**
       * Describing summary of this object
       */
      summary?: string;
      [k: string]: any;
    };
    attributes: {
      /**
       * Name of this custom object type
       */
      name?: string;
      /**
       * Internal identifier of this specific custom object type version
       */
      uuid?: string;
      /**
       * Title for the created custom object type
       */
      title: string;
      /**
       * The current status of this custom object type
       */
      status?: 'active' | 'inactive';
      /**
       * The current version of this custom object type'
       */
      version?: number;
      /**
       * Date this object got created
       */
      date_created?: string;
      /**
       * Last modified date for this object
       */
      last_modified?: string;
      /**
       * Deleted date for this object
       */
      date_deleted?: string;
      /**
       * The version independent UUID for this object
       */
      version_independent_uuid?: string;
      /**
       * Indicates whether this entity is the latest version
       */
      is_active_version?: boolean;
      /**
       * Definition of the related custom fields
       */
      custom_field_definition?: {
        /**
         * List of custom fields
         */
        custom_fields?: {
          /**
           * Label of this field
           */
          label: string;
          /**
           * Name of this field in the catalog
           */
          name: string;
          /**
           * Indicates whether this field is a required field
           */
          is_required?: boolean;
          /**
           * Indicates whether this field is a hidden field, or 'system field'
           */
          is_hidden_field?: boolean;
          /**
           * Description of this field for internal purposes
           */
          description?: string;
          /**
           * Description of this field for public purposes
           */
          external_description?: string;
          /**
           * Type of this custom field
           */
          custom_field_type:
            | 'bankaccount'
            | 'date'
            | 'email'
            | 'geojson'
            | 'option'
            | 'relationship'
            | 'richtext'
            | 'select'
            | 'text'
            | 'textarea'
            | 'url'
            | 'valuta'
            | 'address_v2'
            | 'numeric'
            | 'checkbox';
          /**
           * The magic string of this field
           */
          magic_string?: string;
          /**
           * UUID referencing the field in our attribute catalog
           */
          attribute_uuid?: string;
          /**
           * The CustomField specific attributes for this field
           */
          custom_field_specification?: {
            /**
             * Flag which indicates wether the Geolocation(s) of this custom_field is used in the map of custom_object
             */
            use_on_map?: boolean;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        [k: string]: any;
      };
      /**
       * Authorization settings for this custom object type
       */
      authorization_definition?: {
        /**
         * Optional list of authorizations for this object
         */
        authorizations?: {
          /**
           * Set of permissions
           */
          authorization?: 'read' | 'readwrite' | 'admin';
          /**
           * Role name
           */
          role: {
            /**
             * Unique identifier for this role
             */
            uuid: string;
            /**
             * Name of the role
             */
            name?: string;
            [k: string]: any;
          };
          /**
           * Group name
           */
          department?: {
            /**
             * Unique identifier for this department
             */
            uuid: string;
            /**
             * Name of the department
             */
            name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        [k: string]: any;
      };
    };
    relationships?: {
      latest_version?: {
        data: {
          id: string;
          type: 'custom_object_type';
        };
        links?: {
          links?: {
            self: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
      };
    };
    links?: {
      links?: {
        self: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
  }

  /**
   * Definition of a object type related custom field
   */
  export interface CustomObjectTypeCustomField {
    /**
     * Label of this field
     */
    label: string;
    /**
     * Name of this field in the catalog
     */
    name: string;
    /**
     * Indicates whether this field is a required field
     */
    is_required?: boolean;
    /**
     * Indicates whether this field is a hidden field, or 'system field'
     */
    is_hidden_field?: boolean;
    /**
     * Description of this field for internal purposes
     */
    description?: string;
    /**
     * Description of this field for public purposes
     */
    external_description?: string;
    /**
     * Type of this custom field
     */
    custom_field_type:
      | 'bankaccount'
      | 'date'
      | 'email'
      | 'geojson'
      | 'option'
      | 'relationship'
      | 'richtext'
      | 'select'
      | 'text'
      | 'textarea'
      | 'url'
      | 'valuta'
      | 'address_v2'
      | 'numeric'
      | 'checkbox';
    /**
     * The magic string of this field
     */
    magic_string?: string;
    /**
     * UUID referencing the field in our attribute catalog
     */
    attribute_uuid?: string;
    /**
     * The CustomField specific attributes for this field
     */
    custom_field_specification?: {
      /**
       * Flag which indicates wether the Geolocation(s) of this custom_field is used in the map of custom_object
       */
      use_on_map?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  /**
   * Custom field definition pointing to configured custom fields
   */
  export interface CustomObjectTypeCustomFieldDefinition {
    /**
     * List of custom fields
     */
    custom_fields?: {
      /**
       * Label of this field
       */
      label: string;
      /**
       * Name of this field in the catalog
       */
      name: string;
      /**
       * Indicates whether this field is a required field
       */
      is_required?: boolean;
      /**
       * Indicates whether this field is a hidden field, or 'system field'
       */
      is_hidden_field?: boolean;
      /**
       * Description of this field for internal purposes
       */
      description?: string;
      /**
       * Description of this field for public purposes
       */
      external_description?: string;
      /**
       * Type of this custom field
       */
      custom_field_type:
        | 'bankaccount'
        | 'date'
        | 'email'
        | 'geojson'
        | 'option'
        | 'relationship'
        | 'richtext'
        | 'select'
        | 'text'
        | 'textarea'
        | 'url'
        | 'valuta'
        | 'address_v2'
        | 'numeric'
        | 'checkbox';
      /**
       * The magic string of this field
       */
      magic_string?: string;
      /**
       * UUID referencing the field in our attribute catalog
       */
      attribute_uuid?: string;
      /**
       * The CustomField specific attributes for this field
       */
      custom_field_specification?: {
        /**
         * Flag which indicates wether the Geolocation(s) of this custom_field is used in the map of custom_object
         */
        use_on_map?: boolean;
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    [k: string]: any;
  }

  /**
   * Authorization settings for this object type
   */
  export interface CustomObjectTypeAuthorization {
    /**
     * Set of permissions
     */
    authorization?: 'read' | 'readwrite' | 'admin';
    /**
     * Role name
     */
    role: {
      /**
       * Unique identifier for this role
       */
      uuid: string;
      /**
       * Name of the role
       */
      name?: string;
      [k: string]: any;
    };
    /**
     * Group name
     */
    department?: {
      /**
       * Unique identifier for this department
       */
      uuid: string;
      /**
       * Name of the department
       */
      name?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  /**
   * A role in the organization
   */
  export interface CustomObjectTypeRole {
    /**
     * Unique identifier for this role
     */
    uuid: string;
    /**
     * Name of the role
     */
    name?: string;
    [k: string]: any;
  }

  /**
   * A department within an organization
   */
  export interface CustomObjectTypeDepartment {
    /**
     * Unique identifier for this department
     */
    uuid: string;
    /**
     * Name of the department
     */
    name?: string;
    [k: string]: any;
  }

  /**
   * Authorization settings for this object type
   */
  export interface CustomObjectTypeAuthorizationDefinition {
    /**
     * Optional list of authorizations for this object
     */
    authorizations?: {
      /**
       * Set of permissions
       */
      authorization?: 'read' | 'readwrite' | 'admin';
      /**
       * Role name
       */
      role: {
        /**
         * Unique identifier for this role
         */
        uuid: string;
        /**
         * Name of the role
         */
        name?: string;
        [k: string]: any;
      };
      /**
       * Group name
       */
      department?: {
        /**
         * Unique identifier for this department
         */
        uuid: string;
        /**
         * Name of the department
         */
        name?: string;
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    [k: string]: any;
  }

  /**
   * Authorization settings for this object type
   */
  export interface CustomObjectTypeRelationship {
    /**
     * Name of this relationship in the catalog
     */
    name?: string;
    /**
     * UUID of related Custom Object Type
     */
    custom_object_type_uuid?: string;
    /**
     * UUID of related Case Type
     */
    case_type_uuid?: string;
    /**
     * Description of this field for internal purposes
     */
    description?: string;
    /**
     * Description of this field for public purposes
     */
    external_description?: string;
    /**
     * Indicate that this relationship is required on creation of this object
     */
    is_required?: boolean;
    [k: string]: any;
  }

  /**
   * Relationship settings for this object type
   */
  export interface CustomObjectTypeRelationshipDefinition {
    /**
     * Optional list of relationships for this object
     */
    relationships?: {
      /**
       * Name of this relationship in the catalog
       */
      name?: string;
      /**
       * UUID of related Custom Object Type
       */
      custom_object_type_uuid?: string;
      /**
       * UUID of related Case Type
       */
      case_type_uuid?: string;
      /**
       * Description of this field for internal purposes
       */
      description?: string;
      /**
       * Description of this field for public purposes
       */
      external_description?: string;
      /**
       * Indicate that this relationship is required on creation of this object
       */
      is_required?: boolean;
      [k: string]: any;
    }[];
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface CustomObjectArchiveMetadata {
    /**
     * Archival status for this custom object
     */
    status?: 'archived' | 'to destroy' | 'to preserve';
    /**
     * Archival ground for this custom object
     */
    ground?: string;
    /**
     * Archival retention for this custom object
     */
    retention?: number;
    [k: string]: any;
  }

  /**
   * A custom object
   */
  export interface CustomObject {
    type: string;
    id: string;
    meta: {
      /**
       * Describing summary of this object
       */
      summary?: string;
      [k: string]: any;
    };
    attributes: {
      /**
       * Name of the related object type
       */
      name?: string;
      /**
       * Title of this specific object
       */
      title?: string;
      /**
       * Internal identifier of this specific custom object version
       */
      uuid?: string;
      /**
       * The current status of this custom object
       */
      status?: 'active' | 'inactive' | 'draft';
      /**
       * The current version of this custom object
       */
      version?: number;
      /**
       * Date this custom object got created
       */
      date_created?: string;
      /**
       * Last modified date for this custom object
       */
      last_modified?: string;
      /**
       * Deleted date for this custom object
       */
      date_deleted?: string;
      /**
       * The version independent UUID for this object
       */
      version_independent_uuid?: string;
      /**
       * Indicates whether this entity is the latest version
       */
      is_active_version?: boolean;
      /**
       * Key-value pair of custom fields
       */
      custom_fields?: {
        [k: string]: any;
      };
      /**
       * Archiving metadata for this object
       */
      archive_metadata?: {
        /**
         * Archival status for this custom object
         */
        status?: 'archived' | 'to destroy' | 'to preserve';
        /**
         * Archival ground for this custom object
         */
        ground?: string;
        /**
         * Archival retention for this custom object
         */
        retention?: number;
        [k: string]: any;
      };
      [k: string]: any;
    };
    relationships?: {
      custom_object_type?: {
        data: {
          id: string;
          type: 'custom_object_type';
        };
        links?: {
          links?: {
            self: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
      };
    };
  }

  export interface TimelineEntity {
    id?: string;
    type?: string;
    meta?: {
      /**
       * Describing summary of this object
       */
      summary?: string;
      [k: string]: any;
    };
    attributes?: {
      id: number;
      type: string;
      date: string;
      user: string;
      description: string;
    };
    links?: {
      links?: {
        self: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  /**
   * An enumeration.
   */
  export type DefAuthorization = 'read' | 'readwrite' | 'admin';

  /**
   * Definition of a object type related custom field
   */
  export interface DefCustomField {
    /**
     * UUID referencing the field in our attribute catalog
     */
    attribute_uuid?: string;
    /**
     * Specification around a certain custom field type
     */
    custom_field_specification?:
      | {
          /**
           * Name of the relationship
           */
          name?: string;
          /**
           * An enumeration.
           */
          type: 'case' | 'custom_object' | 'document' | 'subject';
          /**
           * Use the GeoLocation(s) and Addresse(s) of relationship(custom_object) on the map of custom_object.
           */
          use_on_map?: boolean;
          /**
           * UUID of the relationship type
           */
          uuid?: string;
          [k: string]: any;
        }
      | {
          /**
           * Use the address_v2 or geolocation on the map of custom_object.
           */
          use_on_map?: boolean;
          [k: string]: any;
        };
    /**
     * An enumeration.
     */
    custom_field_type?:
      | 'relationship'
      | 'text'
      | 'option'
      | 'select'
      | 'textarea'
      | 'richtext'
      | 'checkbox'
      | 'date'
      | 'geojson'
      | 'valuta'
      | 'email'
      | 'url'
      | 'bankaccount'
      | 'address_v2'
      | 'numeric';
    /**
     * Description of this field for internal purposes
     */
    description?: string;
    /**
     * Description of this field for public purposes
     */
    external_description?: string;
    /**
     * Indicates whether this field is a hidden field, or 'system field'
     */
    is_hidden_field?: boolean;
    /**
     * Indicates whether this field is a required field
     */
    is_required?: boolean;
    /**
     * Label of this field
     */
    label: string;
    /**
     * The magic string of this field
     */
    magic_string?: string;
    /**
     * Name of this field in the catalog
     */
    name: string;
    /**
     * List of possible options for this custom field
     */
    options?: string[];
    [k: string]: any;
  }

  /**
   * Custom field definition pointing to configured custom fields
   */
  export interface DefCustomFieldDefinition {
    /**
     * List of custom fields
     */
    custom_fields?: {
      /**
       * UUID referencing the field in our attribute catalog
       */
      attribute_uuid?: string;
      /**
       * Specification around a certain custom field type
       */
      custom_field_specification?:
        | {
            /**
             * Name of the relationship
             */
            name?: string;
            /**
             * An enumeration.
             */
            type: 'case' | 'custom_object' | 'document' | 'subject';
            /**
             * Use the GeoLocation(s) and Addresse(s) of relationship(custom_object) on the map of custom_object.
             */
            use_on_map?: boolean;
            /**
             * UUID of the relationship type
             */
            uuid?: string;
            [k: string]: any;
          }
        | {
            /**
             * Use the address_v2 or geolocation on the map of custom_object.
             */
            use_on_map?: boolean;
            [k: string]: any;
          };
      /**
       * An enumeration.
       */
      custom_field_type?:
        | 'relationship'
        | 'text'
        | 'option'
        | 'select'
        | 'textarea'
        | 'richtext'
        | 'checkbox'
        | 'date'
        | 'geojson'
        | 'valuta'
        | 'email'
        | 'url'
        | 'bankaccount'
        | 'address_v2'
        | 'numeric';
      /**
       * Description of this field for internal purposes
       */
      description?: string;
      /**
       * Description of this field for public purposes
       */
      external_description?: string;
      /**
       * Indicates whether this field is a hidden field, or 'system field'
       */
      is_hidden_field?: boolean;
      /**
       * Indicates whether this field is a required field
       */
      is_required?: boolean;
      /**
       * Label of this field
       */
      label: string;
      /**
       * The magic string of this field
       */
      magic_string?: string;
      /**
       * Name of this field in the catalog
       */
      name: string;
      /**
       * List of possible options for this custom field
       */
      options?: string[];
      [k: string]: any;
    }[];
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCustomFieldMapSpecification {
    /**
     * Use the address_v2 or geolocation on the map of custom_object.
     */
    use_on_map?: boolean;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCustomFieldRelationshipSpecification {
    /**
     * Name of the relationship
     */
    name?: string;
    /**
     * An enumeration.
     */
    type: 'case' | 'custom_object' | 'document' | 'subject';
    /**
     * Use the GeoLocation(s) and Addresse(s) of relationship(custom_object) on the map of custom_object.
     */
    use_on_map?: boolean;
    /**
     * UUID of the relationship type
     */
    uuid?: string;
    [k: string]: any;
  }

  /**
   * An enumeration.
   */
  export type DefCustomFieldRelationshipTypes =
    | 'case'
    | 'custom_object'
    | 'document'
    | 'subject';

  /**
   * An enumeration.
   */
  export type DefCustomFieldTypes =
    | 'relationship'
    | 'text'
    | 'option'
    | 'select'
    | 'textarea'
    | 'richtext'
    | 'checkbox'
    | 'date'
    | 'geojson'
    | 'valuta'
    | 'email'
    | 'url'
    | 'bankaccount'
    | 'address_v2'
    | 'numeric';

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCustomObjectArchiveMetadata {
    /**
     * Archival ground for this custom object
     */
    ground?: string;
    /**
     * Archival retention for this custom object
     */
    retention?: number;
    /**
     * An enumeration.
     */
    status?: 'archived' | 'to destroy' | 'to preserve';
    [k: string]: any;
  }

  /**
   * Auditlog for this Custom Object Type
   */
  export interface DefCustomObjectTypeAuditLog {
    /**
     * Description of this change
     */
    description: string;
    /**
     * Components affected by this change
     */
    updated_components: (
      | 'attributes'
      | 'authorizations'
      | 'custom_fields'
      | 'relationships'
    )[];
    [k: string]: any;
  }

  /**
   * Authorization settings for this object type
   */
  export interface DefCustomObjectTypeAuthorization {
    /**
     * An enumeration.
     */
    authorization: 'read' | 'readwrite' | 'admin';
    /**
     * Group name
     */
    department: {
      /**
       * Name of the department
       */
      name: string;
      /**
       * Unique identifier for this department
       */
      uuid: string;
      [k: string]: any;
    };
    /**
     * Role name
     */
    role: {
      /**
       * Name of the role
       */
      name: string;
      /**
       * Unique identifier for this role
       */
      uuid: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  /**
   * Authorization settings for this object type
   */
  export interface DefCustomObjectTypeAuthorizationDefinition {
    /**
     * Optional list of authorizations for this object
     */
    authorizations?: {
      /**
       * An enumeration.
       */
      authorization: 'read' | 'readwrite' | 'admin';
      /**
       * Group name
       */
      department: {
        /**
         * Name of the department
         */
        name: string;
        /**
         * Unique identifier for this department
         */
        uuid: string;
        [k: string]: any;
      };
      /**
       * Role name
       */
      role: {
        /**
         * Name of the role
         */
        name: string;
        /**
         * Unique identifier for this role
         */
        uuid: string;
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    [k: string]: any;
  }

  /**
   * A department within an organization
   */
  export interface DefCustomObjectTypeDepartment {
    /**
     * Name of the department
     */
    name: string;
    /**
     * Unique identifier for this department
     */
    uuid: string;
    [k: string]: any;
  }

  /**
   * Authorization settings for this object type
   */
  export interface DefCustomObjectTypeRelationship {
    /**
     * UUID of related Case Type
     */
    case_type_uuid?: string;
    /**
     * UUID of related Custom Object Type
     */
    custom_object_type_uuid?: string;
    /**
     * Description of this field for internal purposes
     */
    description?: string;
    /**
     * Description of this field for public purposes
     */
    external_description?: string;
    /**
     * Indicate that this relationship is required on creation of this object
     */
    is_required?: boolean;
    /**
     * Name of this relationship
     */
    name: string;
    [k: string]: any;
  }

  /**
   * Relationship settings for this object type
   */
  export interface DefCustomObjectTypeRelationshipDefinition {
    /**
     * Optional list of relationships for this object
     */
    relationships?: {
      /**
       * UUID of related Case Type
       */
      case_type_uuid?: string;
      /**
       * UUID of related Custom Object Type
       */
      custom_object_type_uuid?: string;
      /**
       * Description of this field for internal purposes
       */
      description?: string;
      /**
       * Description of this field for public purposes
       */
      external_description?: string;
      /**
       * Indicate that this relationship is required on creation of this object
       */
      is_required?: boolean;
      /**
       * Name of this relationship
       */
      name: string;
      [k: string]: any;
    }[];
    [k: string]: any;
  }

  /**
   * A role in the organization
   */
  export interface DefCustomObjectTypeRole {
    /**
     * Name of the role
     */
    name: string;
    /**
     * Unique identifier for this role
     */
    uuid: string;
    [k: string]: any;
  }

  /**
   * An enumeration.
   */
  export type DefRelatedSubjectTypes = 'person' | 'organization' | 'employee';

  /**
   * An enumeration.
   */
  export type DefValidArchiveStatus = 'archived' | 'to destroy' | 'to preserve';

  /**
   * An enumeration.
   */
  export type DefValidAuditLogComponents =
    | 'attributes'
    | 'authorizations'
    | 'custom_fields'
    | 'relationships';

  /**
   * An enumeration.
   */
  export type DefValidCaseStatus = 'new' | 'open' | 'stalled' | 'resolved';

  /**
   * An enumeration.
   */
  export type DefValidObjectStatus = 'active' | 'inactive' | 'draft';

  /**
   * An enumeration.
   */
  export type DefValidObjectTypeStatus = 'active' | 'inactive';

  export interface EntityCustomObject {
    /**
     * Content of a user defined Custom Object
     */
    data: {
      attributes: {
        /**
         * Archiving metadata for this object
         */
        archive_metadata: {
          /**
           * Archival ground for this custom object
           */
          ground?: string;
          /**
           * Archival retention for this custom object
           */
          retention?: number;
          /**
           * An enumeration.
           */
          status?: 'archived' | 'to destroy' | 'to preserve';
          [k: string]: any;
        };
        /**
         * Key-value pair of custom fields
         */
        custom_fields?: {
          [k: string]: any;
        };
        /**
         * Date this custom object got created
         */
        date_created: string;
        /**
         * Deleted date for this custom object
         */
        date_deleted?: string;
        /**
         * External reference this specific object
         */
        external_reference?: string;
        /**
         * Indicates whether this entity is the latest version
         */
        is_active_version: boolean;
        /**
         * Last modified date for this custom object
         */
        last_modified: string;
        /**
         * Name of the related object type
         */
        name: string;
        /**
         * An enumeration.
         */
        status?: 'active' | 'inactive' | 'draft';
        /**
         * Title of this specific object
         */
        title: string;
        /**
         * The current version of this custom object
         */
        version: number;
        /**
         * The version independent UUID for this object
         */
        version_independent_uuid?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        cases?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        custom_object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        custom_objects?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        documents?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        subjects?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCustomObjectList {
    data: {
      attributes: {
        /**
         * Archiving metadata for this object
         */
        archive_metadata: {
          /**
           * Archival ground for this custom object
           */
          ground?: string;
          /**
           * Archival retention for this custom object
           */
          retention?: number;
          /**
           * An enumeration.
           */
          status?: 'archived' | 'to destroy' | 'to preserve';
          [k: string]: any;
        };
        /**
         * Key-value pair of custom fields
         */
        custom_fields?: {
          [k: string]: any;
        };
        /**
         * Date this custom object got created
         */
        date_created: string;
        /**
         * Deleted date for this custom object
         */
        date_deleted?: string;
        /**
         * External reference this specific object
         */
        external_reference?: string;
        /**
         * Indicates whether this entity is the latest version
         */
        is_active_version: boolean;
        /**
         * Last modified date for this custom object
         */
        last_modified: string;
        /**
         * Name of the related object type
         */
        name: string;
        /**
         * An enumeration.
         */
        status?: 'active' | 'inactive' | 'draft';
        /**
         * Title of this specific object
         */
        title: string;
        /**
         * The current version of this custom object
         */
        version: number;
        /**
         * The version independent UUID for this object
         */
        version_independent_uuid?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        cases?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        custom_object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        custom_objects?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        documents?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        subjects?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCustomObjectType {
    /**
     * Definition of an object
     */
    data: {
      attributes: {
        /**
         * Auditlog for this Custom Object Type Version
         */
        audit_log: {
          /**
           * Description of this change
           */
          description: string;
          /**
           * Components affected by this change
           */
          updated_components: (
            | 'attributes'
            | 'authorizations'
            | 'custom_fields'
            | 'relationships'
          )[];
          [k: string]: any;
        };
        /**
         * Authorization settings for this custom object type
         */
        authorization_definition?: {
          /**
           * Optional list of authorizations for this object
           */
          authorizations?: {
            /**
             * An enumeration.
             */
            authorization: 'read' | 'readwrite' | 'admin';
            /**
             * Group name
             */
            department: {
              /**
               * Name of the department
               */
              name: string;
              /**
               * Unique identifier for this department
               */
              uuid: string;
              [k: string]: any;
            };
            /**
             * Role name
             */
            role: {
              /**
               * Name of the role
               */
              name: string;
              /**
               * Unique identifier for this role
               */
              uuid: string;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * Integer of folder in our elements catalog
         */
        catalog_folder_id?: number;
        /**
         * Definition of the related custom fields
         */
        custom_field_definition?: {
          /**
           * List of custom fields
           */
          custom_fields?: {
            /**
             * UUID referencing the field in our attribute catalog
             */
            attribute_uuid?: string;
            /**
             * Specification around a certain custom field type
             */
            custom_field_specification?:
              | {
                  /**
                   * Name of the relationship
                   */
                  name?: string;
                  /**
                   * An enumeration.
                   */
                  type: 'case' | 'custom_object' | 'document' | 'subject';
                  /**
                   * Use the GeoLocation(s) and Addresse(s) of relationship(custom_object) on the map of custom_object.
                   */
                  use_on_map?: boolean;
                  /**
                   * UUID of the relationship type
                   */
                  uuid?: string;
                  [k: string]: any;
                }
              | {
                  /**
                   * Use the address_v2 or geolocation on the map of custom_object.
                   */
                  use_on_map?: boolean;
                  [k: string]: any;
                };
            /**
             * An enumeration.
             */
            custom_field_type?:
              | 'relationship'
              | 'text'
              | 'option'
              | 'select'
              | 'textarea'
              | 'richtext'
              | 'checkbox'
              | 'date'
              | 'geojson'
              | 'valuta'
              | 'email'
              | 'url'
              | 'bankaccount'
              | 'address_v2'
              | 'numeric';
            /**
             * Description of this field for internal purposes
             */
            description?: string;
            /**
             * Description of this field for public purposes
             */
            external_description?: string;
            /**
             * Indicates whether this field is a hidden field, or 'system field'
             */
            is_hidden_field?: boolean;
            /**
             * Indicates whether this field is a required field
             */
            is_required?: boolean;
            /**
             * Label of this field
             */
            label: string;
            /**
             * The magic string of this field
             */
            magic_string?: string;
            /**
             * Name of this field in the catalog
             */
            name: string;
            /**
             * List of possible options for this custom field
             */
            options?: string[];
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * Date this object got created
         */
        date_created: string;
        /**
         * Deleted date for this object
         */
        date_deleted?: string;
        /**
         * An external reference for the object
         */
        external_reference?: string;
        /**
         * Indicates whether this entity is the latest version
         */
        is_active_version: boolean;
        /**
         * Last modified date for this object
         */
        last_modified: string;
        /**
         * Name of this object type
         */
        name: string;
        /**
         * Relationship settings for this custom object type
         */
        relationship_definition?: {
          /**
           * Optional list of relationships for this object
           */
          relationships?: {
            /**
             * UUID of related Case Type
             */
            case_type_uuid?: string;
            /**
             * UUID of related Custom Object Type
             */
            custom_object_type_uuid?: string;
            /**
             * Description of this field for internal purposes
             */
            description?: string;
            /**
             * Description of this field for public purposes
             */
            external_description?: string;
            /**
             * Indicate that this relationship is required on creation of this object
             */
            is_required?: boolean;
            /**
             * Name of this relationship
             */
            name: string;
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * An enumeration.
         */
        status?: 'active' | 'inactive';
        /**
         * Title for the created object types
         */
        title?: string;
        /**
         * The current version of this object type'
         */
        version: number;
        /**
         * The version independent UUID for this object
         */
        version_independent_uuid?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        latest_version?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCustomObjectTypeList {
    data: {
      attributes: {
        /**
         * Auditlog for this Custom Object Type Version
         */
        audit_log: {
          /**
           * Description of this change
           */
          description: string;
          /**
           * Components affected by this change
           */
          updated_components: (
            | 'attributes'
            | 'authorizations'
            | 'custom_fields'
            | 'relationships'
          )[];
          [k: string]: any;
        };
        /**
         * Authorization settings for this custom object type
         */
        authorization_definition?: {
          /**
           * Optional list of authorizations for this object
           */
          authorizations?: {
            /**
             * An enumeration.
             */
            authorization: 'read' | 'readwrite' | 'admin';
            /**
             * Group name
             */
            department: {
              /**
               * Name of the department
               */
              name: string;
              /**
               * Unique identifier for this department
               */
              uuid: string;
              [k: string]: any;
            };
            /**
             * Role name
             */
            role: {
              /**
               * Name of the role
               */
              name: string;
              /**
               * Unique identifier for this role
               */
              uuid: string;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * Integer of folder in our elements catalog
         */
        catalog_folder_id?: number;
        /**
         * Definition of the related custom fields
         */
        custom_field_definition?: {
          /**
           * List of custom fields
           */
          custom_fields?: {
            /**
             * UUID referencing the field in our attribute catalog
             */
            attribute_uuid?: string;
            /**
             * Specification around a certain custom field type
             */
            custom_field_specification?:
              | {
                  /**
                   * Name of the relationship
                   */
                  name?: string;
                  /**
                   * An enumeration.
                   */
                  type: 'case' | 'custom_object' | 'document' | 'subject';
                  /**
                   * Use the GeoLocation(s) and Addresse(s) of relationship(custom_object) on the map of custom_object.
                   */
                  use_on_map?: boolean;
                  /**
                   * UUID of the relationship type
                   */
                  uuid?: string;
                  [k: string]: any;
                }
              | {
                  /**
                   * Use the address_v2 or geolocation on the map of custom_object.
                   */
                  use_on_map?: boolean;
                  [k: string]: any;
                };
            /**
             * An enumeration.
             */
            custom_field_type?:
              | 'relationship'
              | 'text'
              | 'option'
              | 'select'
              | 'textarea'
              | 'richtext'
              | 'checkbox'
              | 'date'
              | 'geojson'
              | 'valuta'
              | 'email'
              | 'url'
              | 'bankaccount'
              | 'address_v2'
              | 'numeric';
            /**
             * Description of this field for internal purposes
             */
            description?: string;
            /**
             * Description of this field for public purposes
             */
            external_description?: string;
            /**
             * Indicates whether this field is a hidden field, or 'system field'
             */
            is_hidden_field?: boolean;
            /**
             * Indicates whether this field is a required field
             */
            is_required?: boolean;
            /**
             * Label of this field
             */
            label: string;
            /**
             * The magic string of this field
             */
            magic_string?: string;
            /**
             * Name of this field in the catalog
             */
            name: string;
            /**
             * List of possible options for this custom field
             */
            options?: string[];
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * Date this object got created
         */
        date_created: string;
        /**
         * Deleted date for this object
         */
        date_deleted?: string;
        /**
         * An external reference for the object
         */
        external_reference?: string;
        /**
         * Indicates whether this entity is the latest version
         */
        is_active_version: boolean;
        /**
         * Last modified date for this object
         */
        last_modified: string;
        /**
         * Name of this object type
         */
        name: string;
        /**
         * Relationship settings for this custom object type
         */
        relationship_definition?: {
          /**
           * Optional list of relationships for this object
           */
          relationships?: {
            /**
             * UUID of related Case Type
             */
            case_type_uuid?: string;
            /**
             * UUID of related Custom Object Type
             */
            custom_object_type_uuid?: string;
            /**
             * Description of this field for internal purposes
             */
            description?: string;
            /**
             * Description of this field for public purposes
             */
            external_description?: string;
            /**
             * Indicate that this relationship is required on creation of this object
             */
            is_required?: boolean;
            /**
             * Name of this relationship
             */
            name: string;
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * An enumeration.
         */
        status?: 'active' | 'inactive';
        /**
         * Title for the created object types
         */
        title?: string;
        /**
         * The current version of this object type'
         */
        version: number;
        /**
         * The version independent UUID for this object
         */
        version_independent_uuid?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        latest_version?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityDepartment {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        /**
         * Longer description of the department
         */
        description?: string;
        /**
         * Name of the department
         */
        name: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        parent?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityDepartmentList {
    data: {
      attributes: {
        /**
         * Longer description of the department
         */
        description?: string;
        /**
         * Name of the department
         */
        name: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        parent?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityDepartmentSummary {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        /**
         * Name of the department
         */
        name: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityDepartmentSummaryList {
    data: {
      attributes: {
        /**
         * Name of the department
         */
        name: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRelatedCase {
    /**
     * Content of related_case
     */
    data: {
      attributes: {
        /**
         * Case ID
         */
        number: number;
        /**
         * Progress status of the case
         */
        progress: number;
        /**
         * Result of the case
         */
        result?: string;
        /**
         * An enumeration.
         */
        status: 'new' | 'open' | 'stalled' | 'resolved';
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        assignee?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        case_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRelatedCaseList {
    data: {
      attributes: {
        /**
         * Case ID
         */
        number: number;
        /**
         * Progress status of the case
         */
        progress: number;
        /**
         * Result of the case
         */
        result?: string;
        /**
         * An enumeration.
         */
        status: 'new' | 'open' | 'stalled' | 'resolved';
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        assignee?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        case_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRelatedObject {
    /**
     * Content of related_custom_object
     */
    data: {
      attributes: {
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRelatedObjectList {
    data: {
      attributes: {
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRelatedSubject {
    /**
     * Content of related_subject
     */
    data: {
      attributes: {
        /**
         * Roles of the subject
         */
        roles?: {
          /**
           * Pydantic based Entity object
           *
           * Entity object based on pydantic and the "new way" of creating entities in
           * our minty platform. It has the same functionality as the EntityBase object,
           * but does not depend on it. Migrationpaths are unsure.
           */
          data: {
            attributes: {
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        /**
         * An enumeration.
         */
        type: 'person' | 'organization' | 'employee';
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRelatedSubjectList {
    data: {
      attributes: {
        /**
         * Roles of the subject
         */
        roles?: {
          /**
           * Pydantic based Entity object
           *
           * Entity object based on pydantic and the "new way" of creating entities in
           * our minty platform. It has the same functionality as the EntityBase object,
           * but does not depend on it. Migrationpaths are unsure.
           */
          data: {
            attributes: {
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        /**
         * An enumeration.
         */
        type: 'person' | 'organization' | 'employee';
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRelatedSubjectRole {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRelatedSubjectRoleList {
    data: {
      attributes: {
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRole {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        /**
         * Longer description of the role
         */
        description?: string;
        /**
         * Name of the role
         */
        name: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        parent?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRoleList {
    data: {
      attributes: {
        /**
         * Longer description of the role
         */
        description?: string;
        /**
         * Name of the role
         */
        name: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        parent?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntitySearchResult {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        /**
         * Description of the object represented by this search result
         */
        description?: string;
        /**
         * Type of the object represented by this search result
         */
        result_type: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        case?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        custom_object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntitySearchResultList {
    data: {
      attributes: {
        /**
         * Description of the object represented by this search result
         */
        description?: string;
        /**
         * Type of the object represented by this search result
         */
        result_type: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        case?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        custom_object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityTimelineEntry {
    /**
     * Entity represents properties of timeline entry
     */
    data: {
      attributes: {
        /**
         * Date of the event
         */
        date: string;
        /**
         * Description of the event
         */
        description: string;
        /**
         * Idof the the event
         */
        id: number;
        /**
         * Type of the the event
         */
        type: string;
        /**
         * User of the event
         */
        user: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityTimelineEntryList {
    data: {
      attributes: {
        /**
         * Date of the event
         */
        date: string;
        /**
         * Description of the event
         */
        description: string;
        /**
         * Idof the the event
         */
        id: number;
        /**
         * Type of the the event
         */
        type: string;
        /**
         * User of the event
         */
        user: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface Entities {
    [k: string]: any;
  }
}
