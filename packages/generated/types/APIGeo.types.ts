// Generated on: Tue Sep 15 2020 08:14:02 GMT+0200 (Central European Summer Time)
// Environment used: https://development.zaaksysteem.nl
//
// This file was automatically generated. DO NOT MODIFY IT BY HAND.
// Instead, rerun generation of Geo domain.

/* eslint-disable */
export namespace APIGeo {
  export interface GetGeoFeaturesRequestParams {
    uuid?: string;
    [k: string]: any;
  }

  export type GetGeoFeaturesResponseBody = ({
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    links?: {
      self?: string;
      prev?: string;
      next?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }) & {
    data?: {
      attributes?: {
        /**
         * GeoJSON object detailing the feature
         */
        geo_json: {
          [k: string]: any;
        };
        [k: string]: any;
      };
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type?: string;
      [k: string]: any;
    }[];
    links?: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  };

  export interface EntityCustomObject {
    /**
     * Entity representing a custom object as seen by the geo domain.
     *
     * It is very minimal on purpose, as the "actual" custom object is part of the
     * case-management domain; this only has the version-independent UUID as and a
     * list of magic strings that should be represented on the map.
     */
    data?: {
      attributes?: {
        /**
         * List of magic strings of custom fields that should be shown on the map
         */
        map_magic_strings: string[];
        [k: string]: any;
      };
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type?: string;
      [k: string]: any;
    };
    links?: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCustomObjectList {
    data?: {
      attributes?: {
        /**
         * List of magic strings of custom fields that should be shown on the map
         */
        map_magic_strings: string[];
        [k: string]: any;
      };
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type?: string;
      [k: string]: any;
    }[];
    links?: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityGeoFeature {
    /**
     * Contains helper methods and variables for GeoFeature
     */
    data?: {
      attributes?: {
        /**
         * GeoJSON object detailing the feature
         */
        geo_json: {
          [k: string]: any;
        };
        [k: string]: any;
      };
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type?: string;
      [k: string]: any;
    };
    links?: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityGeoFeatureList {
    data?: {
      attributes?: {
        /**
         * GeoJSON object detailing the feature
         */
        geo_json: {
          [k: string]: any;
        };
        [k: string]: any;
      };
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type?: string;
      [k: string]: any;
    }[];
    links?: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface Entities {
    [k: string]: any;
  }
}
