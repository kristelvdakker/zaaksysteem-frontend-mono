// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
/* eslint-disable complexity */

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const InlineChunkHtmlPlugin = require('react-dev-utils/InlineChunkHtmlPlugin');
const TerserPlugin = require('terser-webpack-plugin');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');

if (!process.env.NODE_ENV) throw new Error('Empty NODE_ENV env variable');
const isEnvDevelopment = process.env.NODE_ENV === 'development';
const isEnvProduction = process.env.NODE_ENV === 'production';

module.exports = app => {
  return {
    mode: isEnvProduction ? 'production' : isEnvDevelopment && 'development',
    bail: isEnvProduction,
    devtool: 'source-map',
    stats: 'minimal',
    entry: { [app.name]: app.indexPath },
    output: {
      path: path.resolve(__dirname, 'build', 'apps', app.name),
      futureEmitAssets: true,
      pathinfo: isEnvDevelopment,
      filename: isEnvProduction
        ? 'index.[contenthash:8].js'
        : isEnvDevelopment && '[name].js',
      chunkFilename: isEnvProduction
        ? 'index.[contenthash:8].chunk.js'
        : isEnvDevelopment && '[name].chunk.js',
      publicPath: app.publicUrlFrag,
      devtoolModuleFilenameTemplate: isEnvProduction
        ? info =>
            path
              .relative(app.srcPath, info.absoluteResourcePath)
              .replace(/\\/g, '/')
        : isEnvDevelopment &&
          (info => path.resolve(info.absoluteResourcePath).replace(/\\/g, '/')),
      jsonpFunction: `webpackJsonp${app.name}`,
      globalObject: 'this',
    },
    optimization: {
      minimize: isEnvProduction,
      minimizer: [
        new TerserPlugin({
          terserOptions: {
            parse: {
              ecma: 8,
            },
            compress: {
              ecma: 5,
              warnings: false,
              comparisons: false,
              inline: 2,
            },
            mangle: {
              safari10: true,
            },
            output: {
              ecma: 5,
              comments: false,
              ascii_only: true,
            },
          },
          sourceMap: true,
        }),
      ],
      splitChunks: {
        chunks: 'all',
        name: isEnvDevelopment,
      },
      runtimeChunk: {
        name: entrypoint => `runtime-${entrypoint.name}`,
      },
    },
    resolve: {
      modules: ['node_modules'],
      extensions: ['.js', '.ts', '.tsx', '.svg', '.css'],
      alias: {
        '@zaaksysteem': path.resolve(__dirname, 'packages'),
      },
    },
    module: {
      strictExportPresence: true,
      rules: [
        {
          test: /\.(js|mjs|jsx|ts|tsx)$/,
          include: [
            app.srcPath,
            path.resolve(__dirname, 'packages', 'common', 'src'),
            path.resolve(__dirname, 'packages', 'ui', 'App'),
            path.resolve(__dirname, 'packages', 'communication-module', 'src'),
            path.resolve(__dirname, 'packages', 'kitchen-sink', 'source'),
          ],
          loader: require.resolve('babel-loader'),
          options: {
            babelrc: false,
            configFile: false,
            cacheDirectory: true,
            cacheCompression: false,
            compact: true,
            presets: [
              [
                '@babel/preset-env',
                {
                  useBuiltIns: 'entry',
                  corejs: 3,
                  exclude: ['transform-typeof-symbol'],
                },
              ],
              '@babel/preset-typescript',
              [
                '@babel/preset-react',
                {
                  development: isEnvDevelopment,
                  useBuiltIns: true,
                },
              ],
            ],
            plugins: [
              '@babel/plugin-proposal-optional-chaining',
              '@babel/plugin-proposal-nullish-coalescing-operator',
              [
                '@babel/plugin-proposal-class-properties',
                {
                  loose: true,
                },
              ],
              [
                '@babel/plugin-transform-runtime',
                {
                  regenerator: true,
                },
              ],
            ],
          },
        },
        {
          test: /.css$/,
          use: ['style-loader', 'css-loader'],
          sideEffects: true,
        },
        {
          test: /.(svg|woff|woff2)$/,
          use: 'url-loader',
        },
      ],
    },
    plugins: [
      new HtmlWebpackPlugin(
        Object.assign(
          {},
          {
            inject: true,
            template: app.htmlPath,
          },
          isEnvProduction
            ? {
                minify: {
                  removeComments: true,
                  collapseWhitespace: true,
                  removeRedundantAttributes: true,
                  useShortDoctype: true,
                  removeEmptyAttributes: true,
                  removeStyleLinkTypeAttributes: true,
                  keepClosingSlash: true,
                  minifyJS: true,
                  minifyCSS: true,
                  minifyURLs: true,
                },
              }
            : undefined
        )
      ),
      isEnvProduction &&
        new InlineChunkHtmlPlugin(HtmlWebpackPlugin, [/runtime-.+[.]js/]),
      new WebpackManifestPlugin({
        fileName: 'asset-manifest.json',
        publicPath: app.publicUrlFrag,
        generate: (seed, files, entrypoints) => {
          const manifestFiles = files.reduce((manifest, file) => {
            manifest[file.name] = file.path;
            return manifest;
          }, seed);
          const entrypointFiles = entrypoints[app.name].filter(
            fileName => !fileName.endsWith('.map')
          );

          return {
            files: manifestFiles,
            entrypoints: entrypointFiles,
          };
        },
      }),
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    ].filter(Boolean),
    node: {
      module: 'empty',
      dgram: 'empty',
      dns: 'mock',
      fs: 'empty',
      http2: 'empty',
      net: 'empty',
      tls: 'empty',
      child_process: 'empty',
    },
    performance: false,
  };
};
