user nobody nogroup;
worker_processes auto;          # auto-detect number of logical CPU cores

events {
    worker_connections 512;       # set the max number of simultaneous connections (per worker process)
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    server_tokens off;

    keepalive_timeout  65;

    limit_conn_zone $host zone=perserver:10m;

    gzip on;
    gzip_disable "MSIE [1-6]\.";
    gzip_http_version 1.0;
    gzip_vary off;
    gzip_comp_level 6;
    gzip_min_length 256;
    gzip_buffers 16 8k;
    gzip_proxied any;

    gzip_types
       application/javascript
       application/json
       application/manifest+json
       application/x-font-ttf
       application/x-javascript
       application/x-web-app-manifest+json
       application/xhtml+xml
       application/xml+rss
       application/xml
       font/opentype
       image/bmp
       image/svg+xml
       image/x-icon
       text/cache-manifest
       text/css
       text/plain
       text/vcard
       text/vtt
       text/xml
       text/x-component
       text/javascript
       text/x-cross-domain-policy;

    server_names_hash_bucket_size 128;
    types_hash_max_size 2048;
    types_hash_bucket_size 64;

    log_format logstash_json escape=json '{"time":"$time_iso8601", '
	                                   '"zs_component":"zaaksysteem-frontend-mono", '
                                           '"zs_pod_name":"$hostname", '
                                           '"message":"$request", '
                                           '"req": { '
                                               '"remote_addr":"$remote_addr", '
                                               '"hostname":"$host", '
                                               '"port":"$server_port", '
                                               '"body_bytes_sent": $body_bytes_sent, '
                                               '"status": $status, '
                                               '"request_uri": "$request_uri", '
                                               '"request_method": "$request_method", '
                                               '"request_time": $request_time, '
					       '"request_length":"$request_length", '
                                               '"http_referrer": "$http_referer", '
                                               '"http_user_agent": "$http_user_agent", '
					       '"connection_requests":"$connection_requests", '
					       '"pipeline":"$pipe"}}';

    access_log /dev/stdout logstash_json;
    error_log /dev/stderr warn;

    server {
        listen 82 default_server;
        listen 80 default_server;
        server_name zaaksysteem;        # Mandatory to make limit_conn work...

        root /www/data;

        location ~ ^/(?<app>main|admin|my-pip|external-components|objection-app) {
            try_files $uri /$app/index.html;

            error_page 404 =200 /$app/index.html;
        }

        location ^~ /\.ht {
            deny all;
        }
    }
}
