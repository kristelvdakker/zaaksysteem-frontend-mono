module.exports = async ({ config }) => {
  config.node = {
    __dirname: true,
  };

  return config;
};
