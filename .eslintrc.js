// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
const ERROR = 'error';
const OFF = 'off';
const ID_LENGTH = 2;

module.exports = {
  parser: 'babel-eslint',
  extends: [
    'prettier',
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:jsx-a11y/recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:import/typescript',
  ],
  plugins: ['prettier', 'jsx-a11y', 'react', 'react-hooks', 'header', 'jest'],
  settings: {
    react: {
      version: '16',
    },
  },
  env: {
    browser: true,
    node: true,
    es6: true,
    jest: true,
  },
  overrides: [
    {
      files: ['**/*.ts?(x)'],
      parser: '@typescript-eslint/parser',
      parserOptions: {
        ecmaVersion: 2018,
        sourceType: 'module',
        ecmaFeatures: {
          jsx: true,
        },

        // typescript-eslint specific options
        warnOnUnsupportedTypeScriptVersion: false,
      },
      plugins: ['@typescript-eslint'],
      // If adding a typescript-eslint version of an existing ESLint rule,
      // make sure to disable the ESLint rule here.
      rules: {
        // TypeScript's `noFallthroughCasesInSwitch` option is more robust (#6906)
        'default-case': OFF,
        // 'tsc' already handles this (https://github.com/typescript-eslint/typescript-eslint/issues/291)
        'no-dupe-class-members': OFF,
        // 'tsc' already handles this (https://github.com/typescript-eslint/typescript-eslint/issues/477)
        'no-undef': OFF,
        'no-use-before-define': OFF,
        '@typescript-eslint/no-use-before-define': [OFF],
        'no-unused-vars': OFF,
        '@typescript-eslint/no-unused-vars': [
          ERROR,
          {
            args: 'none',
            ignoreRestSiblings: true,
          },
        ],
        'no-useless-constructor': OFF,
        '@typescript-eslint/no-useless-constructor': 'warn',
      },
    },
  ],
  rules: {
    'id-length': [
      ERROR,
      {
        min: ID_LENGTH,
        exceptions: ['t'],
      },
    ],
    complexity: [ERROR, 6],
    'no-magic-numbers': 0,
    'no-confusing-arrow': [OFF],
    'no-inline-comments': [OFF],
    'prettier/prettier': [ERROR],
    'react/sort-comp': OFF,
    'jsx-a11y/click-events-have-key-events': [ERROR],
    'jsx-a11y/no-static-element-interactions': [ERROR],
    'react/button-has-type': [ERROR],
    'react/display-name': 0,
    'react/no-danger': [ERROR],
    'react/no-multi-comp': [
      ERROR,
      {
        ignoreStateless: true,
      },
    ],
    'react-hooks/rules-of-hooks': [ERROR],
    'react/prop-types': [OFF],
    'react/self-closing-comp': [ERROR],
    'import/named': [OFF],
    'import/default': [OFF],
    'import/no-named-as-default': [OFF],
    'import/no-named-as-default-member': [OFF],
    'import/namespace': [OFF],
    'import/export': [OFF],
    'import/no-duplicates': [OFF],
    'import/order': [
      'error',
      {
        groups: [
          'builtin',
          'external',
          'internal',
          'parent',
          'sibling',
          'index',
        ],
        pathGroups: [
          {
            pattern: '@*/**',
            group: 'external',
          },
        ],
      },
    ],
    'header/header': [
      2,
      'line',
      ' This file is part of Zaaksysteem, which is released under the EUPL.\n See file LICENSE for full license details.',
    ],
  },
};
