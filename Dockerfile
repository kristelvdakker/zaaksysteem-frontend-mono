FROM nginx:latest

COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY build/apps/ /www/data/
COPY apps/objection-app /www/data/objection-app
