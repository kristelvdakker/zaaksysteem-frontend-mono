// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { SessionRootStateType } from '@zaaksysteem/common/src/store/session/session.reducer';
import Breadcrumbs, {
  defaultItemRenderer,
} from '@mintlab/ui/App/Material/Breadcrumbs/Breadcrumbs';
import {
  BreadcrumbItemType,
  BreadcrumbRendererType,
} from '@mintlab/ui/App/Material/Breadcrumbs/Breadcrumbs.types';
import Button from '@mintlab/ui/App/Material/Button';
import AddElement from '@zaaksysteem/common/src/components/dialogs/AddElement/AddElement';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { CaseCreateDialog } from '@zaaksysteem/common/src/components/dialogs/CaseCreate/CaseCreateDialog';
import { useBreadcrumbBarStyles } from './BreadcrumbBar.styles';
import Search from './Search/Search';
import { getSections } from './BreadcrumbBar.library';
import locale from './BreadcrumbBar.locale';

export type BreadcrumbBarPropsType = {
  breadcrumbs: BreadcrumbItemType[];
};

const itemRenderer: BreadcrumbRendererType = (itemProps, isLastItem) => {
  const { item, index, classes } = itemProps;
  const className = isLastItem ? classes.last : classes.link;

  return item.path.indexOf('/main') === -1 ? (
    defaultItemRenderer(itemProps, isLastItem)
  ) : (
    <Link
      key={index}
      to={item.path}
      className={className}
      {...(isLastItem && { 'aria-current': 'page' })}
    >
      {item.label}
    </Link>
  );
};

const BreadcrumbBar: React.ComponentType<BreadcrumbBarPropsType> = ({
  breadcrumbs,
}) => {
  const [t] = useTranslation('');
  const classes = useBreadcrumbBarStyles();
  const [addElementOpen, setAddElementOpen] = useState(false);
  const [createCaseOpen, setCreateCaseOpen] = useState(false);
  const capabilities = useSelector(
    (state: SessionRootStateType) =>
      state.session?.data?.logged_in_user?.capabilities || []
  );

  return (
    <header className={classes.wrapper}>
      <div className={classes.breadCrumbs}>
        <Breadcrumbs
          items={breadcrumbs}
          itemRenderer={itemRenderer}
          lastItemRenderer={itemRenderer}
        />
      </div>
      <div className={classes.search}>
        <Search />
      </div>
      <div>
        <Button
          presets={['primary', 'contained']}
          icon="add"
          action={() => setAddElementOpen(true)}
        >
          {t('BreadcrumbBar:new')}
        </Button>
      </div>
      {addElementOpen ? (
        <AddElement
          t={t}
          hide={() => setAddElementOpen(false)}
          sections={getSections({
            t,
            contactAllowed: capabilities.includes('contact_nieuw'),
            setCreateCaseOpen,
            setAddElementOpen,
          })}
          title={t('BreadcrumbBar:new')}
        />
      ) : null}

      <CaseCreateDialog
        open={createCaseOpen}
        onClose={() => setCreateCaseOpen(false)}
      />
    </header>
  );
};

/* eslint-disable-next-line */
export default (props: BreadcrumbBarPropsType) => (
  <I18nResourceBundle resource={locale} namespace="BreadcrumbBar">
    <BreadcrumbBar {...props} />
  </I18nResourceBundle>
);
