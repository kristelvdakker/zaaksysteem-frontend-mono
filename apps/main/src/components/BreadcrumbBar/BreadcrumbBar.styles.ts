// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useBreadcrumbBarStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: '16px 32px',
      height: 75,
      borderBottom: `1px solid ${greyscale.dark}`,
    },
    breadCrumbs: {
      flex: 1,
    },
    search: {
      minWidth: 280,
      paddingLeft: 20,
      paddingRight: 20,
    },
  })
);
