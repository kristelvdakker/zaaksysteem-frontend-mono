// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export type OptionType = {
  value: string | number;
  label: string;
  subLabel?: string;
  url?: string;
  icon?: React.ReactElement;
};

export type ResultType = {
  id: number;
  object: {
    [key: string]: any;
  };
  object_type: string;
  [key: string]: any;
};
