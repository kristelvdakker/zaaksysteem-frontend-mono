// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { useState } from 'react';
import { Select } from '@mintlab/ui/App/Zaaksysteem/Select';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import MultilineOption from '@mintlab/ui/App/Zaaksysteem/Select/Option/MultilineOption';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { useTheme } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';
import { useTranslation } from 'react-i18next';
import { getChoices, getStyles } from './library';
import { OptionType } from './Search.types';

const Search: React.ComponentType<{}> = () => {
  const [input, setInput] = useState('');
  const [value, setValue] = useState(null as OptionType | null);
  const [t] = useTranslation();
  const theme = useTheme<Theme>();

  return (
    <DataProvider
      providerArguments={[input]}
      autoProvide={input !== ''}
      provider={getChoices}
    >
      {({ data, busy }: { data: any[] | null; busy: boolean }) => {
        const normalizedChoices = data || [];

        return (
          <Select
            generic={true}
            value={value}
            onChange={({ target: { value } }) => {
              setValue(value);
              if (value && value.url) window.top.location = value.url;
            }}
            choices={normalizedChoices}
            isClearable={true}
            loading={busy}
            isMulti={false}
            getChoices={setInput}
            components={{
              Option: MultilineOption,
            }}
            styles={getStyles(theme)}
            startAdornment={<Icon size="small">{iconNames.search}</Icon>}
            translations={{
              'form:choose': t('common:dialog.search'),
              'form:loading': t('common:general.loading'),
              'form:beginTyping': '',
              'form:creatable': '',
              'form:create': '',
            }}
            filterOption={option => Boolean(option)}
          />
        );
      }}
    </DataProvider>
  );
};

export default Search;
