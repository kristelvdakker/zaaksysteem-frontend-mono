// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { selectStyleSheet } from '@mintlab/ui/App/Zaaksysteem/Select/Generic/GenericSelect.style';
import { Theme } from '@mintlab/ui/types/Theme';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { OptionType, ResultType } from './Search.types';

/* eslint complexity: [2, 11] */
export const getLabel = (result: ResultType): OptionType => {
  const { object_type, id, object } = result;

  if (object_type === 'file') {
    return {
      value: result.id,
      label: result.label,
      icon: <ZsIcon size="small">entityType.inverted.file</ZsIcon>,
      url: `/intern/zaak/${object.case_id}/documenten`,
    };
  }

  if (object_type === 'zaak') {
    const {
      status_formatted,
      zaaktype_node_id: { titel },
      behandelaar,
    } = object;

    return {
      value: id,
      label: `${object.id}: ${titel} (${status_formatted})`,
      subLabel: `${behandelaar?.naam || ''}`,
      icon: <ZsIcon size="small">entityType.inverted.case</ZsIcon>,
      url: `/intern/zaak/${object.id}`,
    };
  }

  if (object_type.match(/persoon|bedrijf/)) {
    return {
      value: id,
      label: object.label,
      icon: (
        <ZsIcon size="small">
          {object_type.match(/persoon/)
            ? 'entityType.inverted.person'
            : 'entityType.inverted.organization'}
        </ZsIcon>
      ),
      url: `/betrokkene/${object.id}/?gm=1&type=${object.object_type}`,
    };
  }

  if (object_type.match(/object/)) {
    return {
      value: id,
      label: object.label || '',
      icon: <ZsIcon size="small">entityType.inverted.other</ZsIcon>,
      url: `/main/object/${id}`,
    };
  }

  return {
    value: id,
    label: object.label || object.search_order || object.title || object.name,
    icon: <ZsIcon size="small">entityType.inverted.other</ZsIcon>,
  };
};

// when changing this to v2 we need to ensure that
// - old objects link to the old objectview
// - new objects link to the new objectview AND that they link there using the version-independent-uuid
export const getChoices = async (query: string) => {
  if (!query) return [];
  try {
    const results = await request<any>(
      'GET',
      `/objectsearch?object_type=&query=${query}&zapi_num_rows=100&zapi_page=1`
    );
    return results.json.entries.map(getLabel);
  } catch (err) {
    //
  }

  return [];
};

export const getStyles = (theme: Theme) => {
  const styles = {
    ...selectStyleSheet({ theme }),
    menu: (provided: () => any) => ({
      ...provided,
      width: 500,
      right: 0,
    }),
  };

  return styles;
};
