// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/* eslint react/no-danger: 0 */
import React, { Fragment, useState } from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import { useSelector } from 'react-redux';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
// @ts-ignore
import documentExplorerLocale from '@zaaksysteem/common/src/components/DocumentExplorer/locale/locale';
import Button from '@mintlab/ui/App/Material/Button';
import ConfirmDialog from '@zaaksysteem/common/src/components/ConfirmDialog/ConfirmDialog';
import { useDocumentExplorer } from '@zaaksysteem/common/src/components/DocumentExplorer/useDocumentExplorer';
import * as selectors from '@zaaksysteem/common/src/components/DocumentExplorer/store/selectors';
import { getColumns } from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/utils/getColumns';
import { isDocument } from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/utils/isDocument';
import { hasUserDocumentIntakerRole } from '@zaaksysteem/common/src/store/session/session.selectors';
import FileExplorer from '@zaaksysteem/common/src/components/DocumentExplorer/components/FileExplorer/FileExplorer';
import {
  PresetLocations,
  StoreShapeType,
  FiltersType,
} from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/types/types';
import {
  DirectoryItemType,
  DocumentItemType,
} from '@zaaksysteem/common/src/components/DocumentExplorer/components/FileExplorer/types/FileExplorerTypes';
import { useDocumentExplorerStyles } from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/DocumentExplorer.style';
import { DocumentPreviewModal } from '@zaaksysteem/common/src/components/DocumentPreview';
import { FileUploadDialog } from '@zaaksysteem/common/src/components/dialogs/FileUploadDialog/FileUploadDialog';
import Search from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/library/Search';
import {
  useFilters,
  filterFunctions,
} from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/library/useFilters';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { APIDocument } from '@zaaksysteem/generated';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
//@ts-ignore
import IconButton from '@material-ui/core/IconButton';
import { CaseCreateDialog } from '@zaaksysteem/common/src/components/dialogs/CaseCreate/CaseCreateDialog';
import intakeLocale from './locale';
import { AddToCaseDialog } from './AddToCase/AddToCaseDialog';
import Menu from './components/Menu/Menu';
import { deleteDocument } from './Delete/deleteDocument';
import PropertiesDialog from './Properties/PropertiesDialog';
import AssignDialog from './Assign/AssignDialog';
import Assignment from './components/Assignment';
import { useIntakeStyles } from './Intake.styles';
import Preview from './Preview/Preview';

type DialogsType =
  | 'fileUpload'
  | 'delete'
  | 'addToCase'
  | 'properties'
  | 'preview'
  | 'caseCreate'
  | 'assign';

const INITIAL_DIALOGS: Record<DialogsType, boolean> = {
  delete: false,
  addToCase: false,
  properties: false,
  fileUpload: false,
  preview: false,
  caseCreate: false,
  assign: false,
};

/* eslint complexity: [2, 9] */
const IntakeDocuments: React.ComponentType = () => {
  const classes = useDocumentExplorerStyles();
  const intakeClasses = useIntakeStyles();
  const [t] = useTranslation();
  const getURL = (state: StoreShapeType) => {
    const { search, location } = state;

    return buildUrl<APIDocument.GetDirectoryEntriesForIntakeRequestParams>(
      '/api/v2/document/get_directory_entries_for_intake',
      {
        filter: location === PresetLocations.Search ? { fulltext: search } : {},
      }
    );
  };

  const [
    state,
    actions,
    ServerErrorDialog,
    openServerErrorDialog,
  ] = useDocumentExplorer({
    t,
    getURL,
  });

  const {
    doRefreshAction,
    doNavigateAction,
    doToggleSelectAction,
    doFileOpenAction,
    doSearchAction,
    doSearchCloseAction,
  } = actions;
  const { location, loading } = state;
  const [previewItem, setPreviewItem] = useState<null | DocumentItemType>(null);
  const isIntaker = useSelector(hasUserDocumentIntakerRole);
  const [filters, setFilters] = useState<FiltersType>({
    assignment: isIntaker
      ? filterFunctions.assignment.unassigned
      : filterFunctions.assignment.assigned,
  });
  const filterComponents = useFilters(setFilters, filters);
  const [dialogs, setDialogs] = useState(INITIAL_DIALOGS);

  const dialogChange = (type: DialogsType, setting: boolean) =>
    setDialogs({
      ...dialogs,
      [type]: setting,
    });

  const { name, mimeType, modified, description } = getColumns({
    t,
    classes,
  });

  const organizedItems = selectors.getItemsFilteredAndSortedByNameAndType(
    state,
    filters
  );
  const selectedItems = organizedItems.filter(item => item.selected);
  const selectedUuid = selectedItems.length ? selectedItems[0].uuid : '';

  const columns = [
    name({
      fileNameAction: item => {
        if (item.preview) {
          return (event: React.MouseEvent) => {
            event.preventDefault();
            event.stopPropagation();
            if (item.preview) {
              setPreviewItem(item);
              dialogChange('preview', true);
            }
          };
        }
      },
      /* eslint-disable-next-line */
      iconRenderer: (item, icon) => {
        return item?.thumbnail?.url ? (
          <Tooltip
            classes={{
              popper: intakeClasses.popper,
            }}
            enterDelay={300}
            title={<Preview url={item.thumbnail.url} />}
          >
            {icon}
          </Tooltip>
        ) : (
          icon
        );
      },
      tooltipSuffix: item =>
        item.document_number ? ' | ' + item.document_number : undefined,
      showNotAccepted: false,
      showRejected: true,
    }),
    {
      label: '',
      name: 'assignment',
      width: 40,
      showFromWidth: 600,
      disableSort: true,
      /* eslint-disable-next-line */
      cellRenderer: ({ rowData }: { rowData: DocumentItemType }) => (
        <Assignment assignment={rowData.assignment} />
      ),
    },
    mimeType(),
    description(),
    modified(),
    {
      label: '',
      name: 'open',
      width: 66,
      disableSort: true,
      /* eslint-disable-next-line */
      cellRenderer: ({ rowData }: { rowData: DocumentItemType }) => {
        if (isDocument(rowData) && rowData.preview?.url) {
          return (
            <Tooltip
              title={t('DocumentExplorer:columns.open.toolTip')}
              placement={'top-start'}
              enterDelay={400}
            >
              <IconButton
                href={rowData.preview?.url}
                target="_blank"
                title={t('DocumentExplorer:columns.open.label')}
                color="inherit"
              >
                <Icon size="small" color="primary">
                  {iconNames.open_in_new}
                </Icon>
              </IconButton>
            </Tooltip>
          );
        }

        return <Fragment />;
      },
    },
  ];
  const inFolderLocation = ![PresetLocations.Home as string].includes(location);

  return (
    <I18nResourceBundle
      resource={documentExplorerLocale}
      namespace="DocumentExplorer"
    >
      <I18nResourceBundle resource={intakeLocale} namespace="Intake">
        <div className={classNames(classes.wrapper, intakeClasses.wrapper)}>
          <DocumentPreviewModal
            open={dialogs.preview}
            title={previewItem?.name || ''}
            url={previewItem?.preview?.url || ''}
            downloadUrl={previewItem?.download?.url}
            contentType={previewItem?.preview?.contentType || ''}
            onClose={() => dialogChange('preview', false)}
          />
          {ServerErrorDialog}
          <div className={classes.topBar}>
            <div className={classes.searchAndFiltersContainer}>
              <Search onChange={doSearchAction} onClose={doSearchCloseAction} />
              {filterComponents.assignment}
            </div>

            <Button
              action={() => dialogChange('fileUpload', true)}
              presets={['text', 'primary', 'large']}
              classes={{
                label: classes.actionButtonLabel,
              }}
              title={t('DocumentExplorer:addFiles.button')}
            >
              {t('DocumentExplorer:addFiles.button')}
            </Button>
          </div>
          <div className={classes.toolbar}>
            <div className={classes.actionButtons}>
              <Menu
                state={state}
                actions={{
                  addToCase: () => dialogChange('addToCase', true),
                  delete: () => dialogChange('delete', true),
                  properties: () => dialogChange('properties', true),
                  caseCreate: () => dialogChange('caseCreate', true),
                  assign: () => dialogChange('assign', true),
                }}
              />

              <FileUploadDialog
                onConfirm={() => {
                  doRefreshAction();
                  dialogChange('fileUpload', false);
                }}
                onClose={() => dialogChange('fileUpload', false)}
                open={dialogs.fileUpload}
                {...(inFolderLocation && { directoryUUID: location })}
              />
              <AddToCaseDialog
                selectedDocuments={selectedItems as DocumentItemType[]}
                open={dialogs.addToCase}
                onClose={() => dialogChange('addToCase', false)}
                onConfirm={() => {
                  dialogChange('addToCase', false);
                  doRefreshAction();
                }}
              />
              <CaseCreateDialog
                open={dialogs.caseCreate}
                selectedDocumentUuid={selectedUuid}
                onClose={() => dialogChange('caseCreate', false)}
              />
              <ConfirmDialog
                open={dialogs.delete}
                onConfirm={async () => {
                  const promises = selectedItems.map(item =>
                    deleteDocument({
                      document_uuid: item.uuid || '',
                      reason: t('Intake:delete.reason'),
                    })
                  );

                  Promise.all(promises)
                    .then(() => {
                      doRefreshAction();
                    })
                    .catch(error => {
                      openServerErrorDialog(error);
                    })
                    .finally(() => {
                      dialogChange('delete', false);
                    });
                }}
                onClose={() => dialogChange('delete', false)}
                title={t('Intake:delete.title')}
                body={
                  <div
                    dangerouslySetInnerHTML={{
                      __html: t('Intake:delete.body'),
                    }}
                  />
                }
              />
              {dialogs.properties && (
                <PropertiesDialog
                  uuid={selectedUuid}
                  onClose={() => dialogChange('properties', false)}
                  onConfirm={() => {
                    dialogChange('properties', false);
                    doRefreshAction();
                  }}
                  onServerError={openServerErrorDialog}
                />
              )}
              {dialogs.assign && (
                <AssignDialog
                  selectedDocuments={selectedItems}
                  open={true}
                  onClose={() => dialogChange('assign', false)}
                  onConfirm={() => {
                    dialogChange('assign', false);
                    doRefreshAction();
                  }}
                />
              )}
            </div>
          </div>
          <div className={classes.table}>
            <FileExplorer
              items={organizedItems}
              columns={columns}
              onFolderOpen={folder => doNavigateAction(folder.uuid)}
              onRowClick={({ rowData }: { rowData: DirectoryItemType }) => {
                if (isDocument(rowData)) {
                  doToggleSelectAction(rowData);
                }
              }}
              onFileOpen={doFileOpenAction}
              loading={loading}
              noRowsMessage={t('DocumentExplorer:table.noRows')}
              rowHeight={50}
              selectable={true}
            />
          </div>
        </div>
      </I18nResourceBundle>
    </I18nResourceBundle>
  );
};

export default IntakeDocuments;
