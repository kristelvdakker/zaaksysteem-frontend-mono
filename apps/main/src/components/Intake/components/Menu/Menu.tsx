// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
//@ts-ignore
import IconButton from '@material-ui/core/IconButton';
import Button from '@mintlab/ui/App/Material/Button';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { StoreShapeType } from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/types/types';
import { useMenuStyles } from './Menu.styles';
import locale from './locale';

type MenuPropsType = {
  state: StoreShapeType;
  actions: {
    addToCase: () => void;
    delete: () => void;
    properties: () => void;
    caseCreate: () => void;
    assign: () => void;
  };
};

/* eslint complexity: [2, 8] */
const Menu = ({ state, actions }: MenuPropsType) => {
  const { items } = state;
  const classes = useMenuStyles();
  const [t] = useTranslation('IntakeMenu');

  const selectedItems = items.filter(item => item.selected);
  const selectedSingleItem = selectedItems.length === 1;
  const hasSelectedItems = selectedItems.length > 0;
  const selectedOnlyDocuments = selectedItems.every(
    item => item.type === 'document'
  );

  const buttons = {
    caseCreate: selectedSingleItem && selectedOnlyDocuments && (
      <Tooltip key="caseCreate" title={t('caseCreate')}>
        <Button
          presets={['default', 'medium', 'contained']}
          action={actions.caseCreate}
          classes={{
            root: classes.textButton,
          }}
        >
          {t('IntakeMenu:caseCreate')}
        </Button>
      </Tooltip>
    ),
    addToCase: hasSelectedItems && selectedOnlyDocuments && (
      <Tooltip key="addToCase" title={t('addToCase')}>
        <Button
          presets={['default', 'medium', 'contained']}
          action={actions.addToCase}
          classes={{
            root: classes.textButton,
          }}
        >
          {t('IntakeMenu:addToCase')}
        </Button>
      </Tooltip>
    ),
    assign: hasSelectedItems && (
      <Tooltip key="assign" title={t('assign')}>
        <Button
          presets={['default', 'medium', 'contained']}
          action={actions.assign}
          classes={{
            root: classes.textButton,
          }}
        >
          {t('IntakeMenu:assign')}
        </Button>
      </Tooltip>
    ),
    delete: hasSelectedItems && (
      <Tooltip key="delete" title={t('delete')}>
        <IconButton onClick={actions.delete} color="inherit">
          <Icon size="small">{iconNames.delete}</Icon>
        </IconButton>
      </Tooltip>
    ),
    properties: selectedSingleItem && (
      <Tooltip key="properties" title={t('properties')}>
        <IconButton onClick={actions.properties} color="inherit">
          <Icon size="small">{iconNames.info_outlined}</Icon>
        </IconButton>
      </Tooltip>
    ),
  };

  return <div className={classes.buttons}>{Object.values(buttons)}</div>;
};

/* eslint-disable-next-line */
export default (props: MenuPropsType) => (
  <I18nResourceBundle resource={locale} namespace="IntakeMenu">
    <Menu {...props} />
  </I18nResourceBundle>
);
