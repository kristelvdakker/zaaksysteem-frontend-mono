// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { mimeTypeToInfo } from '@zaaksysteem/common/src/components/DocumentExplorer/components/FileExplorer/library/getInfo';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon/';
import { APIDocument } from '@zaaksysteem/generated';
import { SingleStepFormDefinition } from '@zaaksysteem/common/src/components/form/types';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { FormValuesType } from '@zaaksysteem/common/src/components/form/types/generic.types';
import { FormDefinitionField } from '@zaaksysteem/common/src/components/form/types/fieldDefinition.types';
import SuffixTextField from '@zaaksysteem/common/src/components/form/fields/SuffixTextField/SuffixTextField';
import { normalizeValue } from '@zaaksysteem/common/src/library/normalizeValue';
import { getRules } from '../rules/dateRules';
import { getFormDefinition } from './getFormDefinition';
import Additional from './Additional';
import { FormDefinitionFieldsType } from './getFormDefinition';

type PropertiesDialogPropsType = {
  uuid: string;
  onClose: () => void;
  onConfirm: () => void;
  onServerError: OpenServerErrorDialogType;
};

const PropertiesDialog: React.ComponentType<PropertiesDialogPropsType> = ({
  uuid,
  onClose,
  onConfirm,
  onServerError,
}) => {
  const [formDefinition, setFormDefinition] = useState<
    SingleStepFormDefinition<FormDefinitionFieldsType>
  >([]);
  const [saving, setSaving] = useState(false);
  const [t] = useTranslation();
  const rules = getRules({ t });

  useEffect(() => {
    (async function () {
      try {
        const results = await getFormDefinition({
          t,
          uuid,
        });
        setFormDefinition(
          results as SingleStepFormDefinition<FormDefinitionFieldsType>
        );
      } catch (error) {
        onServerError(error);
      }
    })();
  }, []);

  const onSubmit = async (values: FormValuesType) => {
    setSaving(true);

    try {
      await request<APIDocument.UpdateDocumentResponseBody>(
        'POST',
        '/api/v2/document/update_document',
        {
          document_uuid: uuid,
          basename: values.basename,
          description: values.description,
          document_category: normalizeValue(values.category) || null,
          origin: normalizeValue(values.origin) || null,
          confidentiality: normalizeValue(values.confidentiality) || null,
          origin_date: values.date,
        }
      );
      onConfirm();
    } catch (error) {
      onServerError(error);
    }
    setSaving(false);
  };

  const getIcon = () => {
    const DEFAULT = 'file.default';
    const field: FormDefinitionField = formDefinition.find(
      definition => definition.name === 'additional'
    ) as FormDefinitionField;

    if (!field) return DEFAULT;

    //@ts-ignore
    return mimeTypeToInfo(field.value.mimetype || DEFAULT).icon;
  };

  return formDefinition && formDefinition.length ? (
    <FormDialog
      formDefinition={formDefinition}
      title={t('Intake:properties.title')}
      icon={(<ZsIcon size="large">{getIcon()}</ZsIcon>) as any}
      onClose={onClose}
      saving={saving}
      scope="document-properties"
      open={true}
      onSubmit={onSubmit}
      fieldComponents={{
        additional: Additional,
        SuffixTextField,
      }}
      rules={rules}
    />
  ) : null;
};

export default PropertiesDialog;
