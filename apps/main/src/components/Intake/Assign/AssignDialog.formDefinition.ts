// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { SingleStepFormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { FormValuesType } from './AssignDialog.types';

export const getFormDefinition = (
  t: i18next.TFunction
): SingleStepFormDefinition<FormValuesType> => [
  {
    name: 'type',
    type: fieldTypes.RADIO_GROUP,
    value: 'user',
    required: true,
    label: t('assignDocument.type'),
    applyBackgroundColor: false,
    choices: [
      {
        label: t('assignDocument.contact.user'),
        value: 'user',
      },
      {
        label: t('assignDocument.contact.role'),
        value: 'role',
      },
    ],
  },
  {
    name: 'contact',
    type: fieldTypes.CONTACT_FINDER,
    value: null,
    required: false,
    label: t('assignDocument.contact.label'),
    translations: {
      'form:choose': t('assignDocument.contact.placeholder'),
    },
    config: {
      filterTypes: ['employee'],
    },
  },
  {
    name: 'group',
    type: fieldTypes.DEPARTMENT_FINDER,
    value: null,
    required: false,
    translations: {
      'form:choose': t('assignDocument.group.placeholder'),
    },
    label: t('assignDocument.group.label'),
  },
  {
    name: 'role',
    type: fieldTypes.ROLE_FINDER,
    value: null,
    required: false,
    placeholder: t('selectRole'),
    label: t('assignDocument.role.label'),
    translations: {
      'form:choose': t('assignDocument.role.placeholder'),
    },
  },
];
