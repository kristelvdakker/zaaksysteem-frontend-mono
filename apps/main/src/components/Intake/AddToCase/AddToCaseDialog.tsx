// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { DocumentItemType } from '@zaaksysteem/common/src/components/DocumentExplorer/components/FileExplorer/types/FileExplorerTypes';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APIDocument } from '@zaaksysteem/generated';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types';
import SuffixTextField from '@zaaksysteem/common/src/components/form/fields/SuffixTextField/SuffixTextField';
import { getRules } from '../rules/dateRules';
import { getFormDefinition, FormValuesType } from './addToCase.formDefinition';
import { addToCase } from './addToCase';
import {
  DocumentLabelSelect,
  DOCUMENT_LABEL_SELECT,
} from './DocumentLabelSelect';

type AddToCaseDialogPropsType = {
  selectedDocuments: DocumentItemType[];
  open: boolean;
  onClose: () => void;
  onConfirm: () => void;
};

export const AddToCaseDialog: React.ComponentType<AddToCaseDialogPropsType> = ({
  selectedDocuments,
  open,
  onClose,
  onConfirm,
}) => {
  const [saving, setSaving] = React.useState(false);
  const [initializing, setInitializing] = React.useState(true);
  const [selectedDocumentMetadata, setMetadata] = React.useState({});
  const [formDefinition, setFormDefinition] = React.useState(
    [] as FormDefinition
  );
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [t] = useTranslation('');
  const selectedDocument = selectedDocuments[0];
  const rules = getRules({ t });

  React.useEffect(() => {
    if (selectedDocuments.length === 1 && open) {
      request<APIDocument.GetDocumentResponseBody>(
        'GET',
        buildUrl('/api/v2/document/get_document', {
          document_uuid: selectedDocument.uuid,
        })
      ).then(response => {
        setFormDefinition(
          getFormDefinition(
            t,
            {
              description: selectedDocument.description,
              origin: response.data?.attributes.metadata?.origin,
              date: response.data?.attributes.metadata?.origin_date || null,
              basename: selectedDocument.name,
              extension: selectedDocument.extension,
            },
            false
          )
        );
        setInitializing(false);
        setMetadata({
          ...(response.data?.attributes.metadata || {}),
          basename: response.data?.attributes.basename,
        });
      });
    } else if (selectedDocuments.length > 1 && open) {
      setFormDefinition(getFormDefinition(t, {}, true));
      setInitializing(false);
    } else {
      setInitializing(true);
    }
  }, [open]);

  const handleSubmit = async (formValues: FormValuesType) => {
    setSaving(true);

    const document_label_uuids = formValues.document_label_uuids.map(
      option => option.value
    );

    const mainPromise = Promise.all(
      selectedDocuments.map(({ uuid }) =>
        addToCase({
          case_uuid: formValues.case_uuid,
          document_label_uuids,
          document_uuid: uuid,
        })
      )
    )
      .then(onConfirm)
      .catch(openServerErrorDialog)
      .then(() => setSaving(false));

    if (selectedDocuments.length === 1) {
      const origin = (formValues.origin as any)?.value || formValues.origin;
      const description = formValues.description;
      const date = formValues.date || null;
      const basename = formValues.basename;

      await request<APIDocument.UpdateDocumentResponseBody>(
        'POST',
        '/api/v2/document/update_document',
        {
          ...selectedDocumentMetadata,
          document_uuid: selectedDocument.uuid,
          description,
          origin,
          origin_date: date,
          basename,
        }
      );
    }

    await mainPromise;
  };

  return (
    <React.Fragment>
      {ServerErrorDialog}
      <FormDialog
        formDefinition={formDefinition}
        title={t('main:intake.addToCase.title')}
        icon="arrow_back"
        onClose={onClose}
        saving={saving}
        scope="add-to-case"
        open={open}
        initializing={initializing}
        fieldComponents={{
          [DOCUMENT_LABEL_SELECT]: DocumentLabelSelect,
          SuffixTextField,
        }}
        onSubmit={(formValues: any) => handleSubmit(formValues)}
        saveLabel={t('common:forms.add')}
        rules={rules}
      />
    </React.Fragment>
  );
};
