// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { SingleStepFormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { DOCUMENT_LABEL_SELECT } from './DocumentLabelSelect';

export type FormValuesType = {
  case_uuid: string;
  description: string;
  origin: 'Inkomend';
  document_label_uuids: { value: string; label: string }[];
  date: string | null;
  basename: string;
};

export const getFormDefinition: (
  t: i18next.TFunction,
  initialValues: {
    description?: string;
    origin?: string;
    date?: string | null;
    basename?: string;
    extension?: string;
  },
  compact: boolean
) => SingleStepFormDefinition<FormValuesType> = (t, initialValues, compact) => {
  const originChoices = [
    {
      label: t('main:intake.addToCase.origin.choices.inkomend'),
      value: 'Inkomend',
    },
    {
      label: t('main:intake.addToCase.origin.choices.uitgaand'),
      value: 'Uitgaand',
    },
    {
      label: t('main:intake.addToCase.origin.choices.intern'),
      value: 'Intern',
    },
  ];
  return [
    {
      name: 'basename',
      type: 'SuffixTextField',
      label: t('main:intake.addToCase.name.label'),
      placeholder: t('main:intake.addToCase.name.placeholder'),
      value: initialValues.basename || '',
      required: true,
      hint: t('main:intake.addToCase.name.hint'),
      config: {
        suffix: initialValues.extension || '',
      },
    },
    {
      name: 'case_uuid',
      type: fieldTypes.CASE_FINDER,
      value: '',
      required: true,
      label: t('main:intake.addToCase.case_uuid.label'),
      isSearchable: true,
      filter: { status: ['new', 'open', 'stalled'] },
      translations: {
        'form:choose': t('main:intake.addToCase.case_uuid.placeholder'),
      },
    },
    {
      name: 'description',
      type: fieldTypes.TEXTAREA,
      multi: true,
      value: initialValues.description,
      required: false,
      hidden: compact,
      label: t('main:intake.addToCase.description.label'),
      placeholder: t('main:intake.addToCase.description.placeholder'),
    },
    {
      name: 'origin',
      type: fieldTypes.SELECT,
      value:
        originChoices.find(option => option.value === initialValues.origin) ||
        originChoices[0],
      choices: originChoices,
      hidden: compact,
      label: t('main:intake.addToCase.origin.label'),
    },
    {
      name: 'date',
      type: fieldTypes.DATEPICKER,
      value: initialValues.date,
      required: false,
      variant: 'inline',
      placeholder: t('Intake:properties.additional.date.placeholder'),
    },
    {
      name: 'document_label_uuids',
      type: DOCUMENT_LABEL_SELECT,
      value: [],
      isSearchable: false,
      required: false,
      isMulti: true,
      hidden: compact,
      label: t('main:intake.addToCase.document_label_uuids.label'),
      noOptionsMessage: () =>
        t('main:intake.addToCase.document_label_uuids.noOptions'),
      translations: {
        'form:choose': t(
          'main:intake.addToCase.document_label_uuids.placeholder'
        ),
      },
    },
  ];
};
