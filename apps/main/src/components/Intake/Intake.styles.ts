// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useIntakeStyles = makeStyles(() => {
  return {
    wrapper: {
      maxWidth: 1100,
      margin: 'auto',
    },
    popper: {
      '& > div': {
        maxWidth: 'none',
        borderRadius: 2,
        padding: 4,
      },
    },
  };
});
