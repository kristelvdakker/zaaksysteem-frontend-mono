// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APIDocument } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';

export const deleteDocument = async (
  body: APIDocument.DeleteDocumentRequestBody
) =>
  await request<APIDocument.DeleteDocumentResponseBody>(
    'POST',
    '/api/v2/document/delete_document',
    body
  );
