// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import {
  GEOJSON_MAP_INPUT,
  ADDRESS,
} from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';

const Demo = () => {
  React.useEffect(() => {
    document.body.style.height = '200px';
    document.body.style.overflow = 'scroll';
  }, []);

  const [mapValue, setMapValue] = React.useState(null);
  //@ts-ignore
  const { fields } = useForm({
    formDefinition: [
      {
        name: 'location',
        type: GEOJSON_MAP_INPUT,
        required: false,
        label: 'This map will show features of object',
        placeholder: 'placeholder',
        readOnly: true,
        value: null,
      },
      {
        name: 'adddress',
        type: ADDRESS,
        required: false,
        label: 'Address field',
        placeholder: 'placeholder',
        readOnly: false,
        value: null,
      },
    ],
  });

  const getObjectGeoFeatures = (uuid: string) => {
    request('GET', '/api/v2/geo/get_geo_features?uuid=' + uuid).then(
      response => {
        setMapValue(response.data[0].attributes.geo_json);
      }
    );
  };

  return (
    <React.Fragment>
      <Select
        creatable={true}
        isMulti={false}
        onChange={event => getObjectGeoFeatures(event.target.value.value)}
        name="Object Uuid"
        placeholder="Type Object uuid and hit enter"
      />
      {fields.map(({ FieldComponent, label, value, name, ...rest }, index) => {
        return index === 0 ? (
          <div key={name} style={{ padding: 10, width: 650 }}>
            <h3>{label}</h3>
            <div style={{ height: 400, width: 650 }}>
              {
                <FieldComponent
                  {...rest}
                  name={name}
                  value={mapValue || value}
                />
              }
            </div>
            {<span>Value: {JSON.stringify(mapValue || value)}</span>}
          </div>
        ) : (
          <div key={name} style={{ padding: 10, width: 650 }}>
            <h3>{label}</h3>
            {<FieldComponent {...rest} name={name} value={value} />}
            {<span>Value: {JSON.stringify(value)}</span>}
          </div>
        );
      })}
    </React.Fragment>
  );
};

export default Demo;
