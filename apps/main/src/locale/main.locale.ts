// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    modules: {
      dashboard: 'Dashboard',
      object: 'Object',
      communication: 'Communicatie',
      admin: 'Beheren',
    },
    links: {
      intake: 'Documentintake',
      search: 'Uitgebreid zoeken',
      contactSearch: 'Contacten zoeken',
      export: 'Exportbestanden',
      help: 'Help',
      about: 'Over Zaaksysteem.nl',
      aboutShort: 'Over…',
      logout: 'Uitloggen',
    },
    intake: {
      addToCase: {
        title: 'Toevoegen aan lopende zaak',
        name: {
          label: 'Documentnaam (zonder bestandsextensie)',
          placeholder: 'Bestandsnaam',
          hint:
            'Voer de naam van het document in. Voeg geen bestandsextensie aan het einde toe.',
        },
        origin: {
          label: 'Richting',
          choices: {
            inkomend: 'Inkomend',
            uitgaand: 'Uitgaand',
            intern: 'Intern',
          },
        },
        description: {
          placeholder: 'Omschrijving',
          label: 'Omschrijving',
        },
        case_uuid: {
          label: 'Zaak',
          placeholder: 'Selecteer zaak…',
        },
        document_label_uuids: {
          label: 'Zaakdocument',
          placeholder: 'Selecteer zaakdocument',
          noOptions: 'Geen zaakdocumenten',
        },
      },
      caseCreate: {
        title: 'Zaak aanmaken',
        save: 'Volgende',
        caseType: {
          label: 'Zaaktype',
          placeholder: 'Selecteer zaaktype',
        },
        requestor: {
          label: 'Aanvrager',
          placeholder: 'Selecteer aanvrager',
          choices: {
            person: 'Burger',
            organization: 'Organisatie',
            employee: 'Medewerker',
          },
          import: 'Importeer een contact',
        },
        recipient: {
          label: 'Ontvanger',
          placeholder: 'Selecteer ontvanger',
        },
        contactChannel: {
          label: 'Contactkanaal',
          placeholder: 'Selecteer contactkanaal',
        },
      },
    },
  },
};
