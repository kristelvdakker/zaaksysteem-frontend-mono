// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICaseManagement } from '@zaaksysteem/generated';
import { buildUrl } from '@mintlab/kitchen-sink/source';

const fetchRelatedSubjects = async (contactId: string) => {
  const url = buildUrl<APICaseManagement.GetSubjectRequestParams>(
    '/api/v2/cm/subject/get_related_subjects',
    {
      subject_uuid: contactId,
    }
  );
  const result = await request('GET', url);

  return result.data || [];
};

const fetchRelatedObjects = async (contactId: string) => {
  const url = buildUrl<APICaseManagement.GetSubjectRequestParams>(
    '/api/v2/cm/subject/get_related_objects',
    {
      subject_uuid: contactId,
    }
  );
  const result = await request('GET', url);

  return result.data || [];
};

export const handlePromise = async (contactId: string) => {
  const relatedSubjects = await fetchRelatedSubjects(contactId);
  const relatedObjects = await fetchRelatedObjects(contactId);

  const subjects = relatedSubjects.map((row: any) => ({
    uuid: row.id || '',
    name: row.meta?.summary || '',
    subjectType: row.attributes?.type,
    roleType: row.attributes?.roles,
  }));

  const objects = relatedObjects.map((row: any) => ({
    uuid: row.id,
    name: row.meta?.summary || '',
    objectType: row.relationships?.object_type?.meta.summary,
  }));

  return {
    subjects,
    objects,
  };
};
