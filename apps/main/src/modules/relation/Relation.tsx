// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { RouteComponentProps } from 'react-router';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import RelatedSubjects from '../objectView/components/Relationships/components/RelatedSubjects';
import RelatedObjects from '../objectView/components/Relationships/components/RelatedObjects';
import locale from '../objectView/locale/objectView.locale';
import { handlePromise } from './Relation.actions';
import { RelationPropTypes } from './Relation.types';

const Relation: React.FunctionComponent<RelationPropTypes> = ({
  contactId,
}) => {
  return (
    <DataProvider
      provider={handlePromise}
      providerArguments={[contactId]}
      autoProvide={true}
    >
      {({ data, busy }) => {
        if (busy) {
          return <Loader />;
        }

        return (
          data && (
            <>
              <RelatedSubjects subjects={data.subjects} />
              <RelatedObjects objects={data.objects} />
            </>
          )
        );
      }}
    </DataProvider>
  );
};

const RelationModule: React.FunctionComponent<
  RouteComponentProps<{
    contactId: string;
  }>
> = ({ match }) => (
  <I18nResourceBundle resource={locale} namespace="objectView">
    <Relation contactId={match.params.contactId} />
  </I18nResourceBundle>
);

export default RelationModule;
