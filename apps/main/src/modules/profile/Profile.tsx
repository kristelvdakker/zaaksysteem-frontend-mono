// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { RouteComponentProps } from 'react-router';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import Button from '@mintlab/ui/App/Material/Button';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { SubHeader } from '../objectView/components/attributes/SubHeader';
import { handlePromise } from './Profile.actions';
import { ProfilePropsType } from './Profile.types';
import { useProfileStyles } from './Profile.style';
import locale from './profile.locale';
import { Attributes } from './Attributes';

const Profile: React.FunctionComponent<ProfilePropsType> = ({ match }) => {
  const { contactId, contactType } = match.params;
  const [t] = useTranslation();
  const classes = useProfileStyles();

  return (
    <DataProvider
      provider={handlePromise}
      providerArguments={[t, contactId, contactType]}
      autoProvide={true}
    >
      {({ data, busy }) => {
        if (busy || !data) {
          return <Loader />;
        }

        const { title, description, formDefinition, uuid } = data;

        const locationHref = (uuid: number) =>
          (window.top.location.href = `/main/object/${uuid}`);

        return (
          <div className={classes.wrapper}>
            <SubHeader title={title} description={description} />
            <Attributes formDefinition={formDefinition} />
            <Button
              onClick={() => locationHref(uuid)}
              presets={['default', 'link']}
            >
              {t('profile:link')}
            </Button>
          </div>
        );
      }}
    </DataProvider>
  );
};

const ProfileModule: React.FunctionComponent<
  RouteComponentProps<{
    contactId: string;
    contactType: string;
  }>
> = ({ match }) => (
  <I18nResourceBundle resource={locale} namespace="profile">
    <Profile match={match} />
  </I18nResourceBundle>
);

export default ProfileModule;
