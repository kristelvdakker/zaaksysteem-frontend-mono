// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import fecha from 'fecha';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICaseManagement } from '@zaaksysteem/generated';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { ColumnType } from '@mintlab/ui/App/Zaaksysteem/SortableTable/types/SortableTableTypes';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { ItemValueType } from '@zaaksysteem/common/src/components/form/fields/Options/Options.types';
import {
  CaseTableRowType,
  ClassesType,
  TasksRowType,
  rowType,
  FilterType,
  Types,
} from './types/Tasks.types';

export const getTasks = async () => {
  const url = buildUrl<APICaseManagement.GetTaskListRequestParams>(
    '/api/v2/cm/task/get_task_list',
    {
      filter: {
        'attributes.completed': false,
      },
      page: 1,
      rows_per_page: 1000,
    }
  );

  const results = await request('GET', url);

  return results.data || [];
};

const getDays = (classes: ClassesType, end_date?: string) => {
  if (end_date) {
    const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    const today = new Date().setHours(0, 0, 0);
    const parts = end_date.split('-').map(part => Number(part));
    const deadline = new Date(
      parts[0],
      parts[1],
      parts[2],
      0,
      0,
      0,
      0
    ).getTime();

    const isPastDate = today > deadline && today - deadline > oneDay;
    const daysLeft = Math.round(
      Math.abs((today.valueOf() - deadline.valueOf()) / oneDay)
    );

    return (
      end_date && (
        <div
          className={`${classes.daysLeft} ${
            isPastDate ? classes.hasPassed : ''
          }`}
        >
          {isPastDate ? `${'-'}${daysLeft}` : daysLeft}
        </div>
      )
    );
  } else {
    return '';
  }
};

export const getRows = (
  data: rowType,
  searchTerm: string,
  filters: FilterType[]
): TasksRowType[] => {
  if (!data) return [];

  const filter_parameter = (type: string) => {
    return filters
      .filter(filter => filter?.data?.type === type)
      .map(filter => filter.value);
  };

  return data
    .map(row => ({
      uuid: row.id,
      name: `tasktable`,
      display_name: row.relationships?.assignee?.data.meta.display_name,
      assignee_id: row.relationships?.assignee?.data.id,
      display_number: row.relationships?.case.meta?.display_number,
      case_id: row.relationships?.case.data.id,
      title: row.attributes.title,
      due_date: row.attributes.due_date,
      case_type: row.relationships?.case_type?.data.id,
      department: row.relationships?.department?.data.id,
    }))
    .filter(row => {
      const assignees = filter_parameter('assignee');
      return (
        !assignees.length ||
        (row.assignee_id && assignees.includes(row.assignee_id.toString()))
      );
    })
    .filter(row => {
      const case_number = filter_parameter('case_number');
      return (
        !case_number.length ||
        (row.display_number &&
          case_number.includes(row.display_number.toString()))
      );
    })
    .filter(row => {
      const case_type = filter_parameter('case_type');
      return (
        !case_type.length ||
        (row.case_type && case_type.includes(row.case_type.toString()))
      );
    })
    .filter(row => {
      const department = filter_parameter('department');
      return (
        !department.length ||
        (row.department && department.includes(row.department.toString()))
      );
    })
    .filter(row => {
      const keyword = filter_parameter('keyword');
      return (
        !keyword.length ||
        (row.title &&
          keyword.some(
            (keywordEntry: string) =>
              row.title.toLowerCase().indexOf(keywordEntry.toLowerCase()) !== -1
          ))
      );
    })
    .filter(row => {
      return Object.values(row).some(cell =>
        cell
          ? cell.toString().toLowerCase().includes(searchTerm.toLowerCase())
          : false
      );
    });
};

export const getAllColumns = ({
  classes,
  t,
}: {
  classes?: ClassesType;
  t: i18next.TFunction;
}): ColumnType[] => {
  return [
    {
      name: 'display_number',
      width: 140,
      label: t('tasks:columns.display_number.label'),
    },
    {
      name: 'title',
      width: 200,
      label: t('tasks:columns.title.label'),
      flexGrow: 1,
    },
    {
      name: 'display_name',
      width: 200,
      label: 'Behandelaar',
    },
    {
      name: 'due_date',
      width: 200,
      label: t('tasks:columns.due_date.label'),
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: { rowData: CaseTableRowType }) => (
        <div>
          {rowData.due_date &&
            fecha.format(
              new Date(rowData.due_date as string),
              t('common:dates.dateFormatTextShort')
            )}
        </div>
      ),
    },
    {
      name: 'days_left',
      width: 100,
      label: t('tasks:columns.days_left.label'),
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: { rowData: CaseTableRowType }) =>
        classes ? getDays(classes, rowData.due_date) : '',
    },
  ];
};

export const getWidgetData = async (
  widgetUuid: string
): Promise<{
  filters: FilterType[];
  columns: ItemValueType[];
}> => {
  const url = buildUrl<any>('/api/v1/dashboard/widget', {
    page: 1,
    rows_per_page: 20,
  });

  const data = await request('GET', url);

  const widgetFilters = data.result.instance.rows.find(
    (option: any) => option.reference === widgetUuid
  );

  return {
    filters: widgetFilters?.instance?.data?.filters || [],
    columns: widgetFilters?.instance?.data?.columns || [],
  };
};

export const getIcon = (type: Types) => {
  const map: { [index: string]: React.ReactElement } = {
    assignee: <Icon size="extraSmall">{iconNames.person}</Icon>,
    keyword: <Icon size="extraSmall">{iconNames.spellcheck}</Icon>,
    case_number: <Icon size="extraSmall">{iconNames.hash}</Icon>,
    case_type: <Icon size="extraSmall">{iconNames.label}</Icon>,
    department: <Icon size="extraSmall">{iconNames.people}</Icon>,
  };
  return map[type];
};

export const getFilteredColumns = ({
  allColumns,
  columns,
}: {
  allColumns: ColumnType[];
  columns: ItemValueType[];
}) =>
  columns
    .map(filter =>
      filter.active ? allColumns.find(col => col.name === filter.id) : null
    )
    .filter(Boolean);
