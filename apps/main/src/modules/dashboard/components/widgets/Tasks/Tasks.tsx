// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { SessionRootStateType } from '@zaaksysteem/common/src/store/session/session.reducer';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { useSortableTableStyles } from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable.style';
import { ItemValueType } from '@zaaksysteem/common/src/components/form/fields/Options/Options.types';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Widget from '../Widget';
import { updateWidgetPostMessage } from '../Widget.library';
import { useDashboardSortableTableStyles } from '../../Dashboard.style';
import TasksHeader from './TasksHeader';
import { useTasksStyles } from './Tasks.style';
import locale from './Tasks.locale';
import {
  getTasks,
  getRows,
  getAllColumns,
  getWidgetData,
  getFilteredColumns,
} from './Tasks.library';
import {
  TasksPropsType,
  FilterType,
  CaseTableRowType,
} from './types/Tasks.types';

const Tasks: React.FunctionComponent<TasksPropsType> = ({ match }) => {
  const [searchTerm, setSearchTerm] = useState('');
  const [filters, setFilters] = useState<FilterType[]>([]);
  const [columns, setColumns] = useState<ItemValueType[]>([]);
  const [effectBusy, setEffectBusy] = useState(true);
  const [t] = useTranslation();
  const classes = useTasksStyles();
  const tableStyles = useSortableTableStyles();
  const dashboardStyles = useDashboardSortableTableStyles();
  const widgetUuid = match.params.widgetUuid;
  const didMountRef = useRef(false);
  const allColumns = getAllColumns({ classes, t });

  const userUuid = useSelector(
    (state: SessionRootStateType) => state.session.data?.logged_in_user.uuid
  );

  const userDisplayName = useSelector(
    (state: SessionRootStateType) =>
      state.session.data?.logged_in_user.display_name
  );

  const locationHref = (id: number) =>
    (window.top.location.href = `/intern/zaak/${id}`);

  const handleSearch = (ev: string) => {
    return setSearchTerm(ev);
  };

  useEffect(() => {
    getWidgetData(widgetUuid).then(({ filters, columns }) => {
      setFilters(
        filters && filters.length
          ? filters
          : [
              {
                label: userDisplayName,
                value: userUuid || '',
                data: { type: 'assignee' },
              },
            ]
      );

      setColumns(
        columns && columns.length
          ? columns
          : allColumns.map(col => ({
              id: col.name,
              value: String(col.label),
              active: true,
              isNew: false,
            }))
      );

      setEffectBusy(false);
    });
  }, []);

  useEffect(() => {
    if (!filters || !columns) return;

    if (didMountRef.current) {
      updateWidgetPostMessage({ widgetUuid, filters, columns });
    } else {
      didMountRef.current = true;
    }
  }, [filters, columns]);

  return (
    <DataProvider
      provider={getTasks}
      providerArguments={[userUuid]}
      autoProvide={true}
    >
      {({ data, busy }) => {
        if (busy || effectBusy) {
          return <Loader />;
        }

        const filteredRows = getRows(data, searchTerm, filters);
        const filteredColumns = getFilteredColumns({ allColumns, columns });

        return (
          data && (
            <Widget
              header={
                <TasksHeader
                  title={t('tasks:title', {
                    counter: filteredRows.length,
                  })}
                  widgetUuid={widgetUuid}
                  onChange={handleSearch}
                  filters={filters}
                  setFilters={setFilters}
                  columns={columns}
                  setColumns={setColumns}
                />
              }
            >
              <SortableTable
                rows={filteredRows}
                //@ts-ignore
                columns={filteredColumns}
                noRowsMessage={
                  searchTerm || filters.length
                    ? t('tasks:noRowsFilters')
                    : t('tasks:no_rows')
                }
                loading={false}
                rowHeight={53}
                onRowClick={(
                  { rowData }: { rowData: CaseTableRowType },
                  event: React.MouseEvent
                ) => {
                  locationHref(rowData.display_number);
                  event.preventDefault();
                }}
                onRowDoubleClick={() => {}}
                styles={{ ...tableStyles, ...dashboardStyles }}
              />
            </Widget>
          )
        );
      }}
    </DataProvider>
  );
};

const TasksModule: React.FunctionComponent<
  RouteComponentProps<{
    widgetUuid: string;
  }>
> = ({ match }) => (
  <I18nResourceBundle resource={locale} namespace="tasks">
    <Tasks match={match} />
  </I18nResourceBundle>
);

export default TasksModule;
