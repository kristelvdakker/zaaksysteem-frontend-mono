// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { RowType } from '@mintlab/ui/App/Zaaksysteem/SortableTable/types/SortableTableTypes';
import { useTasksStyles } from '../Tasks.style';

export type ClassesType = ReturnType<typeof useTasksStyles>;

export type Types =
  | 'assignee'
  | 'keyword'
  | 'case_number'
  | 'case_type'
  | 'department';

export type TasksPropsType = {
  match: {
    params: {
      widgetUuid: string;
    };
  };
};

export type CaseTableRowType = {
  display_number: number;
  due_date: string | undefined;
  days_left: string | undefined;
};

export interface TasksRowType extends RowType {
  display_number?: number;
  display_name?: string;
  title: string;
  due_date: string;
}

export type rowType = APICaseManagement.GetTaskListResponseBody['data'];

export interface FilterDataType {
  type: Types;
}

export type FilterType = ValueType<string, FilterDataType>;

export type TasksFormDefinitionType = {
  assignee: FilterType;
  case_number: FilterType;
  keyword: FilterType;
  department: FilterType;
  case_type: FilterType;
};

export type FormValuesType = {
  [T in Types]: FilterType[];
};
