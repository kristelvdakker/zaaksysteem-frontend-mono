// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { ItemValueType } from '@zaaksysteem/common/src/components/form/fields/Options/Options.types';
import * as yup from 'yup';

type ColumnsConfigDialogPropsType = {
  value: ItemValueType[];
  onClose: () => void;
  onSubmit: (val: any) => void;
};

const ColumnsConfigDialog: React.ComponentType<ColumnsConfigDialogPropsType> = ({
  value,
  onClose,
  onSubmit,
}) => {
  const [t] = useTranslation();

  const formDefinition = [
    {
      name: 'columns',
      type: fieldTypes.OPTIONS,
      value,
      required: true,
      config: {
        allowNew: false,
      },
      multiValue: true,
    },
  ];

  return (
    <FormDialog
      formDefinition={formDefinition}
      title={t('tasks:columnsSettings')}
      onClose={onClose}
      saving={false}
      scope="columns-config-dialog"
      open={true}
      onSubmit={onSubmit}
      validationMap={{
        columns: activeValidation,
      }}
    />
  );
};

const activeValidation = () =>
  yup.mixed().test('has-active', '-', value =>
    //@ts-ignore
    value.some(item => item.active)
  );

export default ColumnsConfigDialog;
