// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const locale = {
  nl: {
    save: 'Opslaan',
    cancel: 'Annuleren',
    fields: {
      assignee: {
        label: 'Behandelaar',
        choose: 'Zoek een behandelaar…',
      },
      case_nr: {
        label: 'Zaaknummer',
        choose: 'Voeg een zaaknummer toe…',
      },
      keyword: {
        label: 'Taak met de woorden',
        choose: 'Voeg een woord toe…',
      },
      department: {
        label: 'Afdeling',
        choose: 'Kies een afdeling…',
      },
      case_type: {
        label: 'Zaaktype',
        choose: 'Zoek een zaaktype…',
      },
    },
  },
};

export default locale;
