// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
//import React from 'react';
import { SingleStepFormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { Theme } from '@mintlab/ui/types/Theme';
import { DepartmentFinderOptionType } from '@zaaksysteem/common/src/components/form/fields/DepartmentFinder/DepartmentFinder.types';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import * as i18next from 'i18next';
import {
  FilterType,
  TasksFormDefinitionType,
  FormValuesType,
  Types,
} from '../../Tasks/types/Tasks.types';
import { getIcon } from '../../Tasks/Tasks.library';
import { multiValueLabelStyles } from '../MultiValueLabel.style';

type AnyFilterOptionType = ValueType<string, any> | DepartmentFinderOptionType;
const noValue = (filters: FilterType[]) => !filters || !filters.length;

export const getMultiValueValue = ({
  filters,
  type,
}: {
  filters: FilterType[];
  type: Types;
}) => {
  if (noValue(filters)) return null;
  const results = filters
    .map(filter => (filter.data?.type === type ? filter : null))
    .filter(Boolean);
  return results && results.length ? results : null;
};

export const getFormDefinition = ({
  filters,
  t,
  theme,
}: {
  filters: FilterType[];
  t: i18next.TFunction;
  theme: Theme;
}): SingleStepFormDefinition<TasksFormDefinitionType> => {
  const styles = getStyles(theme);
  return [
    {
      name: 'assignee',
      label: t('fields.assignee.label'),
      type: fieldTypes.CONTACT_FINDER,
      value: getMultiValueValue({ filters, type: 'assignee' }),
      multiValue: true,
      translations: {
        'form:choose': t('fields.assignee.choose'),
      },
      config: {
        filterTypes: ['employee'],
        multiValueLabelIcon: () => getIcon('assignee'),
      },
      styles,
    },
    {
      name: 'case_number',
      label: t('fields.case_nr.label'),
      type: fieldTypes.MULTI_VALUE_TEXT,
      value: getMultiValueValue({ filters, type: 'case_number' }),
      multiValue: true,
      translations: {
        'form:creatable': t('fields.case_nr.choose'),
      },
      config: {
        multiValueLabelIcon: () => getIcon('case_number'),
      },
      styles,
    },
    {
      name: 'keyword',
      label: t('fields.keyword.label'),
      type: fieldTypes.MULTI_VALUE_TEXT,
      value: getMultiValueValue({ filters, type: 'keyword' }),
      multiValue: true,
      translations: {
        'form:creatable': t('fields.keyword.choose'),
      },
      config: {
        multiValueLabelIcon: () => getIcon('keyword'),
      },
      styles,
    },
    {
      name: 'department',
      label: t('fields.department.label'),
      type: fieldTypes.DEPARTMENT_FINDER,
      value: getMultiValueValue({ filters, type: 'department' }),
      multiValue: true,
      translations: {
        'form:choose': t('fields.department.choose'),
      },
      config: {
        multiValueLabelIcon: () => getIcon('department'),
      },
      styles,
    },
    {
      name: 'case_type',
      label: t('fields.case_type.label'),
      value: getMultiValueValue({ filters, type: 'case_type' }),
      multiValue: true,
      type: fieldTypes.CASE_TYPE_FINDER,
      translations: {
        'form:choose': t('fields.case_type.choose'),
      },
      config: {
        multiValueLabelIcon: () => getIcon('case_type'),
      },
      styles,
    },
  ];
};

const hasValue = (value: FilterType[]) => {
  if (Array.isArray(value)) {
    return value && value.length;
  }

  return Boolean(value);
};

export const getReturnValues = (values: FormValuesType): FilterType[] => {
  return [
    ...(hasValue(values.assignee)
      ? values.assignee.map(getChoice('assignee'))
      : []),
    ...(hasValue(values.department)
      ? values.department.map(getChoice('department'))
      : []),
    ...(hasValue(values.case_type)
      ? values.case_type.map(getChoice('case_type'))
      : []),
    ...(hasValue(values.case_number)
      ? values.case_number.map(getChoice('case_number'))
      : []),
    ...(hasValue(values.keyword)
      ? values.keyword.map(getChoice('keyword'))
      : []),
  ];
};

const getChoice = (type: Types) => (
  filter: AnyFilterOptionType
): FilterType => ({
  value: filter.value,
  label: filter?.data?.textLabel || filter.label,
  data: { type },
});

const getStyles = (theme: Theme) => {
  return {
    ...multiValueLabelStyles({
      theme,
    }),
    valueContainer(base: any) {
      return {
        ...base,
        padding: '11px 14px',
      };
    },
  };
};
