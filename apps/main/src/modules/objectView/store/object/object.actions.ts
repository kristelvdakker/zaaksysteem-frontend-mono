// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICaseManagement } from '@zaaksysteem/generated';
import { OBJECT_FETCH, OBJECT_UPDATE } from './object.constants';

const fetchObjectAction = createAjaxAction(OBJECT_FETCH);
const updateObjectAction = createAjaxAction(OBJECT_UPDATE);

export type FetchObjectActionPayloadType = {
  uuid: string;
};

export const fetchObject = (uuid: string) =>
  fetchObjectAction<FetchObjectActionPayloadType>({
    url: buildUrl<APICaseManagement.GetCustomObjectRequestParams>(
      '/api/v2/cm/custom_object/get_custom_object',
      {
        uuid,
      }
    ),
    method: 'GET',
  });

export const updateObject = (
  relatedCasesUuids: string[],
  objectUuid: string,
  values: { [key: string]: any }
) => {
  const data: any /* APICaseManagement.UpdateCustomObjectRequestBody */ = {
    uuid: v4(),
    existing_uuid: objectUuid,
    custom_fields: values,
    // this is mock data, but it's required by the API
    archive_metadata: {
      status: 'to preserve',
      ground: '',
      retention: 0,
    },
    related_to: {
      cases: relatedCasesUuids,
    },
  };

  return updateObjectAction<APICaseManagement.UpdateCustomObjectResponseBody>({
    url: '/api/v2/cm/custom_object/update_custom_object',
    method: 'POST',
    data,
    payload: data,
  });
};
