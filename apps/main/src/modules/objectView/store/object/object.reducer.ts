// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Reducer } from 'redux';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { APICaseManagement } from '@zaaksysteem/generated';
import { handleAjaxStateChange } from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import { ObjectType } from '../../types/ObjectView.types';
import { OBJECT_FETCH } from './object.constants';
import { FetchObjectActionPayloadType } from './object.actions';

export interface ObjectStateType {
  object?: ObjectType;
  state: AjaxState;
}

const initialState: ObjectStateType = {
  state: AJAX_STATE_INIT,
};

const handleFetchObjectSuccess = (
  state: ObjectStateType,
  action: AjaxAction<
    APICaseManagement.GetCustomObjectResponseBody,
    FetchObjectActionPayloadType
  >
): ObjectStateType => {
  const object = action.payload.response.data;

  if (!object) {
    return state;
  }

  const {
    id,
    attributes: {
      custom_fields,
      date_created,
      last_modified,
      status,
      title,
      version_independent_uuid,
    },
    relationships,
  } = object;

  const relatedCasesUuids = relationships?.cases
    ?.map(caseRelation => caseRelation.data.id || '')
    .filter(Boolean);

  return {
    ...state,
    object: {
      uuid: id,
      customFieldsValues: custom_fields || {},
      lastModified: last_modified,
      registrationDate: date_created,
      title: title || '',
      objectTypeVersionUuid:
        object.relationships?.custom_object_type?.data.id || '',
      versionIndependentUuid: version_independent_uuid || '',
      status,
      relatedCasesUuids,
    },
  };
};

const handleObjectViewStateChange = handleAjaxStateChange(OBJECT_FETCH);

export const objectReducer: Reducer<ObjectStateType, AjaxAction<unknown>> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case OBJECT_FETCH.PENDING:
    case OBJECT_FETCH.ERROR:
      return handleObjectViewStateChange(state, action as AjaxAction<{}>);
    case OBJECT_FETCH.SUCCESS:
      return handleObjectViewStateChange(
        handleFetchObjectSuccess(
          state,
          action as AjaxAction<
            APICaseManagement.GetCustomObjectResponseBody,
            FetchObjectActionPayloadType
          >
        ),
        action
      );

    default:
      return state;
  }
};

export default objectReducer;
