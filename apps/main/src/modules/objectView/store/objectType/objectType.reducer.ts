// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Reducer } from 'redux';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { APICaseManagement } from '@zaaksysteem/generated';
import { handleAjaxStateChange } from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import { ObjectTypeType } from '../../types/ObjectView.types';
import { FETCH_OBJECT_TYPE } from './objectType.constants';
import { FetchObjectTypeActionPayloadType } from './objectType.actions';

export interface ObjectTypeStateType {
  objectType?: ObjectTypeType;
  state: AjaxState;
}

const initialState: ObjectTypeStateType = {
  state: AJAX_STATE_INIT,
};

const handleObjectTypeFetchSuccess = (
  state: ObjectTypeStateType,
  action: AjaxAction<
    APICaseManagement.GetCustomObjectTypeResponseBody,
    FetchObjectTypeActionPayloadType
  >
): ObjectTypeStateType => {
  const { response } = action.payload;

  if (!response.data?.attributes?.custom_field_definition?.custom_fields) {
    return {
      ...state,
    };
  }

  return {
    ...state,
    objectType: {
      ...response.data.attributes,
      customFieldsDefinition:
        response.data.attributes.custom_field_definition.custom_fields,
    },
  };
};

const handleObjectTypeStateChange = handleAjaxStateChange(FETCH_OBJECT_TYPE);

export const objectTypeReducer: Reducer<
  ObjectTypeStateType,
  AjaxAction<unknown>
> = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_OBJECT_TYPE.PENDING:
    case FETCH_OBJECT_TYPE.ERROR:
      return handleObjectTypeStateChange(state, action as AjaxAction<{}>);
    case FETCH_OBJECT_TYPE.SUCCESS:
      return handleObjectTypeStateChange(
        handleObjectTypeFetchSuccess(
          state,
          action as AjaxAction<
            APICaseManagement.GetCustomObjectTypeResponseBody,
            FetchObjectTypeActionPayloadType
          >
        ),
        action
      );

    default:
      return state;
  }
};

export default objectTypeReducer;
