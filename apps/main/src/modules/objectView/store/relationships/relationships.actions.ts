// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICaseManagement } from '@zaaksysteem/generated';
import {
  FETCH_CASES,
  FETCH_OBJECTS,
  FETCH_SUBJECTS,
} from './relationships.constants';

const fetchCasesAction = createAjaxAction(FETCH_CASES);
const fetchObjectsAction = createAjaxAction(FETCH_OBJECTS);
const fetchSubjectsAction = createAjaxAction(FETCH_SUBJECTS);

export type PayloadType = { uuid: string };

export const fetchCases = (uuid: string) =>
  fetchCasesAction<PayloadType>({
    url: buildUrl<APICaseManagement.GetRelatedCasesRequestParams>(
      '/api/v2/cm/custom_object/get_related_cases',
      {
        uuid,
      }
    ),
    method: 'GET',
  });

export const fetchObjects = (uuid: string) =>
  fetchObjectsAction<PayloadType>({
    url: buildUrl<APICaseManagement.GetRelatedObjectsRequestParams>(
      '/api/v2/cm/custom_object/get_related_objects',
      {
        uuid,
      }
    ),
    method: 'GET',
  });

export const fetchSubjects = (uuid: string) =>
  fetchSubjectsAction<PayloadType>({
    url: buildUrl<APICaseManagement.GetRelatedSubjectsRequestParams>(
      '/api/v2/cm/custom_object/get_related_subjects',
      {
        uuid,
      }
    ),
    method: 'GET',
  });
