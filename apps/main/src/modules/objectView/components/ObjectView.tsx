// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Route, Switch } from 'react-router-dom';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { AJAX_STATE_VALID } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { objectStateSelector } from '../store/selectors/objectStateSelector';
import { PanelLayout } from '../../../components/PanelLayout/PanelLayout';
import { Panel } from '../../../components/PanelLayout/Panel';
import BreadcrumbBar, {
  BreadcrumbBarPropsType,
} from '../../../components/BreadcrumbBar/BreadcrumbBar';
import { createPath } from '../../../library/createPath';
import { ObjectViewSideMenu } from './ObjectViewSideMenu/ObjectViewSideMenu';
import { useObjectViewStyles } from './ObjectView.style';
import Attributes from './attributes/Attributes';
import { ObjectMap } from './ObjectMap';
import Relationships from './Relationships/Relationships';

export interface ObjectViewPropsType {
  prefix: string;
}

const ObjectView: React.FunctionComponent<ObjectViewPropsType> = ({
  prefix,
}) => {
  const {
    object: { state: objectViewState, object },
    objectType: { state: objectTypeState, objectType },
  } = useSelector(objectStateSelector);
  const classes = useObjectViewStyles();
  const [t] = useTranslation('main');
  const dataLoaded =
    objectViewState === AJAX_STATE_VALID &&
    objectTypeState === AJAX_STATE_VALID;

  if (!object || !objectType || !dataLoaded) {
    return <Loader />;
  }

  const createPathObject = createPath(`${prefix}/object/${object.uuid}`);
  const baseRoute = `${prefix}/object/:id`;

  const breadcrumbs: BreadcrumbBarPropsType['breadcrumbs'] = [
    {
      label: t('modules.dashboard'),
      path: '/intern',
    },
    {
      label: object.title,
      path: createPathObject(),
    },
  ];

  return (
    <div className={classes.wrapper}>
      <BreadcrumbBar breadcrumbs={breadcrumbs} />
      <div className={classes.content}>
        <Switch>
          <Route
            exact
            path={baseRoute}
            render={() => (
              <PanelLayout>
                <Panel type="side">
                  <ObjectViewSideMenu
                    createPath={createPathObject}
                    selectedItem="attributes"
                  />
                </Panel>
                <Panel>
                  <Attributes object={object} objectType={objectType} />
                </Panel>
              </PanelLayout>
            )}
          />
          <Route
            exact
            path={`${baseRoute}/timeline`}
            render={() => (
              <PanelLayout>
                <Panel type="side">
                  <ObjectViewSideMenu
                    createPath={createPathObject}
                    selectedItem="timeline"
                  />
                </Panel>
                <Panel />
              </PanelLayout>
            )}
          />
          <Route
            exact
            path={`${baseRoute}/map`}
            render={() => (
              <PanelLayout>
                <Panel type="side">
                  <ObjectViewSideMenu
                    createPath={createPathObject}
                    selectedItem="map"
                  />
                </Panel>
                <Panel>
                  <ObjectMap
                    object={object}
                    objectType={objectType}
                    objectUuid={object.versionIndependentUuid}
                  />
                </Panel>
              </PanelLayout>
            )}
          />
          <Route
            exact
            path={`${baseRoute}/relationships`}
            render={() => (
              <PanelLayout>
                <Panel type="side">
                  <ObjectViewSideMenu
                    createPath={createPathObject}
                    selectedItem="relationships"
                  />
                </Panel>
                <Panel>
                  <Relationships object={object} />
                </Panel>
              </PanelLayout>
            )}
          />
        </Switch>
      </div>
    </div>
  );
};

export default ObjectView;
