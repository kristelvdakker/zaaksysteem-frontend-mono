// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  SideMenu,
  SideMenuItemType,
} from '@mintlab/ui/App/Zaaksysteem/SideMenu';
import { TypePath } from '../../../../library/createPath';
import { useObjectViewSideMenuStyles } from './ObjectViewSideMenu.styles';

export type ObjectViewSideMenuPropsType = {
  selectedItem: 'attributes' | 'timeline' | 'map' | 'relationships';
  createPath: TypePath;
};

export const ObjectViewSideMenu: React.ComponentType<ObjectViewSideMenuPropsType> = ({
  selectedItem,
  createPath,
}) => {
  const [t] = useTranslation('objectView');
  const classes = useObjectViewSideMenuStyles();

  const MenuItemLink = React.forwardRef<any, SideMenuItemType>(
    ({ href, ...restProps }, ref) => <Link to={href || ''} {...restProps} />
  );
  MenuItemLink.displayName = 'MenuItemLink';

  const menuItems: SideMenuItemType[] = [
    {
      id: 'attributes',
      label: t('objectView:sideMenu.attributes'),
      href: createPath(),
      icon: <Icon>{iconNames.list_alt}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'map',
      label: t('objectView:sideMenu.map'),
      href: createPath('/map'),
      icon: <Icon>{iconNames.map}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'relationships',
      label: t('objectView:sideMenu.relationships'),
      href: createPath('/relationships'),
      icon: <Icon>{iconNames.link}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'timeline',
      label: t('objectView:sideMenu.timeline'),
      href: createPath('/timeline'),
      icon: <Icon>{iconNames.schedule}</Icon>,
      component: MenuItemLink,
    },
  ].map<SideMenuItemType>(menuItem => ({
    ...menuItem,
    selected: selectedItem === menuItem.id,
  }));

  return (
    <div className={classes.wrapper}>
      <SideMenu items={menuItems} />
    </div>
  );
};
