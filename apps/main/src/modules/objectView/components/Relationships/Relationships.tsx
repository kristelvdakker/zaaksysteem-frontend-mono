// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { AJAX_STATE_VALID } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { H2 } from '@mintlab/ui/App/Material/Typography';
import {
  fetchCases,
  fetchObjects,
  fetchSubjects,
} from '../../store/relationships/relationships.actions';
import { ObjectViewStateType } from '../../store/objectView.reducer';
import { ObjectType } from '../../types/ObjectView.types';
import { useRelationshipsStyles } from './Relationships.styles';
import RelatedObjects from './components/RelatedObjects';
import RelatedCases from './components/RelatedCases';
import RelatedSubjects from './components/RelatedSubjects';

type StateType = {
  [k: string]: any;
  objectView: ObjectViewStateType;
};

type RelationshipsPropsType = {
  object: ObjectType;
};

const Relationships: React.FunctionComponent<RelationshipsPropsType> = ({
  object: { versionIndependentUuid },
}) => {
  const dispatch = useDispatch();
  const classes = useRelationshipsStyles();
  const { cases, objects, subjects } = useSelector(
    (state: StateType) => state.objectView.relationships
  );
  const [t] = useTranslation('main');

  useEffect(() => {
    dispatch(fetchCases(versionIndependentUuid));
    dispatch(fetchObjects(versionIndependentUuid));
    dispatch(fetchSubjects(versionIndependentUuid));
  }, []);

  if (cases.state != AJAX_STATE_VALID || objects.state != AJAX_STATE_VALID) {
    return <Loader />;
  }

  return (
    <div className={classes.wrapper}>
      <H2
        classes={{
          root: classes.mainTitle,
        }}
      >
        {t('objectView:relationships.title')}
      </H2>

      <RelatedCases cases={cases.cases} />
      <RelatedObjects objects={objects.objects} />
      <RelatedSubjects subjects={subjects.subjects} />
    </div>
  );
};

export default Relationships;
