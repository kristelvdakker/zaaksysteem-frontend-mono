// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { useTranslation } from 'react-i18next';
import { H4 } from '@mintlab/ui/App/Material/Typography';
import { RelatedObjectRowType } from '../../../types/ObjectView.types';
import { getObjectsColumns } from '../Relationships.library';

type RelatedObjectsPropsType = {
  objects: RelatedObjectRowType[];
};

const RelatedObjects: React.FunctionComponent<RelatedObjectsPropsType> = ({
  objects,
}) => {
  const [t] = useTranslation('main');

  return (
    <div>
      <div
        style={{
          minHeight: 200,
        }}
      >
        <H4>{t('objectView:relationships.objects.title')}</H4>
        <SortableTable
          rows={objects}
          columns={getObjectsColumns({ t })}
          noRowsMessage={t('common:general.noResults')}
          loading={false}
          rowHeight={50}
          onRowClick={({ rowData }: { rowData: RelatedObjectRowType }) => {
            // this is using the uuid specific for this version of the object
            window.top.location.href = `/main/object/${rowData.uuid}`;
          }}
        />
      </div>
    </div>
  );
};

export default RelatedObjects;
