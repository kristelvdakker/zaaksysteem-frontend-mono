// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { useTranslation } from 'react-i18next';
import { H4 } from '@mintlab/ui/App/Material/Typography';
import { RelatedSubjectRowType } from '../../../types/ObjectView.types';
import { getSubjectColumns } from '../Relationships.library';

type RelatedSubjectsPropsType = {
  subjects: RelatedSubjectRowType[];
};

const RelatedSubjects: React.FunctionComponent<RelatedSubjectsPropsType> = ({
  subjects,
}) => {
  const [t] = useTranslation('main');

  return subjects ? (
    <div
      style={{
        minHeight: 200,
      }}
    >
      <H4>{t('objectView:relationships.subjects.title')}</H4>
      <SortableTable
        rows={subjects}
        columns={getSubjectColumns({ t })}
        noRowsMessage={t('common:general.noResults')}
        loading={false}
        rowHeight={50}
        onRowClick={({ rowData }: { rowData: RelatedSubjectRowType }) => {
          window.open(`/redirect/contact_page?uuid=${rowData.uuid}`, '_blank');
        }}
      />
    </div>
  ) : null;
};

export default RelatedSubjects;
