// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { H4, Body1 } from '@mintlab/ui/App/Material/Typography';
//@ts-ignore
import { unique } from '@mintlab/kitchen-sink/source';
import { useSubHeaderStyles } from './Subheader.style';

export type SubHeaderPropsType = {
  title: string;
  description: string;
  styles?: ReturnType<typeof useSubHeaderStyles>;
};

export const SubHeader: React.FunctionComponent<SubHeaderPropsType> = ({
  title,
  description,
  styles,
}) => {
  const SubHeaderStyles = useSubHeaderStyles();
  const classes = styles || SubHeaderStyles;
  const descriptionId = unique('table-description');

  return (
    <div className={classes.wrapper}>
      <H4 classes={{ root: classes.title }}>{title}</H4>
      <Body1 classes={{ root: classes.description }} id={descriptionId}>
        {description}
      </Body1>
    </div>
  );
};
