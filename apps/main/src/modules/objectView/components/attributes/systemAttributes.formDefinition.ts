// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';

export const getSystemAttributesFormDefinition: (
  t: i18next.TFunction,
  initialValues: {
    uuid: string;
    status?: string;
    registrationDate?: string;
    lastModified?: string;
  }
) => AnyFormDefinitionField[] = (
  t,
  { uuid, status, registrationDate, lastModified }
) => [
  {
    name: 'name',
    type: fieldTypes.TEXT,
    value: uuid,
    label: t('attributes.systemAttributes.uuid'),
    readOnly: true,
  },
  {
    name: 'status',
    type: fieldTypes.TEXT,
    value: status,
    label: t('attributes.systemAttributes.status'),
    readOnly: true,
  },
  {
    name: 'registrationDate',
    type: fieldTypes.DATEPICKER,
    value: registrationDate,
    label: t('attributes.systemAttributes.registrationDate'),
    readOnly: true,
    dateFormat: t('common:dates.dateFormat'),
  },
  {
    name: 'lastModified',
    type: fieldTypes.DATEPICKER,
    value: lastModified,
    label: t('attributes.systemAttributes.lastModified'),
    readOnly: true,
    dateFormat: t('common:dates.dateFormat'),
  },
];
