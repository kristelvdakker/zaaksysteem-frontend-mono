// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useAttributesStyles = makeStyles(
  ({ mintlab: { greyscale, radius } }: Theme) => ({
    wrapper: {
      width: '100%',
      padding: 12,
    },
    header: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    headerTitle: {
      padding: 20,
    },
    sidePanel: {
      padding: 12,
      minWidth: 360,
    },
    formWrapper: {
      padding: 20,
    },
    buttons: {
      margin: 20,
      display: 'flex',
      justifyContent: 'flex-end',
    },
    button: {
      backgroundColor: greyscale.dark,
      boxShadow: 'none',
      borderRadius: radius.buttonSmall,
      width: 120,
      marginLeft: 10,
    },
  })
);
