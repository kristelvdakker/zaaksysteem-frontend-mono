// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useSubHeaderStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      width: '100%',
      margin: '0px 20px',
    },
    title: {
      padding: '5px 0',
    },
    description: {
      padding: '5px 0',
      color: greyscale.evenDarker,
    },
  })
);
