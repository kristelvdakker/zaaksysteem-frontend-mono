// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    attributes: {
      title: 'Kenmerken',
      actions: {
        cancel: 'Annuleren',
        edit: 'Bewerken',
        save: 'Opslaan',
      },
      customAttributes: {
        title: 'Specifieke kenmerken',
        description: 'De eigen kenmerken van dit object.',
      },
      systemAttributes: {
        title: 'Algemene kenmerken',
        description: 'De systeemkenmerken van dit object.',
        uuid: 'UUID',
        status: 'Status',
        registrationDate: 'Registratiedatum',
        lastModified: 'Gewijzigd op',
      },
    },
    sideMenu: {
      attributes: 'Kenmerken',
      timeline: 'Tijdlijn',
      map: 'Kaart',
      relationships: 'Relaties',
    },
    status: {
      active: 'Actief',
      inactive: 'Inactief',
      draft: 'Concept',
    },
    relationships: {
      title: 'Relaties',
      cases: {
        title: 'Zaken',
        columns: {
          nr: 'Nr.',
          status: 'Status',
          progress: 'Voortgang',
          caseType: 'Zaaktype',
          extra: 'Extra informatie',
          assignee: 'Behandelaar',
          result: 'Resultaat',
        },
      },
      objects: {
        title: 'Objecten',
        columns: {
          name: 'Naam',
          objectType: 'Objecttype',
        },
      },
      subjects: {
        title: 'Betrokkenen',
        columns: {
          name: 'Naam',
          subjectType: 'Type',
          roleType: 'Rol',
          person: 'Burger',
          organization: 'Organisatie',
          employee: 'Medewerker',
        },
      },
    },
  },
};
