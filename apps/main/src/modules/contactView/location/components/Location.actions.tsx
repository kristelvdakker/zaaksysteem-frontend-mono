// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICaseManagement } from '@zaaksysteem/generated';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { fetchAddressChoices } from '@zaaksysteem/common/src/components/form/fields/Address/Address.library';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';

const fetchSubject = async (contactId: string, contactType: string) => {
  const getFilterParameters = () => {
    switch (contactType) {
      case 'employee':
        return {
          employee_uuid: contactId,
        };

      case 'organization':
        return {
          organization_uuid: contactId,
        };

      case 'person':
        return {
          person_uuid: contactId,
        };

      default:
        return {};
    }
  };

  const url = buildUrl<APICaseManagement.GetSubjectRequestParams>(
    '/api/v2/cm/subject/get_subject',
    getFilterParameters()
  );
  const result = await request('GET', url);

  return result.data || [];
};

export const fetchContact = (
  openServerErrorDialog: OpenServerErrorDialogType
) => async (contactId: string, contactType: string) => {
  const subject = await fetchSubject(contactId, contactType).catch(
    openServerErrorDialog
  );

  const location_address =
    subject.attributes.location_address ||
    subject.attributes.residence_address ||
    subject.attributes.correspondence_address;
  const emptyFilter = (part: any) => part;
  const address = location_address
    ? [
        [
          location_address.street,
          location_address.street_number,
          location_address.street_letter,
        ]
          .filter(emptyFilter)
          .join(' '),
        location_address.zipcode,
        location_address.city,
      ].join(', ')
    : '';

  const geo = address
    ? await fetchAddressChoices(openServerErrorDialog)(address, '', false)
    : null;

  const coordinates = geo
    ? geo[0].value.geojson.features[0].geometry.coordinates
    : null;

  return {
    contact: {
      uuid: subject.id,
      address: address,
      location_address: location_address,
      geojson: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {},
            geometry: {
              type: 'Point',
              coordinates,
            },
          },
        ],
      } as const,
    },
  };
};
