// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Location from './components/Location';

export default function LocationModule() {
  return <Location />;
}
