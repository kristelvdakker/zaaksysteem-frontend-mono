// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    sideMenu: {
      attributes: 'Kenmerken',
      data: 'Gegevens',
      overview: 'Overzicht',
      timeline: 'Tijdlijn',
      map: 'Kaart',
      relationships: 'Relaties',
      communication: 'Communicatie',
      cases: 'Zaken',
    },
  },
};
