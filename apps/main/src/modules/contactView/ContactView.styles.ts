// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
  wrapper: {
    width: '100%',
    height: '100%',
  },
  content: {
    height: 'calc(100% - 75px)',
  },
  sideMenuPanel: {
    padding: 12,
  },
}));
