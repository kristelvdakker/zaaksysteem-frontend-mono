// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { RouteComponentProps } from 'react-router';
import { useTranslation } from 'react-i18next';
import { Route, Switch } from 'react-router-dom';
import { PanelLayout } from '../../components/PanelLayout/PanelLayout';
import { Panel } from '../../components/PanelLayout/Panel';
import BreadcrumbBar, {
  BreadcrumbBarPropsType,
} from '../../components/BreadcrumbBar/BreadcrumbBar';
import { createPath } from '../../library/createPath';
import ContactModule from './contact';
import LocationModule from './location';
import { ContactViewSideMenu } from './ContactViewSideMenu';
import { useStyles } from './ContactView.styles';

export interface ContactViewPropsType {
  prefix: string;
  match: RouteComponentProps<{
    type: string;
    uuid: string;
  }>['match'];
}

const ContactView: React.FunctionComponent<ContactViewPropsType> = ({
  prefix,
  match,
}) => {
  const classes = useStyles();
  const [t] = useTranslation('main');
  const type = match.params.type;
  const uuid = match.params.uuid;
  const createPathContact = createPath(
    `${prefix}/contact-view/${type}/${uuid}`
  );
  const baseRoute = `${prefix}/contact-view/:type/:uuid`;

  const breadcrumbs: BreadcrumbBarPropsType['breadcrumbs'] = [
    {
      label: t('modules.dashboard'),
      path: '/intern',
    },
  ];

  return (
    <div className={classes.wrapper}>
      <BreadcrumbBar breadcrumbs={breadcrumbs} />
      <div className={classes.content}>
        <Switch>
          <Route
            exact
            path={baseRoute}
            render={() => (
              <PanelLayout>
                <Panel type="side">
                  <ContactViewSideMenu createPath={createPathContact} />
                </Panel>
              </PanelLayout>
            )}
          />
          <Route
            exact
            path={`${baseRoute}/communication`}
            render={() => (
              <PanelLayout>
                <Panel type="side">
                  <ContactViewSideMenu
                    createPath={createPathContact}
                    selectedItem="communication"
                  />
                </Panel>
                <Panel>
                  <ContactModule />
                </Panel>
              </PanelLayout>
            )}
          />
          <Route
            exact
            path={`${baseRoute}/map`}
            render={() => (
              <PanelLayout>
                <Panel type="side">
                  <ContactViewSideMenu
                    createPath={createPathContact}
                    selectedItem="map"
                  />
                </Panel>
                <Panel>
                  <LocationModule />
                </Panel>
              </PanelLayout>
            )}
          />
        </Switch>
      </div>
    </div>
  );
};

export default ContactView;
