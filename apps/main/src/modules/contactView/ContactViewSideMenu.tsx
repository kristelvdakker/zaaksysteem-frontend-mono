// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  SideMenu,
  SideMenuItemType,
} from '@mintlab/ui/App/Zaaksysteem/SideMenu';
import { TypePath } from '../../library/createPath';

export type ContactViewSideMenuPropsType = {
  selectedItem?:
    | 'overview'
    | 'data'
    | 'communication'
    | 'cases'
    | 'timeline'
    | 'map'
    | 'relationships';
  createPath: TypePath;
};

export const ContactViewSideMenu: React.ComponentType<ContactViewSideMenuPropsType> = ({
  selectedItem,
  createPath,
}) => {
  const [t] = useTranslation('contactView');

  const MenuItemLink = React.forwardRef<any, SideMenuItemType>(
    ({ href, ...restProps }, ref) => <Link to={href || ''} {...restProps} />
  );
  MenuItemLink.displayName = 'MenuItemLink';

  const menuItems: SideMenuItemType[] = [
    {
      id: 'overview',
      label: t('contactView:sideMenu.overview'),
      href: createPath('/overview'),
      icon: <Icon>{iconNames.list_alt}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'data',
      label: t('contactView:sideMenu.data'),
      href: createPath('/data'),
      icon: <Icon>{iconNames.person}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'communication',
      label: t('contactView:sideMenu.communication'),
      href: createPath('/communication'),
      icon: <Icon>{iconNames.alternate_email}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'cases',
      label: t('contactView:sideMenu.cases'),
      href: createPath('/cases'),
      icon: <Icon>{iconNames.list}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'map',
      label: t('contactView:sideMenu.map'),
      href: createPath('/map'),
      icon: <Icon>{iconNames.map}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'relationships',
      label: t('contactView:sideMenu.relationships'),
      href: createPath('/timeline'),
      icon: <Icon>{iconNames.link}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'timeline',
      label: t('contactView:sideMenu.timeline'),
      href: createPath('/timeline'),
      icon: <Icon>{iconNames.schedule}</Icon>,
      component: MenuItemLink,
    },
  ].map<SideMenuItemType>(menuItem => ({
    ...menuItem,
    selected: selectedItem === menuItem.id,
  }));

  return (
    <div>
      <SideMenu items={menuItems} />
    </div>
  );
};
