// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Contact from './components/Contact';

export default function ContactModule() {
  return <Contact />;
}
