// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Route, Switch } from 'react-router-dom';
import CommunicationModule from '@zaaksysteem/communication-module/src';

const Contact = () => {
  return (
    <Switch>
      <Route
        path={`/:prefix/contact-view/:type/:uuid/communication`}
        render={({ match }) => (
          <CommunicationModule
            capabilities={{
              allowSplitScreen: true,
              canAddAttachmentToCase: true,
              canAddSourceFileToCase: true,
              canAddThreadToCase: false,
              canCreateContactMoment: true,
              canCreatePipMessage: false,
              canCreateEmail: false,
              canCreateNote: true,
              canCreateMijnOverheid: false,
              canDeleteMessage: true,
              canImportMessage: false,
              canSelectCase: true,
              canSelectContact: false,
              canFilter: true,
              canOpenPDFPreview: true,
            }}
            context="contact"
            contactUuid={match.params.uuid}
            rootPath={match.url}
          />
        )}
      />
    </Switch>
  );
};

export default Contact;
