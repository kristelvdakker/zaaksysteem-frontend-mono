// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { IModule } from 'redux-dynamic-modules';
import { caseReducer, CaseRootStateType } from './case.reducer';
import { fetchCase } from './case.actions';

export function getCaseModule(
  caseIdFromRoute: string
): IModule<CaseRootStateType> {
  return {
    id: 'case',
    reducerMap: {
      case: caseReducer as any,
    },
    initialActions: [fetchCase(caseIdFromRoute) as any],
  };
}
