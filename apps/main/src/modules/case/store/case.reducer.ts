// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Reducer } from 'redux';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import handleAjaxStateChange from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import { CASE_FETCH } from './case.constants';

export interface CaseStateType {
  id: string | null;
  requestor: {
    dateCreated: string;
    displayName: string;
    subjectType: string;
    reference: string;
  } | null;
  status: string | null;
  instance: any;
  state: AjaxState;
}

export interface CaseRootStateType {
  case: CaseStateType;
}

const initialState: CaseStateType = {
  id: null,
  requestor: null,
  status: null,
  state: AJAX_STATE_INIT,
  instance: null,
};

type CaseFetchResponseBody = {
  result: {
    instance: {
      requestor: {
        reference: string;
        instance: {
          date_created: string;
          display_name: string;
          subject_type: string;
        };
      };
      status: string;
      id: string;
    };
  };
};

const normalizeCaseData = (
  response: CaseFetchResponseBody
): Omit<CaseStateType, 'state'> => {
  const { requestor, id, status } = response.result.instance;
  const {
    reference,
    instance: { date_created, display_name, subject_type },
  } = requestor;

  return {
    id,
    requestor: {
      dateCreated: date_created,
      displayName: display_name,
      subjectType: subject_type,
      reference,
    },
    status,
    instance: response.result.instance,
  };
};

const handleCaseStateChange = handleAjaxStateChange(CASE_FETCH);

export const caseReducer: Reducer<
  CaseStateType,
  AjaxAction<CaseFetchResponseBody>
> = (state = initialState, action): CaseStateType => {
  switch (action.type) {
    case CASE_FETCH.PENDING:
    case CASE_FETCH.ERROR:
      return {
        ...state,
        ...handleCaseStateChange(state, action),
      };

    case CASE_FETCH.SUCCESS:
      return {
        ...handleCaseStateChange(state, action),
        ...normalizeCaseData(action.payload.response),
      };

    default:
      return state;
  }
};
