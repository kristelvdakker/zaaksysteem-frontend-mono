// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    serverErrors: {
      'case_management/custom_object/object_type/not_found':
        'U heeft onvoldoende rechten rechten om objecten van dit type aan te maken, te bewerken of te verwijderen.',
    },
    form: {
      create: 'Aanmaken',
      update: 'Bijwerken',
      delete: 'Verwijderen',
    },
    confirm: {
      delete: {
        title: 'Verwijderen',
        description:
          'Weet u zeker dat u dit object uit de zaak wilt verwijderen?',
      },
    },
  },
};
