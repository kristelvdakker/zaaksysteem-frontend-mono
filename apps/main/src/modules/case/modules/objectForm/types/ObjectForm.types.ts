// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated';

export type CustomFieldType = { type: string; value: any };

export type ObjectType = {
  uuid: string;
  customFieldsValues: {
    [k: string]: CustomFieldType;
  };
  relatedCasesUuids?: string[];
  [key: string]: any;
};

export type ObjectTypeType = {
  versionUuid: string;
  customFieldsDefinition: APICaseManagement.CustomObjectTypeCustomFieldDefinition[];
  catalog_folder_id?: number;
  date_created?: string;
  date_deleted?: string;
  external_reference?: string;
  is_active_version?: boolean;
  last_modified?: string;
  name?: string;
  status?: string;
  title?: string;
  version?: number;
  version_independent_uuid?: string;
};

export type ObjectTypeFormShapeType = {
  [key: string]: any;
};

export type CaseType = {
  customFields: CustomFieldType;
};
