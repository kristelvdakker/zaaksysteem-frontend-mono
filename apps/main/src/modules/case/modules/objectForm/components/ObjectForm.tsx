// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/* eslint react/no-danger: 0 */
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import ConfirmDialog from '@zaaksysteem/common/src/components/ConfirmDialog/ConfirmDialog';
import { AJAX_STATE_PENDING } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import generateCustomFieldFormDefinition from '@zaaksysteem/common/src/components/form/library/generateCustomFieldFormDefinition';
import generateCustomFieldValues from '@zaaksysteem/common/src/components/form/library/generateCustomFieldValues';
import formatGetCaseCustomFields from '@zaaksysteem/common/src/components/form/library/formatGetCaseCustomFields';
import { generateValidationMap } from '@zaaksysteem/common/src/components/form/validation/library/generateValidationMap';
import { Button } from '@mintlab/ui/App/Material/Button';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import {
  createObjectAction,
  updateObjectAction,
  deleteObjectAction,
} from '../store/object/object.actions';
import {
  ObjectTypeType,
  ObjectType,
  CaseType,
} from '../types/ObjectForm.types';
import { objectSelector } from './../store/selectors/objectSelector';
import { objectTypeSelector } from './../store/selectors/objectTypeSelector';
import { caseSelector } from './../store/selectors/caseSelector';
import { useObjectFormStyles } from './ObjectForm.style';

export interface ObjectFormPropsType {
  caseUuid: string;
  type: 'create' | 'update';
}

interface ObjectFormInnerProps {
  objectType: ObjectTypeType;
  object?: ObjectType;
  caseObj: CaseType;
  type: 'create' | 'update';
  caseUuid: string;
}

/* eslint complexity: [2, 12] */
const InnerObjectForm: React.FunctionComponent<ObjectFormInnerProps> = ({
  objectType,
  object,
  caseObj,
  type,
  caseUuid,
}) => {
  const caseInstance = useSelector((store: any) => store.case.instance);
  const updateMode = type === 'update';
  const customFieldsValues =
    object?.customFieldsValues ||
    (!updateMode && caseObj
      ? formatGetCaseCustomFields(caseObj.customFields)
      : {});

  const formDefinition = generateCustomFieldFormDefinition({
    customFieldsDefinition: objectType.customFieldsDefinition,
    customFieldsValues,
    config: {
      context: {
        type: 'CaseObjectForm' as const,
        data: {
          case: caseInstance,
          caseType: caseInstance.casetype,
          objectType: objectType || null,
          object: object || null,
        },
      },
    },
  });
  const validationMap = generateValidationMap(formDefinition);

  const {
    fields,
    formik: { isValid, values },
  } = useForm({
    isInitialValid: true,
    enableReinitialize: true,
    formDefinition,
    validationMap,
  });

  const classes = useObjectFormStyles();
  const [t] = useTranslation('objectForm');
  const dispatch = useDispatch();
  const [showConfirm, setShowConfirm] = useState(false);

  if (updateMode && !object) {
    throw new Error('Cannot enter update mode without object instance');
  } else {
    return (
      <div className={classes.wrapper}>
        <div className={classes.scrollWrapper}>
          {fields.map(
            ({ FieldComponent, name, error, touched, value, ...rest }) => {
              const restValues = {
                ...cloneWithout(rest, 'type', 'classes'),
                disabled: values.completed,
              };

              return (
                <FormControlWrapper
                  {...restValues}
                  compact={true}
                  error={error}
                  touched={touched}
                  key={name}
                >
                  <FieldComponent
                    name={name}
                    value={value}
                    key={name}
                    {...restValues}
                  />
                </FormControlWrapper>
              );
            }
          )}

          <div className={classes.actionWrapper}>
            <Button
              presets={['contained', 'medium', 'primary']}
              className={classes.button}
              action={() =>
                dispatch(
                  updateMode && object
                    ? updateObjectAction(
                        object.relatedCasesUuids || [],
                        object.uuid,
                        generateCustomFieldValues(values, fields)
                      )
                    : createObjectAction(
                        caseUuid,
                        objectType.versionUuid,
                        generateCustomFieldValues(values, fields)
                      )
                )
              }
              disabled={!isValid}
            >
              {t(`form.${type}`)}
            </Button>
            {updateMode && object && (
              <Button
                presets={['contained', 'medium']}
                className={classes.button}
                color="danger"
                action={() => setShowConfirm(true)}
              >
                {t('form.delete')}
              </Button>
            )}
            {updateMode && object && (
              <ConfirmDialog
                open={showConfirm}
                onConfirm={() => {
                  dispatch(deleteObjectAction(object.uuid));
                  setShowConfirm(false);
                }}
                onClose={() => setShowConfirm(false)}
                title={t('confirm.delete.title')}
                body={<div>{t('confirm.delete.description')}</div>}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
};

export const ObjectForm: React.ComponentType<ObjectFormPropsType> = ({
  caseUuid,
  type,
}) => {
  const { state: objectState, object } = useSelector(objectSelector);
  const { state: objectTypeState, objectType } = useSelector(
    objectTypeSelector
  );
  const { state: caseState, caseObj } = useSelector(caseSelector);
  const isUpdateMode = type === 'update';
  const loading =
    objectTypeState === AJAX_STATE_PENDING ||
    objectState === AJAX_STATE_PENDING ||
    caseState === AJAX_STATE_PENDING;

  return loading || !objectType || !caseObj || (isUpdateMode && !object) ? (
    <Loader />
  ) : (
    <InnerObjectForm
      caseUuid={caseUuid}
      caseObj={caseObj}
      objectType={objectType}
      object={object}
      type={type}
    />
  );
};

export default ObjectForm;
