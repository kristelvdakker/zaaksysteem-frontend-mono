// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ObjectFormRootStateType } from '../objectForm.reducer';
import { ObjectStateType } from '../object/object.reducer';

export const objectSelector = (
  state: ObjectFormRootStateType
): ObjectStateType => state.object;
