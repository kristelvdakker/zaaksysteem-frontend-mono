// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Reducer } from 'redux';
import { APICaseManagement } from '@zaaksysteem/generated';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { handleAjaxStateChange } from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { ObjectType } from '../../types/ObjectForm.types';
import { OBJECT_FETCH } from './object.constants';

export interface ObjectStateType {
  object?: ObjectType;
  state: AjaxState;
}

const initialState: ObjectStateType = {
  state: AJAX_STATE_INIT,
};

const handleObjectFetchSuccess = (
  state: ObjectStateType,
  action: AjaxAction<APICaseManagement.GetCustomObjectResponseBody>
): ObjectStateType => {
  if (!action.payload.response || !action.payload.response.data) {
    return state;
  }

  const {
    id,
    attributes: { custom_fields },
    relationships,
  } = action.payload.response.data;

  const relatedCasesUuids = relationships?.cases
    ?.map(caseRelation => caseRelation.data.id || '')
    .filter(Boolean);

  // none of the attributes are marked as required, even though they are
  if (!custom_fields) {
    return state;
  }

  return {
    ...state,
    object: {
      // from the case_type call we receive the version_independent_id
      // but for object_create we need the version_id
      uuid: id,
      customFieldsValues: custom_fields,
      relatedCasesUuids,
    },
  };
};

const handleObjectTypeStateChange = handleAjaxStateChange(OBJECT_FETCH);

/* eslint complexity: [2, 16] */
export const objectReducer: Reducer<ObjectStateType, AjaxAction<unknown>> = (
  state = initialState,
  action
) => {
  const { type } = action;
  state;
  switch (type) {
    case OBJECT_FETCH.PENDING:
    case OBJECT_FETCH.ERROR:
      return handleObjectTypeStateChange(state, action as AjaxAction<{}>);
    case OBJECT_FETCH.SUCCESS:
      return handleObjectTypeStateChange(
        handleObjectFetchSuccess(
          state,
          action as AjaxAction<APICaseManagement.GetCustomObjectResponseBody>
        ),
        action
      );

    default:
      return state;
  }
};

export default objectReducer;
