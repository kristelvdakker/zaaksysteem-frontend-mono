// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

//@ts-ignore
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { APICaseManagement } from '@zaaksysteem/generated';
import { OBJECT_TYPE_FETCH } from './objectType.constants';

export const fetchObjectTypeAction = (objectTypeUuid: string) => {
  const url = buildUrl<APICaseManagement.GetCustomObjectTypeRequestParams>(
    '/api/v2/cm/custom_object_type/get_persistent_custom_object_type',
    {
      uuid: objectTypeUuid,
      use: 'write',
    }
  );

  return createAjaxAction(
    OBJECT_TYPE_FETCH
  )<APICaseManagement.GetCustomObjectTypeResponseBody>({
    url,
    method: 'GET',
  });
};
