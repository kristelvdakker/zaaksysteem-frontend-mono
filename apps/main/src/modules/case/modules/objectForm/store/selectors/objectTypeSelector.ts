// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ObjectFormRootStateType } from '../objectForm.reducer';
import { ObjectTypeStateType } from '../objectType/objectType.reducer';

export const objectTypeSelector = (
  state: ObjectFormRootStateType
): ObjectTypeStateType => state.objectType;
