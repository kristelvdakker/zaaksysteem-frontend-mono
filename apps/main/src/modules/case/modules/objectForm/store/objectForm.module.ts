// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { IModule } from 'redux-dynamic-modules';
import { objectReducer } from './object/object.reducer';
import { objectTypeReducer } from './objectType/objectType.reducer';
import { caseReducer } from './case/case.reducer';
import { objectTypeMiddleware } from './objectForm.middleware';
import { fetchObjectAction } from './object/object.actions';
import { fetchObjectTypeAction } from './objectType/objectType.actions';
import { ObjectFormRootStateType } from './objectForm.reducer';
import { fetchCaseAction } from './case/case.actions';

export function getObjectFormModule(
  objectUuid: string,
  objectTypeUuid: string,
  caseUuid: string
): IModule<ObjectFormRootStateType> {
  return {
    id: 'objectType',
    reducerMap: {
      caseObj: caseReducer as any,
      object: objectReducer as any,
      objectType: objectTypeReducer as any,
    },
    middlewares: [objectTypeMiddleware],
    initialActions: [
      ...(objectUuid ? [fetchObjectAction(objectUuid) as any] : []),
      fetchObjectTypeAction(objectTypeUuid) as any,
      fetchCaseAction(caseUuid) as any,
    ],
  };
}
