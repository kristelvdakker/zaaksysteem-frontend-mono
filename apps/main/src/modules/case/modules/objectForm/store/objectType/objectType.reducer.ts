// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Reducer } from 'redux';
import { APICaseManagement } from '@zaaksysteem/generated';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { handleAjaxStateChange } from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { OBJECT_TYPE_FETCH } from './objectType.constants';
import { ObjectTypeType } from './../../types/ObjectForm.types';

export interface ObjectTypeStateType {
  objectType?: ObjectTypeType;
  state: AjaxState;
}

const initialState: ObjectTypeStateType = {
  state: AJAX_STATE_INIT,
};

const handleObjectTypeFetchSuccess = (
  state: ObjectTypeStateType,
  action: AjaxAction<
    APICaseManagement.GetCustomObjectResponseBody,
    APICaseManagement.GetCustomObjectTypeRequestParams
  >
): ObjectTypeStateType => {
  if (!action.payload.response || !action.payload.response.data) {
    return state;
  }

  const {
    id,
    attributes: {
      custom_field_definition: { custom_fields },
    },
  } = action.payload.response.data;

  // none of the attributes are marked as required, even though they are
  if (!custom_fields) {
    return state;
  }

  return {
    ...state,
    objectType: {
      // from the case_type call we receive the version_independent_id
      // but for object_create we need the version_id
      versionUuid: id,
      customFieldsDefinition: custom_fields,
      ...action.payload.response.data.attributes,
    },
  };
};

const handleObjectTypeStateChange = handleAjaxStateChange(OBJECT_TYPE_FETCH);

/* eslint complexity: [2, 16] */
export const objectTypeReducer: Reducer<
  ObjectTypeStateType,
  AjaxAction<unknown>
> = (state = initialState, action) => {
  const { type } = action;
  state;
  switch (type) {
    case OBJECT_TYPE_FETCH.PENDING:
    case OBJECT_TYPE_FETCH.ERROR:
      return handleObjectTypeStateChange(state, action as AjaxAction<{}>);
    case OBJECT_TYPE_FETCH.SUCCESS:
      return handleObjectTypeStateChange(
        handleObjectTypeFetchSuccess(
          state,
          action as AjaxAction<
            APICaseManagement.GetCustomObjectResponseBody,
            APICaseManagement.GetCustomObjectTypeRequestParams
          >
        ),
        action
      );

    default:
      return state;
  }
};

export default objectTypeReducer;
