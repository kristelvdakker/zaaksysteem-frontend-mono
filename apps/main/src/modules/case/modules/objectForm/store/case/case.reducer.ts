// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Reducer } from 'redux';
import { APICaseManagement } from '@zaaksysteem/generated';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { handleAjaxStateChange } from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { CaseType } from '../../types/ObjectForm.types';
import { CASE_V2_FETCH } from './case.constants';

export interface CaseStateType {
  caseObj?: CaseType;
  state: AjaxState;
}

const initialState: CaseStateType = {
  state: AJAX_STATE_INIT,
};

const handleCaseFetchSuccess = (
  state: CaseStateType,
  action: AjaxAction<
    APICaseManagement.GetCaseResponseBody,
    APICaseManagement.GetCaseRequestParams
  >
): CaseStateType => {
  if (!action.payload.response || !action.payload.response.data) {
    return state;
  }

  const {
    attributes: { custom_fields },
  } = action.payload.response.data;

  // none of the attributes are marked as required, even though they are
  if (!custom_fields) {
    return state;
  }

  return {
    ...state,
    caseObj: {
      customFields: custom_fields,
    },
  };
};

const handleCaseStateChange = handleAjaxStateChange(CASE_V2_FETCH);

/* eslint complexity: [2, 16] */
export const caseReducer: Reducer<CaseStateType, AjaxAction<unknown>> = (
  state = initialState,
  action
) => {
  const { type } = action;
  state;
  switch (type) {
    case CASE_V2_FETCH.PENDING:
    case CASE_V2_FETCH.ERROR:
      return handleCaseStateChange(state, action as AjaxAction<{}>);
    case CASE_V2_FETCH.SUCCESS:
      return handleCaseStateChange(
        handleCaseFetchSuccess(
          state,
          action as AjaxAction<
            APICaseManagement.GetCaseResponseBody,
            APICaseManagement.GetCaseRequestParams
          >
        ),
        action
      );

    default:
      return state;
  }
};

export default caseReducer;
