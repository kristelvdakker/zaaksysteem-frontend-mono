// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxConstants } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';

export const OBJECT_FETCH = createAjaxConstants('OBJECT_FETCH');
export const OBJECT_CREATE = createAjaxConstants('OBJECT_CREATE');
export const OBJECT_UPDATE = createAjaxConstants('OBJECT_UPDATE');
export const OBJECT_DELETE = createAjaxConstants('OBJECT_DELETE');
