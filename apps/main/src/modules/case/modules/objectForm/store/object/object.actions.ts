// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { APICaseManagement } from '@zaaksysteem/generated';
import {
  OBJECT_FETCH,
  OBJECT_CREATE,
  OBJECT_UPDATE,
  OBJECT_DELETE,
} from './object.constants';

export const fetchObjectAction = (objectUuid: string) => {
  const url = buildUrl<APICaseManagement.GetCustomObjectTypeRequestParams>(
    '/api/v2/cm/custom_object/get_custom_object',
    {
      uuid: objectUuid,
    }
  );

  return createAjaxAction(
    OBJECT_FETCH
  )<APICaseManagement.GetCustomObjectResponseBody>({
    url,
    method: 'GET',
  });
};

export const createObjectAction = (
  caseUuid: string,
  objectTypeUuid: string,
  values: { [key: string]: any }
) => {
  const data: APICaseManagement.CreateCustomObjectRequestBody = {
    uuid: v4(),
    custom_object_type_uuid: objectTypeUuid,
    custom_fields: values,
    // this is mock data, but it's required by the API
    archive_metadata: {
      status: 'to preserve',
      ground: '',
      retention: 0,
    },
    related_to: {
      cases: [caseUuid],
    },
  };

  return createAjaxAction(
    OBJECT_CREATE
  )<APICaseManagement.CreateCustomObjectResponseBody>({
    url: '/api/v2/cm/custom_object/create_custom_object',
    method: 'POST',
    data,
    payload: data,
  });
};

export const updateObjectAction = (
  relatedCasesUuids: string[],
  objectUuid: string,
  values: { [key: string]: any }
) => {
  const data: any /* APICaseManagement.UpdateCustomObjectRequestBody */ = {
    uuid: v4(),
    existing_uuid: objectUuid,
    custom_fields: values,
    // this is mock data, but it's required by the API
    archive_metadata: {
      status: 'to preserve',
      ground: '',
      retention: 0,
    },
    related_to: {
      cases: relatedCasesUuids,
    },
  };

  return createAjaxAction(
    OBJECT_UPDATE
  )<APICaseManagement.UpdateCustomObjectResponseBody>({
    url: '/api/v2/cm/custom_object/update_custom_object',
    method: 'POST',
    data,
    payload: data,
  });
};

export const deleteObjectAction = (uuid: string) => {
  return createAjaxAction(
    OBJECT_DELETE
  )<APICaseManagement.DeleteCustomObjectResponseBody>({
    url: buildUrl('/api/v2/cm/custom_object/delete_custom_object', { uuid }),
    method: 'POST',
  });
};
