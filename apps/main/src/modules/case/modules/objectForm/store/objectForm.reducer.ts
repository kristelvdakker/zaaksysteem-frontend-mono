// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { combineReducers } from 'redux';
import objectTypeReducer, {
  ObjectTypeStateType,
} from './objectType/objectType.reducer';
import objectReducer, { ObjectStateType } from './object/object.reducer';
import { caseReducer, CaseStateType } from './case/case.reducer';

export interface ObjectFormRootStateType {
  object: ObjectStateType;
  objectType: ObjectTypeStateType;
  caseObj: CaseStateType;
}

export const objectFormReducer = combineReducers({
  object: objectReducer,
  objectType: objectTypeReducer,
  caseObj: caseReducer,
});

export default objectFormReducer;
