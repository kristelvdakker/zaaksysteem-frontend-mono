// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ObjectFormRootStateType } from '../objectForm.reducer';
import { CaseStateType } from '../case/case.reducer';

export const caseSelector = (state: ObjectFormRootStateType): CaseStateType =>
  state.caseObj;
