// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { RouterRootState } from 'connected-react-router';
import { Middleware } from 'redux';
import { MiddlewareHelper } from '@zaaksysteem/common/src/types/MiddlewareHelper';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { APICaseManagement } from '@zaaksysteem/generated';
import { ObjectFormRootStateType } from './objectForm.reducer';
import {
  OBJECT_CREATE,
  OBJECT_UPDATE,
  OBJECT_DELETE,
} from './object/object.constants';

const results: any = {
  'OBJECT_CREATE:SUCCESS': 'created',
  'OBJECT_UPDATE:SUCCESS': 'updated',
  'OBJECT_DELETE:SUCCESS': 'unrelated',
};

const handleObjectSuccess: MiddlewareHelper<
  ObjectFormRootStateType & RouterRootState,
  AjaxAction<{}, APICaseManagement.CreateCustomObjectTypeResponseBody>
> = (store, next, action) => {
  next(action);

  const uuid = action.payload.uuid || action.payload.custom_object_uuid;
  const result = results[action.type];

  window.top.postMessage(
    { type: 'handleObjectSuccess', data: { uuid, result } },
    '*'
  );
};

export const objectTypeMiddleware: Middleware<
  {},
  ObjectFormRootStateType & RouterRootState
> = store => next => action => {
  switch (action.type) {
    case OBJECT_CREATE.SUCCESS:
    case OBJECT_UPDATE.SUCCESS:
    case OBJECT_DELETE.SUCCESS:
      return handleObjectSuccess(store, next, action);
    default:
      return next(action);
  }
};
