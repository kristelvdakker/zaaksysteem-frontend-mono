// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

//@ts-ignore
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { APICaseManagement } from '@zaaksysteem/generated';
import { CASE_V2_FETCH } from './case.constants';

export const fetchCaseAction = (caseUuid: string) => {
  const url = buildUrl<APICaseManagement.GetCaseRequestParams>(
    '/api/v2/cm/case/get_case',
    {
      case_uuid: caseUuid,
    }
  );

  return createAjaxAction(CASE_V2_FETCH)<APICaseManagement.GetCaseResponseBody>(
    {
      url,
      method: 'GET',
    }
  );
};
