// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect } from 'react';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import useMessages from '@zaaksysteem/common/src/library/useMessages';
import { getObjectFormModule } from './store/objectForm.module';
import ObjectForm, { ObjectFormPropsType } from './components/ObjectForm';
import locale from './locale/objectForm.locale';

type ObjectFormModulePropsType = {
  rootPath: string;
  objectTypeUuid: string;
  objectUuid: string;
} & ObjectFormPropsType;

const ObjectFormModule: React.ComponentType<ObjectFormModulePropsType> = ({
  objectTypeUuid,
  objectUuid,
  caseUuid,
  type,
}) => {
  const [t] = useTranslation('objectForm');
  const [, addMessages, removeMessages] = useMessages();

  useEffect(() => {
    const messages: { [key: string]: any } = t('serverErrors', {
      returnObjects: true,
    });

    addMessages(messages);

    return () => removeMessages(messages);
  }, []);

  return (
    <DynamicModuleLoader
      modules={[getObjectFormModule(objectUuid, objectTypeUuid, caseUuid)]}
    >
      <I18nResourceBundle resource={locale} namespace="objectForm">
        <ObjectForm caseUuid={caseUuid} type={type} />
      </I18nResourceBundle>
    </DynamicModuleLoader>
  );
};

export default ObjectFormModule;
