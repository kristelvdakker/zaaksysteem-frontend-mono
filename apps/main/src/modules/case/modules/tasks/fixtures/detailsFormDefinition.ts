// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  FormDefinition,
  FormDefinitionField,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { ContactFinderConfigType } from '@zaaksysteem/common/src/components/form/fields/ContactFinder/ContactFinder.types';
import { CaseTask } from '../types/List.types';

const formDefinition: FormDefinition<CaseTask> = [
  {
    name: 'title',
    type: fieldTypes.TEXT,
    required: true,
    placeholder: 'tasks:fields.title.placeholder',
    value: '',
  },
  {
    name: 'description',
    type: fieldTypes.TEXTAREA,
    isMultiline: true,
    required: false,
    placeholder: 'tasks:fields.description.placeholder',
    rows: 5,
    value: '',
  },
  <FormDefinitionField<CaseTask, ContactFinderConfigType>>{
    name: 'assignee',
    type: fieldTypes.CONTACT_FINDER,
    required: false,
    label: 'tasks:fields.assignee.label',
    translations: {
      'form:choose': 'tasks:fields.assignee.placeholder',
    },
    value: null,
    config: {
      filterTypes: ['employee'],
    },
  },
  {
    name: 'due_date',
    type: fieldTypes.DATEPICKER,
    variant: 'inline',
    required: false,
    label: 'tasks:fields.due_date.label',
    fullWidth: true,
    placeholder: 'tasks:fields.due_date.placeholder',
    value: null,
  },
  {
    name: 'completed',
    type: 'hidden',
    value: false,
  },
];

export default formDefinition;
