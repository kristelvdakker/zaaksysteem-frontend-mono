// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import { AutoSizer, List } from 'react-virtualized';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { TaskItem } from '../TaskItem/TaskItem';
import { CaseTask } from '../../../types/List.types';
import { TaskCreatorContainer } from '../TaskCreator/TaskCreatorContainer';
import { TasksContextType } from '../../../types/Context.types';
import { TasksPlaceholder } from '../TasksPlaceholder/TasksPlaceholder';
import { useTasksStyles } from '../../Tasks.style';

interface RenderListType {
  width: number;
  height: number;
}

export type TaskListPropsType = {
  initialBootOrPending: boolean;
  tasks: CaseTask[];
  tasksContext: TasksContextType;
  fetchTasks: (tasksContext: TasksContextType) => void;
  completeTask: (task: CaseTask) => void;
};

export const TaskList: React.ComponentType<TaskListPropsType> = ({
  tasks,
  fetchTasks,
  tasksContext,
  completeTask,
  initialBootOrPending,
}) => {
  const classes = useTasksStyles();

  React.useEffect(() => {
    if (initialBootOrPending) fetchTasks(tasksContext);
  }, []);

  const rowRenderer = ({ index, style }: any) => {
    const task = tasks[index];
    return (
      <TaskItem
        {...task}
        style={style}
        rootPath={tasksContext.rootPath}
        onToggle={() => completeTask(task)}
        key={task.task_uuid}
      />
    );
  };

  return (
    <div className={classes.wrapper}>
      {initialBootOrPending ? (
        <Loader />
      ) : (
        <React.Fragment>
          <TaskCreatorContainer />
          {tasks.length ? (
            <AutoSizer>
              {({ width, height }: RenderListType) => (
                <List
                  height={height - 120}
                  rowCount={tasks.length}
                  rowHeight={85}
                  rowRenderer={rowRenderer}
                  width={width}
                  style={{ outline: 'none' }}
                />
              )}
            </AutoSizer>
          ) : (
            <TasksPlaceholder />
          )}
        </React.Fragment>
      )}
    </div>
  );
};
