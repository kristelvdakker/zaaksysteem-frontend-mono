// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import translateFormDefinition from '@zaaksysteem/common/src/components/form/library/translateFormDefinition';
import { Button } from '@mintlab/ui/App/Material/Button';
import PlusButtonSpaceWrapper from '@zaaksysteem/common/src/components/PlusButtonSpaceWrapper/PlusButtonSpaceWrapper';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types';
import { CaseTask } from '../../types/List.types';
import { useTasksStyles } from '../Tasks.style';
import { useDetailStyles } from './Details.style';
import Toolbar from './Toolbar/Toolbar';

export type DetailsPropsType = {
  formDefinition: FormDefinition;
  editAction: (values: Partial<CaseTask>) => void;
  setCompletionAction: (completed: boolean) => void;
  deleteAction: () => void;
  rootPath: string;
} & Pick<CaseTask, 'is_editable' | 'can_set_completion'>;

export const Details: React.ComponentType<DetailsPropsType> = ({
  formDefinition,
  editAction,
  setCompletionAction,
  deleteAction,
  rootPath,
  is_editable,
  can_set_completion,
}) => {
  const classes = useDetailStyles();
  const tasksClasses = useTasksStyles();
  const [t] = useTranslation();
  const { fields, formik } = useForm({
    formDefinition: translateFormDefinition(formDefinition, t),
    enableReinitialize: true,
    isInitialValid: true,
  });

  return (
    <div className={tasksClasses.wrapperDetails}>
      <Toolbar
        deleteAction={deleteAction}
        setCompletionAction={setCompletionAction}
        rootPath={rootPath}
        isValid={formik.isValid}
        is_editable={is_editable}
        can_set_completion={can_set_completion}
        completed={formik.values.completed as any}
        t={t}
      />

      <div className={classes.scrollWrapper}>
        <PlusButtonSpaceWrapper>
          {fields.map(
            ({ FieldComponent, name, error, touched, value, ...rest }) => {
              const restValues = {
                ...cloneWithout(
                  rest,
                  'setFieldValue',
                  'setFieldTouched',
                  'type',
                  'classes'
                ),
                disabled: !is_editable || formik.values.completed,
                ...(name === 'due_date' && {
                  onClose: () => formik.setFieldValue(name, null),
                }),
              };

              return (
                <FormControlWrapper
                  {...restValues}
                  compact={true}
                  classes={{
                    wrapper: classes.formControlWrapper,
                  }}
                  error={error}
                  touched={touched}
                  key={name}
                >
                  <FieldComponent
                    name={name}
                    value={value}
                    key={name}
                    {...restValues}
                  />
                </FormControlWrapper>
              );
            }
          )}

          {formik.values.completed && can_set_completion && (
            <Button
              presets={['contained', 'medium', 'primary']}
              action={() => setCompletionAction(false)}
              disabled={!formik.isValid}
              fullWidth
            >
              {t('tasks:form.reOpen')}
            </Button>
          )}

          {!formik.values.completed && is_editable && (
            <Button
              presets={['contained', 'medium', 'primary']}
              action={() => editAction(formik.values)}
              disabled={!formik.isValid}
              fullWidth
            >
              {t('common:dialog.save')}
            </Button>
          )}
        </PlusButtonSpaceWrapper>
      </div>
    </div>
  );
};
