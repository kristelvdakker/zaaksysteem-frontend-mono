// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import fecha from 'fecha';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { CaseTask } from '../../../../types/List.types';
import { useTaskItemAddonsStyles } from './TaskItemAddons.style';

export type TaskItemAddonsPropsType = Pick<
  CaseTask,
  'assignee' | 'due_date' | 'description'
>;

/*eslint complexity: ["error", 13]*/
const TaskItemAddons: React.ComponentType<TaskItemAddonsPropsType> = ({
  assignee,
  description,
  due_date,
}) => {
  const classes = useTaskItemAddonsStyles();
  const [t] = useTranslation();

  const addons = [
    due_date && (
      <span key="due_date" className={classes.addon}>
        <Icon
          classes={{
            root: classes.addonIcon,
          }}
          size={10}
        >
          {iconNames.event_available}
        </Icon>
        {fecha.format(
          new Date(due_date as string),
          t('common:dates.dateFormatTextShort')
        )}
      </span>
    ),

    description && (
      <span key="note" className={classes.addon}>
        <Icon classes={{ root: classes.addonIcon }} size={10}>
          {iconNames.note}
        </Icon>
        {t('tasks:note')}
      </span>
    ),

    assignee && (
      <div key="assignee" className={classes.addon}>
        <Icon
          classes={{
            root: classes.addonIcon,
          }}
          size={10}
        >
          {iconNames.person}
        </Icon>
        {assignee ? assignee.label : ''}
      </div>
    ),
  ];
  const activeAddons = addons.filter(Boolean);

  return (
    <React.Fragment>
      {activeAddons.map((element, index) => (
        <React.Fragment key={index}>
          {element}
          {index !== activeAddons.length - 1 && (
            <span className={classes.bull}>&bull;</span>
          )}
        </React.Fragment>
      ))}
    </React.Fragment>
  );
};

export default TaskItemAddons;
