// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { TasksRootStateType } from '../../../store/tasks.reducer';
import { createTaskAction } from '../../../store/tasks/tasks.actions';
import { TaskCreatorPropsType, TaskCreator } from './TaskCreator';

type PropsFromStateType = Pick<TaskCreatorPropsType, 'tasksContext'>;

const mapStateToProps = ({
  tasks: { context },
}: TasksRootStateType): PropsFromStateType => {
  return { tasksContext: context };
};

type PropsFromDispatchType = Pick<TaskCreatorPropsType, 'onCreateTask'>;

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatchType => ({
  onCreateTask(title, caseContext) {
    const action = createTaskAction(title, caseContext);
    if (action !== null) {
      dispatch(action as any);
    }
  },
});

export const TaskCreatorContainer = connect<
  PropsFromStateType,
  PropsFromDispatchType,
  {},
  TasksRootStateType
>(
  mapStateToProps,
  mapDispatchToProps
)(TaskCreator);
