// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useTasksPlaceholderStyle = makeStyles(() => ({
  container: {
    paddingTop: 20,
  },
}));
