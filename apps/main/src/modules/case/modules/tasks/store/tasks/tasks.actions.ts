// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { APICaseManagement } from '@zaaksysteem/generated';
import { TasksContextType } from '../../types/Context.types';
import { CaseTask } from '../../types/List.types';
import {
  TASKS_FETCH,
  TASK_CREATE,
  TASK_EDIT,
  TASK_DELETE,
  TASK_SET_COMPLETE_STATUS,
} from './tasks.constants';

export const fetchTasksAction = ({ caseUuid, phase }: TasksContextType) => {
  const url = buildUrl<APICaseManagement.GetTaskListRequestParams>(
    '/api/v2/cm/task/get_task_list',
    {
      filter: {
        'attributes.phase': phase,
        'relationships.case.id': caseUuid,
      },
    }
  );

  return createAjaxAction(
    TASKS_FETCH
  )<APICaseManagement.GetTaskListResponseBody>({
    url,
    method: 'GET',
  });
};

export const createTaskAction = (
  title: string,
  { caseUuid, phase }: TasksContextType
) => {
  const url = buildUrl<any>('/api/v2/cm/task/create', {
    case_uuid: caseUuid,
    phase,
  });

  const data = {
    title,
    case_uuid: caseUuid,
    phase,
    task_uuid: v4(),
  };

  return createAjaxAction(TASK_CREATE)<
    APICaseManagement.CreateTaskResponseBody,
    APICaseManagement.CreateTaskRequestBody
  >({
    url,
    method: 'POST',
    payload: data,
    data,
  });
};

export const editTask = (values: Partial<CaseTask>) => {
  const normalizedAPIValues = {
    ...values,
    assignee: values.assignee ? values.assignee.value : null,
  };

  return createAjaxAction(TASK_EDIT)<APICaseManagement.UpdateTaskResponseBody>({
    url: '/api/v2/cm/task/update',
    method: 'POST',
    data: normalizedAPIValues,
    payload: values,
  });
};

export const deleteTask = (task_uuid: CaseTask['task_uuid']) =>
  createAjaxAction(TASK_DELETE)<
    APICaseManagement.DeleteTaskResponseBody,
    APICaseManagement.DeleteTaskRequestBody
  >({
    url: '/api/v2/cm/task/delete',
    method: 'POST',
    payload: { task_uuid },
    data: {
      task_uuid,
    },
  });

export const setCompletion = (
  task_uuid: CaseTask['task_uuid'],
  completed: CaseTask['completed']
) => {
  const data = {
    task_uuid,
    completed,
  };

  return createAjaxAction(TASK_SET_COMPLETE_STATUS)<
    APICaseManagement.SetTaskCompletionResponseBody,
    APICaseManagement.SetTaskCompletionRequestBody
  >({
    url: '/api/v2/cm/task/set_completion',
    method: 'POST',
    data,
    payload: data,
  });
};
