// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const TASKS_SET_CONTEXT = 'TASKS_SET_CONTEXT';
