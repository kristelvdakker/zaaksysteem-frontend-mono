// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ActionCreator } from 'redux';
import { ActionWithPayload } from '@zaaksysteem/common/src/types/ActionWithPayload';
import { TasksContextType } from '../../types/Context.types';
import { TASKS_SET_CONTEXT } from './tasks.context.constants';

export type TasksSetContextActionPayload = TasksContextType;

export const setTasksContext: ActionCreator<
  ActionWithPayload<TasksSetContextActionPayload>
> = (context: TasksSetContextActionPayload) => ({
  type: TASKS_SET_CONTEXT,
  payload: context,
});
