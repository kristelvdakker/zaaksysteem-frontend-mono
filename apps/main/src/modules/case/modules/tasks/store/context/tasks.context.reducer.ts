// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Reducer } from 'redux';
import { ActionWithPayload } from '@zaaksysteem/common/src/types/ActionWithPayload';
import { TasksContextType } from '../../types/Context.types';
import { TASKS_SET_CONTEXT } from './tasks.context.constants';
import { TasksSetContextActionPayload } from './tasks.context.actions';

const initialState: TasksContextType = {
  rootPath: '/',
  caseUuid: '',
  phase: 0,
};

const context: Reducer<
  TasksContextType,
  ActionWithPayload<TasksSetContextActionPayload>
> = (state = initialState, action) => {
  if (action.type === TASKS_SET_CONTEXT) {
    return {
      ...state,
      ...action.payload,
    };
  }

  return state;
};

export default context;
