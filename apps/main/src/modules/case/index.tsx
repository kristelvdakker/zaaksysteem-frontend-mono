// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import { RouteComponentProps } from 'react-router';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { getCaseModule } from './store/case.module';
import CaseContainer from './components/CaseContainer';
import locale from './locale/case.locale';

const CaseModule: React.FunctionComponent<
  RouteComponentProps<{
    caseId: string;
  }>
> = ({ match }) => (
  <DynamicModuleLoader modules={[getCaseModule(match.params.caseId)]}>
    <I18nResourceBundle resource={locale} namespace="case">
      <CaseContainer />
    </I18nResourceBundle>
  </DynamicModuleLoader>
);

export default CaseModule;
