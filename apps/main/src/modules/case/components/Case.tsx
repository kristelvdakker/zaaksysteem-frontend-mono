// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Route, Switch } from 'react-router-dom';
import CommunicationModule from '@zaaksysteem/communication-module/src';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
//@ts-ignore
import { objectifyParams } from '@mintlab/kitchen-sink/source/url';
import ObjectFormModule from './../modules/objectForm';
import TaskModule from './../modules/tasks';

export interface CasePropsType {
  caseUuid: string;
  contactUuid: string;
  contactName: string;
  busy: boolean;
  status: string | null;
  canCreatePipMessage: boolean;
}

/* eslint complexity: [2, 9] */
const Case: React.ComponentType<CasePropsType> = ({
  caseUuid,
  contactUuid,
  contactName,
  busy,
  status,
  canCreatePipMessage,
}) => {
  if (busy) {
    return <Loader />;
  }

  const caseOpen = status !== 'resolved';

  return (
    <React.Fragment>
      <Switch>
        <Route
          path={`/:prefix/case/:caseId/communication`}
          render={({ match }) => (
            <CommunicationModule
              capabilities={{
                allowSplitScreen: true,
                canAddAttachmentToCase: caseOpen,
                canAddSourceFileToCase: caseOpen,
                canAddThreadToCase: false,
                canCreateContactMoment: true,
                canCreatePipMessage: caseOpen && canCreatePipMessage,
                canCreateEmail:
                  process.env.WEBPACK_BUILD_TARGET !== 'production' && caseOpen,
                canCreateNote: true,
                canCreateMijnOverheid: false,
                canDeleteMessage: caseOpen,
                canImportMessage: caseOpen,
                canSelectCase: false,
                canSelectContact: true,
                canFilter: true,
                canOpenPDFPreview: true,
              }}
              context="case"
              caseUuid={caseUuid}
              contactUuid={contactUuid}
              contactName={contactName}
              rootPath={match.url}
            />
          )}
        />
        <Route
          path={`/:prefix/case/:caseId/phase/:phase/tasks`}
          render={({
            match: {
              params: { caseId, phase },
              url,
            },
          }) => <TaskModule caseUuid={caseId} phase={phase} rootPath={url} />}
        />
        <Route
          path={`/:prefix/case/:caseId/object/:type(create|update)/:objectUuid?`}
          render={({
            history: {
              location: { search },
            },
            match: {
              params: { caseId, type, objectUuid },
              url,
            },
          }) => {
            const [, queryParam] = search.split('?');
            const { objectTypeUuid } = objectifyParams(queryParam);

            return (
              <ObjectFormModule
                type={type}
                caseUuid={caseId}
                objectTypeUuid={objectTypeUuid}
                objectUuid={objectUuid}
                rootPath={url}
              />
            );
          }}
        />
      </Switch>
    </React.Fragment>
  );
};

export default Case;
