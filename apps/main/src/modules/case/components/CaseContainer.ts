// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { AJAX_STATE_VALID } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { CaseRootStateType } from '../store/case.reducer';
import Case, { CasePropsType } from './Case';

type PropsFromStateType = Pick<
  CasePropsType,
  | 'busy'
  | 'caseUuid'
  | 'contactName'
  | 'contactUuid'
  | 'status'
  | 'canCreatePipMessage'
>;

const mapStateToProps = (state: CaseRootStateType): PropsFromStateType => {
  if (
    !state.case ||
    state.case.state !== AJAX_STATE_VALID ||
    !state.case.requestor
  ) {
    return {
      busy: true,
      caseUuid: '',
      contactName: '',
      contactUuid: '',
      status: null,
      canCreatePipMessage: false,
    };
  }

  const {
    case: {
      id: caseUuid,
      requestor: {
        reference: contactUuid,
        displayName: contactName,
        subjectType,
      },
      status,
    },
  } = state;

  return {
    status,
    contactUuid,
    contactName,
    caseUuid: caseUuid || '',
    busy: false,
    canCreatePipMessage: subjectType !== 'employee',
  };
};

const CaseContainer = connect(mapStateToProps)(Case);

export default CaseContainer;
