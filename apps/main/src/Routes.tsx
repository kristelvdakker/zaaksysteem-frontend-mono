// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { AJAX_STATE_VALID } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
//@ts-ignore
import ErrorBoundary from '@zaaksysteem/common/src/components/ErrorBoundary/ErrorBoundary';
import { SessionRootStateType } from '@zaaksysteem/common/src/store/session/session.reducer';
import CommunicationModule from '@zaaksysteem/communication-module/src';
import { LocationMap } from './modules/contactView/location/components/Location';
import { Layout } from './components/Layout/Layout';
import { useAppRootStyle } from './App.style';

const CaseModule = React.lazy(
  () => import(/* webpackChunkName: "case" */ './modules/case')
);

const DashboardModule = React.lazy(
  () => import(/* webpackChunkName: "dashboard" */ './modules/dashboard')
);

const ProfileModule = React.lazy(
  () => import(/* webpackChunkName: "relation" */ './modules/profile/Profile')
);

const RelationModule = React.lazy(
  () => import(/* webpackChunkName: "relation" */ './modules/relation/Relation')
);

const CustomerContactModule = React.lazy(
  () =>
    import(
      /* webpackChunkName: "customer-contact" */ './modules/customerContact'
    )
);

const Intake = React.lazy(
  () => import(/* webpackChunkName: "intake" */ './components/Intake/Intake')
);

const ObjectViewModule = React.lazy(
  () => import(/* webpackChunkName: "object" */ './modules/objectView')
);

const Demo = React.lazy(
  () => import(/* webpackChunkName: "demo" */ './components/Demo/Demo')
);

const ContactViewModule = React.lazy(
  () => import(/* webpackChunkName: "object" */ './modules/contactView')
);

export interface RoutesPropsType {
  ready: boolean;
  prefix: string;
}

const Routes: React.ComponentType<RoutesPropsType> = ({ ready, prefix }) => {
  const classes = useAppRootStyle();
  if (!ready) {
    return <Loader delay={200} />;
  }

  return (
    <div className={classes.app}>
      <ErrorBoundary>
        <Switch>
          <Route path={`${prefix}/case/:caseId`} component={CaseModule} />
          <Route
            path={`${prefix}/contact/:contactId/communication`}
            render={({ match }) => (
              <CommunicationModule
                capabilities={{
                  allowSplitScreen: true,
                  canAddAttachmentToCase: true,
                  canAddSourceFileToCase: true,
                  canAddThreadToCase: false,
                  canCreateContactMoment: true,
                  canCreatePipMessage: false,
                  canCreateEmail: false,
                  canCreateNote: true,
                  canCreateMijnOverheid: false,
                  canDeleteMessage: true,
                  canImportMessage: false,
                  canSelectCase: true,
                  canSelectContact: false,
                  canFilter: true,
                  canOpenPDFPreview: true,
                }}
                context="contact"
                contactUuid={match.params.contactId}
                rootPath={match.url}
              />
            )}
          />
          <Route
            path={`${prefix}/profile/:contactType/:contactId`}
            component={ProfileModule}
          />
          <Route
            path={`${prefix}/location/:type/:uuid`}
            render={({ match }) => (
              <LocationMap uuid={match.params.uuid} type={match.params.type} />
            )}
          />
          <Route
            path={`${prefix}/relation/:contactId`}
            component={RelationModule}
          />
          <Route
            path={`${prefix}/object/:uuid`}
            render={props => (
              <Layout>
                <ObjectViewModule prefix={prefix} {...props} />
              </Layout>
            )}
          />
          <Route
            path={`${prefix}/customer-contact`}
            component={CustomerContactModule}
          />
          <Route path={`${prefix}/intake`} component={Intake} />
          <Route path={`${prefix}/demo`} component={Demo} />
          <Route path={`${prefix}/dashboard`} component={DashboardModule} />
          <Route
            path={`${prefix}/contact-view/:type/:uuid`}
            render={props => (
              <Layout>
                <ContactViewModule prefix={prefix} {...props} />
              </Layout>
            )}
          />
        </Switch>
      </ErrorBoundary>
    </div>
  );
};

type PropsFromState = Pick<RoutesPropsType, 'ready'>;

const mapStateToProps = ({
  session: { state, data },
}: SessionRootStateType): PropsFromState => {
  if (state !== AJAX_STATE_VALID || !data || !data.logged_in_user) {
    return {
      ready: false,
    };
  }

  return {
    ready: true,
  };
};

export default connect<PropsFromState, {}, {}, SessionRootStateType>(
  mapStateToProps
)(Routes);
