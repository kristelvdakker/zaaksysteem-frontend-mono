# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.58.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.58.0...@zaaksysteem/main@0.58.1) (2020-12-21)

**Note:** Version bump only for package @zaaksysteem/main





# [0.58.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.57.6...@zaaksysteem/main@0.58.0) (2020-12-18)


### Features

* **ObjectForm:** MINTY-5537 - Prefill case data when creating a new object ([996ee0f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/996ee0f7422a81e84b04c77140b3f148af1c8908))





## [0.57.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.57.5...@zaaksysteem/main@0.57.6) (2020-12-18)

**Note:** Version bump only for package @zaaksysteem/main





## [0.57.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.57.4...@zaaksysteem/main@0.57.5) (2020-12-17)

**Note:** Version bump only for package @zaaksysteem/main





## [0.57.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.57.3...@zaaksysteem/main@0.57.4) (2020-12-10)

**Note:** Version bump only for package @zaaksysteem/main





## [0.57.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.57.2...@zaaksysteem/main@0.57.3) (2020-12-04)

**Note:** Version bump only for package @zaaksysteem/main





## [0.57.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.57.1...@zaaksysteem/main@0.57.2) (2020-12-03)


### Bug Fixes

* **Tasks:** MINTY-5672 - change timing of middleware calls ([a685e0a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a685e0af8cb4c7dc2cfee78945e4339bb54fdb00))





## [0.57.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.57.0...@zaaksysteem/main@0.57.1) (2020-11-25)

**Note:** Version bump only for package @zaaksysteem/main





# [0.57.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.56.2...@zaaksysteem/main@0.57.0) (2020-11-20)


### Features

* **ObjectForm:** MINTY-5648 - Include all case relations when updating objects ([853dab3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/853dab3cc0cad370d6fb9c8962989898029eef88))





## [0.56.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.56.1...@zaaksysteem/main@0.56.2) (2020-11-19)

**Note:** Version bump only for package @zaaksysteem/main





## [0.56.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.56.0...@zaaksysteem/main@0.56.1) (2020-11-18)

**Note:** Version bump only for package @zaaksysteem/main





# [0.56.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.55.0...@zaaksysteem/main@0.56.0) (2020-11-13)


### Features

* **ContactView:** MINTY-5151 - iframe relations ([698f05f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/698f05f))





# [0.55.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.54.1...@zaaksysteem/main@0.55.0) (2020-11-12)


### Features

* **ContactView:** MINTY-5165 - iframe custom fields contact tab ([e1f6310](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e1f6310))





## [0.54.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.54.0...@zaaksysteem/main@0.54.1) (2020-11-11)

**Note:** Version bump only for package @zaaksysteem/main





# [0.54.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.53.6...@zaaksysteem/main@0.54.0) (2020-11-11)


### Features

* **Tasks:** MINTY-5546 - add ability to order and filter columns ([292f80a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/292f80a))





## [0.53.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.53.5...@zaaksysteem/main@0.53.6) (2020-11-11)

**Note:** Version bump only for package @zaaksysteem/main





## [0.53.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.53.4...@zaaksysteem/main@0.53.5) (2020-10-30)

**Note:** Version bump only for package @zaaksysteem/main





## [0.53.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.53.3...@zaaksysteem/main@0.53.4) (2020-10-26)

**Note:** Version bump only for package @zaaksysteem/main





## [0.53.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.53.2...@zaaksysteem/main@0.53.3) (2020-10-26)


### Bug Fixes

* **ContactFinder:** MINTY-5313 - fix broken contact finder configs ([73f6c23](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/73f6c23))





## [0.53.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.53.1...@zaaksysteem/main@0.53.2) (2020-10-15)

**Note:** Version bump only for package @zaaksysteem/main





## [0.53.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.53.0...@zaaksysteem/main@0.53.1) (2020-10-15)

**Note:** Version bump only for package @zaaksysteem/main





# [0.53.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.52.0...@zaaksysteem/main@0.53.0) (2020-10-15)


### Features

* **Intake:** MINTY-5324 - add extension hints and label to Document Intake ([e6d0049](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e6d0049))
* **ObjectView:** MINTY-5260 - related subjects in object overview ([3fb33fc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3fb33fc))





# [0.52.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.51.2...@zaaksysteem/main@0.52.0) (2020-10-08)


### Features

* **Dashboard:** MINTY-4976 - Assignee filter option ([5804b60](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5804b60))
* **TasksWidget:** MINTY-4976 - add Tasks widget features with overlay form ([ba9c2bf](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ba9c2bf))
* **TasksWidget:** MINTY-4976 - change outputFormat to config ([069fd79](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/069fd79))
* **TasksWidget:** MINTY-4976 - fix crash bug with department finder labels ([b372f6e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b372f6e))
* **TasksWidget:** MINTY-4976 - fix keywords filtering, too many post calls ([482ab37](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/482ab37))
* **TasksWidget:** MINTY-4976 - misc MR related code improvements ([8a27f33](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8a27f33))





## [0.51.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.51.1...@zaaksysteem/main@0.51.2) (2020-10-06)

**Note:** Version bump only for package @zaaksysteem/main





## [0.51.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.51.0...@zaaksysteem/main@0.51.1) (2020-10-02)

**Note:** Version bump only for package @zaaksysteem/main





# [0.51.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.50.6...@zaaksysteem/main@0.51.0) (2020-09-30)


### Features

* **Intake:** MINTY-5066 & MINTY-5069 - change intake behaviour in Pip/Intake ([aaa8bdf](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/aaa8bdf))
* **Intake:** MINTY-5069 - change IconButton to A ([7b8e700](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7b8e700))





## [0.50.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.50.5...@zaaksysteem/main@0.50.6) (2020-09-30)

**Note:** Version bump only for package @zaaksysteem/main





## [0.50.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.50.4...@zaaksysteem/main@0.50.5) (2020-09-29)

**Note:** Version bump only for package @zaaksysteem/main





## [0.50.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.50.3...@zaaksysteem/main@0.50.4) (2020-09-29)

**Note:** Version bump only for package @zaaksysteem/main





## [0.50.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.50.2...@zaaksysteem/main@0.50.3) (2020-09-25)

**Note:** Version bump only for package @zaaksysteem/main





## [0.50.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.50.1...@zaaksysteem/main@0.50.2) (2020-09-23)

**Note:** Version bump only for package @zaaksysteem/main





## [0.50.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.50.0...@zaaksysteem/main@0.50.1) (2020-09-18)

**Note:** Version bump only for package @zaaksysteem/main





# [0.50.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.49.2...@zaaksysteem/main@0.50.0) (2020-09-17)


### Features

* **Dashboard:** MINTY-4975 - Widget search ([d2c4a5c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d2c4a5c))





## [0.49.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.49.1...@zaaksysteem/main@0.49.2) (2020-09-17)

**Note:** Version bump only for package @zaaksysteem/main





## [0.49.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.49.0...@zaaksysteem/main@0.49.1) (2020-09-17)

**Note:** Version bump only for package @zaaksysteem/main





# [0.49.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.48.2...@zaaksysteem/main@0.49.0) (2020-09-16)


### Features

* **Main:** MINTY-4759 - add menu items to main menu bar ([8ed4876](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8ed4876))





## [0.48.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.48.1...@zaaksysteem/main@0.48.2) (2020-09-16)

**Note:** Version bump only for package @zaaksysteem/main





## [0.48.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.48.0...@zaaksysteem/main@0.48.1) (2020-09-15)

**Note:** Version bump only for package @zaaksysteem/main





# [0.48.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.47.0...@zaaksysteem/main@0.48.0) (2020-09-15)


### Features

* **Intake:** MINTY-5020 - change text on Add To Case dialog ([7b01b36](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7b01b36))
* **Main:** MINTY-4808 - create case from + button ([2f1fbe9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2f1fbe9))





# [0.47.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.46.0...@zaaksysteem/main@0.47.0) (2020-09-14)


### Features

* **Dashboard:** Minty 4889 - Be able to delete task widget from dashboard ([a048c23](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a048c23))





# [0.46.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.45.1...@zaaksysteem/main@0.46.0) (2020-09-04)


### Features

* **ObjectView:** Implement readOnly mode for objectView ([3ae03a7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3ae03a7))





## [0.45.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.45.0...@zaaksysteem/main@0.45.1) (2020-09-03)

**Note:** Version bump only for package @zaaksysteem/main





# [0.45.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.44.0...@zaaksysteem/main@0.45.0) (2020-09-03)


### Features

* **ObjectView:** MINTY-4867, MINTY-4877 - add relationships overview to Object View ([57ef51d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/57ef51d))





# [0.44.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.43.4...@zaaksysteem/main@0.44.0) (2020-09-03)


### Features

* **Main:** MINTY-4758 - start new action from Object View ([1867c5b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1867c5b))





## [0.43.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.43.3...@zaaksysteem/main@0.43.4) (2020-09-03)

**Note:** Version bump only for package @zaaksysteem/main





## [0.43.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.43.2...@zaaksysteem/main@0.43.3) (2020-09-01)

**Note:** Version bump only for package @zaaksysteem/main





## [0.43.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.43.1...@zaaksysteem/main@0.43.2) (2020-09-01)

**Note:** Version bump only for package @zaaksysteem/main





## [0.43.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.43.0...@zaaksysteem/main@0.43.1) (2020-09-01)

**Note:** Version bump only for package @zaaksysteem/main





# [0.43.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.42.0...@zaaksysteem/main@0.43.0) (2020-08-27)


### Features

* **Dashboard:** MINTY-4800 - add Tasks Dashboard widget merged ([df0dffc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/df0dffc))





# [0.42.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.41.0...@zaaksysteem/main@0.42.0) (2020-08-21)


### Features

* **objectForm:** MINTY-4749 - Add support for currency, email, webaddress, iban, date ([f18d1ea](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f18d1ea))





# [0.41.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.40.2...@zaaksysteem/main@0.41.0) (2020-08-20)


### Features

* **Main:** MINTY-4752 - add Search from Object View ([ab753fb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ab753fb))





## [0.40.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.40.1...@zaaksysteem/main@0.40.2) (2020-08-19)

**Note:** Version bump only for package @zaaksysteem/main





## [0.40.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.40.0...@zaaksysteem/main@0.40.1) (2020-08-17)

**Note:** Version bump only for package @zaaksysteem/main





# [0.40.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.36.2...@zaaksysteem/main@0.40.0) (2020-08-11)


### Features

* **Intake:** MINTY-4507 - add thumbnail feature to Intake ([7695409](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7695409))
* **Object:** Add support for relationship custom_object and subject ([6e83436](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6e83436))





# [0.39.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.36.2...@zaaksysteem/main@0.39.0) (2020-08-07)


### Features

* **Intake:** MINTY-4507 - add thumbnail feature to Intake ([7695409](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7695409))
* **Object:** Add support for relationship custom_object and subject ([6e83436](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6e83436))





# [0.38.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.36.2...@zaaksysteem/main@0.38.0) (2020-08-07)


### Features

* **Intake:** MINTY-4507 - add thumbnail feature to Intake ([7695409](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7695409))
* **Object:** Add support for relationship custom_object and subject ([6e83436](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6e83436))





# [0.37.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.36.2...@zaaksysteem/main@0.37.0) (2020-08-06)


### Features

* **Intake:** MINTY-4507 - add thumbnail feature to Intake ([7695409](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7695409))
* **Object:** Add support for relationship custom_object and subject ([6e83436](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6e83436))





## [0.36.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.36.2...@zaaksysteem/main@0.36.3) (2020-08-06)

**Note:** Version bump only for package @zaaksysteem/main





## [0.36.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.36.1...@zaaksysteem/main@0.36.2) (2020-08-06)

**Note:** Version bump only for package @zaaksysteem/main





## [0.36.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.36.0...@zaaksysteem/main@0.36.1) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/main





# [0.36.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.35.9...@zaaksysteem/main@0.36.0) (2020-07-23)


### Features

* **Intake:** Add support in External Contact for importing companies ([c43bdeb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c43bdeb))





## [0.35.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.35.8...@zaaksysteem/main@0.35.9) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/main





## [0.35.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.35.7...@zaaksysteem/main@0.35.8) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/main





## [0.35.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.35.6...@zaaksysteem/main@0.35.7) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/main





## [0.35.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.35.5...@zaaksysteem/main@0.35.6) (2020-07-17)

**Note:** Version bump only for package @zaaksysteem/main





## [0.35.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.35.4...@zaaksysteem/main@0.35.5) (2020-07-16)

**Note:** Version bump only for package @zaaksysteem/main





## [0.35.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.35.3...@zaaksysteem/main@0.35.4) (2020-07-15)

**Note:** Version bump only for package @zaaksysteem/main





## [0.35.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.35.2...@zaaksysteem/main@0.35.3) (2020-07-15)

**Note:** Version bump only for package @zaaksysteem/main





## [0.35.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.35.1...@zaaksysteem/main@0.35.2) (2020-07-15)

**Note:** Version bump only for package @zaaksysteem/main





## [0.35.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.35.0...@zaaksysteem/main@0.35.1) (2020-07-14)

**Note:** Version bump only for package @zaaksysteem/main





# [0.35.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.34.5...@zaaksysteem/main@0.35.0) (2020-07-09)


### Features

* **External Contacts:** MINTY-4347: add External Contact search/import for persons ([ef299fe](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ef299fe))





## [0.34.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.34.4...@zaaksysteem/main@0.34.5) (2020-07-09)

**Note:** Version bump only for package @zaaksysteem/main





## [0.34.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.34.3...@zaaksysteem/main@0.34.4) (2020-07-02)

**Note:** Version bump only for package @zaaksysteem/main





## [0.34.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.34.2...@zaaksysteem/main@0.34.3) (2020-06-30)

**Note:** Version bump only for package @zaaksysteem/main





## [0.34.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.34.1...@zaaksysteem/main@0.34.2) (2020-06-26)

**Note:** Version bump only for package @zaaksysteem/main





## [0.34.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.34.0...@zaaksysteem/main@0.34.1) (2020-06-25)

**Note:** Version bump only for package @zaaksysteem/main





# [0.34.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.33.5...@zaaksysteem/main@0.34.0) (2020-06-24)


### Features

* **Object:** MINTY-4239 - Allow objects to be updated and unrelated ([e9f3eb0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e9f3eb0))
* **Object:** MINTY-4239 - Relate created objects to the case ([a73e78e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a73e78e))





## [0.33.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.33.4...@zaaksysteem/main@0.33.5) (2020-06-23)

**Note:** Version bump only for package @zaaksysteem/main





## [0.33.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.33.3...@zaaksysteem/main@0.33.4) (2020-06-18)

**Note:** Version bump only for package @zaaksysteem/main





## [0.33.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.33.2...@zaaksysteem/main@0.33.3) (2020-06-17)

**Note:** Version bump only for package @zaaksysteem/main





## [0.33.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.33.1...@zaaksysteem/main@0.33.2) (2020-06-12)

**Note:** Version bump only for package @zaaksysteem/main





## [0.33.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.33.0...@zaaksysteem/main@0.33.1) (2020-06-11)

**Note:** Version bump only for package @zaaksysteem/main





# [0.33.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.32.0...@zaaksysteem/main@0.33.0) (2020-06-11)


### Features

* **Intake:** MINTY-4179 - add document number tooltip ([9cf6c1f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9cf6c1f))





# [0.32.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.31.2...@zaaksysteem/main@0.32.0) (2020-06-09)


### Features

* **Intake:** MINTY-4138 - prefill case create dialog ([fe80a63](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/fe80a63))





## [0.31.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.31.1...@zaaksysteem/main@0.31.2) (2020-06-09)


### Bug Fixes

* **Intake:** MINTY-4139 - fix error on sorting by date ([bcae3c4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/bcae3c4))





## [0.31.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.31.0...@zaaksysteem/main@0.31.1) (2020-06-08)

**Note:** Version bump only for package @zaaksysteem/main





# [0.31.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.30.4...@zaaksysteem/main@0.31.0) (2020-06-05)


### Features

* **Case:** Pass object uuid to angular when creating an object ([9c0919e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9c0919e))





## [0.30.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.30.3...@zaaksysteem/main@0.30.4) (2020-06-05)

**Note:** Version bump only for package @zaaksysteem/main





## [0.30.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.30.2...@zaaksysteem/main@0.30.3) (2020-06-05)

**Note:** Version bump only for package @zaaksysteem/main





## [0.30.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.30.1...@zaaksysteem/main@0.30.2) (2020-06-03)

**Note:** Version bump only for package @zaaksysteem/main





## [0.30.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.30.0...@zaaksysteem/main@0.30.1) (2020-05-29)

**Note:** Version bump only for package @zaaksysteem/main





# [0.30.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.29.0...@zaaksysteem/main@0.30.0) (2020-05-28)


### Features

* **case:** MINTY-4034 improve submit button text ([5123187](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5123187))





# [0.29.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.28.3...@zaaksysteem/main@0.29.0) (2020-05-28)


### Features

* **case:** MINTY-4034 allow objects to be created from the case view ([ee6dd74](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ee6dd74))





## [0.28.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.28.2...@zaaksysteem/main@0.28.3) (2020-05-27)

**Note:** Version bump only for package @zaaksysteem/main





## [0.28.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.28.1...@zaaksysteem/main@0.28.2) (2020-05-19)


### Bug Fixes

* **Intake:** MINTY-4020: fix displaying for date field, alignment properties ([b832159](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b832159))





## [0.28.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.28.0...@zaaksysteem/main@0.28.1) (2020-05-14)

**Note:** Version bump only for package @zaaksysteem/main





# [0.28.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.27.5...@zaaksysteem/main@0.28.0) (2020-05-14)


### Features

* **Intake:** MINTY-3868 - add description column to Intake ([c1c6df2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c1c6df2))





## [0.27.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.27.4...@zaaksysteem/main@0.27.5) (2020-05-13)

**Note:** Version bump only for package @zaaksysteem/main





## [0.27.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.27.3...@zaaksysteem/main@0.27.4) (2020-05-12)

**Note:** Version bump only for package @zaaksysteem/main





## [0.27.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.27.2...@zaaksysteem/main@0.27.3) (2020-05-12)

**Note:** Version bump only for package @zaaksysteem/main





## [0.27.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.27.1...@zaaksysteem/main@0.27.2) (2020-05-12)

**Note:** Version bump only for package @zaaksysteem/main





## [0.27.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.27.0...@zaaksysteem/main@0.27.1) (2020-05-11)

**Note:** Version bump only for package @zaaksysteem/main





# [0.27.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.26.1...@zaaksysteem/main@0.27.0) (2020-05-11)


### Features

* **ObjectView:** MINTY-3809 Fetch Object type and display label, magic_string and description in Object View ([088f049](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/088f049))





## [0.26.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.26.0...@zaaksysteem/main@0.26.1) (2020-05-01)

**Note:** Version bump only for package @zaaksysteem/main





# [0.26.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.25.1...@zaaksysteem/main@0.26.0) (2020-05-01)


### Features

* **CreateObject:** MINTY-3472 - add Rights step to Object Create with associated components ([8699e78](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8699e78))





## [0.25.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.25.0...@zaaksysteem/main@0.25.1) (2020-04-30)

**Note:** Version bump only for package @zaaksysteem/main





# [0.25.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.24.8...@zaaksysteem/main@0.25.0) (2020-04-30)


### Features

* **ObjectView:** MINTY-3779 Implement side menu for object attributes view ([ef41a85](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ef41a85))
* **SideMenu:** MINTY-3779 Add SideMenu component ([ec155a4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ec155a4))





## [0.24.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.24.7...@zaaksysteem/main@0.24.8) (2020-04-30)

**Note:** Version bump only for package @zaaksysteem/main





## [0.24.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.24.6...@zaaksysteem/main@0.24.7) (2020-04-28)

**Note:** Version bump only for package @zaaksysteem/main





## [0.24.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.24.5...@zaaksysteem/main@0.24.6) (2020-04-20)

**Note:** Version bump only for package @zaaksysteem/main





## [0.24.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.24.4...@zaaksysteem/main@0.24.5) (2020-04-16)

**Note:** Version bump only for package @zaaksysteem/main





## [0.24.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.24.3...@zaaksysteem/main@0.24.4) (2020-04-16)

**Note:** Version bump only for package @zaaksysteem/main





## [0.24.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.24.2...@zaaksysteem/main@0.24.3) (2020-04-16)

**Note:** Version bump only for package @zaaksysteem/main





## [0.24.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.24.1...@zaaksysteem/main@0.24.2) (2020-04-16)

**Note:** Version bump only for package @zaaksysteem/main





## [0.24.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.24.0...@zaaksysteem/main@0.24.1) (2020-04-16)

**Note:** Version bump only for package @zaaksysteem/main





# [0.24.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.23.2...@zaaksysteem/main@0.24.0) (2020-04-14)


### Features

* **NavigationBar:** MINTY-3629 Alter main layout to incorporate `NavigationBar` component ([6d73c7d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6d73c7d))





## [0.23.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.23.1...@zaaksysteem/main@0.23.2) (2020-04-14)

**Note:** Version bump only for package @zaaksysteem/main





## [0.23.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.23.0...@zaaksysteem/main@0.23.1) (2020-04-10)

**Note:** Version bump only for package @zaaksysteem/main





# [0.23.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.22.0...@zaaksysteem/main@0.23.0) (2020-04-09)


### Features

* **BreadcrumbBar:** MINTY-3628 Implement header containing breadcrumb component which can be used in modules ([4a85e72](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4a85e72))





# [0.22.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.21.3...@zaaksysteem/main@0.22.0) (2020-04-09)


### Features

* **TaskItem:** MINTY-3573 - Display details for closed task item ([eff342e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/eff342e))





## [0.21.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.21.2...@zaaksysteem/main@0.21.3) (2020-04-07)

**Note:** Version bump only for package @zaaksysteem/main





## [0.21.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.21.1...@zaaksysteem/main@0.21.2) (2020-04-06)

**Note:** Version bump only for package @zaaksysteem/main





## [0.21.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.21.0...@zaaksysteem/main@0.21.1) (2020-04-03)

**Note:** Version bump only for package @zaaksysteem/main





# [0.21.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.20.0...@zaaksysteem/main@0.21.0) (2020-04-02)


### Features

* **Communication:** MINTY-3432 - add attachment PDF preview ([5bebd92](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5bebd92))





# [0.20.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.6...@zaaksysteem/main@0.20.0) (2020-04-01)


### Features

* **ObjectView:** Implement first iteration of Object View ([daa2da5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/daa2da5))





## [0.19.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.5...@zaaksysteem/main@0.19.6) (2020-04-01)

**Note:** Version bump only for package @zaaksysteem/main





## [0.19.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.4...@zaaksysteem/main@0.19.5) (2020-04-01)

**Note:** Version bump only for package @zaaksysteem/main





## [0.19.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.3...@zaaksysteem/main@0.19.4) (2020-03-31)

**Note:** Version bump only for package @zaaksysteem/main





## [0.19.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.2...@zaaksysteem/main@0.19.3) (2020-03-31)

**Note:** Version bump only for package @zaaksysteem/main





## [0.19.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.1...@zaaksysteem/main@0.19.2) (2020-03-30)

**Note:** Version bump only for package @zaaksysteem/main





## [0.19.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.0...@zaaksysteem/main@0.19.1) (2020-03-30)

**Note:** Version bump only for package @zaaksysteem/main





# [0.19.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.18.3...@zaaksysteem/main@0.19.0) (2020-03-30)


### Features

* **FormRenderer:** Improve performance and readability of `FormRenderer` component ([f310425](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f310425))
* **FormRenderer:** Refactor formRenderer to hook and update Formik to latest version 2.1.4 ([c5d1e71](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c5d1e71))





## [0.18.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.18.2...@zaaksysteem/main@0.18.3) (2020-03-27)

**Note:** Version bump only for package @zaaksysteem/main





## [0.18.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.18.1...@zaaksysteem/main@0.18.2) (2020-03-20)

**Note:** Version bump only for package @zaaksysteem/main





## [0.18.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.18.0...@zaaksysteem/main@0.18.1) (2020-03-19)

**Note:** Version bump only for package @zaaksysteem/main





# [0.18.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.17.2...@zaaksysteem/main@0.18.0) (2020-03-19)


### Features

* **DocumentExplorer:** MINTY-3300 - add initial PDF preview features, misc. ([a131814](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a131814))
* **DocumentExplorer:** MINTY-3300 - change Loader, exports ([b25d770](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b25d770))
* **DocumentExplorer:** MINTY-3300 - split up DocumentExplorer functionality, create new Intake entrypoint, add Selectable state to table ([5bae0d3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5bae0d3))
* **Intake:** MINTY-3437 - add search component ([2bb171b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2bb171b))





## [0.17.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.17.1...@zaaksysteem/main@0.17.2) (2020-03-17)

**Note:** Version bump only for package @zaaksysteem/main





## [0.17.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.17.0...@zaaksysteem/main@0.17.1) (2020-03-17)

**Note:** Version bump only for package @zaaksysteem/main





# [0.17.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.10...@zaaksysteem/main@0.17.0) (2020-03-05)


### Features

* **CaseDocuments:** add misc features ([8845760](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8845760))
* **Communication:** MINTY-3241 Re-enable adding notes and contactmoments on closed cases ([b21dd23](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b21dd23))
* **DocumentPreview:** MINTY-2960 Add case document preview to dev dashboard ([0f6fb22](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0f6fb22))





## [0.16.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.9...@zaaksysteem/main@0.16.10) (2020-03-03)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.8...@zaaksysteem/main@0.16.9) (2020-02-24)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.7...@zaaksysteem/main@0.16.8) (2020-02-21)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.6...@zaaksysteem/main@0.16.7) (2020-02-17)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.5...@zaaksysteem/main@0.16.6) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.4...@zaaksysteem/main@0.16.5) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.3...@zaaksysteem/main@0.16.4) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.2...@zaaksysteem/main@0.16.3) (2020-02-10)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.1...@zaaksysteem/main@0.16.2) (2020-02-07)

**Note:** Version bump only for package @zaaksysteem/main

## [0.16.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.0...@zaaksysteem/main@0.16.1) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/main

# [0.16.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.10...@zaaksysteem/main@0.16.0) (2020-02-06)

### Features

- **Communication:** MINTY-2962 Extract Pip form and reuse the same form for create and reply ([42848aa](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/42848aa))

## [0.15.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.9...@zaaksysteem/main@0.15.10) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.8...@zaaksysteem/main@0.15.9) (2020-01-30)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.7...@zaaksysteem/main@0.15.8) (2020-01-30)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.6...@zaaksysteem/main@0.15.7) (2020-01-23)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.5...@zaaksysteem/main@0.15.6) (2020-01-23)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.4...@zaaksysteem/main@0.15.5) (2020-01-22)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.3...@zaaksysteem/main@0.15.4) (2020-01-14)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.1...@zaaksysteem/main@0.15.3) (2020-01-14)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.1...@zaaksysteem/main@0.15.2) (2020-01-13)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.0...@zaaksysteem/main@0.15.1) (2020-01-13)

**Note:** Version bump only for package @zaaksysteem/main

# [0.15.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.14.0...@zaaksysteem/main@0.15.0) (2020-01-10)

### Features

- **Communication:** MINTY-2665 Use `ContactFinder` component to search for email recipients ([c0a4ad6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c0a4ad6))

# [0.14.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.13.0...@zaaksysteem/main@0.14.0) (2020-01-09)

### Bug Fixes

- (CaseManagement) truncate task title ([c5d397b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c5d397b))
- **Tasks:** MINTY-2769 - fix redirect logic in middleware ([1d09b68](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1d09b68))
- **Tasks:** MINTY-2816 - change get_task_list endpoint to filter ([f127e36](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f127e36))
- **Tasks:** MINTY-2822 - fix displayname + typing issues ([6f5c722](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6f5c722))

### Features

- **CaseManagement:** add task creator ([939fbbb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/939fbbb))
- **ContactFinder:** MINTY-2618 - add type filter support for ContactFinder ([d837b4b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d837b4b))
- **DatePicker:** MINTY-2601 - add onClose ability to DatePicker ([732401e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/732401e))
- **Tasks:** add misc. style/UX improvements ([b619af4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b619af4))
- **Tasks:** Add the basic setup for the task module ([ecc9d4c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ecc9d4c))
- **Tasks:** MINTY-1575 - add basic form fields, DatePicker ([07ed5e5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/07ed5e5))
- **Tasks:** MINTY-1575 - add edit, delete, unlock functionality ([8dca70b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8dca70b))
- **Tasks:** MINTY-1575 - fix build/import problems ([23c0700](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/23c0700))
- **Tasks:** MINTY-2672 - call postmessage for every action ([99bd8fb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/99bd8fb))
- **Tasks:** MINTY-2695 - cancel adding of task on blur ([99f96fb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/99f96fb))
- **Tasks:** MINTY-2695 - process some MR feedback ([8bd89cc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8bd89cc))
- **Tasks:** MINTY-2695 - rewrite internal logic to immediate responding to user actions ([c0f74c2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c0f74c2))

# [0.13.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.6...@zaaksysteem/main@0.13.0) (2020-01-09)

### Bug Fixes

- **Communication:** MINTY-2262 - allow e-mail addresses over multiple lines ([db44093](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/db44093))
- **UI:** Fix aria-labels on snackbar and dialogs ([775a287](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/775a287))

### Features

- **Case:** Allow status of case to determine capabilities of communication module ([1a7a781](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1a7a781))
- **Communication:** MINTY-1798 Add `canImportMessage` capability ([c10c88e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c10c88e))
- **Communication:** MINTY-1925 Make deleting a message controllable via a capability ([704ee17](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/704ee17))
- **Communication:** MINTY-2034 Call `/get_thread_list` with correct params according to context of communication-module ([7fef0f2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7fef0f2))
- **i18n:** Move common locale namespace to `@zaaksysteem/common` package ([4a36e92](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4a36e92))
- **Message:** Allow source files of messages to be added to the case documents ([6819f60](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6819f60))
- **Message:** Make deletion of messages depended on case status ([c1968df](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c1968df))
- **Thread:** Allow actions on case-messages to be performed outside of case-context ([2608cbc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2608cbc))
- **Thread:** Allow threads to be linked to cases from main view ([44a8a51](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/44a8a51))

## [0.12.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.5...@zaaksysteem/main@0.12.6) (2019-12-19)

**Note:** Version bump only for package @zaaksysteem/main

## [0.12.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.4...@zaaksysteem/main@0.12.5) (2019-11-21)

**Note:** Version bump only for package @zaaksysteem/main

## [0.12.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.3...@zaaksysteem/main@0.12.4) (2019-10-29)

**Note:** Version bump only for package @zaaksysteem/main

## [0.12.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.2...@zaaksysteem/main@0.12.3) (2019-10-25)

**Note:** Version bump only for package @zaaksysteem/main

## [0.12.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.1...@zaaksysteem/main@0.12.2) (2019-10-22)

**Note:** Version bump only for package @zaaksysteem/main

## [0.12.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.0...@zaaksysteem/main@0.12.1) (2019-10-17)

**Note:** Version bump only for package @zaaksysteem/main

# [0.12.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.11.3...@zaaksysteem/main@0.12.0) (2019-10-17)

### Features

- **Communciation:** MINTY-1790 Add communication module on `/main/customer-contact` route ([c90005c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c90005c))
- **Communication:** MINTY-1790 Add contacts and customer-contact to main dashboard for testing purposes ([fb27a9a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/fb27a9a))
- **Snackbar:** MINTY-1788 Implement snackbar in pip and main app ([dcbe4da](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/dcbe4da))

## [0.11.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.11.2...@zaaksysteem/main@0.11.3) (2019-09-30)

**Note:** Version bump only for package @zaaksysteem/main

## [0.11.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.11.1...@zaaksysteem/main@0.11.2) (2019-09-26)

**Note:** Version bump only for package @zaaksysteem/main

## [0.11.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.11.0...@zaaksysteem/main@0.11.1) (2019-09-26)

**Note:** Version bump only for package @zaaksysteem/main

# [0.11.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.10.0...@zaaksysteem/main@0.11.0) (2019-09-09)

### Bug Fixes

- **Typescript:** fix misc. errors ([a5eb790](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a5eb790))

### Features

- **Apps:** Implement typescript ([583155f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/583155f))
- **Note:** Implement typescript ([47808bc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/47808bc))
- **SwitchViewButton:** Implement typescript ([b119c2d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b119c2d))
- **Tabs:** Implement typescript ([7eabf36](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7eabf36))
- **Thread:** Implement typescript ([896ada2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/896ada2))
- **ThreadPlaceholder:** Implement typescript ([be532c7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/be532c7))
- **TypeScript:** Add tsconfig.json, which is needed to enable typescript ([da89a6b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/da89a6b))

# [0.10.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.9.1...@zaaksysteem/main@0.10.0) (2019-09-06)

### Bug Fixes

- **Communication:** restrict rendering contentarea when on threads-only route ([747c4ce](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/747c4ce))

### Features

- **Communication:** MINTY-1289 Add link to case when thread is displayed in the context of a contact ([ed0bd1e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ed0bd1e))
- **Communication:** MINTY-1294 Implement `CaseFinder` component in add `ContactMoment` form ([27af541](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/27af541))
- **Communication:** MINTY-1402 - add support for saving a note ([4fd9e80](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4fd9e80))

## [0.9.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.9.0...@zaaksysteem/main@0.9.1) (2019-09-05)

**Note:** Version bump only for package @zaaksysteem/main

# [0.9.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.8.1...@zaaksysteem/main@0.9.0) (2019-08-29)

### Bug Fixes

- **Communication:** fix invalid value when saving a contact ([4ea871b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4ea871b))
- **Communication:** MINTY-1386: fix uuid of contact when saving ([771f520](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/771f520))
- **Communication:** MINTY-1391 - display thread content with newlines ([f754156](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f754156))
- **Communication:** Update thread unwrapping to the fixed API ([3604122](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3604122))

### Features

- **Communication:** add person/company icons to Contactfinder, some styling ([a2f2da2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a2f2da2))
- **Communication:** Add thread views for contactmoment and note ([e60867e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e60867e))
- **Communication:** Add view for notes ([4481b91](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4481b91))
- **Communication:** change breakpoints so medium will show thread list ([1239fba](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1239fba))
- **Communication:** Implement API v2 for threadview ([c5f2689](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c5f2689))
- **Communication:** Implement placeholder for the view side ([4271423](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4271423))
- **Communication:** MINTY-1115 Add `SearchField` to `Theads` ([5ec462a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ec462a))
- **Communication:** MINTY-1115 Add `Threads` filter component ([409738f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/409738f))
- **Communication:** MINTY-1115 Add direction icon to indicate incoming or outgoing messages ([6b8e4fa](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6b8e4fa))
- **Communication:** MINTY-1115 Move add and refresh button to `Threads` list ([e806749](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e806749))
- **Communication:** MINTY-1126 Style add button ([594ab7a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/594ab7a))
- **Communication:** MINTY-1260 - save Contactmoment, misc. styling and refactoring ([1c1b872](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1c1b872))
- **Communication:** MINTY-1289 Add communication module to contact route ([5ab6ba4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ab6ba4))
- **Communication:** Process MR feedback ([8b17f80](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8b17f80))
- **Communication:** tweak some misc. styling ([e1a16b5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e1a16b5))
- **Communication:** Tweak some styling ([bb1158d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/bb1158d))
- **ErrorHandling:** MINTY-1126 Centralized error handling displaying a `Alert` for every failed async action ([d65d459](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d65d459))
- **UI:** Upgrade to MUI4 ([ac921f6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ac921f6))

## [0.8.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.8.0...@zaaksysteem/main@0.8.1) (2019-08-27)

**Note:** Version bump only for package @zaaksysteem/main

# [0.8.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.7.2...@zaaksysteem/main@0.8.0) (2019-08-22)

### Features

- **apps:** Prevent automatic opening of browser tabs on start ([4740421](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4740421))

## [0.7.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.7.1...@zaaksysteem/main@0.7.2) (2019-08-12)

**Note:** Version bump only for package @zaaksysteem/main

## [0.7.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.7.0...@zaaksysteem/main@0.7.1) (2019-08-06)

**Note:** Version bump only for package @zaaksysteem/main

# [0.7.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.6.0...@zaaksysteem/main@0.7.0) (2019-08-01)

### Features

- **Iframe:** MINTY-1121 When running in an iframe, notify top window of location changes ([4618672](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4618672))

# [0.6.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.5.0...@zaaksysteem/main@0.6.0) (2019-07-31)

### Features

- **Communication:** MINTY-1126 Add case reducer which provides information needed for communication module & add temporary dashboard, useful for navigating to open cases ([6d70c7d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6d70c7d))

# [0.5.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.4.1...@zaaksysteem/main@0.5.0) (2019-07-31)

### Features

- **Communication:** add general Communication setup and Thread list ([7cb92e6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7cb92e6))

## [0.4.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.4.0...@zaaksysteem/main@0.4.1) (2019-07-30)

**Note:** Version bump only for package @zaaksysteem/main

# [0.4.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.3.0...@zaaksysteem/main@0.4.0) (2019-07-29)

### Features

- **Dates:** MINTY-1120 Add localized date formatting lib `fecha` ([84b6693](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/84b6693))

# [0.3.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.2.0...@zaaksysteem/main@0.3.0) (2019-07-29)

### Features

- **i18n:** MINTY-1120 Add i18next to setup and define APP_CONTEXT_ROOT variable on build time. ([a3e6a4c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a3e6a4c))

# [0.2.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.1.0...@zaaksysteem/main@0.2.0) (2019-07-25)

### Features

- **main:** MINTY-1237 Add session state to @zaaksysteem/common and implement in app ([575898f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/575898f))

# 0.1.0 (2019-07-25)

### Features

- **main:** MINTY-1232 Add `connected-react-router` package to link router to redux store ([a673309](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a673309))

## [0.1.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/compare/@zaaksysteem/case-management@0.1.0...@zaaksysteem/case-management@0.1.1) (2019-07-23)

**Note:** Version bump only for package @zaaksysteem/case-management

# 0.1.0 (2019-07-18)

### Features

- **CI:** MINTY-1120 Add gitlab CI file ([f4e971e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/f4e971e))
- **Publishing:** MINTY-1120 Implement lerna publish command voor npm packages ([35934cc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/35934cc))
- **Setup:** MINTY-1120 Add eslint and prettier ([0e6b94c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/0e6b94c))
- **Setup:** MINTY-1120 Add storybook ([1dc9406](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/1dc9406))
- **Setup:** MINTY-1120 Centralize create react app overrides in common package ([3cdc9d7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/3cdc9d7))
- **Setup:** MINTY-1120 Setup monorepo for all apps and packges using lerna and yarn workspaces ([6bd626e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/6bd626e))
