// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const progressBar = document.querySelector('.progress-bar');
const loader = document.querySelector('.loader');
const selectionStep = document.querySelector('.selectionStep');
const nextStepButton = document.querySelector('.nextButton');
const previousStep = document.querySelector('.steps');
const templatesEl = document.querySelector('.templates');
const objectItemsSelection = document.querySelector('.objectItemsSelection');
const currentStepContainer = document.querySelector('.current-step');
const personalDetailsStep = document.querySelector('.personalDetailsStep');
const doneScreen = document.querySelector('.done');
const details = document.querySelector('.details');
const detailsNumber = document.querySelector('.details-block-input.number');
const detailsYear = document.querySelector('.details-block-input.year');
const emailErrorMessage = document.querySelector('.field-explanation');

let inputFields;

const selectedObjects = [];

let initData = {
  token: new URLSearchParams(window.location.search).get('token'),
  year: undefined,
  aanslagnumber: undefined,
  koppelAppObjectionIntegration: undefined,
  koppelAppCaseIntegration: undefined,
  koppelAppCyclomediaIntegration: undefined,
  objects: [],
};

const currencyIntl = new Intl.NumberFormat('nl-NL', {
  style: 'currency',
  currency: 'EUR',
});

const once = fn => {
  if (!once[fn.toString()]) {
    once[fn.toString()] = fn;
    fn();
  }
};

const showError = () => {
  loader.style.animation = 'none';
  alert('Er is iets fout gegaan. Probeer het opnieuw.');
};

const recalculateBar = val => {
  progressBar.children[0].style.width = val * 100 + '%';
};

const showNode = (node, flex = false) => {
  showNode.el && (showNode.el.style.display = 'none');
  showNode.el = node;
  node && (node.style.display = flex ? 'flex' : 'block');
};

const templates = {
  objectItem: templatesEl.children[0],
  objectForm: templatesEl.children[1],
};

const setPreviousStep = text => {
  if (!text) {
    previousStep.style.display = 'none';
  } else {
    previousStep.style.display = 'flex';
    previousStep.children[1].innerText = text;
  }
};

const setNextStep = text => {
  if (!text) {
    nextStepButton.style.display = 'none';
  } else {
    nextStepButton.style.display = 'block';
    nextStepButton.innerText = text;
  }
};

const disableNextStep = () => {
  nextStepButton.setAttribute('disabled', '');
};

const enableNextStep = () => {
  nextStepButton.removeAttribute('disabled');
};

const makeToggleObjectSelectionListener = (object, node) => () => {
  if (selectedObjects.includes(object)) {
    selectedObjects.splice(selectedObjects.indexOf(object), 1);
    node.classList.remove('object-selected');
  } else {
    selectedObjects.push(object);
    node.classList.add('object-selected');
  }

  validateCurrentStep();
};

const validateCurrentStep = () => {
  const nonEmpty = inputFields
    .filter(field => field.offsetParent !== null)
    .every(field =>
      field.type === 'checkbox' ? field.checked : Boolean(field.value)
    );
  const selection = selectedObjects.length !== 0;

  const emailField = inputFields.find(
    field => field.offsetParent !== null && field.name === 'email'
  );

  const email = emailField ? emailRegex.test(emailField.value) : true;

  nonEmpty && email && selection ? enableNextStep() : disableNextStep();
};

const getPayload = () => {
  let values = {};

  inputFields.forEach(field => {
    if (field.name && field.tagName === 'INPUT') {
      values[field.name] = field.value;
    }
  });

  return inputFields
    .filter(
      field =>
        field.tagName === 'TEXTAREA' &&
        field.value &&
        selectedObjects.find(
          object => object.id === field.getAttribute('objId')
        )
    )
    .map(field => ({
      ...initData.objects.find(obj => obj.id === field.getAttribute('objId'))
        .original,
      ...values,
      WOZ_BEZWAAR_OMSCHRIJVING: field.value,
      meta: { name: 'bezwaar' },
    }));
};

const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
const initStep = {
  initialize: () => {
    progressBar.style.backgroundColor = 'transparent';
    showNode(loader);
    setNextStep(null);
    setPreviousStep(null);

    fetch('/api/v1/sysin/interface/get_by_module_name/koppelapp_objections')
      .then(rz => rz.json())
      .then(res => {
        const config = res.result.instance.rows[0].instance.interface_config;
        initData.koppelAppObjectionIntegration = config.endpoint_get;
        initData.koppelAppCaseIntegration = config.endpoint_post;
        initData.koppelAppCyclomediaIntegration = config.endpoint_cyclomedia;

        fetch(initData.koppelAppObjectionIntegration, {
          method: 'POST',
          body: initData.token,
          headers: { 'Content-Type': 'application/json' },
        })
          .then(rz => rz.json())
          .then(res => {
            prepareFormData(res);
          })
          .catch(() => {
            showError();
          })
          .then(() => pushStep(true)());
      })
      .catch(() => showError());
  },
  selection: () => {
    showNode(selectionStep);
    setNextStep('Starten');
    setPreviousStep(null);
    details.style.display = 'flex';
    progressBar.style.backgroundColor = '#E0E0E0';
    recalculateBar(0);
    steps = [
      initStep.selection,
      initStep.objectDetails,
      initStep.contactInfo,
      initStep.finish,
    ];

    once(() => {
      if (initData.objects.length === 1) {
        initData.objects.forEach(object => {
          const node = templates.objectItem.cloneNode(true);
          node.children[2].children[0].innerText = object.street;
          node.children[2].children[1].innerText = object.postal;
          node.children[2].children[2].innerText = object.worth;
          node.classList.add('object-selected');
          selectedObjects.push(object);
          objectItemsSelection.appendChild(node);
          object.itemNode = node;
        });

        selectionStep.children[0].innerText =
          'Met dit formulier kunt u bezwaar maken tegen de WOZ-waarde van het volgende woning:';
      } else {
        initData.objects.forEach(object => {
          const node = templates.objectItem.cloneNode(true);
          node.children[2].children[0].innerText = object.street;
          node.children[2].children[1].innerText = object.postal;
          node.children[2].children[2].innerText = object.worth;

          node.addEventListener(
            'click',
            makeToggleObjectSelectionListener(object, node)
          );
          objectItemsSelection.appendChild(node);
          object.itemNode = node;
        });

        selectionStep.children[0].innerText =
          'Selecteer een of meerdere objecten om te starten met uw bezwaar:';
      }

      initData.objects.forEach(object => {
        fetch(initData.koppelAppCyclomediaIntegration, {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({
            postcode: object.postcode,
            house_number: object.houseNumber,
          }),
        })
          .then(rz => rz.blob())
          .then(blob => {
            const itemImg = object.itemNode.querySelector('.object-image>img');
            const formImg = object.formNode.querySelector('.object-image>img');
            const url = URL.createObjectURL(blob);

            itemImg.src = url;
            formImg.src = url;
          })
          .catch(console.log)
          .then(() => {
            setTimeout(() => {
              document.querySelectorAll('.loader-sm-container').forEach(el => {
                el.remove();
              });
            }, 200);
          });
      });

      detailsYear.innerText = initData.year;
      detailsNumber.innerText = initData.aanslagnumber;

      initData.objects.forEach(object => {
        const node = templates.objectForm.cloneNode(true);
        node.querySelector('.object-details-street').innerText = object.street;
        node.querySelector('.object-details-postal-code').innerText =
          object.postal;
        node.querySelector('.object-details-woz-worth').innerText =
          object.worth;
        node.querySelector('textarea').setAttribute('objId', object.id);

        currentStepContainer.appendChild(node);
        object.formNode = node;
      });

      inputFields = [...document.querySelectorAll('input,textarea')];

      inputFields.forEach(field => {
        field.addEventListener('input', validateCurrentStep);
      });

      const emailField = inputFields.find(field => field.name === 'email');

      emailField.addEventListener('change', () => {
        if (!emailRegex.test(emailField.value)) {
          emailErrorMessage.style.display = 'block';
        }
      });

      emailField.addEventListener('focusin', () => {
        emailErrorMessage.style.display = 'none';
      });

      inputFields.find(field => field.name === 'name').value = initData.name;
    });
    validateCurrentStep();
  },
  objectDetails: forwards => {
    if (forwards) {
      const generatedSteps = selectedObjects.map((object, ix) => {
        return () => {
          const curr = ix + 1;
          const total = selectedObjects.length + 1;
          setNextStep('Volgende stap');
          setPreviousStep(`Stap ${curr} van ${total}`);
          recalculateBar(curr / (total + 1));
          showNode(object.formNode);
          validateCurrentStep();
        };
      });
      steps = [
        initStep.selection,
        initStep.objectDetails,
        ...generatedSteps,
        initStep.contactInfo,
        initStep.formPost,
        initStep.finish,
      ];
      pushStep(true)();
    } else {
      pushStep(false)();
    }
  },
  contactInfo: () => {
    const curr = selectedObjects.length + 1;
    recalculateBar(curr / (curr + 1));
    showNode(personalDetailsStep);
    setNextStep('Bezwaar versturen');
    setPreviousStep(`Stap ${curr} van ${curr}`);
    validateCurrentStep();
  },
  formPost: () => {
    showNode(loader);
    setNextStep(null);
    setPreviousStep(null);
    details.style.display = 'none';
    progressBar.style.backgroundColor = 'transparent';
    progressBar.children[0].style.width = '0';

    Promise.all(
      getPayload().map(payload =>
        fetch(initData.koppelAppCaseIntegration, {
          method: 'POST',
          body: JSON.stringify(payload),
          headers: { 'Content-Type': 'application/json' },
        })
      )
    )
      .then(() => {
        pushStep(true)();
      })
      .catch(showError);
  },
  finish: () => {
    showNode(doneScreen);
    setNextStep(null);
    setPreviousStep(null);
    progressBar.style.backgroundColor = 'transparent';
    progressBar.children[0].style.width = '0';
  },
};

let currentStep;
let steps = [initStep.initialize, initStep.selection];

const pushStep = forwards => () => {
  currentStep = steps[steps.indexOf(currentStep) + (forwards ? 1 : -1)];
  currentStep(forwards);
};

const prepareFormData = data => {
  initData.aanslagnumber = data[0].WOZ_AANSLAG;
  initData.year = data[0].WOZ_AANSLAGJAAR;
  initData.name = data[0].SUBJECT_NAAM;
  initData.objects = data.map(obj => {
    const houseNumber =
      obj.WOZ_HUISNUMMER +
      obj.WOZ_HUISNUMMERLETTER +
      (obj.WOZ_HUISNUMMERTOEVOEGING ? '-' + obj.WOZ_HUISNUMMERTOEVOEGING : '');
    return {
      original: obj,
      id: obj.id,
      worth:
        'WOZ-waarde: ' + currencyIntl.format(obj.WOZ_WAARDE).replace(',00', ''),
      postal: obj.WOZ_POSTCODE + ' ' + obj.WOZ_PLAATS,
      aanslagnumber: obj.WOZ_AANSLAG,
      houseNumber,
      postcode: obj.WOZ_POSTCODE,
      street: obj.WOZ_STRAAT + ' ' + houseNumber,
    };
  });
};

nextStepButton.addEventListener('click', pushStep(true));
previousStep.children[0].addEventListener('click', pushStep(false));
pushStep(true)();
window.pushStep = pushStep;
