# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.9.22](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.21...@zaaksysteem/pip@0.9.22) (2020-12-21)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.21](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.20...@zaaksysteem/pip@0.9.21) (2020-12-18)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.20](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.19...@zaaksysteem/pip@0.9.20) (2020-12-18)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.19](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.18...@zaaksysteem/pip@0.9.19) (2020-12-17)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.18](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.17...@zaaksysteem/pip@0.9.18) (2020-12-10)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.17](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.16...@zaaksysteem/pip@0.9.17) (2020-12-04)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.16](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.15...@zaaksysteem/pip@0.9.16) (2020-11-25)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.15](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.14...@zaaksysteem/pip@0.9.15) (2020-11-20)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.14](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.13...@zaaksysteem/pip@0.9.14) (2020-11-19)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.13](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.12...@zaaksysteem/pip@0.9.13) (2020-11-18)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.12](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.11...@zaaksysteem/pip@0.9.12) (2020-11-11)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.11](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.10...@zaaksysteem/pip@0.9.11) (2020-11-11)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.9...@zaaksysteem/pip@0.9.10) (2020-11-11)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.8...@zaaksysteem/pip@0.9.9) (2020-10-30)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.7...@zaaksysteem/pip@0.9.8) (2020-10-26)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.6...@zaaksysteem/pip@0.9.7) (2020-10-26)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.5...@zaaksysteem/pip@0.9.6) (2020-10-15)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.4...@zaaksysteem/pip@0.9.5) (2020-10-15)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.3...@zaaksysteem/pip@0.9.4) (2020-10-15)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.2...@zaaksysteem/pip@0.9.3) (2020-10-08)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.1...@zaaksysteem/pip@0.9.2) (2020-10-06)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.9.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.9.0...@zaaksysteem/pip@0.9.1) (2020-10-02)

**Note:** Version bump only for package @zaaksysteem/pip

# [0.9.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.8.6...@zaaksysteem/pip@0.9.0) (2020-09-30)

### Features

- **Intake:** MINTY-5066 & MINTY-5069 - change intake behaviour in Pip/Intake ([aaa8bdf](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/aaa8bdf))

## [0.8.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.8.5...@zaaksysteem/pip@0.8.6) (2020-09-30)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.8.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.8.4...@zaaksysteem/pip@0.8.5) (2020-09-29)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.8.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.8.3...@zaaksysteem/pip@0.8.4) (2020-09-25)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.8.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.8.2...@zaaksysteem/pip@0.8.3) (2020-09-23)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.8.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.8.1...@zaaksysteem/pip@0.8.2) (2020-09-18)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.8.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.8.0...@zaaksysteem/pip@0.8.1) (2020-09-17)

**Note:** Version bump only for package @zaaksysteem/pip

# [0.8.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.7.0...@zaaksysteem/pip@0.8.0) (2020-09-17)

### Features

- **PIP:** MINTY-3636 - Make prefilled case uuids visible as read-only ([f2eba03](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f2eba03))

# [0.7.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.78...@zaaksysteem/pip@0.7.0) (2020-09-16)

### Features

- **Main:** MINTY-4759 - add menu items to main menu bar ([8ed4876](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8ed4876))

## [0.6.78](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.77...@zaaksysteem/pip@0.6.78) (2020-09-16)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.77](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.76...@zaaksysteem/pip@0.6.77) (2020-09-15)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.76](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.75...@zaaksysteem/pip@0.6.76) (2020-09-15)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.75](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.74...@zaaksysteem/pip@0.6.75) (2020-09-04)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.74](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.73...@zaaksysteem/pip@0.6.74) (2020-09-03)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.73](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.72...@zaaksysteem/pip@0.6.73) (2020-09-03)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.72](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.71...@zaaksysteem/pip@0.6.72) (2020-09-03)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.71](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.70...@zaaksysteem/pip@0.6.71) (2020-09-03)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.70](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.69...@zaaksysteem/pip@0.6.70) (2020-09-01)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.69](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.68...@zaaksysteem/pip@0.6.69) (2020-09-01)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.68](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.67...@zaaksysteem/pip@0.6.68) (2020-09-01)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.67](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.66...@zaaksysteem/pip@0.6.67) (2020-08-27)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.66](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.65...@zaaksysteem/pip@0.6.66) (2020-08-21)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.65](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.64...@zaaksysteem/pip@0.6.65) (2020-08-20)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.64](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.63...@zaaksysteem/pip@0.6.64) (2020-08-19)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.63](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.62...@zaaksysteem/pip@0.6.63) (2020-08-17)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.62](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.57...@zaaksysteem/pip@0.6.62) (2020-08-11)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.61](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.57...@zaaksysteem/pip@0.6.61) (2020-08-07)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.60](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.57...@zaaksysteem/pip@0.6.60) (2020-08-07)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.59](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.57...@zaaksysteem/pip@0.6.59) (2020-08-06)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.58](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.57...@zaaksysteem/pip@0.6.58) (2020-08-06)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.57](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.56...@zaaksysteem/pip@0.6.57) (2020-08-06)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.56](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.55...@zaaksysteem/pip@0.6.56) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.55](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.54...@zaaksysteem/pip@0.6.55) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.54](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.53...@zaaksysteem/pip@0.6.54) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.53](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.52...@zaaksysteem/pip@0.6.53) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.52](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.51...@zaaksysteem/pip@0.6.52) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.51](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.50...@zaaksysteem/pip@0.6.51) (2020-07-17)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.50](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.49...@zaaksysteem/pip@0.6.50) (2020-07-15)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.49](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.48...@zaaksysteem/pip@0.6.49) (2020-07-15)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.48](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.47...@zaaksysteem/pip@0.6.48) (2020-07-15)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.47](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.46...@zaaksysteem/pip@0.6.47) (2020-07-14)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.46](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.45...@zaaksysteem/pip@0.6.46) (2020-07-09)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.45](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.44...@zaaksysteem/pip@0.6.45) (2020-07-09)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.44](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.43...@zaaksysteem/pip@0.6.44) (2020-07-02)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.43](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.42...@zaaksysteem/pip@0.6.43) (2020-06-30)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.42](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.41...@zaaksysteem/pip@0.6.42) (2020-06-26)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.41](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.40...@zaaksysteem/pip@0.6.41) (2020-06-25)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.40](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.39...@zaaksysteem/pip@0.6.40) (2020-06-24)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.39](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.38...@zaaksysteem/pip@0.6.39) (2020-06-23)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.38](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.37...@zaaksysteem/pip@0.6.38) (2020-06-18)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.37](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.36...@zaaksysteem/pip@0.6.37) (2020-06-17)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.36](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.35...@zaaksysteem/pip@0.6.36) (2020-06-12)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.35](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.34...@zaaksysteem/pip@0.6.35) (2020-06-11)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.34](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.33...@zaaksysteem/pip@0.6.34) (2020-06-11)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.33](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.32...@zaaksysteem/pip@0.6.33) (2020-06-09)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.32](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.31...@zaaksysteem/pip@0.6.32) (2020-06-05)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.31](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.30...@zaaksysteem/pip@0.6.31) (2020-06-05)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.30](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.29...@zaaksysteem/pip@0.6.30) (2020-06-03)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.29](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.28...@zaaksysteem/pip@0.6.29) (2020-05-29)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.28](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.27...@zaaksysteem/pip@0.6.28) (2020-05-28)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.27](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.26...@zaaksysteem/pip@0.6.27) (2020-05-27)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.26](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.25...@zaaksysteem/pip@0.6.26) (2020-05-19)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.25](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.24...@zaaksysteem/pip@0.6.25) (2020-05-14)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.24](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.23...@zaaksysteem/pip@0.6.24) (2020-05-14)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.23](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.22...@zaaksysteem/pip@0.6.23) (2020-05-13)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.22](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.21...@zaaksysteem/pip@0.6.22) (2020-05-12)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.21](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.20...@zaaksysteem/pip@0.6.21) (2020-05-12)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.20](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.19...@zaaksysteem/pip@0.6.20) (2020-05-12)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.19](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.18...@zaaksysteem/pip@0.6.19) (2020-05-11)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.18](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.17...@zaaksysteem/pip@0.6.18) (2020-05-01)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.17](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.16...@zaaksysteem/pip@0.6.17) (2020-05-01)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.16](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.15...@zaaksysteem/pip@0.6.16) (2020-04-30)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.15](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.14...@zaaksysteem/pip@0.6.15) (2020-04-30)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.14](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.13...@zaaksysteem/pip@0.6.14) (2020-04-30)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.13](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.12...@zaaksysteem/pip@0.6.13) (2020-04-28)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.12](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.11...@zaaksysteem/pip@0.6.12) (2020-04-28)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.11](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.10...@zaaksysteem/pip@0.6.11) (2020-04-20)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.9...@zaaksysteem/pip@0.6.10) (2020-04-16)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.8...@zaaksysteem/pip@0.6.9) (2020-04-16)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.7...@zaaksysteem/pip@0.6.8) (2020-04-16)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.6...@zaaksysteem/pip@0.6.7) (2020-04-16)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.5...@zaaksysteem/pip@0.6.6) (2020-04-14)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.4...@zaaksysteem/pip@0.6.5) (2020-04-10)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.3...@zaaksysteem/pip@0.6.4) (2020-04-09)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.2...@zaaksysteem/pip@0.6.3) (2020-04-07)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.1...@zaaksysteem/pip@0.6.2) (2020-04-06)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.6.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.0...@zaaksysteem/pip@0.6.1) (2020-04-03)

### Bug Fixes

- **PipDocuments:** MINTY-3619 - fix PipDocuments not showing all rows ([67cf987](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/67cf987))

# [0.6.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.11...@zaaksysteem/pip@0.6.0) (2020-04-02)

### Features

- **Communication:** MINTY-3432 - add attachment PDF preview ([5bebd92](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5bebd92))

## [0.5.11](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.10...@zaaksysteem/pip@0.5.11) (2020-04-01)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.5.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.9...@zaaksysteem/pip@0.5.10) (2020-04-01)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.5.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.8...@zaaksysteem/pip@0.5.9) (2020-04-01)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.5.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.7...@zaaksysteem/pip@0.5.8) (2020-03-31)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.5.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.6...@zaaksysteem/pip@0.5.7) (2020-03-31)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.5.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.5...@zaaksysteem/pip@0.5.6) (2020-03-30)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.5.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.4...@zaaksysteem/pip@0.5.5) (2020-03-30)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.5.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.3...@zaaksysteem/pip@0.5.4) (2020-03-30)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.5.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.2...@zaaksysteem/pip@0.5.3) (2020-03-27)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.5.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.1...@zaaksysteem/pip@0.5.2) (2020-03-20)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.5.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.0...@zaaksysteem/pip@0.5.1) (2020-03-19)

**Note:** Version bump only for package @zaaksysteem/pip

# [0.5.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.4.2...@zaaksysteem/pip@0.5.0) (2020-03-19)

### Features

- **DocumentExplorer:** MINTY-3300 - add initial PDF preview features, misc. ([a131814](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a131814))
- **DocumentExplorer:** MINTY-3300 - change Loader, exports ([b25d770](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b25d770))
- **DocumentExplorer:** MINTY-3300 - split up DocumentExplorer functionality, create new Intake entrypoint, add Selectable state to table ([5bae0d3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5bae0d3))
- **Intake:** MINTY-3437 - add search component ([2bb171b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2bb171b))

## [0.4.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.4.1...@zaaksysteem/pip@0.4.2) (2020-03-17)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.4.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.4.0...@zaaksysteem/pip@0.4.1) (2020-03-17)

**Note:** Version bump only for package @zaaksysteem/pip

# [0.4.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.23...@zaaksysteem/pip@0.4.0) (2020-03-05)

### Bug Fixes

- **CaseDocuments:** apply MR comments ([778fd18](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/778fd18))

### Features

- **CaseDocuments:** add misc features ([8845760](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8845760))
- **CaseDocuments:** MINTY-3043, MINTY-3045: get documents from server, implement breadcrumb, error handling ([9519313](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9519313))
- **CaseDocuments:** MINTY-3150 - show file status if not accepted ([7425526](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7425526))
- **CaseDocuments:** MINTY-3191 - add misc. UX tweaks, code layout ([11658c1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/11658c1))
- **CaseDocuments:** MINTY-3254 - set canUpload capability according to query parameter ([b98bc1e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b98bc1e))
- **Documents:** MINTY-2861 - add initial CaseDocuments ([237e411](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/237e411))

## [0.3.23](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.22...@zaaksysteem/pip@0.3.23) (2020-03-03)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.22](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.21...@zaaksysteem/pip@0.3.22) (2020-02-24)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.21](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.20...@zaaksysteem/pip@0.3.21) (2020-02-21)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.20](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.19...@zaaksysteem/pip@0.3.20) (2020-02-17)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.19](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.18...@zaaksysteem/pip@0.3.19) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.18](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.17...@zaaksysteem/pip@0.3.18) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.17](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.16...@zaaksysteem/pip@0.3.17) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.16](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.15...@zaaksysteem/pip@0.3.16) (2020-02-10)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.15](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.14...@zaaksysteem/pip@0.3.15) (2020-02-07)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.14](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.13...@zaaksysteem/pip@0.3.14) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.13](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.12...@zaaksysteem/pip@0.3.13) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.12](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.11...@zaaksysteem/pip@0.3.12) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.11](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.10...@zaaksysteem/pip@0.3.11) (2020-01-30)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.9...@zaaksysteem/pip@0.3.10) (2020-01-30)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.8...@zaaksysteem/pip@0.3.9) (2020-01-23)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.7...@zaaksysteem/pip@0.3.8) (2020-01-23)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.6...@zaaksysteem/pip@0.3.7) (2020-01-22)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.5...@zaaksysteem/pip@0.3.6) (2020-01-14)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.3...@zaaksysteem/pip@0.3.5) (2020-01-14)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.3...@zaaksysteem/pip@0.3.4) (2020-01-13)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.2...@zaaksysteem/pip@0.3.3) (2020-01-13)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.1...@zaaksysteem/pip@0.3.2) (2020-01-10)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.0...@zaaksysteem/pip@0.3.1) (2020-01-09)

**Note:** Version bump only for package @zaaksysteem/pip

# [0.3.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.2.6...@zaaksysteem/pip@0.3.0) (2020-01-09)

### Bug Fixes

- **Communication:** MINTY-2262 - allow e-mail addresses over multiple lines ([db44093](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/db44093))
- **UI:** Fix aria-labels on snackbar and dialogs ([775a287](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/775a287))

### Features

- **Communication:** MINTY-1798 Add `canImportMessage` capability ([c10c88e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c10c88e))
- **Communication:** MINTY-1925 Make deleting a message controllable via a capability ([704ee17](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/704ee17))
- **Communication:** MINTY-2034 Call `/get_thread_list` with correct params according to context of communication-module ([7fef0f2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7fef0f2))
- **i18n:** Move common locale namespace to `@zaaksysteem/common` package ([4a36e92](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4a36e92))
- **Message:** Allow source files of messages to be added to the case documents ([6819f60](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6819f60))
- **Thread:** Allow threads to be linked to cases from main view ([44a8a51](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/44a8a51))

## [0.2.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.2.5...@zaaksysteem/pip@0.2.6) (2019-12-19)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.2.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.2.4...@zaaksysteem/pip@0.2.5) (2019-11-21)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.2.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.2.3...@zaaksysteem/pip@0.2.4) (2019-10-29)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.2.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.2.2...@zaaksysteem/pip@0.2.3) (2019-10-25)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.2.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.2.1...@zaaksysteem/pip@0.2.2) (2019-10-22)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.2.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.2.0...@zaaksysteem/pip@0.2.1) (2019-10-17)

**Note:** Version bump only for package @zaaksysteem/pip

# 0.2.0 (2019-10-17)

### Features

- **Snackbar:** MINTY-1788 Implement snackbar in pip and main app ([dcbe4da](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/dcbe4da))
