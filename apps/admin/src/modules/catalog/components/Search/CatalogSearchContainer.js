// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Search from '../../../../components/Search/Search';
import {
  doCatalogSearch,
  doExitSearch,
} from '../../store/search/search.actions';

const CatalogSearchContainer = ({ query, onSearch, onClear, t }) => (
  <Search
    query={query || ''}
    onSearch={onSearch}
    onClear={onClear}
    placeholder={t('catalog:search')}
    scope="catalog-search"
  />
);

const mapStateToProps = state => {
  if (!state.catalog) {
    return {
      query: '',
    };
  }

  const {
    catalog: {
      search: { query },
    },
  } = state;
  return {
    query,
  };
};

const mapDispatchToProps = dispatch => ({
  onSearch: query => dispatch(doCatalogSearch(query)),
  exitSearch: bindActionCreators(doExitSearch, dispatch),
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { query } = stateProps;
  const { exitSearch } = dispatchProps;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onClear: query ? exitSearch : null,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(CatalogSearchContainer);
