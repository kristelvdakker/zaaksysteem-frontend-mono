// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { get } from '@mintlab/kitchen-sink/source';
import Detail from '../Detail/Detail';
import { DateTime, Default, NavigateTo } from '../Values';
import Group from '../Group/Group';
import CopyToClipboard from '../Action/CopyToClipboard';
import OpenLink from '../Action/OpenLink';
import { getPathToItem } from '../../../library/pathGetters';

const parseMagicString = string => `[[${string}]]`;
const domain = get(window, 'top.location.origin', '');

/* eslint complexity: [2, 17] */
const Details = ({
  details: {
    lastModified,
    locationFolder,
    magicString,
    identification,
    valueType,
    uuid,
    versionUuid,
    internalUrl,
    caseTypeApi,
    used_in_case_types,
    enclosedDocument,
    status,
  },
  t,
  doNavigate,
  type,
}) => (
  <React.Fragment>
    {lastModified && (
      <Detail title={t('catalog:detailView.lastModified')} icon="access_time">
        <DateTime value={lastModified.value} />
      </Detail>
    )}
    <Detail icon="folder">
      <NavigateTo
        value={getPathToItem(locationFolder.value) || '/'}
        title={locationFolder.title || t('catalog:detailView.rootFolder')}
        doNavigate={doNavigate}
      />
    </Detail>
    {magicString && (
      <Detail title={t('catalog:detailView.magicString')} icon="photo_filter">
        <Default value={parseMagicString(magicString.value)} />
        <CopyToClipboard value={parseMagicString(magicString.value)} t={t} />
      </Detail>
    )}
    {identification && (
      <Detail title={t('catalog:detailView.identification')} icon="fingerprint">
        <Default value={identification.value} />
      </Detail>
    )}
    {valueType && (
      <Detail title={t('catalog:detailView.valueType')} icon="view_quilt">
        <Default
          value={t(
            `attribute:fields.attribute_type.choices:${valueType.value}`
          )}
        />
      </Detail>
    )}
    {enclosedDocument && (
      <Detail title={t('catalog:detailView.enclosedDocument')} icon="save_alt">
        <Default value={enclosedDocument.value} />
        <OpenLink
          icon="save_alt"
          title={t('catalog:detailView.saveFile')}
          link={`${domain}${enclosedDocument.link}`}
        />
      </Detail>
    )}
    {type === 'custom_object_type' && (
      <React.Fragment>
        <Detail title={t('catalog:detailView.status')} icon="verified_user">
          <Default
            value={
              status.value.toLowerCase() === 'active'
                ? t('catalog:detailView.active')
                : t('catalog:detailView.concept')
            }
          />
        </Detail>
      </React.Fragment>
    )}
    {(type === 'case_type' ||
      type === 'object_type' ||
      type === 'custom_object_type') && (
      <React.Fragment>
        <Detail title={t('catalog:detailView.uuid')} icon="code">
          <Default value={uuid.value} />
          <CopyToClipboard value={uuid.value} t={t} />
        </Detail>
      </React.Fragment>
    )}
    {type === 'custom_object_type' && (
      <React.Fragment>
        <Detail title={t('catalog:detailView.versionUuid')} icon="code">
          <Default value={versionUuid.value} />
          <CopyToClipboard value={versionUuid.value} t={t} />
        </Detail>
      </React.Fragment>
    )}
    {type === 'case_type' && (
      <React.Fragment>
        <Group title={t('catalog:detailView.directLinksTitle')}>
          {internalUrl && (
            <Detail icon="link" offset={false}>
              <NavigateTo
                value={`${domain}${internalUrl.value}`}
                title={t('catalog:detailView.internalUrl')}
                doNavigate={doNavigate}
                internal={false}
              />
              <CopyToClipboard value={`${domain}${internalUrl.value}`} t={t} />
            </Detail>
          )}
          <Detail icon="link" offset={false}>
            <NavigateTo
              value={`${domain}${caseTypeApi.value}`}
              title={t('catalog:detailView.caseTypeApi')}
              doNavigate={doNavigate}
              internal={false}
            />
            <CopyToClipboard value={`${domain}${caseTypeApi.value}`} t={t} />
          </Detail>
        </Group>
      </React.Fragment>
    )}
    {used_in_case_types && (
      <Group title={t('catalog:detailView.relationsTitle')}>
        <Detail icon="ballot" truncate={false}>
          <Default value={used_in_case_types.value} />
        </Detail>
      </Group>
    )}
  </React.Fragment>
);
export default Details;
