// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { FormFieldComponentType } from '@zaaksysteem/common/src/components/form/types/Form2.types';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { TextField } from '@mintlab/ui/App/Material/TextField';
import { generateMagicString } from './MagicStringGenerator.library';

export const MAGIC_STRING_GENERATOR = 'msg';

export const MagicStringGenerator: FormFieldComponentType<
  string,
  { rawName?: string }
> = props => {
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const rawName = props?.config?.rawName;

  React.useEffect(() => {
    !props.disabled &&
      rawName &&
      generateMagicString(rawName, openServerErrorDialog).then(ms => {
        props.setFieldValue(props.name, ms, false);
      });
  }, [rawName]);

  return (
    <React.Fragment>
      <TextField {...props} />
      {ServerErrorDialog}
    </React.Fragment>
  );
};
