// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { request } from '@zaaksysteem/common/src/library/request/request';

export const fetchLocationChoices = (
  onError: OpenServerErrorDialogType
) => async (uuid: string) => {
  const body = await request<{
    result: { instance: { data: { id: string; label: string }[] } };
  }>('GET', `/api/v1/sysin/interface/${uuid}/trigger/get_location_list`).catch(
    err => void setTimeout(() => onError(err), 1500)
  );
  return body
    ? body.result.instance.data.map(location => ({
        value: location.id,
        label: location.label,
      }))
    : [];
};
