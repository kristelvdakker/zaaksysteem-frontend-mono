// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICatalog } from '@zaaksysteem/generated/types/APICatalog.types';
import { AJAX_STATE_PENDING } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { normalizeValue } from '@zaaksysteem/common/src/library/normalizeValue';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';

export const defaultInitialValues = {
  attribute_type: null,
  name: '',
  public_name: '',
  magic_string: '',
  type_multiple: false,
  sensitive_field: false,
  help: '',
  value_default: '',
  document_origin: null,
  document_trust_level: null,
  document_category: null,
  relationship_data: { type: null, name: null, uuid: null },
  relationship_type: null,
  object_type: null,
  attribute_values: [],
  appointment_location_id: null,
  appointment_product_id: null,
  appointment_interface_uuid: '',
  commit_message: 'Nieuw aangemaakt',
};

/* eslint complexity: [2, 8] */
/* eslint-disable getter-return */
export const mangleValuesForSave = ({
  values,
  id,
  currentFolderUUID,
  appointment_interface_uuid,
}: {
  values: any;
  id: string | null;
  currentFolderUUID: string | null;
  appointment_interface_uuid: string | undefined;
}) => {
  const isNew = !id;
  const attribute_uuid = isNew ? v4() : id;
  const mangledValues = {} as any;

  Object.keys(values).forEach(key => {
    switch (key) {
      case 'name':
        mangledValues[key] = values[key];
        break;
      case 'document_category':
      case 'document_trust_level':
      case 'document_origin':
        mangledValues[key] = normalizeValue(values[key]);
        break;
      case 'attribute_values':
        mangledValues[key] = ((values.attribute_values as any[]) || []).map(
          (item, index) => ({
            ...cloneWithout(item, 'id', 'isNew'),
            sort_order: index,
          })
        );
        break;
      default:
        mangledValues[key] = values[key];
    }
  }, {});

  mangledValues.category_uuid = currentFolderUUID;
  mangledValues.appointment_interface_uuid = appointment_interface_uuid || null;

  //add relationship data
  if (mangledValues.relationship_type) {
    mangledValues.relationship_data = {
      type: normalizeValue(mangledValues.relationship_type),
    };

    if (mangledValues.object_type) {
      mangledValues.relationship_data.name = mangledValues.object_type.label;
      mangledValues.relationship_data.uuid = mangledValues.object_type.value;
    }
  }

  return {
    fields: mangledValues,
    attribute_uuid,
    isNew,
  };
};

export const attributeFormSelector = (state: {
  catalog: {
    attribute: {
      id: string | null;
      values: any;
      appointment_interface_uuid: string | undefined;
      state: string;
      savingState: string;
    };
    items: { currentFolderUUID: string | null };
  };
}) => {
  const values = state.catalog.attribute.values;
  const parsedValues = values
    ? {
        ...values,
        ...(values?.relationship_data?.type && {
          relationship_type: values?.relationship_data?.type,
        }),
        ...(values?.relationship_data?.uuid && {
          object_type: {
            label: values.relationship_data.name,
            value: values.relationship_data.uuid,
          },
        }),
      }
    : null;

  if (parsedValues?.relationship_data) delete parsedValues.relationship_data;

  return {
    editedAttributeUuid: state.catalog.attribute.id,
    editedAttributeInitialValues: parsedValues,
    attributeSaveDestinationFolder: state.catalog.items.currentFolderUUID,
    loading: state.catalog.attribute.state === AJAX_STATE_PENDING,
    saving: state.catalog.attribute.savingState === AJAX_STATE_PENDING,
  };
};

export const fetchAppointmentIntegrationId = async (
  onError: OpenServerErrorDialogType
) => {
  const body = await request<APICatalog.GetActiveAppointmentIntegrationsResponseBody>(
    'GET',
    '/api/v2/admin/integrations/get_active_appointment_integrations',
    null,
    { type: 'json', cached: true }
  ).catch(onError);

  return body && body.data && body.data[0]
    ? (body.data[0].id as string | undefined)
    : '';
};
