// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Button from '@mintlab/ui/App/Material/Button';
import Render from '@mintlab/ui/App/Abstract/Render';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import DropdownMenu, {
  DropdownMenuList,
} from '@mintlab/ui/App/Zaaksysteem/DropdownMenu';
import { withStyles } from '@material-ui/styles';
import { buttonBarStyleSheet } from './ButtonBar.style';

/**
 * @reactProps {Object} classes
 * @reactProps {Array} actionButtons
 * @reactProps {Array} advancedActionButtons
 * @reactProps {Array} permanentButtons
 * @return {ReactElement}
 */
const ButtonBar = ({
  classes,
  actionButtons,
  advancedActionButtons,
  permanentButtons,
}) => (
  <div className={classes.wrapper}>
    <Render condition={actionButtons.length || advancedActionButtons.length}>
      <div className={classes.segment}>
        <Render condition={actionButtons.length}>
          {actionButtons.map(({ action, active, tooltip, type }) => (
            <Tooltip key={type} title={tooltip} placement="bottom">
              <Button
                action={action}
                presets={['icon', ...(active ? ['primary'] : [])]}
                scope={`catalog-header:button-bar:${type}`}
              >
                {type}
              </Button>
            </Tooltip>
          ))}
        </Render>
        <Render condition={advancedActionButtons.length}>
          <DropdownMenu
            transformOrigin={{
              vertical: -50,
              horizontal: 'center',
            }}
            trigger={
              <Button
                presets={['icon']}
                scope={`catalog-header:button-bar:advanced`}
              >
                more_vert
              </Button>
            }
          >
            <DropdownMenuList
              items={advancedActionButtons.map(({ action, title }) => ({
                action,
                label: title,
                scope: `catalog-header:button-bar:${title}`,
              }))}
            />
          </DropdownMenu>
        </Render>
      </div>
    </Render>
    <div className={classes.segment}>
      {permanentButtons.map(({ action, active, tooltip, type }) => (
        <Tooltip key={type} title={tooltip} placement="bottom">
          <Button
            action={action}
            presets={['icon', ...(active ? ['primary'] : [])]}
            scope={`catalog-header:button-bar:${type}`}
          >
            {type}
          </Button>
        </Tooltip>
      ))}
    </div>
  </div>
);

export default withStyles(buttonBarStyleSheet)(ButtonBar);
