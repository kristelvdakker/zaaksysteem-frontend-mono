// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import Button from '@mintlab/ui/App/Material/Button';
import { useListStyle } from '@mintlab/ui/App/Zaaksysteem/List';
import { useSubForm } from '@zaaksysteem/common/src/components/form/hooks/useSubForm';
import {
  Rule,
  setFieldValue,
  hideFields,
  showFields,
  hasValue,
  transferValueAsConfig,
} from '@zaaksysteem/common/src/components/form/rules';
import {
  FormFieldPropsType,
  FormRendererFormField,
} from '@zaaksysteem/common/src/components/form/types/fieldComponent.types';
import * as i18next from 'i18next';
import getFormDefinition from './getFormDefinition';
import { useAuthorizationFormStyle } from './Authorization.style';

const AuthorizationForm: React.ComponentType<
  FormFieldPropsType & {
    t: i18next.TFunction;
    remove: () => void;
  }
> = ({ t, formik, name, registerValidation, remove, validateForm }) => {
  const classes = useAuthorizationFormStyle();
  const listClasses = useListStyle();

  const rules = [
    new Rule()
      .when('department', hasValue)
      .then(showFields(['role']))
      .else(hideFields(['role']))
      .and(setFieldValue('role', null as any)),
    new Rule()
      .when('role', hasValue)
      .then(showFields(['permission']))
      .else(hideFields(['permission'])),
    new Rule()
      .when(() => true)
      .then(transferValueAsConfig('department', 'role', 'parentRoleUuid')),
  ];

  const { fields } = useSubForm({
    namespace: name,
    formDefinition: getFormDefinition({ t }),
    formik,
    registerValidation,
    rules,
    validateForm,
  });

  const renderFormField = (field: FormRendererFormField) => {
    const {
      FieldComponent,
      name,
      label,
      applyBackgroundColor,
      error,
      touched,
      ...rest
    } = field;
    const { required, help, hint } = rest;

    return (
      <FormControlWrapper
        label={label}
        help={help}
        hint={hint}
        error={error}
        touched={touched}
        required={required}
        applyBackgroundColor={applyBackgroundColor}
        key={`authorization-field-component-${name}`}
      >
        <FieldComponent {...field} />
      </FormControlWrapper>
    );
  };

  return (
    <div className={classNames(listClasses.itemContainer, classes.wrapper)}>
      <div className={classes.fields}>
        {fields.map(field => renderFormField(field))}
      </div>
      <Button
        className={listClasses.removeButton}
        presets={['icon', 'small']}
        action={remove}
      >
        close
      </Button>
    </div>
  );
};

export default AuthorizationForm;
