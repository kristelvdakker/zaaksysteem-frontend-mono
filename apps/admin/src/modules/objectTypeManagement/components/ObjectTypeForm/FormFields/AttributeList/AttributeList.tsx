// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
//@ts-ignore
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  SortableList,
  useListStyle,
  listSelectStyleSheet,
} from '@mintlab/ui/App/Zaaksysteem/List';
import { useMultiValueField } from '@zaaksysteem/common/src/components/form/hooks/useMultiValueField';
import { FormRendererFormField } from '@zaaksysteem/common/src/components/form/types/fieldComponent.types';
import { fetchAttributeChoices } from './fetchAttributeChoices';
import { AttributeForm } from './AttributeForm';
import { AttributeItemType } from './types';

const extractSelectedValue = (event: any) => event.target.value.value;

export const AttributeList: React.ComponentType<
  FormRendererFormField<any, any, AttributeItemType[]>
> = props => {
  const { setFieldValue, name, placeholder } = props;
  const [input, setInput] = React.useState('');
  const classes = useListStyle();
  const { fields, add } = useMultiValueField<any, AttributeItemType>(props);
  const value = fields.map(field => field.value);

  return (
    <div className={classes.listContainer}>
      <SortableList
        value={fields.map(field => ({ ...field, id: field.value.id }))}
        onReorder={reorderedFields =>
          setFieldValue(
            name,
            reorderedFields.map(field => field.value)
          )
        }
        renderItem={field => (
          <AttributeForm {...field} name={field.name as any} />
        )}
      />
      <DataProvider
        autoProvide={input !== ''}
        provider={fetchAttributeChoices}
        providerArguments={[input]}
      >
        {({ data, busy }) => {
          return (
            <Select
              name={name}
              generic={true}
              loading={busy}
              onChange={event => add(extractSelectedValue(event))}
              startAdornment={<Icon size="small">{iconNames.search}</Icon>}
              isMulti={false}
              value={null}
              isClearable={false}
              choices={
                input.length > 2 && data
                  ? data.filter(option =>
                      value.every(item => item.id !== option.value.id)
                    )
                  : []
              }
              getChoices={setInput}
              createStyleSheet={listSelectStyleSheet}
              placeholder={placeholder}
            />
          );
        }}
      </DataProvider>
    </div>
  );
};
