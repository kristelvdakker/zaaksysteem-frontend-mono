// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { AttributeItemType } from './FormFields/AttributeList/types';
import { AuthorizationType } from './FormFields/AuthorizationList/types';

export type ObjectTypeFormShapeType = {
  uuid?: string;
  name: string;
  title: string;
  external_reference: string;
  components_changed: string[];
  changes: string;
  custom_fields: AttributeItemType[];
  authorizations: AuthorizationType[];
  caseTypeRelations: { id: string; name: string }[];
  objectTypeRelations: { id: string; name: string }[];
};
