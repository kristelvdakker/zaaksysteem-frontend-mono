// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { FormDefinitionStep } from '@zaaksysteem/common/src/components/form/types';
import { CASETYPE_RELATION_LIST } from '../../../../../../components/Form/Constants/fieldTypes';
import { OBJECTTYPE_RELATION_LIST } from '../../../../../../components/Form/Constants/fieldTypes';
import { ObjectTypeFormShapeType } from '../../ObjectTypeForm.types';

export function getRelationsStep(
  t: i18next.TFunction
): FormDefinitionStep<ObjectTypeFormShapeType> {
  return {
    title: t('objectTypeManagement:form.steps.relations.title'),
    fields: [
      {
        name: 'caseTypeRelations',
        type: CASETYPE_RELATION_LIST,
        value: [],
        multiValue: true,
        label: t('objectTypeManagement:form.fields.caseTypeRelations.label'),
        help: t('objectTypeManagement:form.fields.caseTypeRelations.help'),
        placeholder: t(
          'objectTypeManagement:form.fields.custom_fields.placeholder'
        ),
      },
      {
        name: 'objectTypeRelations',
        type: OBJECTTYPE_RELATION_LIST,
        value: [],
        multiValue: true,
        label: t('objectTypeManagement:form.fields.objectTypeRelations.label'),
        help: t('objectTypeManagement:form.fields.objectTypeRelations.help'),
        placeholder: t(
          'objectTypeManagement:form.fields.custom_fields.placeholder'
        ),
      },
    ],
  };
}
