// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { TEXT } from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { FormDefinitionStep } from '@zaaksysteem/common/src/components/form/types';
import { ObjectTypeFormShapeType } from '../../ObjectTypeForm.types';

export function getGeneralStep(
  t: i18next.TFunction
): FormDefinitionStep<ObjectTypeFormShapeType> {
  return {
    title: t('objectTypeManagement:form.steps.general.title'),
    description: t('objectTypeManagement:form.steps.general.description'),
    fields: [
      {
        name: 'name',
        type: TEXT,
        value: '',
        required: true,
        label: t('objectTypeManagement:form.fields.name.label'),
        help: t('objectTypeManagement:form.fields.name.help'),
        placeholder: t('objectTypeManagement:form.fields.name.placeholder'),
      },
      {
        name: 'title',
        type: TEXT,
        value: '',
        required: true,
        label: t('objectTypeManagement:form.fields.title.label'),
        help: t('objectTypeManagement:form.fields.title.help'),
        placeholder: t('objectTypeManagement:form.fields.title.placeholder'),
      },
      {
        name: 'external_reference',
        type: TEXT,
        value: '',
        label: t('objectTypeManagement:form.fields.external_reference.label'),
        help: t('objectTypeManagement:form.fields.external_reference.help'),
        placeholder: t(
          'objectTypeManagement:form.fields.external_reference.placeholder'
        ),
      },
    ],
  };
}
