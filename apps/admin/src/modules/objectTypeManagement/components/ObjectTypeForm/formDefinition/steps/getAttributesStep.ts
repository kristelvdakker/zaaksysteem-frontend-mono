// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { FormDefinitionStep } from '@zaaksysteem/common/src/components/form/types';
import { ATTRIBUTE_LIST } from '../../../../../../components/Form/Constants/fieldTypes';
import { ObjectTypeFormShapeType } from '../../ObjectTypeForm.types';

export function getAttributesStep(
  t: i18next.TFunction
): FormDefinitionStep<ObjectTypeFormShapeType> {
  return {
    title: t('objectTypeManagement:form.steps.attributes.title'),
    fields: [
      {
        name: 'custom_fields',
        type: ATTRIBUTE_LIST,
        value: [],
        required: false,
        multiValue: true,
        placeholder: t(
          'objectTypeManagement:form.fields.custom_fields.placeholder'
        ),
      },
    ],
  };
}
