// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated';
import { AuthorizationType } from '../components/ObjectTypeForm/FormFields/AuthorizationList/types';

type ObjectTypeAttributes = APICaseManagement.CustomObject['attributes'];

export interface ObjectTypeType {
  uuid: ObjectTypeAttributes['uuid'];
  title: ObjectTypeAttributes['title'];
  name: ObjectTypeAttributes['name'];
  status: ObjectTypeAttributes['status'];
  custom_field_definition?: ObjectTypeAttributes['custom_field_definition'];
  objectTypeRelations?: { id: string; name: string }[];
  caseTypeRelations?: { id: string; name: string }[];
  authorizations?: AuthorizationType[];
  audit_log?: { updated_components: string[]; description: string };
  external_reference: string;
}
