// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { getObjectTypeManagementModule } from './store/objectTypeManagement.module';
import ObjecTypeManagement from './components/ObjectTypeManagement';
import locale from './locale/objectTypeManagement.locale';
import { ObjectTypeManagementRouteType } from './types/Route.types';

const ObjectTypeManagementModule: React.ComponentType<ObjectTypeManagementRouteType> = ({
  segments,
  params,
}) => {
  const [type, uuid] = segments;

  return (
    <DynamicModuleLoader modules={[getObjectTypeManagementModule()]}>
      <I18nResourceBundle resource={locale} namespace="objectTypeManagement">
        <ObjecTypeManagement
          type={type}
          uuid={uuid}
          folderUuid={params.folder_uuid}
        />
      </I18nResourceBundle>
    </DynamicModuleLoader>
  );
};

export default ObjectTypeManagementModule;
