// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { APICaseManagement } from '@zaaksysteem/generated';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { ObjectTypeFormShapeType } from '../../components/ObjectTypeForm/ObjectTypeForm.types';
import { createDataFromPayload } from '../../library';
import { OBJECT_TYPE_UPDATE } from './update.constants';

const updateObjectTypeAjaxAction = createAjaxAction(OBJECT_TYPE_UPDATE);

export type UpdateObjectTypePayloadType = ObjectTypeFormShapeType & {
  folderUuid?: string;
  uuid: string;
};

export const updateObjectTypeAction = (
  payload: UpdateObjectTypePayloadType
) => {
  const url = buildUrl<APICaseManagement.UpdateCustomObjectTypeRequestParams>(
    '/api/v2/cm/custom_object_type/update_custom_object_type',
    {
      uuid: payload.uuid,
    }
  );

  const data = createDataFromPayload(payload);

  return updateObjectTypeAjaxAction({
    url,
    method: 'POST',
    data,
    payload,
  });
};
