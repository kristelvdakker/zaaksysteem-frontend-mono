// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { combineReducers } from 'redux';
import objectType, {
  ObjectTypeStateType,
} from './objectType/objectType.reducer';
import { create, CreateStateType } from './create/create.reducer';
import { update, UpdateStateType } from './update/update.reducer';
import {
  unsavedChanges,
  UnsavedChangesStateType,
} from './unsavedChanges/unsavedChanges.reducer';

interface ObjectTypeManagementStateType {
  objectType: ObjectTypeStateType;
  create: CreateStateType;
  update: UpdateStateType;
  unsavedChanges: UnsavedChangesStateType;
}

export interface ObjectTypeManagementRootStateType {
  objectTypeManagement: ObjectTypeManagementStateType;
}

export const objectTypeManagement = combineReducers<ObjectTypeManagementStateType>(
  {
    objectType,
    create,
    update,
    unsavedChanges,
  }
);

export default objectTypeManagement;
