// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Reducer } from 'redux';
import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { handleAjaxStateChange } from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { APICaseManagement } from '@zaaksysteem/generated';
import { AuthorizationType } from '../../components/ObjectTypeForm/FormFields/AuthorizationList/types';
import { ObjectTypeType } from '../../types/ObjectType.types';
import { OBJECT_TYPE_FETCH } from './objectType.constants';

export interface ObjectTypeStateType {
  state: AjaxState;
  objectType?: ObjectTypeType;
}

const initialState: ObjectTypeStateType = {
  state: AJAX_STATE_INIT,
};

const mapCustomFields = (
  custom_field_definition: APICaseManagement.DefCustomFieldDefinition
) => {
  if (!custom_field_definition.custom_fields) return custom_field_definition;

  return {
    ...custom_field_definition,
    custom_fields: custom_field_definition.custom_fields.map(thisField => ({
      ...thisField,
      id: thisField.attribute_uuid,
    })),
  };
};

const mapAuthorizations = (
  authorizations: APICaseManagement.CustomObjectTypeAuthorizationDefinition['authorizations']
): AuthorizationType[] => {
  if (!authorizations) return [];

  return authorizations.map(thisAuthorization => {
    return {
      department: thisAuthorization.department?.uuid || null,
      role: thisAuthorization.role.uuid,
      permission: thisAuthorization.authorization,
    };
  });
};

const mapRelationships = (
  relationships: APICaseManagement.CustomObjectTypeRelationshipDefinition
) => {
  const objectTypeRelations: any[] = [];
  const caseTypeRelations: any[] = [];

  relationships?.relationships?.forEach(rel => {
    const { custom_object_type_uuid, case_type_uuid, name } = rel;
    if (custom_object_type_uuid) {
      objectTypeRelations.push({ id: custom_object_type_uuid, name });
    } else {
      caseTypeRelations.push({ id: case_type_uuid, name });
    }
  });

  return { objectTypeRelations, caseTypeRelations };
};

/* eslint complexity: [2, 7] */
const handleFetchSuccess = (
  state: ObjectTypeStateType,
  action: AjaxAction<APICaseManagement.GetCustomObjectTypeResponseBody>
): ObjectTypeStateType => {
  const { response } = action.payload;

  if (!response || !response.data || !response.data.attributes) {
    return state;
  }

  const {
    id,
    attributes: {
      title,
      status,
      name,
      custom_field_definition,
      authorization_definition,
      relationship_definition,
      external_reference,
    },
  } = response.data;

  return {
    ...state,
    objectType: {
      uuid: id,
      title,
      status,
      name,
      ...(custom_field_definition && {
        custom_field_definition: mapCustomFields(custom_field_definition),
      }),
      authorizations: mapAuthorizations(
        authorization_definition?.authorizations
      ),
      external_reference: external_reference || '',
      ...mapRelationships(relationship_definition || []),
    },
  };
};

export const objectType: Reducer<ObjectTypeStateType, AjaxAction> = (
  state = initialState,
  action
) => {
  const { type } = action;
  const handleFetchObjectTypeStateChange = handleAjaxStateChange(
    OBJECT_TYPE_FETCH
  );

  switch (type) {
    case OBJECT_TYPE_FETCH.PENDING:
    case OBJECT_TYPE_FETCH.ERROR:
      return handleFetchObjectTypeStateChange(state, action);
    case OBJECT_TYPE_FETCH.SUCCESS:
      return handleFetchObjectTypeStateChange(
        handleFetchSuccess(
          state,
          action as AjaxAction<APICaseManagement.GetCustomObjectTypeResponseBody>
        ),
        action
      );
    default:
      return state;
  }
};

export default objectType;
