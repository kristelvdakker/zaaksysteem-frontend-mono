// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    title: 'Objecttypebeheer',
    objecttype: 'Objecttype',
    add: {
      title: 'Voeg toe',
    },
    edit: {},
    form: {
      controls: {
        next: 'Volgende',
        previous: 'Terug',
        submit: 'Opslaan',
      },
      steps: {
        general: {
          title: 'Algemeen',
          description:
            'Voordat je naar de volgende stap kunt gaan, moeten alle verplichte velden die aangegeven zijn met een asterix ingevuld zijn.',
        },
        attributes: { title: 'Kenmerken' },
        relations: { title: 'Relaties' },
        rights: { title: 'Rechten' },
        audit: { title: 'Afronden' },
      },
      fields: {
        name: {
          label: 'Objecttype',
          help: `Geef hier een naam voor het objecttype zoals dit binnen de oplossing wordt getoond. Deze naam wordt o.a. getoond in de catalogus en voor objecten van dit objecttype (objectbeeld). Bijvoorbeeld 'Contract'`,
          placeholder: 'Geef een herkenbare naam voor het objecttype',
        },
        title: {
          label: 'Objecttitel',
          placeholder: 'Geef een titel voor de objecten van dit objecttype',
          help:
            'Hier kan een titel worden samengesteld voor de objecten van dit objecttype. Een titel kan bestaan uit reguliere teksten, maar ook met magic strings die zich in het object bevinden. Bijvoorbeeld [[ztc_contract_soort]] | [[ztc_contract_nummer]].',
        },
        external_reference: {
          label: 'Externe referentie',
          placeholder: 'Referentie voor bron buiten Zaaksysteem.nl',
          help:
            'Hier kan een locatie worden samengesteld voor een object wanneer de bron zich buiten het zaaksysteem bevindt. Bijvoorbeeld https://zaaksysteem.nl/contract/[[ztc_contract_nummer]].',
        },
        components_changed: {
          label: 'Componenten gewijzigd',
          choices: {
            generic: 'Algemeen',
            attributes: 'Kenmerken',
            relations: 'Relaties',
            authorizations: 'Rechten',
          },
        },
        changes: {
          label: 'Wijziging',
          help: 'Geef omschrijving op',
          placeholder: 'Geef omschrijving op',
        },
        custom_fields: {
          placeholder: 'Zoek en voeg toe',
          form: {
            label: {
              placeholder: 'Titel',
              label: 'Titel',
            },
            is_required: {
              label: 'Verplicht veld',
            },
            description: {
              label: 'Toelichting (intern)',
              placeholder: 'Geef omschrijving op',
            },
            external_description: {
              label: 'Toelichting (extern)',
              placeholder: 'Geef omschrijving op',
            },
            is_hidden_field: {
              label: 'Gebruik als systeemkenmerk',
            },
            use_on_map: {
              label: 'Locatie tonen op kaart',
            },
          },
        },
        caseTypeRelations: {
          label: 'Gerelateerde zaaktypen',
          help:
            'Zoek en selecteer een objecttype uit de catalogus om als relatie toe te voegen.',
        },
        objectTypeRelations: {
          label: 'Gerelateerde objecttypen',
          help:
            'Zoek en selecteer een zaaktype uit de catalogus om als relatie toe te voegen.',
        },
        authorization: {
          addButton: 'Rol en afdeling toevoegen',
          departmentLabel: 'Afdeling',
          selectDepartment: 'Selecteer een afdeling',
          authorizationLabel: 'Rechten',
          roleLabel: 'Rol',
          selectRole: 'Selecteer een rol',
          loading: 'Bezig met laden...',
          permissions: {
            read: 'Mag raadplegen',
            readwrite: 'Mag behandelen',
            admin: 'Mag beheren',
          },
        },
      },
    },
  },
};
