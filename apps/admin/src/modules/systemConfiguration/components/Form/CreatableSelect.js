// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createElement } from 'react';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';

const CreatableSelect = props =>
  createElement(Select, {
    creatable: true,
    ...props,
  });

export default CreatableSelect;
