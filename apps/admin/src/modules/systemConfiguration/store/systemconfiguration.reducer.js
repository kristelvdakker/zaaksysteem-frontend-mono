// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  asArray,
  get,
  isPopulatedArray,
  reduceMap,
} from '@mintlab/kitchen-sink/source';
import { handleAjaxStateChange } from '../../../library/redux/ajax/handleAjaxStateChange';
import { AJAX_STATE_INIT } from '../../../library/redux/ajax/createAjaxConstants';
import {
  isArrayOfStrings,
  isCollection,
  isDbObject,
  objectToSelect,
  arrayToSelect,
} from './library/library';
import {
  SYSTEMCONFIGURATION_FETCH,
  SYSTEMCONFIGURATION_UPDATE_CHOICES,
  SYSTEMCONFIGURATION_SAVE,
  SYSTEMCONFIGURATION_DISCARD,
  SYSTEMCONFIGURATION_DISCARD_DONE,
} from './systemconfiguration.constants';

const initialState = {
  items: [],
  state: AJAX_STATE_INIT,
};

/**
 * @param {string} firstObject
 * @param {string} secondObject
 * @return {number}
 */
const sortByLabel = (firstObject, secondObject) =>
  firstObject.label.localeCompare(secondObject.label);

/**
 * @param {Object} parameters
 * @param {string} parameters.type
 * @param {boolean} parameters.required
 * @return {Array}
 */
const getConstraints = ({ type, required }) => {
  const constraints = [];

  if (['email', 'uri', 'number'].includes(type)) {
    constraints.push(type);
  }
  if (required) {
    constraints.push('required');
  }

  return constraints;
};

/**
 * @param {Object} parameters
 * @param {string} parameters.type
 * @param {string} parameters.name
 * @param {Object} parameters.config
 * @return {string}
 */
const getComponentType = ({ type, name, config }) => {
  function getFormat() {
    return get(config, 'format') === 'html' ? 'html' : 'text';
  }

  const map = new Map([
    [
      keyName =>
        keyName === 'custom_relation_roles' ||
        keyName === 'dashboard_ui_xss_uri',
      () => 'creatable',
    ],
    [keyName => keyName === 'allowed_templates', () => 'select'],
    [(keyName, keyType) => keyType === 'boolean', () => 'checkbox'],
    [(keyName, keyType) => keyType === 'text', () => getFormat()],
    [(keyName, keyType) => keyType === 'object_ref', () => 'select'],
  ]);

  return reduceMap({
    map,
    keyArguments: [name, type],
    valueArguments: [name, type],
    fallback: 'text',
  });
};

/**
 * @param {*} value_type
 * @return {Object|undefined}
 */
const getConfig = value_type => get(value_type, 'options');

/**
 * @param {*} name
 * @return {boolean}
 */
const getAutoLoad = name =>
  [
    'case_distributor_group',
    'case_distributor_role',
    'signature_upload_role',
  ].includes(name);

const getChoices = value_type => get(value_type, 'choices');

/**
 * Transform field value(s) from database to valid app state values
 * @param {*} value
 * @return {*}
 */
const dbToApp = value => {
  const map = new Map([
    [
      keyValue => isDbObject(keyValue),
      valueValue => objectToSelect(valueValue),
    ],
    [
      keyValue => isArrayOfStrings(keyValue),
      valueValue => arrayToSelect(valueValue),
    ],
    [
      keyValue => isCollection(keyValue),
      valueValue =>
        asArray(get(valueValue, 'instance.rows')).map(row =>
          objectToSelect(row)
        ),
    ],
  ]);

  return reduceMap({
    map,
    keyArguments: [value],
    valueArguments: [value],
    fallback: value,
  });
};

/**
 * @param {Object} state
 * @param {Object} action
 * @return {Object}
 */
/* eslint complexity: [2, 10] */
const fetchSuccess = (state, action) => {
  const result = get(action, 'payload.response.result.instance');
  const definitionsRows = get(result, 'definitions.instance.rows');
  const itemsRows = get(result, 'items.instance.rows');

  const createItemDefinition = ({
    reference: definitionReference,
    instance: {
      config_item_name: name,
      label,
      value_type,
      value_type_name,
      mutable,
      required,
      mvp: isMulti,
    },
  }) => {
    const item = itemsRows.find(
      ({
        instance: {
          definition: { reference: thisReference },
        },
      }) => thisReference === definitionReference
    );
    const reference = get(item, 'reference');
    const type = value_type_name
      ? value_type_name
      : get(value_type, 'parent_type_name') || get(value_type, 'name');
    const value = dbToApp(get(item, 'instance.value'));
    const config = getConfig(value_type);
    const constraints = getConstraints({
      type,
      required,
    });
    const componentType = getComponentType({
      type,
      name,
      config,
    });
    const autoLoad = getAutoLoad(name);
    const choices = dbToApp(getChoices(config));
    const hasInitialChoices = isPopulatedArray(choices);
    const externalLabel = ['select', 'creatable', 'html'].includes(
      componentType
    );
    const disableFilterOption = ['creatable'].includes(componentType);

    return {
      type: componentType,
      config,
      label,
      name,
      isMultiline: get(config, 'style') === 'paragraph',
      isMulti,
      disabled: !mutable,
      reference,
      required,
      hasInitialChoices,
      externalLabel,
      value,
      state: AJAX_STATE_INIT,
      disableFilterOption,
      ...(constraints && { constraints }),
      ...(autoLoad && { autoLoad }),
      ...(choices && { choices }),
    };
  };

  return {
    ...state,
    items: definitionsRows
      .map(createItemDefinition)
      .filter(value => value.reference),
  };
};

/**
 * @param {Object} state
 * @param {Object} action
 * @return {Object}
 */
const updateChoicesState = (state, action) => {
  const name = get(action, 'payload.name');
  const items = get(state, 'items');
  const index = items ? items.findIndex(item => item.name === name) : undefined;

  if (!index) {
    return state;
  }

  const processStateChange = handleAjaxStateChange(
    SYSTEMCONFIGURATION_UPDATE_CHOICES
  );

  const updatedItem = {
    ...processStateChange(items[index], action),
  };

  if (action.type === SYSTEMCONFIGURATION_UPDATE_CHOICES.SUCCESS) {
    const choices = dbToApp(get(action, 'payload.response.result'));
    const sortedChoices = asArray(choices).sort(sortByLabel);
    updatedItem.choices = sortedChoices;
  }

  return {
    ...state,
    items: Object.assign([], items, { [index]: updatedItem }),
  };
};

/**
 * @param {Object} state
 * @param {Object} action
 * @return {Object}
 */
const saveSuccess = (state, action) => {
  const rows = get(action, 'payload.response.result.instance.rows');
  const items = get(state, 'items');

  const findItem = findName =>
    rows.find(row => get(row, 'instance.name') === findName);

  const newItems = items.map(item => {
    const updated = get(findItem(item.name), 'instance.value');

    if (updated !== undefined) {
      return {
        ...item,
        value: dbToApp(updated),
      };
    }

    return item;
  });

  return {
    ...state,
    items: newItems,
  };
};

const discard = state => ({
  ...state,
  discard: true,
});

const discardDone = state => ({
  ...state,
  discard: undefined,
});

/**
 * @param {Object} state
 * @param {Object} action
 * @return {Object}
 */
export function systemConfiguration(state = initialState, action) {
  const handleAjaxState = handleAjaxStateChange(SYSTEMCONFIGURATION_FETCH);

  switch (action.type) {
    case SYSTEMCONFIGURATION_FETCH.SUCCESS:
      return fetchSuccess(handleAjaxState(state, action), action);
    case SYSTEMCONFIGURATION_UPDATE_CHOICES.PENDING:
    case SYSTEMCONFIGURATION_UPDATE_CHOICES.ERROR:
    case SYSTEMCONFIGURATION_UPDATE_CHOICES.SUCCESS:
      return updateChoicesState(state, action);
    case SYSTEMCONFIGURATION_SAVE.SUCCESS:
      return saveSuccess(state, action);
    case SYSTEMCONFIGURATION_DISCARD:
      return discard(state);
    case SYSTEMCONFIGURATION_DISCARD_DONE:
      return discardDone(state);
    case SYSTEMCONFIGURATION_FETCH.PENDING:
    case SYSTEMCONFIGURATION_FETCH.ERROR:
      return handleAjaxState(state, action);
    default:
      return state;
  }
}
