// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { createElement } from 'react';
import { withStyles } from '@material-ui/styles';
import Icon from '@mintlab/ui/App/Material/Icon';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { filterStyleSheet } from './Filter.style';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Function} props.changeFilterValue
 * @param {string} props.name
 * @param {string} props.value
 * @param {string} props.startAdornmentName
 * @param {Object} props.userTranslations
 * @param {Function} props.fetchUsersList
 * @param {Array<Object>} props.userOptions
 * @param {Function} onChange
 * @return {ReactElement}
 */
export const TextFieldFilter = ({
  classes,
  name,
  value,
  startAdornmentName,
  userTranslations,
  fetchUsersList,
  userOptions,
  onChange,
  loading,
}) => (
  <div className={classes.filterWrapper}>
    <Select
      name={name}
      value={value}
      choices={userOptions}
      generic={true}
      getChoices={fetchUsersList}
      onChange={onChange}
      translations={userTranslations}
      startAdornment={createElement(Icon, {
        size: 'small',
        children: startAdornmentName,
      })}
      filterOption={option => option}
      isClearable={true}
      loading={loading}
    />
  </div>
);

export default withStyles(filterStyleSheet)(TextFieldFilter);
