// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { get } from '@mintlab/kitchen-sink/source';
import { login } from '@zaaksysteem/common/src/store/session/session.actions';
import { showDialog } from '../ui/ui.actions';
import { DIALOG_ERROR } from '../../components/Dialogs/dialogs.constants';
import { getUrl } from '../../library/url';

/**
 * Handles request errors in a generic way.
 *
 * If the HTTP statuscode is 401, we redirect.
 *
 * If the response from the backend contains an error
 * array, we dispatch a Dialog action with an array of
 * translation keys, so we can display the most specific
 * error message possible.
 *
 * Else, we dispatch the dialog with a more generalized
 * translation key, based on the HTTP statuscode.
 *
 * If any of these keys cannot be found in the locale
 * file, the fallback key will be used.
 */

/**
 * @param {*} store
 * @return {Object}
 */
export const errorMiddleware = store => next => action => {
  const { dispatch } = store;

  const ajax = get(action, 'ajax');
  const error = get(action, 'error');

  /**
   * @param {array<string> | string} message
   * @return {Object}
   */
  const dispatchDialog = message =>
    dispatch(
      showDialog({
        type: DIALOG_ERROR,
        options: {
          message,
          fallback: `server:error`,
        },
      })
    );

  if (ajax && error) {
    const { statusCode } = action;
    const { response } = action.payload;
    const { errors } = response;

    if (statusCode === 401) {
      dispatch(login(getUrl()));
      return next(action);
    }

    if (errors) {
      dispatchDialog(errors.map(({ code }) => `server:keys.${code}`));
      return next(action);
    }

    dispatchDialog(`server:status.${statusCode}`);
  }

  return next(action);
};

export default errorMiddleware;
