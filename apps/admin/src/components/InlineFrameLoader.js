// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Render from '@mintlab/ui/App/Abstract/Render';
import { extract } from '@mintlab/kitchen-sink/source';
import InlineFrame from './InlineFrame';
import styleSheet from './InlineFrameLoader.module.css';

/**
 * A loader component for {@link InlineFrame}.
 *
 * @param {Object} props
 * @return {ReactElement}
 */
export const InlineFrameLoader = props => {
  const [loading, iframeProps] = extract('loading', props);

  // ZS-INFO: The `InlineFrame` component cannot be a child
  // of the `Loader` component because it needs to be rendered
  // first in order to set the store's `loading` property.
  return (
    <React.Fragment>
      <Render condition={loading}>
        <div className={styleSheet.loader}>
          <Loader active={true} />
        </div>
      </Render>
      <InlineFrame {...iframeProps} />
    </React.Fragment>
  );
};
