// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import {
  Dialog as UIDialog,
  DialogTitle,
  DialogDivider,
  DialogContent,
  DialogActions,
} from '@mintlab/ui/App/Material/Dialog';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'contained'],
  secondaryPresets: ['default', 'contained'],
});

/**
 * Dialog used to display forms and large amount of data
 *
 * @param {Object} props
 * @param {boolean} [props.open=false]
 * @param {Object} [props.primaryButton]
 * @param {Object} [props.secondaryButton]
 * @param {String} title
 * @param {String} [scope]
 * @param {Function} [onClose]
 * @param {String} [icon]
 * @param {React.children} [children]
 * @return {ReactElement}
 */
export const Dialog = ({
  open = false,
  primaryButton,
  secondaryButton,
  title,
  scope,
  children,
  onClose,
  icon,
  ...rest
}) => {
  const dialogActions = getDialogActions(primaryButton, secondaryButton, scope);

  return (
    <UIDialog aria-label={title} open={open} onClose={onClose} {...rest}>
      <DialogTitle
        elevated={true}
        icon={icon}
        title={title}
        onCloseClick={onClose}
        scope={scope}
      />
      <DialogContent padded={true}>{children}</DialogContent>
      {dialogActions && (
        <React.Fragment>
          <DialogDivider />
          <DialogActions>{dialogActions}</DialogActions>
        </React.Fragment>
      )}
    </UIDialog>
  );
};

export default Dialog;
