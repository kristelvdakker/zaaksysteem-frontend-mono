// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { objectifyParams } from '@mintlab/kitchen-sink/source';
import { InlineFrameLoader } from '../../InlineFrameLoader';

/**
 * @param {string} url
 * @param {Object} routes
 * @return {Array}
 */
export default function getComponent(url, routes) {
  const [pathComponent, paramComponent] = url.split('?');
  const [, , segment, ...rest] = pathComponent.split('/');
  const params = objectifyParams(paramComponent);

  if (!Object.prototype.hasOwnProperty.call(routes, segment)) {
    return null;
  }

  const value = routes[segment];

  // iFrame with legacy src
  if (typeof value === 'string') {
    return [InlineFrameLoader, value];
  }

  // Original React component
  return [value, rest, params];
}
