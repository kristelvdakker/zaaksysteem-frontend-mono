// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Component, createRef } from 'react';
import { getUrl, navigate, parseUrl } from '../library/url';
import { LOGIN_PATH, login } from '../library/auth';
import { getIframeUrl, getParentUrl } from './library/iframe';
import './InlineFrame.css';

const legacyStyles = `/* Overrides for zaaksysteem/frontend */

/* appbar */

#contentwrapper {
  margin-top: 0;
  height: 100%;
  overflow: initial !important;
}

.block-inner-scroll {
  top: 0;
}

/* ZTB progress footer #1: .block is child of form */
/* - algemeen, relaties, acties, kinderen, afronden */
form.ezra_zaaktypebeheer > .block,
/* ZTB progress footer #2: .block is parent of form */
/* - fasen */
#mijlpalen_configureren.mijlpaal_definitie.block,
/* - fase n */
#configureren.mijlpaal_content.block,
/* - rechten */
.form.ztb.auth_definitie.block,
/* Case Type: import */
form[action="/beheer/zaaktypen/import"],
/* Object Type: create, update */
.object-type-edit > .block-progress {
  margin-bottom: 5rem !important;
}

#import_configuration form.import {
  padding-bottom: 0 !important;
}

.sysin-links-content.panel-component {
  top: 100px;
}

.ui-layout-pane {
  height: auto !important;
}

.ui-layout-resizer {
  height: 100% !important;
}

/* plus button */

.block {
  padding: 0 25px 25px;
}

.block-tabbed .block-content {
  padding: 1.5em 1.5em;
}

.block-content {
  margin-bottom: 75px;
}

/* colors */

.breadcrumb,
.block-fullscreen .block-header,
.breadcrumb-in-header .breadcrumb,
.block-tabbed .block-header,
.ui-tabs .ui-widget-header {
  background: #fff;
}

a,
.voortgang button,
.field-help .icon-question-sign {
  color: #1274EB;
}

a:hover,
a:focus,
.voortgang button:hover,
.voortgang button:focus,
.field-help .icon-question-sign:hover,
.field-help .icon-question-sign:focus {
  color: #0257BC;
}

.btn-primary,
.button-primary {
  color: #fff;
  border: 1px solid #1274EB;
  background: #1274EB;
}

.button-primary:hover,
.button-primary:focus {
  border-color: #0257BC;
  background: #0257BC;
}

.ui-tabs .ui-widget-header {
  border-bottom: 1px solid #ddd;
}

.ui-tabs .ui-tabs-nav li {
  border: 1px solid #ddd;
  background-color: #eee;
}

.ui-tabs .ui-tabs-selected {
  border: 1px solid #fff;
  background-color: #fff !important;
}
`;

/**
 * InlineFrame component for seamless integration of
 * server-side legacy content in the React admin app.
 * Consumed by {@link InlineFrameLoader}.
 *
 * @reactProps {string} url
 * @reactProps {Function} onLoad
 * @reactProps {Function} onUnload
 * @reactProps {Function} onOverlayOpen
 * @reactProps {Function} onOverlayClose
 */
export default class InlineFrame extends Component {
  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.registerPostMessages();

    /**
     * @type {RefObject}
     */
    this.iframeElement = createRef();
    /**
     * @type {string}
     */
    this.basePath = this.getBasePath();
  }

  /**
   *  @see https://reactjs.org/docs/react-component.html#componentdidmount
   */
  componentDidMount() {
    this.manageNativeEvents('add');
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#shouldcomponentupdate
   *
   * @param {Object} nextProps
   * @param {string} nextProps.url
   * @return {boolean}
   */
  shouldComponentUpdate({ url }) {
    const { current } = this.iframeElement;
    const { contentWindow, src } = current;
    const currentIframeUrl = getUrl(contentWindow);
    const nextIframeUrl = getIframeUrl(url, this.basePath);

    if (this.isIframeStale(url, nextIframeUrl, currentIframeUrl)) {
      return false;
    }

    // The iframe location must be updated programmatically if the
    // last navigation before navigation was initiated within the iframe
    // is the same as the first navigation afterwards.
    if (this.isIframeOutOfSync(currentIframeUrl, nextIframeUrl, src)) {
      navigate(src, contentWindow);

      return false;
    }

    return true;
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#componentwillunmount
   */
  componentWillUnmount() {
    this.manageNativeEvents('remove');
  }

  /**
   * @return {ReactElement}
   */
  render() {
    const {
      props: { t, url },
    } = this;

    return (
      <iframe
        ref={this.iframeElement}
        src={getIframeUrl(url, this.basePath)}
        title={t('aria:adminIframe')}
      />
    );
  }

  /**
   * @param {Event} event
   * @param {Node} event.target
   * @param {string} event.type
   */
  handleEvent({ data, target, type }) {
    const method = `on${type}`;

    if (typeof this[method] == 'function') {
      if (data) {
        this[method](data);
      } else {
        this[method](target);
      }
    }
  }

  /**
   * Handle events that must be managed manually in the
   * mount lifecycle methods in a single location.
   *
   * @param {string} action
   *   `add` or `remove`
   */
  manageNativeEvents(action) {
    const { current: iframe } = this.iframeElement;

    iframe[`${action}EventListener`]('load', this);
    window[`${action}EventListener`]('message', this);
  }

  /**
   * Map instance props (action dispatchers) to the
   * post message names sent from the iframe window.
   */
  registerPostMessages() {
    const { onOverlayClose, onOverlayOpen } = this.props;

    this.postMessages = {
      'IFRAME:OVERLAY:OPEN': onOverlayOpen,
      'IFRAME:OVERLAY:CLOSE': onOverlayClose,
    };
  }

  /**
   * The `iframe` **element**'s load event listener does always
   *
   * - intercept the login page in the framed window
   * - inject an override style sheet
   * - attach a mutation observer to the iframe DOM for modal dialogs
   * - add an `unload` event listener to the current iframe window
   *
   * **If** navigation was initiated in the iframe, it dispatches a route action.
   * That updates the store but cancels this component's `render` in
   * `shouldComponentUpdate` when the next store state is passed with props.
   *
   * @param {Window} contentWindow
   */
  onload({ contentWindow }) {
    if (this.abortLoadHandler(contentWindow)) {
      return;
    }

    const { onUnload } = this.props;

    contentWindow.addEventListener('unload', onUnload);
    this.setStyleOverride(contentWindow);
    this.syncParentState(contentWindow);
  }

  /**
   * Handle iframe action side effects, if any.
   *
   * @param {Window} iframeWindow
   */
  syncParentState(iframeWindow) {
    const { hasOverlay, onOverlayClose, route } = this.props;

    // Iframe navigation can happen without an open overlay
    // being closed, e.g. "GET after POST".
    if (hasOverlay) {
      onOverlayClose();
    }

    const iframeUrl = getUrl(iframeWindow);
    const parentUrl = getParentUrl(iframeUrl, this.basePath);

    if (this.isNavigationInitiatedInIframe(parentUrl)) {
      route({
        path: parentUrl,
      });
    }
  }

  /**
   * @param {*} messageData
   */
  onmessage(messageData) {
    if (Object.prototype.hasOwnProperty.call(this.postMessages, messageData)) {
      this.postMessages[messageData]();
    }
  }

  /**
   * @param {Window} iframeWindow
   * @return {boolean}
   */
  isSameOrigin(iframeWindow) {
    return iframeWindow.location.origin === window.location.origin;
  }

  /**
   * @param {string} pathComponent
   * @return {boolean}
   */
  isInternPathComponent(pathComponent) {
    const internExpression = /^\/intern(?:$|\/)/;

    return internExpression.test(pathComponent);
  }

  /**
   * @param {Window} iframeWindow
   * @return {boolean}
   */
  isInternWindow(iframeWindow) {
    if (this.isSameOrigin(iframeWindow)) {
      const { pathname } = iframeWindow.location;

      return this.isInternPathComponent(pathname);
    }

    return false;
  }

  /**
   * @param {Window} iframeWindow
   * @return {boolean}
   */
  abortLoadHandler(iframeWindow) {
    if (iframeWindow.location.pathname === LOGIN_PATH) {
      login(getUrl());

      return true;
    }

    if (this.isInternWindow(iframeWindow)) {
      navigate(iframeWindow.location.pathname);

      return true;
    }

    return false;
  }

  /**
   * @param {string} expectedUrl
   *   The app URL that was resolved from
   *   the iframe window's location object.
   * @return {boolean}
   */
  isNavigationInitiatedInIframe(expectedUrl) {
    const currentUrl = getUrl();

    return currentUrl !== expectedUrl;
  }

  /**
   * Dynamically insert the Style Sheet with app specific overrides.
   * Modifying the sources in `zaaksysteem` can cause regressions
   * in `PIP` and `/form`.
   *
   * @param {Window} contentWindow
   */
  setStyleOverride(contentWindow) {
    const style = contentWindow.document.createElement('style');
    style.innerHTML = legacyStyles;

    style.onload = this.props.onLoad;
    contentWindow.document.head.appendChild(style);
  }

  /**
   * Get the app's base path that is configured in nginx.
   * That value should always be resolved programmatically.
   *
   * @example
   * // https://example.org/foo/bar
   * this.getBasePath(); // => '/foo/'
   *
   * @return {string}
   */
  getBasePath() {
    const { pathname } = window.location;
    const [, basePath] = /^(\/[^/]+\/)/.exec(pathname);

    return basePath;
  }

  /**
   * To determine if the iframe is stale,
   * compare the location data of both the
   * parent window and the iframe window.
   *
   * @param {string} nextUrl
   * @param {string} nextIframeUrl
   * @param {string} currentIframeUrl
   * @return {boolean}
   */
  isIframeStale(nextUrl, nextIframeUrl, currentIframeUrl) {
    const { url } = this.props;
    const isParentStale = nextUrl === url;
    const isIframeStale = nextIframeUrl === currentIframeUrl;

    return isParentStale || isIframeStale;
  }

  /**
   * @param {string} currentIframeUrl
   *   The iframe window's current path and query component.
   * @param {string} nextIframeUrl
   *   The iframe window's next path and query component.
   * @param {string} iframeElementSrc
   *   The value of the iframe element's `src` property.
   *   This does **not** reflect navigation that was subsequently
   *   initiated in the iframe, but the last navigation before that.
   * @return {boolean}
   */
  isIframeOutOfSync(currentIframeUrl, nextIframeUrl, iframeElementSrc) {
    const parsedSrcUrl = parseUrl(iframeElementSrc);
    const isBaseRoute = nextIframeUrl === parsedSrcUrl;

    return isBaseRoute && parsedSrcUrl !== currentIframeUrl;
  }
}
