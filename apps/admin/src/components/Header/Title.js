// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { H3 } from '@mintlab/ui/App/Material/Typography';
import { titleStylesheet } from './Title.style';

/**
 * @reactProps {*} children
 * @reactProps {Object} classes
 * @return {ReactElement}
 */
export const Title = ({ titleStyle, classes, children }) => (
  <H3
    classes={{
      root: classNames(classes.title, titleStyle),
    }}
  >
    {children}
  </H3>
);

export default withStyles(titleStylesheet)(Title);
