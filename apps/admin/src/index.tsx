// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import 'core-js/stable';
import 'regenerator-runtime/runtime';
import React from 'react';
import ReactDOM from 'react-dom';
import AdminApp from './App';
import { setupI18n } from './i18n';

window.onerror = null;

setupI18n().then(() => {
  ReactDOM.render(<AdminApp />, document.getElementById('zs-app'));
});
