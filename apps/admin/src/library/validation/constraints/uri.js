// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { hasValue } from '../../../library/value';

/**
 * @param {*} userInput
 * @return {boolean}
 */
const passesRegex = userInput => {
  const regex = /^(?:file:\/\/.*|[a-zA-Z]:\\.*|(?:(?:[a-zA-Z][a-zA-Z0-9+-.]*):\/\/)?(?:\w+@)?(?:[\w-]+)(?:\.[\w-]+)*(?::[0-9]+)?(?:\/.*)?)$/;
  return regex.test(userInput);
};

/**
 *
 * @param {*} userInput
 * @param {Object} config
 * @param {Array} config.allowed_protocols
 * @return {boolean}
 */
const passesAllowedProtocols = (userInput, config) => {
  const hasAllowedProtocols = () => config && config.allowed_protocols;

  if (hasAllowedProtocols()) {
    const matches = /(^.[^:]*):\/\//.exec(userInput);

    if (!matches) {
      return config.allowed_protocols.includes('http');
    }

    const [, protocol] = matches;
    return config.allowed_protocols.includes(protocol.toLowerCase());
  }

  return true;
};

/**
 * @param {string} userInput
 * @param {Object} config
 * @return {boolean}
 */
export const uri = (userInput, config) => {
  if (!hasValue(userInput)) {
    return true;
  }
  return passesRegex(userInput) && passesAllowedProtocols(userInput, config);
};
