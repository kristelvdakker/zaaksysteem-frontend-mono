// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * Redux reducer function factory.
 *
 * @param {*} initialState
 *   Initial store state.
 * @param {Object} reducers
 *   Object that associates action types with state reducer functions.
 * @return {Function}
 */
export const createReducer = (initialState, reducers) => (
  state = initialState,
  action
) => {
  const { payload, type } = action;

  if (typeof reducers[type] === 'function') {
    return reducers[type]({ payload, state });
  }

  return state;
};
