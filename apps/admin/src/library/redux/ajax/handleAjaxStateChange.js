// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  AJAX_STATE_ERROR,
  AJAX_STATE_PENDING,
  AJAX_STATE_VALID,
} from './createAjaxConstants';

/**
 * Reducer for handling Ajax state changes (pending, valid and error)
 * @param {Object} constants
 * @param {string} key
 * @returns {Function}
 */
export const handleAjaxStateChange = (constants, key = 'state') => (
  state,
  action
) => {
  switch (action.type) {
    case constants.PENDING:
      return {
        ...state,
        [key]: AJAX_STATE_PENDING,
      };

    case constants.SUCCESS:
      return {
        ...state,
        [key]: AJAX_STATE_VALID,
      };

    case constants.ERROR:
      return {
        ...state,
        [key]: AJAX_STATE_ERROR,
      };

    default:
      return state;
  }
};

export default handleAjaxStateChange;
