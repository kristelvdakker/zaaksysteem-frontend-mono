// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { initI18n } from '@zaaksysteem/common/src/i18n';
import { initReactI18next } from 'react-i18next';
import fecha from 'fecha';
import locale from './locale';

function initFetcha(i18n) {
  fecha.i18n = {
    ...fecha.i18n,
    ...i18n.t('common:dates', { returnObjects: true }),
  };
}

export async function setupI18n() {
  const i18n = await initI18n(
    {
      lng: 'nl',
      keySeparator: '.',
      defaultNS: 'common',
      fallbackNS: 'common',
      resources: locale,
      interpolation: {
        escapeValue: false,
      },
    },
    [initReactI18next]
  );

  initFetcha(i18n);
}
