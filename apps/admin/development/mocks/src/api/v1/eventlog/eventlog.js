const generateRows = amount => {
  return Array(50)
    .fill({})
    .map((row, index) => ({
      instance: {
        event_id: index,
        date_created: (new Date()).toISOString(),
        created_by: {
          instance: {
            subject: {
              instance: {
                display_name: 'A. Admîn'
              }
            }
          }
        },
        event_data: {
          human_readable: `Log line number ${index + 1}`,
          registration_date: '12-03-2018',
          case_id: 7628
        },
        component: 'zaak',
        case_id: 7628,
      },
    }))
};

const json = {
  status_code: 200,
  api_version: 1,
  result: {
    instance: {
      rows: generateRows(50),
      pager: {
        page: 1,
        pages: 4977,
        rows: 50,
        total_rows: 248837,
      }
    }
  }
};

module.exports = json;
