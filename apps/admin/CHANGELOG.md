# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.36.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.36.4...@zaaksysteem/admin@0.36.5) (2020-12-21)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.36.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.36.3...@zaaksysteem/admin@0.36.4) (2020-12-18)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.36.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.36.2...@zaaksysteem/admin@0.36.3) (2020-12-17)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.36.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.36.1...@zaaksysteem/admin@0.36.2) (2020-12-03)


### Bug Fixes

* **Catalog:** MINTY-5839 - fix saving document attributes ([0a28c81](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0a28c814e1262d4fc07ecec2ce6fd9a932d79785))





## [0.36.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.36.0...@zaaksysteem/admin@0.36.1) (2020-12-03)


### Bug Fixes

* **SystemConfiguration:** MINTY-5402 - fix issues with saving checkbox values ([7195ab3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7195ab3dc95ad22c95ea631f2580f27e9542e4da))





# [0.36.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.35.0...@zaaksysteem/admin@0.36.0) (2020-11-25)


### Features

* **SystemConfiguration:** MINTY-5646 - add configuration options for v1/v2 objects ([3fd25d3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3fd25d30c6c8be3fa71e306f51effedd11f1c005))





# [0.35.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.34.7...@zaaksysteem/admin@0.35.0) (2020-11-20)


### Features

* **Catalog:** MINTY-5678 - Add support for attribute type of appointment_v2 ([55424b7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/55424b78fcb09db841c356d1ba41240bdc21fd22))





## [0.34.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.34.6...@zaaksysteem/admin@0.34.7) (2020-11-20)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.34.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.34.5...@zaaksysteem/admin@0.34.6) (2020-11-19)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.34.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.34.4...@zaaksysteem/admin@0.34.5) (2020-11-11)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.34.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.34.3...@zaaksysteem/admin@0.34.4) (2020-11-11)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.34.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.34.2...@zaaksysteem/admin@0.34.3) (2020-11-11)


### Bug Fixes

* **Catalog:** MINTY-5332 - fix render error on opening I ([0023f8c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0023f8c))





## [0.34.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.34.1...@zaaksysteem/admin@0.34.2) (2020-11-11)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.34.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.34.0...@zaaksysteem/admin@0.34.1) (2020-10-30)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.34.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.33.2...@zaaksysteem/admin@0.34.0) (2020-10-26)


### Features

* **CaseTypeVersion:** MINTY-5058 - change case type version texts ([a14b676](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a14b676))





## [0.33.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.33.1...@zaaksysteem/admin@0.33.2) (2020-10-12)


### Bug Fixes

* **Catalog:** Quick fix saving attribute of type relation ([0865515](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0865515))





## [0.33.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.33.0...@zaaksysteem/admin@0.33.1) (2020-10-08)


### Bug Fixes

* **Catalog:** Quick fix saving attribute of type relation ([1ca6967](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1ca6967))





# [0.33.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.32.4...@zaaksysteem/admin@0.33.0) (2020-10-08)


### Features

* **TasksWidget:** MINTY-4976 - add Tasks widget features with overlay form ([ba9c2bf](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ba9c2bf))
* **TasksWidget:** MINTY-4976 - change outputFormat to config ([069fd79](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/069fd79))





## [0.32.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.32.3...@zaaksysteem/admin@0.32.4) (2020-10-02)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.32.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.32.2...@zaaksysteem/admin@0.32.3) (2020-09-30)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.32.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.32.1...@zaaksysteem/admin@0.32.2) (2020-09-25)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.32.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.32.0...@zaaksysteem/admin@0.32.1) (2020-09-17)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.32.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.31.2...@zaaksysteem/admin@0.32.0) (2020-09-16)


### Features

* **Main:** MINTY-4759 - add menu items to main menu bar ([8ed4876](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8ed4876))





## [0.31.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.31.1...@zaaksysteem/admin@0.31.2) (2020-09-16)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.31.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.31.0...@zaaksysteem/admin@0.31.1) (2020-09-15)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.31.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.30.0...@zaaksysteem/admin@0.31.0) (2020-09-14)


### Features

* **Catalog:** MINTY-4867 - Save objecttype in folder that create was initiated from ([01b56bd](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/01b56bd))





# [0.30.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.29.3...@zaaksysteem/admin@0.30.0) (2020-09-14)


### Bug Fixes

* **Catalog:** Fix entitytype translation ([6c8f161](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6c8f161))


### Features

* **Configuration:** Add support for preferred-channel setting ([1d506d2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1d506d2))





## [0.29.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.29.2...@zaaksysteem/admin@0.29.3) (2020-09-04)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.29.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.29.1...@zaaksysteem/admin@0.29.2) (2020-09-03)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.29.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.29.0...@zaaksysteem/admin@0.29.1) (2020-09-03)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.29.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.28.5...@zaaksysteem/admin@0.29.0) (2020-09-03)


### Features

* **Main:** MINTY-4758 - start new action from Object View ([1867c5b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1867c5b))





## [0.28.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.28.4...@zaaksysteem/admin@0.28.5) (2020-09-03)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.28.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.28.3...@zaaksysteem/admin@0.28.4) (2020-09-02)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.28.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.28.2...@zaaksysteem/admin@0.28.3) (2020-09-01)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.28.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.28.1...@zaaksysteem/admin@0.28.2) (2020-09-01)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.28.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.28.0...@zaaksysteem/admin@0.28.1) (2020-09-01)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.28.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.27.0...@zaaksysteem/admin@0.28.0) (2020-08-27)


### Features

* **Dashboard:** MINTY-4800 - add Tasks Dashboard widget merged ([df0dffc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/df0dffc))





# [0.27.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.26.8...@zaaksysteem/admin@0.27.0) (2020-08-20)


### Features

* **Config:** Add config option to toggle Zoho integration ([53338f9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/53338f9))





## [0.26.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.26.7...@zaaksysteem/admin@0.26.8) (2020-08-20)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.26.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.26.6...@zaaksysteem/admin@0.26.7) (2020-08-19)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.26.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.26.5...@zaaksysteem/admin@0.26.6) (2020-08-17)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.26.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.26.4...@zaaksysteem/admin@0.26.5) (2020-08-12)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.26.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.26.3...@zaaksysteem/admin@0.26.4) (2020-08-12)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.26.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.26.0...@zaaksysteem/admin@0.26.3) (2020-08-11)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.26.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.26.0...@zaaksysteem/admin@0.26.2) (2020-08-07)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.26.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.26.0...@zaaksysteem/admin@0.26.1) (2020-08-07)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.26.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.25.4...@zaaksysteem/admin@0.26.0) (2020-08-06)


### Features

* **Object:** Add support for relationship custom_object and subject ([6e83436](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6e83436))





## [0.25.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.25.4...@zaaksysteem/admin@0.25.5) (2020-08-06)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.25.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.25.3...@zaaksysteem/admin@0.25.4) (2020-08-06)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.25.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.25.2...@zaaksysteem/admin@0.25.3) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.25.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.25.1...@zaaksysteem/admin@0.25.2) (2020-07-23)


### Bug Fixes

* **Catalog:** Fix ObjectTypeSelect setting form value ([87bcc09](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/87bcc09))





## [0.25.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.25.0...@zaaksysteem/admin@0.25.1) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.25.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.24.6...@zaaksysteem/admin@0.25.0) (2020-07-23)


### Features

* **Catalog:** Add support for attribute type Relation ([546907a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/546907a))





## [0.24.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.24.5...@zaaksysteem/admin@0.24.6) (2020-07-17)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.24.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.24.4...@zaaksysteem/admin@0.24.5) (2020-07-16)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.24.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.24.3...@zaaksysteem/admin@0.24.4) (2020-07-15)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.24.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.24.2...@zaaksysteem/admin@0.24.3) (2020-07-14)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.24.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.24.1...@zaaksysteem/admin@0.24.2) (2020-07-09)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.24.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.24.0...@zaaksysteem/admin@0.24.1) (2020-07-09)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.24.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.23.4...@zaaksysteem/admin@0.24.0) (2020-07-02)


### Features

* **configuration:** MINTY-4449 - Remove label from checkboxes in admin configuration ([7f54c8b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7f54c8b))





## [0.23.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.23.3...@zaaksysteem/admin@0.23.4) (2020-07-02)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.23.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.23.2...@zaaksysteem/admin@0.23.3) (2020-06-30)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.23.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.23.1...@zaaksysteem/admin@0.23.2) (2020-06-26)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.23.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.23.0...@zaaksysteem/admin@0.23.1) (2020-06-25)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.23.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.22.12...@zaaksysteem/admin@0.23.0) (2020-06-24)


### Features

* **Object:** MINTY-4239 - Allow objects to be updated and unrelated ([e9f3eb0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e9f3eb0))





## [0.22.12](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.22.11...@zaaksysteem/admin@0.22.12) (2020-06-23)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.22.11](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.22.10...@zaaksysteem/admin@0.22.11) (2020-06-18)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.22.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.22.9...@zaaksysteem/admin@0.22.10) (2020-06-17)


### Bug Fixes

* (Catalog) fix crashing document template create dialog ([af44ded](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/af44ded))





## [0.22.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.22.8...@zaaksysteem/admin@0.22.9) (2020-06-12)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.22.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.22.7...@zaaksysteem/admin@0.22.8) (2020-06-11)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.22.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.22.6...@zaaksysteem/admin@0.22.7) (2020-06-09)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.22.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.22.5...@zaaksysteem/admin@0.22.6) (2020-06-05)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.22.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.22.4...@zaaksysteem/admin@0.22.5) (2020-06-03)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.22.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.22.3...@zaaksysteem/admin@0.22.4) (2020-05-29)


### Bug Fixes

* **catalog:** swap custom object uuid and version_uuid ([6178510](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6178510))





## [0.22.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.22.2...@zaaksysteem/admin@0.22.3) (2020-05-29)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.22.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.22.1...@zaaksysteem/admin@0.22.2) (2020-05-22)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.22.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.22.0...@zaaksysteem/admin@0.22.1) (2020-05-19)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.22.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.21.1...@zaaksysteem/admin@0.22.0) (2020-05-14)


### Features

* **Catalog:** MINTY-3919 - change endpoint and params for deleting custom object type ([9bede85](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9bede85))





## [0.21.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.21.0...@zaaksysteem/admin@0.21.1) (2020-05-14)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.21.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.20.0...@zaaksysteem/admin@0.21.0) (2020-05-14)


### Features

* **Intake:** MINTY-3868 - add description column to Intake ([c1c6df2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c1c6df2))





# [0.20.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.19.1...@zaaksysteem/admin@0.20.0) (2020-05-13)


### Features

* **Catalog:** MINTY-3303 - add more details to Object View for Custom Object Type ([8959f71](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8959f71))





## [0.19.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.19.0...@zaaksysteem/admin@0.19.1) (2020-05-13)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.19.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.18.3...@zaaksysteem/admin@0.19.0) (2020-05-12)


### Features

* **CustomObject:** MINTY-3937 & MINTY-3926 - change authorization to radiobuttons, refactor saving and getting, enable server error dialog ([a2fa2eb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a2fa2eb))





## [0.18.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.18.2...@zaaksysteem/admin@0.18.3) (2020-05-12)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.18.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.18.1...@zaaksysteem/admin@0.18.2) (2020-05-12)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.18.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.18.0...@zaaksysteem/admin@0.18.1) (2020-05-11)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.18.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.17.4...@zaaksysteem/admin@0.18.0) (2020-05-01)


### Features

* **CreateObject:** MINTY-3472 - add Rights step to Object Create with associated components ([8699e78](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8699e78))





## [0.17.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.17.3...@zaaksysteem/admin@0.17.4) (2020-04-30)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.17.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.17.2...@zaaksysteem/admin@0.17.3) (2020-04-29)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.17.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.17.1...@zaaksysteem/admin@0.17.2) (2020-04-29)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.17.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.17.0...@zaaksysteem/admin@0.17.1) (2020-04-28)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.17.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.16.1...@zaaksysteem/admin@0.17.0) (2020-04-24)


### Features

* **Catalog:** MINTY-3784 - Add support for attribute type geojson ([5cb7e2b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5cb7e2b))





## [0.16.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.16.0...@zaaksysteem/admin@0.16.1) (2020-04-16)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.16.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.15.6...@zaaksysteem/admin@0.16.0) (2020-04-15)


### Features

* **ObjectTypeManagement:** MINTY-3627 Allow updating object types ([35f3998](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/35f3998))





## [0.15.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.15.5...@zaaksysteem/admin@0.15.6) (2020-04-14)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.15.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.15.4...@zaaksysteem/admin@0.15.5) (2020-04-14)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.15.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.15.3...@zaaksysteem/admin@0.15.4) (2020-04-10)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.15.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.15.2...@zaaksysteem/admin@0.15.3) (2020-04-09)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.15.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.15.1...@zaaksysteem/admin@0.15.2) (2020-04-06)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.15.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.15.0...@zaaksysteem/admin@0.15.1) (2020-04-03)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.15.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.14.0...@zaaksysteem/admin@0.15.0) (2020-04-01)


### Features

* **ObjectTypeManagement:** Implement Dialog with warning of pending changes that will be lost when leaving the form ([d6ef897](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d6ef897))





# [0.14.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.13.0...@zaaksysteem/admin@0.14.0) (2020-03-31)


### Features

* **ObjectTypeManagement:** MINTY-3473 Implement `afronden` step in wizard ([857b929](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/857b929))





# [0.13.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.12.0...@zaaksysteem/admin@0.13.0) (2020-03-31)


### Features

* **ObjectTypeManagement:** MINTY-3519 Implement add object type action ([a1df778](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a1df778))





# [0.12.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.11.1...@zaaksysteem/admin@0.12.0) (2020-03-30)


### Features

* **ObjectTypeManagement:** MINTY-3469 Create first page of ObjectTypeManagement wizard ([d1b8822](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d1b8822))





## [0.11.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.11.0...@zaaksysteem/admin@0.11.1) (2020-03-30)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.11.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.10.0...@zaaksysteem/admin@0.11.0) (2020-03-30)


### Features

* **FormRenderer:** Refactor formRenderer to hook and update Formik to latest version 2.1.4 ([c5d1e71](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c5d1e71))





# [0.10.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.9.2...@zaaksysteem/admin@0.10.0) (2020-03-27)


### Features

* **ObjectTypeManagement:** MINTY-3466 - Implement page for objecttype management ([375d69d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/375d69d))





## [0.9.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.9.1...@zaaksysteem/admin@0.9.2) (2020-03-23)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.9.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.9.0...@zaaksysteem/admin@0.9.1) (2020-03-20)


### Bug Fixes

* **Catalog:** Rename objecttype v2 ([fb79e15](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/fb79e15))





# [0.9.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.8.2...@zaaksysteem/admin@0.9.0) (2020-03-19)


### Features

* **Catalog:** MINTY-3344 - Add support for custom object types ([55ee940](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/55ee940))





## [0.8.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.8.1...@zaaksysteem/admin@0.8.2) (2020-03-19)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.8.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.8.0...@zaaksysteem/admin@0.8.1) (2020-03-17)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.8.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.10...@zaaksysteem/admin@0.8.0) (2020-03-05)


### Features

* **CaseDocuments:** add misc features ([8845760](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8845760))





## [0.7.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.9...@zaaksysteem/admin@0.7.10) (2020-03-03)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.7.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.8...@zaaksysteem/admin@0.7.9) (2020-02-24)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.7.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.7...@zaaksysteem/admin@0.7.8) (2020-02-21)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.7.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.6...@zaaksysteem/admin@0.7.7) (2020-02-17)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.7.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.5...@zaaksysteem/admin@0.7.6) (2020-02-10)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.7.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.4...@zaaksysteem/admin@0.7.5) (2020-02-07)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.7.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.3...@zaaksysteem/admin@0.7.4) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.7.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.2...@zaaksysteem/admin@0.7.3) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.7.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.1...@zaaksysteem/admin@0.7.2) (2020-01-10)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.7.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.0...@zaaksysteem/admin@0.7.1) (2020-01-09)

### Bug Fixes

- **Admin:** fix Suspense / loading error ([289b4b8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/289b4b8))

# [0.7.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.6.7...@zaaksysteem/admin@0.7.0) (2020-01-09)

### Bug Fixes

- **Communication:** MINTY-2262 - allow e-mail addresses over multiple lines ([db44093](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/db44093))
- **UI:** Fix aria-labels on snackbar and dialogs ([775a287](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/775a287))

### Features

- **Forms:** MINTY-2323 - add generic FormDialog component and implement throughout ([7f46465](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7f46465))
- **ThreadList:** Add tooltip to message counter ([78965a4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/78965a4))

## [0.6.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.6.6...@zaaksysteem/admin@0.6.7) (2019-12-19)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.6.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.6.5...@zaaksysteem/admin@0.6.6) (2019-11-27)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.6.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.6.4...@zaaksysteem/admin@0.6.5) (2019-11-21)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.6.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.6.3...@zaaksysteem/admin@0.6.4) (2019-11-14)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.6.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.6.2...@zaaksysteem/admin@0.6.3) (2019-11-05)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.6.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.6.1...@zaaksysteem/admin@0.6.2) (2019-10-25)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.6.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.6.0...@zaaksysteem/admin@0.6.1) (2019-10-17)

**Note:** Version bump only for package @zaaksysteem/admin

# [0.6.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.5.5...@zaaksysteem/admin@0.6.0) (2019-10-17)

### Bug Fixes

- change tag color, check for internalUrl in Details view ([89f58c3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/89f58c3))
- **Catalog:** Fix spelling on dutch copy translations ([15234ea](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/15234ea))
- **Catalog:** MINTY-1999: prevent an empty list of casetypes in 'used in case types', optional truncating ([943fcbb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/943fcbb))

### Features

- **Catalog:** MINTY-1551, MINTY-1548: add clickable foldername and url to internal registrationform to details, refactor large parts of the view code ([031bfff](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/031bfff))
- **Theme:** adjust some theming for better contrast ([33d10c6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/33d10c6))

## [0.5.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.5.4...@zaaksysteem/admin@0.5.5) (2019-10-11)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.5.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.5.3...@zaaksysteem/admin@0.5.4) (2019-09-30)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.5.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.5.2...@zaaksysteem/admin@0.5.3) (2019-09-26)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.5.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.5.1...@zaaksysteem/admin@0.5.2) (2019-09-09)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.5.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.5.0...@zaaksysteem/admin@0.5.1) (2019-09-06)

**Note:** Version bump only for package @zaaksysteem/admin

# [0.5.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.4.0...@zaaksysteem/admin@0.5.0) (2019-09-05)

### Features

- MINTY-1620: adjust presets for buttons in dialogs/forms ([81adac4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/81adac4))

# [0.4.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.3.2...@zaaksysteem/admin@0.4.0) (2019-08-29)

### Features

- **Alert:** MINTY-1126 Centralize `Alert` component so it can be used in multiple applications ([c44ec56](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c44ec56))
- **Communication:** MINTY-1115 Add `SearchField` to `Theads` ([5ec462a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ec462a))
- **Layout:** Let menu buttons be anchors to allow opening in new tab ([67f6910](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/67f6910))
- **UI:** Upgrade to MUI4 ([ac921f6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ac921f6))

## [0.3.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.3.1...@zaaksysteem/admin@0.3.2) (2019-08-27)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.3.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.3.0...@zaaksysteem/admin@0.3.1) (2019-08-22)

### Bug Fixes

- **admin:** add trailing slash to homepage ([eb5c176](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/eb5c176))

# [0.3.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.2.0...@zaaksysteem/admin@0.3.0) (2019-08-22)

### Features

- **admin:** Always start from homepage ([0375a5d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0375a5d))
- **Admin:** Activate new catalog for trial and master ([7e3a731](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7e3a731))
- **apps:** Prevent automatic opening of browser tabs on start ([4740421](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4740421))

# [0.2.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.1.1...@zaaksysteem/admin@0.2.0) (2019-08-12)

### Features

- **ProximaNova:** MINTY-1306 Change font family to Proxima Nova ([2e3009f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2e3009f))

## [0.1.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.1.0...@zaaksysteem/admin@0.1.1) (2019-08-12)

**Note:** Version bump only for package @zaaksysteem/admin

# 0.1.0 (2019-08-06)

### Features

- **Admin:** MINTY-1281 Add admin build to nginx ([2b58ef2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2b58ef2))
- **Admin:** MINTY-1281 Move admin app to mono repo ([f6a1d35](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f6a1d35))
