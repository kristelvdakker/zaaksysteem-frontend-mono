// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as Leaflet from 'leaflet';
import 'proj4leaflet';
import 'leaflet-draw';
import {
  MapInitType,
  ExternalMapEventType,
  Geojson,
  ExternalMapMessageType,
  PopupFeatureProp,
} from '@mintlab/ui/types/MapIntegration';

let map: Leaflet.Map;
let initData: MapInitType;
let name: string;
let layers: Leaflet.TileLayer[];
let layerControl: Leaflet.Control.Layers;
let mainFeatureGroup = Leaflet.featureGroup();
let marker = Leaflet.marker([0, 0]);

const version = 5;

const resetLayerControl = () => {
  layerControl && layerControl.remove();
  layerControl = Leaflet.control
    .layers(
      {},
      layers.reduce(
        (acc, layer, ix) => ({
          [initData.wmsLayers[ix].label]: layer,
          ...acc,
        }),
        { markers: mainFeatureGroup }
      )
    )
    .addTo(map);
};

const renderPopup = (popup: PopupFeatureProp) => {
  const container = document.createElement('div');
  let title: HTMLSpanElement | HTMLAnchorElement;

  if (popup.title.href) {
    const titleEl = document.createElement('a');
    titleEl.href = popup.title.href;
    titleEl.target = '_parent';
    titleEl.style.textDecoration = 'none';

    title = titleEl;
  } else {
    title = document.createElement('span');
  }

  title.style.fontSize = '14px';
  title.innerText = popup.title.text;

  const rows = popup.rows.map((rowData, ix) => {
    const row = document.createElement('div');
    const rowTitle = document.createElement('span');
    const rowText = document.createElement('span');

    if (rowData.text) {
      rowTitle.innerText = rowData.title + ': ';
      rowTitle.style.fontWeight = 'bold';
      rowText.innerText = rowData.text;

      row.appendChild(rowTitle);
      row.appendChild(rowText);
    }

    if (ix === 0) {
      row.style.paddingTop = '15px';
    }

    return row;
  });

  container.appendChild(title);
  rows.forEach(row => container.appendChild(row));

  return container;
};

const sendMessage = (message: ExternalMapMessageType) => {
  window.top.postMessage(message, window.location.origin);
};

const addFeatureSetterListener = () => {
  window.addEventListener('message', (event: ExternalMapEventType) => {
    if (event.data.type === 'setFeature') {
      const feature = event.data.value;
      drawGeoJson(feature, { append: false, focus: false });
      resetLayerControl();
    }
  });
};

const addMarkerSetterListener = () => {
  window.addEventListener('message', (event: ExternalMapEventType) => {
    if (event.data.type === 'setMarker' && event.data.value) {
      const latlng: [number, number] = [
        event.data.value.coordinates[1],
        event.data.value.coordinates[0],
      ];
      marker.setLatLng(latlng);
      marker.addTo(map);
      map.flyTo(latlng, map.getZoom() > 8 ? undefined : 8);
    } else if (event.data.type === 'setMarker') {
      marker.removeFrom(map);
    }
  });
};

const loadZsConfig = (done: (value?: unknown) => void) => {
  window.addEventListener('message', (event: ExternalMapEventType) => {
    if (event.data.type === 'init') {
      initData = event.data.value;
      name = event.data.name;
      done();

      if (window.top.location.host.includes('development')) {
        console.log(event.data);
      }
    }
  });
};

const postOnFeatureChange = (geojson: Geojson) => {
  sendMessage({
    name,
    version,
    value: geojson,
    type: 'featureChange',
  });
};

const postOnClick = ({ lat, lng }: { lat: number; lng: number }) => {
  sendMessage({
    name,
    version,
    value: { type: 'Point', coordinates: [lng, lat] },
    type: 'click',
  });
};

const createMap = () => {
  //@ts-ignore
  const RD = new Leaflet.Proj.CRS(
    'EPSG:28992',
    '+proj=sterea +lat_0=52.15616055555555 +lon_0=5.38763888888889 +k=0.9999079 +x_0=155000 +y_0=463000 +ellps=bessel +units=m +towgs84=565.2369,50.0087,465.658,-0.406857330322398,0.350732676542563,-1.8703473836068,4.0812 +no_defs',
    {
      resolutions: [
        3440.64,
        1720.32,
        860.16,
        430.08,
        215.04,
        107.52,
        53.76,
        26.88,
        13.44,
        6.72,
        3.36,
        1.68,
        0.84,
        0.42,
        0.21,
      ],
      bounds: Leaflet.bounds(
        [-285401.92, 22598.08],
        [595401.9199999999, 903401.9199999999]
      ),
      origin: [-285401.92, 22598.08],
    }
  );

  map = Leaflet.map('zs-app', { crs: RD, center: initData.center, zoom: 6 });

  const mainLayer = new Leaflet.TileLayer(
    'http://geodata.nationaalgeoregister.nl/tiles/service/tms/1.0.0/opentopoachtergrondkaart/EPSG:28992/{z}/{x}/{y}.png',
    {
      minZoom: 0,
      maxZoom: 13,
      tms: true,
    }
  );

  layers = initData.wmsLayers.map(layerData => {
    return Leaflet.tileLayer.wms(layerData.url, {
      minZoom: 0,
      maxZoom: 13,
      layers: layerData.layers,
      format: 'image/png',
      transparent: true,
    });
  });

  mainLayer.addTo(map);
  layers.forEach(layer => layer.addTo(map).bringToFront());
  mainFeatureGroup.addTo(map);
};

const addDrawPlugins = () => {
  map.addControl(
    //@ts-ignore
    new Leaflet.Control.Draw({
      edit: {
        featureGroup: mainFeatureGroup,
        poly: {
          allowIntersection: false,
        },
      },
      draw: {
        polygon: {
          allowIntersection: false,
          showArea: true,
        },
        circle: false,
        circlemarker: false,
      },
    })
  );

  const currentGeojson = () => {
    return mainFeatureGroup.toGeoJSON();
  };

  map.on('draw:created', event => {
    const layer = event.layer;
    mainFeatureGroup.addLayer(layer);
    postOnFeatureChange(currentGeojson());
  });

  map.on('draw:edited', event => {
    postOnFeatureChange(currentGeojson());
  });

  map.on('draw:deleted', event => {
    postOnFeatureChange(currentGeojson());
  });
};

const drawGeoJson = (
  data: GeoJSON.GeoJsonObject | null,
  options = {
    append: true,
    focus: false,
  }
) => {
  const style = {
    color: '#3388FF',
    weight: 5,
    opacity: 0.65,
  };

  let geoFeatures: GeoJSON.GeoJsonObject[];

  if (!data) {
    geoFeatures = [];
  } else if (data.type === 'FeatureCollection') {
    geoFeatures = (data as GeoJSON.FeatureCollection).features.map(feature => ({
      ...feature,
      type: 'Feature',
    }));
  } else {
    geoFeatures = [data];
  }

  !options.append && mainFeatureGroup.clearLayers();
  const features = geoFeatures.map(geoFeature => {
    return Leaflet.geoJSON(geoFeature, {
      style,
      pointToLayer: (feature, latlng) => {
        const marker = Leaflet.marker(latlng);

        return marker;
      },
      onEachFeature: (feature, layer) => {
        if (feature.properties.popup) {
          layer.bindPopup(renderPopup(feature.properties.popup));
        }
      },
    })
      .addTo(mainFeatureGroup)
      .bringToFront();
  });

  if (options.focus && features.length) {
    const lastFeature = features[features.length - 1];
    map.flyToBounds(lastFeature.getBounds(), { maxZoom: 6 });
  }
};

new Promise(loadZsConfig).then(() => {
  createMap();
  initData.canDrawFeatures && addDrawPlugins();
  map.on('click', event => postOnClick((event as any).latlng));
  initData.initialFeature &&
    drawGeoJson(initData.initialFeature, { append: false, focus: true });
  initData.canSelectLayers && resetLayerControl();
  addFeatureSetterListener();
  addMarkerSetterListener();
});
