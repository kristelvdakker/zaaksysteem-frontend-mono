// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import 'core-js/stable';
import 'regenerator-runtime/runtime';

const queryParam =
  new URLSearchParams(window.location.search).get('component') || '';

const noop = () => {};

const routes: Record<string, () => void> = {
  map: () => import('./map'),
};

const currentRoute = routes[queryParam] || noop;
currentRoute();
