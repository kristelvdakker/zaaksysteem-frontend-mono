// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const path = require('path');
const HtmlPlugin = require('html-webpack-plugin');

if (!process.env.NODE_ENV) throw new Error('Empty NODE_ENV env variable');
const isEnvDevelopment = process.env.NODE_ENV === 'development';
const isEnvProduction = process.env.NODE_ENV === 'production';

const paths = {
  index: path.resolve(__dirname, 'src', 'index.tsx'),
  output: path.resolve(
    __dirname,
    '..',
    '..',
    'build',
    'apps',
    'external-components'
  ),
};

/** @type {webpack.Configuration} */
module.exports = {
  devtool: 'source-map',
  stats: 'minimal',
  mode: isEnvProduction ? 'production' : isEnvDevelopment && 'development',
  entry: {
    index: paths.index,
  },
  output: {
    path: paths.output,
  },
  resolve: {
    extensions: ['.js', '.ts'],
  },
  module: {
    rules: [
      { include: path.resolve('src'), test: /\.ts/, use: ['babel-loader'] },
    ],
  },
  plugins: [new HtmlPlugin({ template: './public/index.html' })],
};
